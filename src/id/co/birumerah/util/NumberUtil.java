package id.co.birumerah.util;

public class NumberUtil {

	public static double roundToDecimals(double number, int decimalPlaces) {
		int temp = (int) ((number * Math.pow(10, decimalPlaces)));
		return (((double) temp)/Math.pow(10, decimalPlaces));
	}
}
