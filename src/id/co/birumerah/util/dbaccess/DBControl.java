package id.co.birumerah.util.dbaccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.StringTokenizer;

import snaq.db.ConnectionPool;

public class DBControl {
	
//	private static final String POOL_NAME = "bsiPool";
//	private static final int MIN_POOL = 5;
//	private static final int MAX_POOL = 30;
//	private static final int MAX_SIZE = 30;
//	private static final long IDLE_TIMEOUT = 1;

	public static Connection getConnection() throws SQLException {
		Connection conn = null;
//		long timeout = 3000;
		System.out.println("***connect to db");
		
		String driver = "com.mysql.jdbc.Driver";
		String dataLine = "";
		String dbLocation = "";
		String dbName = "";
//		String user = "root";
//		String pwd = "bm123456";
		String user = "";
		String pwd = "";
		
		try {
			File inFile = new File("db.config");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inFile)));
			while ((dataLine = br.readLine()) != null) {
//				System.out.println("isi baris config: " + dataLine);
				StringTokenizer token = new StringTokenizer(dataLine, "=");
				String tipe = token.nextToken();
				String isi = token.nextToken();
				if (tipe.equals("database.location")) {
					dbLocation = isi;
				} else if (tipe.equals("database.name")) {
					dbName = isi;
				} else if (tipe.equals("database.user")) {
					user = isi;
				} else if (tipe.equals("database.password")) {
					pwd = isi;
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		String url = "jdbc:mysql://localhost:3306/bsi";
		String url = "jdbc:mysql://" + dbLocation + "/" + dbName;
//		String url = "jdbc:mysql://localhost:3306/bsi-pembayaran";
//		String url = "jdbc:mysql://192.168.1.2:3306/bsi";
//		String url = "jdbc:mysql://111.221.43.111:3306/testing-110712";
//		String url = "jdbc:mysql://111.221.43.111:3306/bsi";
		
		try {
			Class.forName(driver);
//			Class c = Class.forName(driver);
//			try {
//				Driver dDriver = (Driver) c.newInstance();
//				ConnectionPool pool = new ConnectionPool(POOL_NAME, MIN_POOL, MAX_POOL, MAX_SIZE, IDLE_TIMEOUT, url, user, pwd);
//				conn = pool.getConnection(timeout);
//			} catch (InstantiationException e) {
//				e.printStackTrace();
//			} catch (IllegalAccessException e) {
//				e.printStackTrace();
//			}
		} catch (ClassNotFoundException e) {
			throw new SQLException("Unable to load database driver: [" + driver + "]");
		}
		conn = DriverManager.getConnection(url, user, pwd);
		
		return conn;
	}
}
