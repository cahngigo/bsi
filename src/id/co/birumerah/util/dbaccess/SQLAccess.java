package id.co.birumerah.util.dbaccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLAccess {

	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	
	public SQLAccess(DBConnection dbConn) throws SQLException {
		if (dbConn == null) throw new SQLException(
				"No database connection ready to be used.", "", -1);
		conn = dbConn.getConnection();
		if (conn == null) throw new SQLException(
				"No database connection ready to be used.", "", -1);
		stmt = conn.createStatement();
	}
	
	public SQLAccess(DBConnection dbConn, int resultSetType,
			int resultSetConcurrency) throws SQLException {
		if (dbConn == null) throw new SQLException(
				"No database connection ready to be used.", "", -1);
		conn = dbConn.getConnection();
		if (conn == null) throw new SQLException(
				"No database connection ready to be used.", "", -1);
		stmt = conn.createStatement(resultSetType, resultSetConcurrency);
	}
	
	public ResultSet executeQuery(String selectSQL) throws SQLException {
		rs = stmt.executeQuery(selectSQL);
		return rs;
	}
	
	public int executeUpdate(String cmdSQL) throws SQLException {
		return stmt.executeUpdate(cmdSQL);
	}
	
	public boolean execute(String cmdSQL) throws SQLException {
		return stmt.execute(cmdSQL);
	}
	
	public int numOfRows(String tableName, String whereClause) 
			throws SQLException {
		int numOfRows = 0;
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT COUNT(1) AS numOfRows FROM " + tableName + " ");
		
		if ((whereClause != null) && !whereClause.trim().equals(""))
			sb.append("WHERE " + whereClause);
		
		ResultSet rs = executeQuery(sb.toString());
		
		if (rs.next()) numOfRows = rs.getInt("numOfRows");
		return numOfRows;
	}
	
	public void close() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		rs = null;
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		stmt = null;
	}
}
