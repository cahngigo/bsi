package id.co.birumerah.util.dbaccess;

import org.apache.regexp.RE;
import org.apache.regexp.RESyntaxException;

public class SQLValueFilter {

	public static String normalizeString(String sqlValue) {
		String result = null;
		
		if (sqlValue == null) {
			return "";
		}
		
		RE re = null;
		try {
			re = new RE("'");
			result = re.subst(sqlValue, "''");
		} catch (RESyntaxException e) {
			result = sqlValue;
		}
		return result;
	}
}
