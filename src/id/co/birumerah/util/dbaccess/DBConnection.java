package id.co.birumerah.util.dbaccess;

import java.sql.Connection;
import java.sql.SQLException;

public class DBConnection {

	private static String className = DBConnection.class.getName();
	
	private Connection conn = null;
	
	public DBConnection() throws SQLException {
		conn = DBControl.getConnection();
	}
	
	public Connection getConnection() {
		return conn;
	}
	
	public void noTransaction() throws SQLException {
		conn.setAutoCommit(true);
	}
	
	public void beginTransaction() throws SQLException {
		conn.setAutoCommit(false);
	}
	
	public void commitTransaction() throws SQLException {
		conn.commit();
	}
	
	public void rollbackTransaction() {
		if (conn != null) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				System.err.println("An error just catched by " + className
						+ "#rollback.");
				e.printStackTrace();
			}
		}
	}
	
	public void close() {
		if (conn != null) {
			try {
				if (!conn.getAutoCommit()) conn.commit();
				conn.close();
			} catch (SQLException e) {
				System.err.println("An error just catched by " + className
						+ "#closeConnection.");
				e.printStackTrace();
			} finally {
				conn = null;
			}
		}
	}
	
	public boolean isClosed() throws SQLException {
		return conn.isClosed();
	}
}
