package id.co.birumerah.util.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {

	private String from;
	private String to;
	private String subject;
	private String text;
	
	public SendMail(String from, String to, String subject, String text) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.text = text;
	}
	
	public SendMail(String text) {
		this.text = text;
	}
	
	public void send(String tipeMail) {
		String dataLine = "";
		String smtpHost = "";
		String smtpPort = "";
		String password = "";
		String cc = "";
		try {
			File inFile = new File("mail.config");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inFile)));
			while ((dataLine = br.readLine()) != null) {
				StringTokenizer token = new StringTokenizer(dataLine, "=");
				String tipe = token.nextToken();
				String isi = token.hasMoreTokens() ? token.nextToken() : "";
				if (tipe.equals("mail.smtp.host")) {
					smtpHost = isi;
				} else if (tipe.equals("mail.smtp.port")) {
					smtpPort = isi;
				} else if (tipe.equals("mail.from")) {
					this.from = isi;
				} else if (tipe.equals("mail.to.confirmation")) {
					if (tipeMail.equalsIgnoreCase("confirmation")) this.to = isi;
//				} else if (tipe.equals("mail.to.delivery.status")) {
//					if (tipeMail.equalsIgnoreCase("delivery")) this.to = isi;
				} else if (tipe.equals("mail.subject")) {
					this.subject = isi;
				} else if (tipe.equals("mail.from.password")) {
					password = isi;
//				} else if (tipe.equals("mail.cc.confirmation.status")) {
//					if (tipeMail.equalsIgnoreCase("confirmation")) cc = isi;
//				} else if (tipe.equals("mail.cc.delivery.status")) {
//					if (tipeMail.equalsIgnoreCase("delivery")) cc = isi;
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.port", "587");
		props.put("mail.smtp.starttls.enable", "true");
		
		final String pwd = password;
//		Session mailSession = Session.getDefaultInstance(props);
		Session mailSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, pwd);
			}
		});
		Message simpleMessage = new MimeMessage(mailSession);
		
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		InternetAddress ccAddress = null;
		try {
			fromAddress = new InternetAddress(this.from);
			toAddress = new InternetAddress(this.to);
//			if (!cc.equals("")) ccAddress = new InternetAddress(cc);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			System.out.println("From:"+this.from+"; To:"+this.to+"; Message:"+this.text);
			simpleMessage.setFrom(fromAddress);
			simpleMessage.setRecipient(RecipientType.TO, toAddress);
//			simpleMessage.setRecipient(RecipientType.CC, new InternetAddress("alex.nilam@saasten.com"));
//			if (ccAddress != null) {
			if (!cc.equals("")) {
				cc = cc.trim();
				StringTokenizer mailToken = new StringTokenizer(cc, ";");
				while (mailToken.hasMoreTokens()) {
//					simpleMessage.setRecipient(RecipientType.CC, ccAddress);
					ccAddress = new InternetAddress(mailToken.nextToken());
					simpleMessage.addRecipient(RecipientType.CC, ccAddress);
				}
			}
			simpleMessage.setSubject(subject);
			simpleMessage.setText(text);
			
			Transport.send(simpleMessage);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
