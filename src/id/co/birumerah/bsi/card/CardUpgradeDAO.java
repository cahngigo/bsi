package id.co.birumerah.bsi.card;

import java.sql.ResultSet;
import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class CardUpgradeDAO {

	private DBConnection dbConn;
	
	public CardUpgradeDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public int insert(CardUpgrade dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO card_upgrade (");
		sb.append("nomor_kartu,");
		sb.append("serial_number");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getNomorKartu() + "',");
		sb.append("'" + dataObject.getSerialNumber() + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(CardUpgrade dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("nomor_kartu,");
		sb.append("serial_number ");
		sb.append("FROM card_upgrade ");
		sb.append("ORDER BY nomor_kartu DESC LIMIT 1");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setNomorKartu(rs.getString("nomor_kartu"));
				dataObject.setSerialNumber(rs.getString("serial_number"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
