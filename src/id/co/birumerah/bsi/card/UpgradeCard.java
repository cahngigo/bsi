package id.co.birumerah.bsi.card;

import java.util.Random;

import id.co.birumerah.util.dbaccess.DBConnection;

public class UpgradeCard {

	private static final int SERIAL_NUMBER_LENGTH = 12;
	private static final int CARD_NUMBER_LENGTH = 8;
	
	public void generateCard(String startNo, int n) {
		CardUpgradeFcd cuFcd = new CardUpgradeFcd();
		int noKartu;
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			if ("".equals(startNo)) {
				CardUpgrade kartu = cuFcd.getLastCard(dbConn);
				if (kartu != null) {
					startNo = kartu.getNomorKartu();
					noKartu = Integer.parseInt(startNo) + 1;
				} else {
					noKartu = 1;
				}
			} else {
				noKartu = Integer.parseInt(startNo);
			}
			for (int i=noKartu; i<noKartu+n; i++) {
				String nomorKartu = padLeftByZero(i);
				String serialNumber = generateRandomAngka(SERIAL_NUMBER_LENGTH);
				CardUpgrade cardUpgrade = new CardUpgrade();
				cardUpgrade.setNomorKartu(nomorKartu);
				cardUpgrade.setSerialNumber(serialNumber);
				cuFcd.newCard(cardUpgrade, dbConn);
				System.out.println(nomorKartu+", "+serialNumber);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	private String generateRandomAngka(int length) {
		String sumber = "0123456789";
		StringBuffer sb = new StringBuffer();
		Random r = new Random();
		
		for (int i=0; i<length; i++) {
			sb.append(sumber.charAt(r.nextInt(sumber.length())));
		}
		return sb.toString();
	}
	
	private String padLeftByZero(int angka) {
		String kode = "" + angka;
		for (int i=kode.length(); i<CARD_NUMBER_LENGTH; i++) {
			kode = "0" + kode;
		}
		return kode;
	}
}
