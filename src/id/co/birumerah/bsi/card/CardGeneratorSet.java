package id.co.birumerah.bsi.card;

import java.io.Serializable;
import java.util.ArrayList;

public class CardGeneratorSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5050057715100213199L;

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	public CardGeneratorSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(CardGenerator dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public CardGenerator get(int index) {
		CardGenerator result = null;
		
		if ((index >= 0) && (index < length()))
			result = (CardGenerator) set.get(index);
		
		return result;
	}
}
