package id.co.birumerah.bsi.card;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class GenerateCsvFile {

	DataOutputStream dos;
	
	public boolean writeToFile(String filename) {
		try {
			File outFile = new File(filename);
			dos = new DataOutputStream(new FileOutputStream(outFile));
			GenerateCard gen = new GenerateCard();
			String isifile = gen.generateCard(10000);
			System.out.println(isifile);
			dos.writeBytes(isifile);
			dos.close();
		} catch (FileNotFoundException fnfe) {
			return false;
		} catch (IOException ioe) {
			return false;
		}
		return true;
	}
}
