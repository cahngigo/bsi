package id.co.birumerah.bsi.card;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class CardUpgradeFcd {

	private CardUpgrade kartuUpgrade;
	
	public CardUpgradeFcd() {}

	public CardUpgrade getKartuUpgrade() {
		return kartuUpgrade;
	}

	public void setKartuUpgrade(CardUpgrade kartuUpgrade) {
		this.kartuUpgrade = kartuUpgrade;
	}
	
	public void newCard(CardUpgrade kartu, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			CardUpgradeDAO cuDao = new CardUpgradeDAO(dbConn);
			if (cuDao.insert(kartu) < 1) {
				throw new Exception("Gagal generate kartu upgrade.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e.getMessage());
		}
	}
	
	public CardUpgrade getLastCard(DBConnection dbConn) throws Exception {
		try {
			CardUpgrade cardUpgrade = new CardUpgrade();
			CardUpgradeDAO cuDao = new CardUpgradeDAO(dbConn);
			boolean found = cuDao.select(cardUpgrade);
			
			if (found) {
				return cardUpgrade;
			} else {
				System.err.println("No Card Upgrade found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
}
