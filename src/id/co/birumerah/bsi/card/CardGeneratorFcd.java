package id.co.birumerah.bsi.card;

import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;

public class CardGeneratorFcd {

	private CardGenerator kartu;
	
	public CardGeneratorFcd() {}

	public CardGeneratorFcd(CardGenerator kartu) {
		this.kartu = kartu;
	}

	public CardGenerator getKartu() {
		return kartu;
	}

	public void setKartu(CardGenerator kartu) {
		this.kartu = kartu;
	}
	
	public static CardGeneratorSet showAllCard() throws Exception {
		DBConnection dbConn = null;
		CardGeneratorSet cgSet = null;
		
		String whereClause = "1=1 ORDER BY member_id";
		try {
			dbConn = new DBConnection();
			CardGeneratorDAO cgDAO = new CardGeneratorDAO(dbConn);
			cgSet = cgDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return cgSet;
	}
	
	public void newCard(CardGenerator kartu) throws Exception {
		DBConnection dbConn = null;
		
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			CardGeneratorDAO cgDAO = new CardGeneratorDAO(dbConn);
			if (cgDAO.insert(kartu) < 1) {
				throw new Exception("Gagal generate kartu.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void newCard(CardGenerator kartu, DBConnection dbConn) throws Exception {
		
		try {
			dbConn.beginTransaction();
			CardGeneratorDAO cgDAO = new CardGeneratorDAO(dbConn);
			if (cgDAO.insert(kartu) < 1) {
				throw new Exception("Gagal generate kartu.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e.getMessage());
		}
	}
	
	public CardGeneratorSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		CardGeneratorSet cgSet = null;
		
		try {
			dbConn = new DBConnection();
			CardGeneratorDAO cgDAO = new CardGeneratorDAO(dbConn);
			cgSet = cgDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return cgSet;
	}
}
