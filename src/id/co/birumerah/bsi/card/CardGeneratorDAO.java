package id.co.birumerah.bsi.card;

import java.sql.ResultSet;
import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class CardGeneratorDAO {

	DBConnection dbConn;
	
	public CardGeneratorDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public int insert(CardGenerator dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO card_generator (");
		sb.append("member_id,");
		sb.append("activation_no,");
		sb.append("pin,");
		sb.append("has_activated ");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + dataObject.getActivationNo() + "',");
		sb.append("'" + dataObject.getPin() + "',");
		sb.append("" + dataObject.getHasActivated());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(CardGenerator dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE card_generator SET ");
		sb.append("activation_no = '" + dataObject.getActivationNo() + "',");
		sb.append("pin = '" + dataObject.getPin() + "',");
		sb.append("has_activated = " + dataObject.getHasActivated());
		sb.append(" WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(CardGenerator dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("activation_no,");
		sb.append("pin,");
		sb.append("has_activated");
		sb.append(" FROM card_generator");
		sb.append(" WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setActivationNo(rs.getString("activation_no"));
				dataObject.setPin(rs.getString("pin"));
				dataObject.setHasActivated(rs.getInt("has_activated"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public CardGeneratorSet select(String whereClause) throws SQLException {
		CardGeneratorSet dataObjectSet = new CardGeneratorSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("activation_no,");
		sb.append("pin,");
		sb.append("has_activated");
		sb.append(" FROM card_generator");
		sb.append(" WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				CardGenerator dataObject = new CardGenerator();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setActivationNo(rs.getString("activation_no"));
				dataObject.setPin(rs.getString("pin"));
				dataObject.setHasActivated(rs.getInt("has_activated"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
