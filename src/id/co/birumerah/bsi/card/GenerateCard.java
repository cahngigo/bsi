package id.co.birumerah.bsi.card;

import id.co.birumerah.util.dbaccess.DBConnection;

import java.util.Random;

public class GenerateCard {
	
	private static final int ID_NUMBER_LENGTH = 8;
	private static final int ACTIVATION_CODE_LENGTH = 12;
	private static final int PIN_LENGTH = 6;

	public String generateCard(int n) {
//		String whereClause = "1=1 ORDER BY member_id DESC LIMIT 1";
		String whereClause = "member_id NOT LIKE '999999%' ORDER BY member_id DESC LIMIT 1";
		CardGeneratorFcd cgFcd = new CardGeneratorFcd();
		StringBuffer sb = new StringBuffer();
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			CardGeneratorSet set = cgFcd.search(whereClause);
			int highestNum = 0;
			if (set.length() > 0) {
				String lastMemberId = set.get(0).getMemberId();
//				highestNum = Integer.parseInt(lastMemberId.substring(0, lastMemberId.length()-1));
				highestNum = Integer.parseInt(lastMemberId);
			}
			highestNum += 1;
			for (int i=highestNum; i<highestNum+n; i++) {
//				String memberId = padLeftByZero(i) + generateRandomHuruf(1);
				String memberId = padLeftByZero(i);
				String pin = generateRandomAngka(PIN_LENGTH);
				String kodeAktivasi = generateRandomAngka(ACTIVATION_CODE_LENGTH);
				sb.append(memberId).append(",").append(kodeAktivasi).append(",")
					.append(pin).append("\n");
				CardGenerator kartu = new CardGenerator(memberId, kodeAktivasi, pin, 0);
//				cgFcd.newCard(kartu);
				cgFcd.newCard(kartu, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return sb.toString(); 
	}
	
	private String generateRandomHuruf(int length) {
		String sumber = "ABCDEFGHJKLMNPRTWXYZ";
		StringBuffer sb = new StringBuffer();
		Random r = new Random();
		
		for (int i=0; i<length; i++) {
			sb.append(sumber.charAt(r.nextInt(sumber.length())));
		}
		return sb.toString();
	}
	
	private String generateRandomAngka(int length) {
		String sumber = "0123456789";
		StringBuffer sb = new StringBuffer();
		Random r = new Random();
		
		for (int i=0; i<length; i++) {
			sb.append(sumber.charAt(r.nextInt(sumber.length())));
		}
		return sb.toString();
	}
	
	private String padLeftByZero(int angka) {
		String kode = "" + angka;
		for (int i=kode.length(); i<ID_NUMBER_LENGTH; i++) {
			kode = "0" + kode;
		}
		return kode;
	}
}
