package id.co.birumerah.bsi.card;

public class CardUpgrade {

	private String nomorKartu;
	private String serialNumber;
	
	public CardUpgrade() {}

	public String getNomorKartu() {
		return nomorKartu;
	}

	public void setNomorKartu(String nomorKartu) {
		this.nomorKartu = nomorKartu;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
}
