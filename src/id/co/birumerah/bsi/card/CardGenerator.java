package id.co.birumerah.bsi.card;

public class CardGenerator {

	private String memberId;
	private String activationNo;
	private String pin;
	private int hasActivated;
	
	public CardGenerator() {
		super();
	}

	public CardGenerator(String memberId, String activationNo, String pin,
			int hasActivated) {
		this.memberId = memberId;
		this.activationNo = activationNo;
		this.pin = pin;
		this.hasActivated = hasActivated;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getActivationNo() {
		return activationNo;
	}

	public void setActivationNo(String activationNo) {
		this.activationNo = activationNo;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public int getHasActivated() {
		return hasActivated;
	}

	public void setHasActivated(int hasActivated) {
		this.hasActivated = hasActivated;
	}
}
