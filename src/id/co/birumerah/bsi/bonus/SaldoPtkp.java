package id.co.birumerah.bsi.bonus;

public class SaldoPtkp {

	private String memberId;
	private int sisaPtkp;
	
	public SaldoPtkp() {}

	public SaldoPtkp(String memberId, int sisaPtkp) {
		this.memberId = memberId;
		this.sisaPtkp = sisaPtkp;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public int getSisaPtkp() {
		return sisaPtkp;
	}

	public void setSisaPtkp(int sisaPtkp) {
		this.sisaPtkp = sisaPtkp;
	}
}
