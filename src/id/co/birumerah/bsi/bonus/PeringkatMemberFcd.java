package id.co.birumerah.bsi.bonus;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class PeringkatMemberFcd {

	private PeringkatMember peringkat;
	
	public PeringkatMemberFcd() {}

	public PeringkatMemberFcd(PeringkatMember peringkat) {
		this.peringkat = peringkat;
	}

	public PeringkatMember getPeringkat() {
		return peringkat;
	}

	public void setPeringkat(PeringkatMember peringkat) {
		this.peringkat = peringkat;
	}
	
	public PeringkatMemberSet selectAll() throws Exception {
		DBConnection dbConn = null;
		PeringkatMemberSet pmSet = null;
		
		try {
			dbConn = new DBConnection();
			PeringkatMemberDAO pmDAO = new PeringkatMemberDAO(dbConn);
			pmSet = pmDAO.selectAll();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return pmSet;
	}
	
	public PeringkatMemberSet selectAll(DBConnection dbConn) throws Exception {
		PeringkatMemberSet pmSet = null;
		
		try {
			PeringkatMemberDAO pmDAO = new PeringkatMemberDAO(dbConn);
			pmSet = pmDAO.selectAll();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return pmSet;
	}
}
