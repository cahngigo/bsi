package id.co.birumerah.bsi.bonus;

import java.sql.ResultSet;
import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class PeringkatMemberDAO {

	DBConnection dbConn;
	
	public PeringkatMemberDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public PeringkatMemberSet selectAll() throws SQLException {
		PeringkatMemberSet dataObjectSet = new PeringkatMemberSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("tingkat,");
		sb.append("peringkat,");
		sb.append("singkatan,");
		sb.append("min_pvgk ");
		sb.append("FROM peringkat_member ORDER BY tingkat");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				PeringkatMember dataObject = new PeringkatMember();
				dataObject.setTingkat(rs.getInt("tingkat"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setSingkatan(rs.getString("singkatan"));
				dataObject.setMinPvgk(rs.getInt("min_pvgk"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
