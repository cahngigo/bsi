package id.co.birumerah.bsi.bonus;

import java.util.Date;

public class PoinRecord {

	private long id;
	private String memberId;
	private String trxId;
	private Date tanggal;
	private double am;
	private float poin;
	private double bv;
	
	public PoinRecord() {}

	public PoinRecord(long id, String memberId, String trxId, Date tanggal,
			double am, float poin, double bv) {
		this.id = id;
		this.memberId = memberId;
		this.trxId = trxId;
		this.tanggal = tanggal;
		this.am = am;
		this.poin = poin;
		this.bv = bv;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public double getAm() {
		return am;
	}

	public void setAm(double am) {
		this.am = am;
	}

	public float getPoin() {
		return poin;
	}

	public void setPoin(float poin) {
		this.poin = poin;
	}

	public double getBv() {
		return bv;
	}

	public void setBv(double bv) {
		this.bv = bv;
	}
}
