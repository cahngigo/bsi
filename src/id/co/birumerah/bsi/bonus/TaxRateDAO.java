package id.co.birumerah.bsi.bonus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class TaxRateDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public TaxRateDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(TaxRate dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO tax_rate ");
		sb.append("(");
		sb.append("ptkp,");
		sb.append("pkp_from_level2,");
		sb.append("pkp_from_level3,");
		sb.append("pkp_from_level4,");
		sb.append("pct_npwp_level1,");
		sb.append("pct_npwp_level2,");
		sb.append("pct_npwp_level3,");
		sb.append("pct_npwp_level4,");
		sb.append("pct_penalti,");
		sb.append("pct_nonnpwp,");
		sb.append("start_date,");
		sb.append("status");
		sb.append(") VALUES (");
		sb.append("" + dataObject.getPtkp() + ",");
		sb.append("" + dataObject.getPkpFromLevel2() + ",");
		sb.append("" + dataObject.getPkpFromLevel3() + ",");
		sb.append("" + dataObject.getPkpFromLevel4() + ",");
		sb.append("" + dataObject.getPctNpwpLevel1() + ",");
		sb.append("" + dataObject.getPctNpwpLevel2() + ",");
		sb.append("" + dataObject.getPctNpwpLevel3() + ",");
		sb.append("" + dataObject.getPctNpwpLevel4() + ",");
		sb.append("" + dataObject.getPctPenalti() + ",");
		sb.append("" + dataObject.getPctNonnpwp() + ",");
		sb.append("'" + shortFormat.format(dataObject.getStartDate()) + "',");
		sb.append("'" + dataObject.getStatus() + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(TaxRate dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE tax_rate ");
		sb.append("SET ");
		sb.append("ptkp = " + dataObject.getPtkp() + ",");
		sb.append("pkp_from_level2 = " + dataObject.getPkpFromLevel2() + ",");
		sb.append("pkp_from_level3 = " + dataObject.getPkpFromLevel3() + ",");
		sb.append("pkp_from_level4 = " + dataObject.getPkpFromLevel4() + ",");
		sb.append("pct_npwp_level1 = " + dataObject.getPctNpwpLevel1() + ",");
		sb.append("pct_npwp_level2 = " + dataObject.getPctNpwpLevel2() + ",");
		sb.append("pct_npwp_level3 = " + dataObject.getPctNpwpLevel3() + ",");
		sb.append("pct_npwp_level4 = " + dataObject.getPctNpwpLevel4() + ",");
		sb.append("pct_penalti = " + dataObject.getPctPenalti() + ",");
		sb.append("pct_nonnpwp = " + dataObject.getPctNonnpwp() + ",");
		sb.append("start_date = '" + shortFormat.format(dataObject.getStartDate()) + "',");
		sb.append("status = '" + dataObject.getStatus() + "' ");
		sb.append("WHERE rate_id = " + dataObject.getRateId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(TaxRate dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("ptkp,");
		sb.append("pkp_from_level2,");
		sb.append("pkp_from_level3,");
		sb.append("pkp_from_level4,");
		sb.append("pct_npwp_level1,");
		sb.append("pct_npwp_level2,");
		sb.append("pct_npwp_level3,");
		sb.append("pct_npwp_level4,");
		sb.append("pct_penalti,");
		sb.append("pct_nonnpwp,");
		sb.append("start_date,");
		sb.append("status ");
		sb.append("FROM tax_rate ");
		sb.append("WHERE rate_id = " + dataObject.getRateId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setPtkp(rs.getInt("ptkp"));
				dataObject.setPkpFromLevel2(rs.getLong("pkp_from_level2"));
				dataObject.setPkpFromLevel3(rs.getLong("pkp_from_level3"));
				dataObject.setPkpFromLevel4(rs.getLong("pkp_from_level4"));
				dataObject.setPctNpwpLevel1(rs.getFloat("pct_npwp_level1"));
				dataObject.setPctNpwpLevel2(rs.getFloat("pct_npwp_level2"));
				dataObject.setPctNpwpLevel3(rs.getFloat("pct_npwp_level3"));
				dataObject.setPctNpwpLevel4(rs.getFloat("pct_npwp_level4"));
				dataObject.setPctPenalti(rs.getFloat("pct_penalti"));
				dataObject.setPctNonnpwp(rs.getFloat("pct_nonnpwp"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public TaxRateSet select(String whereClause) throws SQLException {
		TaxRateSet dataObjectSet = new TaxRateSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("rate_id,");
		sb.append("ptkp,");
		sb.append("pkp_from_level2,");
		sb.append("pkp_from_level3,");
		sb.append("pkp_from_level4,");
		sb.append("pct_npwp_level1,");
		sb.append("pct_npwp_level2,");
		sb.append("pct_npwp_level3,");
		sb.append("pct_npwp_level4,");
		sb.append("pct_penalti,");
		sb.append("pct_nonnpwp,");
		sb.append("start_date,");
		sb.append("status ");
		sb.append("FROM tax_rate ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				TaxRate dataObject = new TaxRate();
				dataObject.setRateId(rs.getInt("rate_id"));
				dataObject.setPtkp(rs.getInt("ptkp"));
				dataObject.setPkpFromLevel2(rs.getLong("pkp_from_level2"));
				dataObject.setPkpFromLevel3(rs.getLong("pkp_from_level3"));
				dataObject.setPkpFromLevel4(rs.getLong("pkp_from_level4"));
				dataObject.setPctNpwpLevel1(rs.getFloat("pct_npwp_level1"));
				dataObject.setPctNpwpLevel2(rs.getFloat("pct_npwp_level2"));
				dataObject.setPctNpwpLevel3(rs.getFloat("pct_npwp_level3"));
				dataObject.setPctNpwpLevel4(rs.getFloat("pct_npwp_level4"));
				dataObject.setPctPenalti(rs.getFloat("pct_penalti"));
				dataObject.setPctNonnpwp(rs.getFloat("pct_nonnpwp"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
