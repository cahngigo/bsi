package id.co.birumerah.bsi.bonus;

import java.sql.ResultSet;
import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class PeringkatCounterDAO {

	DBConnection dbConn;
	
	public PeringkatCounterDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public int insert(PeringkatCounter dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO peringkat_counter (");             
		sb.append("member_id,");
		sb.append("last_tingkat,");
		sb.append("tingkat_repetitif,");
		sb.append("jml_repetitif,");
		sb.append("tingkat_achieved");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("" + dataObject.getLastTingkat() + ",");
		sb.append("" + dataObject.getTingkatRepetitif() + ",");
		sb.append("" + dataObject.getJmlRepetitif() + ",");
		sb.append("" + dataObject.getTingkatAchieved());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(PeringkatCounter dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE peringkat_counter SET ");
		sb.append("last_tingkat = " + dataObject.getLastTingkat() + ",");
		sb.append("tingkat_repetitif = " + dataObject.getTingkatRepetitif() + ",");
		sb.append("jml_repetitif = " + dataObject.getJmlRepetitif() + ",");
		sb.append("tingkat_achieved = " + dataObject.getTingkatAchieved() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(PeringkatCounter dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("last_tingkat,");
		sb.append("tingkat_repetitif,");
		sb.append("jml_repetitif,");
		sb.append("tingkat_achieved ");
		sb.append("FROM peringkat_counter ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setLastTingkat(rs.getInt("last_tingkat"));
				dataObject.setTingkatRepetitif(rs.getInt("tingkat_repetitif"));
				dataObject.setJmlRepetitif(rs.getInt("jml_repetitif"));
				dataObject.setTingkatAchieved(rs.getInt("tingkat_achieved"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
