package id.co.birumerah.bsi.bonus;

import id.co.birumerah.util.dbaccess.DBConnection;

public class KualifikasiMemberChecker {

	private static final float MINPV_SCD = 300000;
	private static final float MINPV_CD = 100000;
	private static final float MINPV_DM = 30000;
	private static final float MINPV_PM = 10000;
	private static final float MINPV_EM = 3000;
	private static final float MINPV_RM = 1000;
//	private static final float MINPV_GM = 350;
//	private static final float MINPV_SM = 100;
	private static final float PERINGKAT_REPETITIF = 3;
	
//	public static String getKualifikasiMember(float minPvgk) {
	public String getKualifikasiMember(float minPvgk) {
		String kualifikasi = "";
		
		if (minPvgk >= MINPV_SCD) {
			kualifikasi = "STAR Crown Ambassador";
		} else if (minPvgk >= MINPV_CD) {
			kualifikasi = "STAR Crown Director";
		} else if (minPvgk >= MINPV_DM) {
			kualifikasi = "STAR Diamond Director";
		} else if (minPvgk >= MINPV_PM) {
			kualifikasi = "STAR Diamond Manager";
		} else if (minPvgk >= MINPV_EM) {
			kualifikasi = "STAR Emerald Manager";
		} else if (minPvgk >= MINPV_RM) {
			kualifikasi = "STAR Ruby Manager";
//		} else if (minPvgk >= MINPV_GM) {
//			kualifikasi = "Gold Manager";
//		} else if (minPvgk >= MINPV_SM) {
//			kualifikasi = "Silver Manager";
		} else {
			kualifikasi = "Distributor";
		}
		return kualifikasi;
	}
	
//	public static String getPeringkatMember(String memberId, float pvgk, DBConnection dbConn) {
	public String getPeringkatMember(String memberId, float pvgk, DBConnection dbConn) {
		String peringkat = "Distributor";
		int tingkatNow;
		try {
			if (pvgk >= MINPV_SCD) {
				tingkatNow = 6;
			} else if (pvgk >= MINPV_CD) {
				tingkatNow = 5;
			} else if (pvgk >= MINPV_DM) {
				tingkatNow = 4;
			} else if (pvgk >= MINPV_PM) {
				tingkatNow = 3;
			} else if (pvgk >= MINPV_EM) {
				tingkatNow = 2;
			} else if (pvgk >= MINPV_RM) {
				tingkatNow = 1;
			} else {
				tingkatNow = 0;
			}
			PeringkatCounter pCounter = PeringkatCounterFcd.getCounterByMemberId(memberId, dbConn);
			if (pCounter == null) {
//				peringkat = "Distributor";
				if (tingkatNow > 0) {
					pCounter = new PeringkatCounter();
					pCounter.setMemberId(memberId);
					pCounter.setLastTingkat(tingkatNow);
					pCounter.setTingkatRepetitif(tingkatNow);
					pCounter.setJmlRepetitif(1);
					pCounter.setTingkatAchieved(tingkatNow);
					PeringkatCounterFcd pcFcd = new PeringkatCounterFcd(pCounter);
					pcFcd.insertPeringkatCounter(dbConn);
					pcFcd = null;
				}
				peringkat = getPeringkatByTingkat(tingkatNow);
			} else {
//				int tingkatBefore = pCounter.getLastTingkat();
//				int tingkatRepetitif = pCounter.getTingkatRepetitif();
				int tingkatAchieved = pCounter.getTingkatAchieved();
//				if (tingkatNow <= tingkatRepetitif) {
				if (tingkatNow <= tingkatAchieved) {
//					pCounter.setLastTingkat(tingkatNow);
//					pCounter.setJmlRepetitif(1 + pCounter.getJmlRepetitif());
//					if (pCounter.getJmlRepetitif() == PERINGKAT_REPETITIF || pCounter.getTingkatAchieved() == 8) {
//						if (tingkatRepetitif > pCounter.getTingkatAchieved()) {
//							if (tingkatNow < tingkatRepetitif) {
//								tingkatRepetitif = tingkatNow;
//								pCounter.setJmlRepetitif(1);
//							}
//							pCounter.setTingkatAchieved(tingkatRepetitif);
//							peringkat = getPeringkatByTingkat(tingkatRepetitif);
//							pCounter.setTingkatRepetitif(tingkatNow);
//							if (tingkatBefore >= tingkatNow && tingkatRepetitif < tingkatBefore) {
//								pCounter.setJmlRepetitif(2);
//								pCounter.setTingkatRepetitif(tingkatNow);
//							} else if (tingkatBefore >= tingkatNow && tingkatRepetitif == tingkatNow) {
//								pCounter.setJmlRepetitif(0);
//								pCounter.setTingkatRepetitif(tingkatNow);
//							}
//							
//						} else {
//							peringkat = getPeringkatByTingkat(pCounter.getTingkatAchieved());
//							pCounter.setTingkatRepetitif(tingkatNow);
//							pCounter.setJmlRepetitif(0);
//						}						
//					} else {
//						pCounter.setTingkatRepetitif(tingkatNow);
//					}
				} else {
//					pCounter.setLastTingkat(tingkatNow);
//					pCounter.setJmlRepetitif(1 + pCounter.getJmlRepetitif());
//					if (pCounter.getJmlRepetitif() == PERINGKAT_REPETITIF) {
//						if (tingkatRepetitif > pCounter.getTingkatAchieved()) {
//							pCounter.setTingkatAchieved(tingkatRepetitif);
//							peringkat = getPeringkatByTingkat(tingkatRepetitif);
//							if (tingkatRepetitif < tingkatNow && tingkatNow <= tingkatBefore) {
//								pCounter.setTingkatRepetitif(tingkatNow);
//								pCounter.setJmlRepetitif(2);
//							} else if (tingkatRepetitif < tingkatNow && tingkatBefore < tingkatNow && tingkatRepetitif < tingkatBefore) {
//								pCounter.setTingkatRepetitif(tingkatBefore);
//								pCounter.setJmlRepetitif(2);
//							} else {
//								pCounter.setTingkatRepetitif(tingkatNow);
//								pCounter.setJmlRepetitif(1);
//							}
//						} else {
//							peringkat = getPeringkatByTingkat(pCounter.getTingkatAchieved());
//							pCounter.setTingkatRepetitif(tingkatNow);
//							pCounter.setJmlRepetitif(0);
//						}
//					}
					tingkatAchieved = tingkatNow;
					pCounter.setTingkatAchieved(tingkatNow);
					PeringkatCounterFcd pcFcd = new PeringkatCounterFcd();
					pcFcd.updatePeringkatCounter(pCounter, dbConn);
					pcFcd = null;
				}
//				PeringkatCounterFcd pcFcd = new PeringkatCounterFcd();
//				pcFcd.updatePeringkatCounter(pCounter, dbConn);
//				pcFcd = null;
				peringkat = getPeringkatByTingkat(tingkatAchieved);
			}
			pCounter = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return peringkat;
	}
	
//	private static String getPeringkatByTingkat(int tingkat) {
	private String getPeringkatByTingkat(int tingkat) {
		String peringkat = "";
		
		switch (tingkat) {
		case 0:
			peringkat = "Distributor";
			break;
		case 1:
//			peringkat = "Silver Manager";
			peringkat = "STAR Ruby Manager";
			break;
		case 2:
//			peringkat = "Gold Manager";
			peringkat = "STAR Emerald Manager";
			break;
		case 3:
//			peringkat = "Ruby Manager";
			peringkat = "STAR Diamond Manager";
			break;
		case 4:
//			peringkat = "Emerald Manager";
			peringkat = "STAR Diamond Director";
			break;
		case 5:
//			peringkat = "Pearl Manager";
			peringkat = "STAR Crown Director";
			break;
		case 6:
//			peringkat = "Diamond Manager";
			peringkat = "STAR Crown Ambassador";
			break;
//		case 7:
//			peringkat = "Crown Director";
//			break;
//		case 8:
//			peringkat = "Star Crown Director";
//			break;
		default:
			break;
		}
		return peringkat;
	}
}
