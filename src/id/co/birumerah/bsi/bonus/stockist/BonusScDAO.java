package id.co.birumerah.bsi.bonus.stockist;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class BonusScDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusScDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusSc dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_sc (");
		sb.append("stockist_id,");
		sb.append("bulan,");
		sb.append("k_stockist_omzet,");
		sb.append("k_purchase_order,");
		sb.append("k_stock_level,");
		sb.append("bonus_kode_unik,");
		sb.append("top_5 ");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getStockistId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getBulan()) + "',");
		sb.append("" + dataObject.getkStockistOmzet() + ",");
		sb.append("" + dataObject.getkPurchaseOrder() + ",");
		sb.append("" + dataObject.getkStockLevel() + ",");
		sb.append("" + dataObject.getBonusKodeUnik() + ",");
		sb.append("" + dataObject.getTop5() + " ");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusSc dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_sc ");
		sb.append("SET ");
		sb.append("k_stockist_omzet = " + dataObject.getkStockistOmzet() + ",");
		sb.append("k_purchase_order = " + dataObject.getkPurchaseOrder() + ",");
		sb.append("k_stock_level = " + dataObject.getkStockLevel() + ",");
		sb.append("bonus_kode_unik = " + dataObject.getBonusKodeUnik() + ",");
		sb.append("top_5 = " + dataObject.getTop5() + " ");
		sb.append("WHERE stockist_id = '" + dataObject.getStockistId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(BonusSc dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_sc ");
		sb.append("WHERE stockist_id = '" + dataObject.getStockistId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusSc dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("stockist_id,");
		sb.append("bulan,");
		sb.append("k_stockist_omzet,");
		sb.append("k_purchase_order,");
		sb.append("k_stock_level,");
		sb.append("bonus_kode_unik,");
		sb.append("top_5 ");
		sb.append("FROM bonus_sc ");
		sb.append("WHERE stockist_id = '" + dataObject.getStockistId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setkStockistOmzet(rs.getDouble("k_stockist_omzet"));
				dataObject.setkPurchaseOrder(rs.getDouble("k_purchase_order"));
				dataObject.setkStockLevel(rs.getDouble("k_stock_level"));
				dataObject.setBonusKodeUnik(rs.getDouble("bonus_kode_unik"));
				dataObject.setTop5(rs.getDouble("top_5"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BonusScSet select(String whereClause) throws SQLException {
		BonusScSet dataObjectSet = new BonusScSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("stockist_id,");
		sb.append("bulan,");
		sb.append("k_stockist_omzet,");
		sb.append("k_purchase_order,");
		sb.append("k_stock_level,");
		sb.append("bonus_kode_unik,");
		sb.append("top_5 ");
		sb.append("FROM bonus_sc ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BonusSc dataObject = new BonusSc();
				dataObject.setStockistId(rs.getString("stockist_id"));
				dataObject.setBulan(rs.getDate("bulan"));
				dataObject.setkStockistOmzet(rs.getDouble("k_stockist_omzet"));
				dataObject.setkPurchaseOrder(rs.getDouble("k_purchase_order"));
				dataObject.setkStockLevel(rs.getDouble("k_stock_level"));
				dataObject.setBonusKodeUnik(rs.getDouble("bonus_kode_unik"));
				dataObject.setTop5(rs.getDouble("top_5"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
