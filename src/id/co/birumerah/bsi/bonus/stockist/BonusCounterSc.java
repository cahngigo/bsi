package id.co.birumerah.bsi.bonus.stockist;

import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.NumberUtil;
import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.joda.time.MutableDateTime;

public class BonusCounterSc {

	private MutableDateTime beginOfDay;
	private MutableDateTime bulanHitung;
	private double budgetSPlan;
	
	private double budgetUsPct;
	
	//setting-setting untuk Komisi Stockist Omzet
	private double ksoBudgetPct;
	private double ksoMinOmzet;
	private double ksoOmzetPerbagian;
	
	//setting-setting untuk Komisi Purchase Order
	private double kpoBudgetPct;
	private Time kpoMaxPoHour;
	private Time kpoMaxPaymentHour;
	private int kpoGolaMin;
	private int kpoGolaBagian;
	
	//setting-setting untuk Komisi Stock Level
	private double kslBudgetPct;
	private double kslFullPct;
	private double kslMinOmzet;
	private double kslMinStokrata;
	private double kslStokPerbagian;
	
	//setting-setting Bonus Loyalitas Pembayaran Kode Unik
	private double blpukBudgetPct;
	private Time blpukMaxPoHour;
	private Time blpukMaxPaymentHour;
	private int blpukGolaMin;
	private int blpukGolaBagian;
	
	//setting-setting Bonus Top 5 SC
	private double top5BudgetPct;
	private int top5Rank1Bagian;
	private int top5Rank2Bagian;
	private int top5Rank3Bagian;
	private int top5Rank4Bagian;
	private int top5Rank5Bagian;
	
	private double bvTotal;
	
	//untuk production
	public BonusCounterSc() {
		super();
		
		beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		bulanHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		bulanHitung.addMonths(-1);
		
		loadSettingBonusSc();
		hitungBvPerusahaan();
		hitungBudgetSPlan();
	}
	
	//untuk production
	public BonusCounterSc(DBConnection dbConn) {
		super();
		
		beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		bulanHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		bulanHitung.addMonths(-1);
		
		loadSettingBonusSc(dbConn);
//		hitungBvPerusahaan();
		hitungBvPerusahaan(dbConn);
		hitungBudgetSPlan();
	}
	
	//untuk testing
	public BonusCounterSc(MutableDateTime beginOfDay) {
		this.beginOfDay = beginOfDay;
		bulanHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		bulanHitung.addMonths(-1);
		
		loadSettingBonusSc();
		hitungBvPerusahaan();
		hitungBudgetSPlan();
	}
	
	private void loadSettingBonusSc() {
		String whereClause = "status = 'Berlaku' AND start_date < '" 
			+ beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC";
		SettingBonusScFcd sbFcd = new SettingBonusScFcd();
		SettingBonusScSet sbSet = null;
		try {
			sbSet = sbFcd.search(whereClause);
			if (sbSet.length() < 1) {
				System.out.println("Belum ada setting bonus yang berlaku.");
				return;
			}
			SettingBonusSc settings = sbSet.get(0);
			budgetUsPct = settings.getBudgetUsPct();
			ksoBudgetPct = settings.getKsoBudgetPct();
			ksoMinOmzet = settings.getKsoMinOmzet();
			ksoOmzetPerbagian = settings.getKsoOmzetPerbagian();
			
			kpoBudgetPct = settings.getKpoBudgetPct();
			kpoMaxPoHour = settings.getKpoMaxPoHour();
			kpoMaxPaymentHour = settings.getKpoMaxPaymentHour();
			kpoGolaMin = settings.getKpoGolaMin();
			kpoGolaBagian = settings.getKpoGolaBagian();
			
			kslBudgetPct = settings.getKslBudgetPct();
			kslFullPct = settings.getKslFullPct();
			kslMinOmzet = settings.getKslMinOmzet();
			kslMinStokrata = settings.getKslMinStokrata();
			kslStokPerbagian = settings.getKslStokPerbagian();
			
			blpukBudgetPct = settings.getBlpukBudgetPct();
			blpukMaxPoHour = settings.getBlpukMaxPoHour();
			blpukMaxPaymentHour = settings.getBlpukMaxPaymentHour();
			blpukGolaMin = settings.getBlpukGolaMin();
			blpukGolaBagian = settings.getBlpukGolaBagian();
			
			top5BudgetPct = settings.getTop5BudgetPct();
			top5Rank1Bagian = settings.getTop5Rank1Bagian();
			top5Rank2Bagian = settings.getTop5Rank2Bagian();
			top5Rank3Bagian = settings.getTop5Rank3Bagian();
			top5Rank4Bagian = settings.getTop5Rank4Bagian();
			top5Rank5Bagian = settings.getTop5Rank5Bagian();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadSettingBonusSc(DBConnection dbConn) {
		String whereClause = "status = 'Berlaku' AND start_date < '" 
			+ beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC";
		SettingBonusScFcd sbFcd = new SettingBonusScFcd();
		SettingBonusScSet sbSet = null;
		try {
			sbSet = sbFcd.search(whereClause);
			if (sbSet.length() < 1) {
				System.out.println("Belum ada setting bonus yang berlaku.");
				return;
			}
			SettingBonusSc settings = sbSet.get(0);
			budgetUsPct = settings.getBudgetUsPct();
			ksoBudgetPct = settings.getKsoBudgetPct();
			ksoMinOmzet = settings.getKsoMinOmzet();
			ksoOmzetPerbagian = settings.getKsoOmzetPerbagian();
			
			kpoBudgetPct = settings.getKpoBudgetPct();
			kpoMaxPoHour = settings.getKpoMaxPoHour();
			kpoMaxPaymentHour = settings.getKpoMaxPaymentHour();
			kpoGolaMin = settings.getKpoGolaMin();
			kpoGolaBagian = settings.getKpoGolaBagian();
			
			kslBudgetPct = settings.getKslBudgetPct();
			kslFullPct = settings.getKslFullPct();
			kslMinOmzet = settings.getKslMinOmzet();
			kslMinStokrata = settings.getKslMinStokrata();
			kslStokPerbagian = settings.getKslStokPerbagian();
			
			blpukBudgetPct = settings.getBlpukBudgetPct();
			blpukMaxPoHour = settings.getBlpukMaxPoHour();
			blpukMaxPaymentHour = settings.getBlpukMaxPaymentHour();
			blpukGolaMin = settings.getBlpukGolaMin();
			blpukGolaBagian = settings.getBlpukGolaBagian();
			
			top5BudgetPct = settings.getTop5BudgetPct();
			top5Rank1Bagian = settings.getTop5Rank1Bagian();
			top5Rank2Bagian = settings.getTop5Rank2Bagian();
			top5Rank3Bagian = settings.getTop5Rank3Bagian();
			top5Rank4Bagian = settings.getTop5Rank4Bagian();
			top5Rank5Bagian = settings.getTop5Rank5Bagian();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBvPerusahaan() {
		bvTotal = 0;
		try {
//			MemberTreeSet mtSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday();
			//yg utk production yg atas, yg di bawah ini cuma utk ngetes
			MemberTreeSet mtSet = MemberTreeFcd.showAllMemberTree();
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				bvTotal += member.getSaldoBv();
			}
			System.out.println("BV Total = " + bvTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBvPerusahaan(DBConnection dbConn) {
		bvTotal = 0;
		try {
			MemberTreeSet mtSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday(dbConn);
			//yg utk production yg atas, yg di bawah ini cuma utk ngetes
//			MemberTreeSet mtSet = MemberTreeFcd.showAllMemberTree(dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				bvTotal += member.getSaldoBv();
			}
			System.out.println("BV Total = " + bvTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBudgetSPlan() {
		budgetSPlan = bvTotal * budgetUsPct / 100;
		System.out.println("Budget S-Plan = " + budgetSPlan);
	}
	
	@SuppressWarnings("unchecked")
//	public void giveBonusKso() {
	public void giveBonusKso(DBConnection dbConn) {
		StringBuffer sb = new StringBuffer();
//		sb.append("SELECT DISTINCT(stockist_id) stockist_id, SUM(total) total ");
//		sb.append("FROM ( ");
		sb.append("SELECT DISTINCT(stockist_id) stockist_id, SUM(total) total ");
		sb.append("FROM penjualan_langsung ");
		sb.append("WHERE trx_date >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND trx_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND stockist_id LIKE 'SC%' ");
		sb.append("GROUP BY stockist_id ");
//		sb.append("UNION ");
//		sb.append("SELECT DISTINCT(stockist_id) stockist_id, SUM(total) total ");
//		sb.append("FROM pesan_penjualan_sc ");
//		sb.append("WHERE tgl_transaksi >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND tgl_transaksi < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND stockist_id LIKE 'SC%' ");
//		sb.append("GROUP BY stockist_id) omzet ");
//		sb.append("GROUP BY stockist_id");
		System.out.println("Query KSO: " + sb.toString());
		
		ArrayList alStokis = new ArrayList();
		ArrayList alTotal = new ArrayList();
//		DBConnection dbConn = null;
		SQLAccess sqlAccess = null;
		try {
//			dbConn = new DBConnection();
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				String stockistId = rs.getString("stockist_id");
				double total = rs.getDouble("total");
				System.out.println("Stockist "+stockistId+" punya omzet sebesar "+total);
				if (total >= ksoMinOmzet) {
					alStokis.add(stockistId);
					alTotal.add(getBagianScKso(total));
				}
			}
			double bagian = budgetSPlan * ksoBudgetPct / (100 * getTotalBagianKso(alTotal));
			System.out.println("1 bagian KSO = " + bagian);
			for (int i=0; i<alStokis.size(); i++) {
				String stockistId = (String) alStokis.get(i);
				double total = Double.parseDouble(alTotal.get(i).toString());
				System.out.println("Bagian Stockist " + stockistId + " = " + total);
				double bonusKso = NumberUtil.roundToDecimals((total * bagian), 0);
				
				BonusSc bonus = BonusScFcd.getBonusByStockistAndMonth(stockistId, bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusSc();
					bonus.setStockistId(stockistId);
					bonus.setBulan(bulanHitung.toDate());
					bonus.setkStockistOmzet(bonusKso);
					BonusScFcd bsFcd = new BonusScFcd(bonus);
					bsFcd.insertBonus(dbConn);
				} else {
					bonus.setkStockistOmzet(bonusKso);
					BonusScFcd bsFcd = new BonusScFcd();
					bsFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
//			if (dbConn != null) dbConn.close();
		}
	}
	
	private int getBagianScKso(double omzet) {
		int bagian = (int) NumberUtil.roundToDecimals((omzet / ksoOmzetPerbagian), 0);
		
		return bagian;
	}
	
	@SuppressWarnings("unchecked")
	private int getTotalBagianKso(ArrayList alTotal) {
		int totalBagian = 0;
		for (int i=0; i<alTotal.size(); i++) {
//			totalBagian += (int) NumberUtil.roundToDecimals((Double.parseDouble(alTotal.get(i).toString()) / ksoOmzetPerbagian), 0);
			totalBagian += (int) Double.parseDouble(alTotal.get(i).toString());
		}
		System.out.println("Total Bagian KSO = " + totalBagian);
		return totalBagian;
	}
	
	@SuppressWarnings("unchecked")
//	public void giveBonusKpo() {
	public void giveBonusKpo(DBConnection dbConn) {
		ArrayList alStokis = new ArrayList();
		ArrayList alJumlah = new ArrayList();
//		DBConnection dbConn = null;
		SQLAccess sqlAccess = null;
		try {
//			dbConn = new DBConnection();
			String hariLibur = getHariLibur(dbConn);
			StringBuffer sb = new StringBuffer();
//			sb.append("SELECT stockist, SUM(jumlah) jumlah ");
//			sb.append("FROM ( ");
			sb.append("SELECT COUNT(DISTINCT(customer_id)) jumlah, customer_id stockist FROM penjualan_langsung ");
			sb.append("WHERE trx_date >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND trx_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND customer_id LIKE 'SC%' ");
			sb.append("AND TIME(trx_date) < '" + kpoMaxPoHour.toString() + "' AND TIME(payment_date) < '" + kpoMaxPaymentHour.toString() + "' AND DATE(trx_date) = DATE(payment_date) AND DAYNAME(trx_date) <> 'Saturday' AND DAYNAME(trx_date) <> 'Sunday' ");
			sb.append("AND DAY(trx_date) NOT IN (10,28,29,30,31) ");
			if (!"".equals(hariLibur)) {
				sb.append("AND DATE(trx_date) NOT IN " + hariLibur + " ");
			}
			sb.append("GROUP BY customer_id, DATE(trx_date) ");
//			sb.append("UNION ALL ");
//			sb.append("SELECT COUNT(DISTINCT(customer_id)) jumlah, customer_id stockist FROM pesan_penjualan_sc ");
//			sb.append("WHERE tgl_transaksi >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND tgl_transaksi < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND customer_id LIKE 'SC%' ");
//			sb.append("AND DAYNAME(tgl_transaksi) <> 'Saturday' AND DAYNAME(tgl_transaksi) <> 'Sunday' ");
//			sb.append("AND DAY(tgl_transaksi) NOT IN (10,28,29,30,31) ");
//			if (!"".equals(hariLibur)) {
//				sb.append("AND DATE(tgl_transaksi) NOT IN " + hariLibur + " ");
//			}
//			sb.append("AND (TIME(tgl_pesan) < '" + kpoMaxPoHour.toString() + "' AND TIME(tgl_transaksi) < '" + kpoMaxPaymentHour.toString() + "' AND DATE(tgl_pesan) = DATE(tgl_transaksi)) ");
//			sb.append("GROUP BY customer_id, DATE(tgl_transaksi) ");
//			sb.append(") po ");
//			sb.append("GROUP BY stockist");
			System.out.println("Query KPO: " + sb.toString());
			
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				String stockistId = rs.getString("stockist");
				int jumlah = rs.getInt("jumlah");
				if (jumlah >= kpoGolaMin) {
					alStokis.add(stockistId);
					alJumlah.add(jumlah);
				}
			}
			double bagian = budgetSPlan * kpoBudgetPct / (100 * getTotalBagianKpo(alJumlah));
			System.out.println("1 bagian KPO = " + bagian);
			for (int i=0; i<alStokis.size(); i++) {
				String stockistId = (String) alStokis.get(i);
				int jumlah = Integer.parseInt(alJumlah.get(i).toString());
				double bagianStockist = jumlah - kpoGolaMin + kpoGolaBagian;
				System.out.println("Bagian Stockist " + stockistId + " = " + bagianStockist);
//				if (jumlah >= kpoGoldMin) {
//					bagianStockist = kpoGoldBagian;
//				} else if (jumlah >= kpoGolcMin) {
//					bagianStockist = kpoGolcBagian;
//				} else if (jumlah >= kpoGolbMin) {
//					bagianStockist = kpoGolbBagian;
//				} else {
//					bagianStockist = kpoGolaBagian;
//				}
				double bonusKpo = NumberUtil.roundToDecimals((bagianStockist * bagian), 0);
				
				BonusSc bonus = BonusScFcd.getBonusByStockistAndMonth(stockistId, bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusSc();
					bonus.setStockistId(stockistId);
					bonus.setBulan(bulanHitung.toDate());
					bonus.setkPurchaseOrder(bonusKpo);
					BonusScFcd bsFcd = new BonusScFcd(bonus);
					bsFcd.insertBonus(dbConn);
				} else {
					bonus.setkPurchaseOrder(bonusKpo);
					BonusScFcd bsFcd = new BonusScFcd();
					bsFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
//			if (dbConn != null) dbConn.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	private int getTotalBagianKpo(ArrayList alJumlah) {
		int totalBagian = 0;
		for (int i=0; i<alJumlah.size(); i++) {
			int jumlah = Integer.parseInt(alJumlah.get(i).toString());
			int bagian = jumlah - kpoGolaMin + kpoGolaBagian;
//			if (jumlah >= kpoGoldMin) {
//				bagian = kpoGoldBagian;
//			} else if (jumlah >= kpoGolcMin) {
//				bagian = kpoGolcBagian;
//			} else if (jumlah >= kpoGolbMin) {
//				bagian = kpoGolbBagian;
//			} else {
//				bagian = kpoGolaBagian;
//			}
			totalBagian += bagian;
		}
		System.out.println("Total bagian KPO = " + totalBagian);
		
		return totalBagian;
	}
	
	private String getHariLibur(DBConnection dbConn) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT tanggal FROM hari_libur ");
		sb.append("WHERE tanggal >= '" + bulanHitung.toString("yyyy-MM-dd") + "' AND tanggal < '" + beginOfDay.toString("yyyy-MM-dd") + "'");
		SQLAccess sqlAccess = null;
		StringBuffer tanggal = new StringBuffer();
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			int baris = 0;
			while (rs.next()) {
				if (baris == 0) {
					tanggal.append("(");
				} else {
					tanggal.append(",");
				}
				DateFormat shortFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date tglLibur = rs.getDate("tanggal");
				tanggal.append("'" + shortFormat.format(tglLibur) + "'");
				baris++;
			}
			if (baris > 0) {
				tanggal.append(")");
			}
			rs.close();
			return tanggal.toString();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
//	public void giveBonusKsl() {
	public void giveBonusKsl(DBConnection dbConn) {
		StringBuffer sb = new StringBuffer();
//		sb.append("SELECT DISTINCT(stockist_id) stockist_id, SUM(total) total ");
//		sb.append("FROM ( ");
		sb.append("SELECT DISTINCT(stockist_id) stockist_id, SUM(total) total ");
		sb.append("FROM penjualan_langsung ");
		sb.append("WHERE trx_date >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND trx_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND stockist_id LIKE 'SC%' ");
		sb.append("AND jenis_produk = 2 ");
		sb.append("GROUP BY stockist_id ");
//		sb.append("UNION ");
//		sb.append("SELECT DISTINCT(stockist_id) stockist_id, SUM(total) total ");
//		sb.append("FROM pesan_penjualan_sc ");
//		sb.append("WHERE tgl_transaksi >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND tgl_transaksi < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND stockist_id LIKE 'SC%' ");
//		sb.append("GROUP BY stockist_id) omzet ");
//		sb.append("GROUP BY stockist_id");
		System.out.println("Query KSL: "  + sb.toString());
		
		ArrayList alStokis = new ArrayList();
//		DBConnection dbConn = null;
		SQLAccess sqlAccess = null;
		try {
//			dbConn = new DBConnection();
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				String stockistId = rs.getString("stockist_id");
				double total = rs.getDouble("total");
				if (total >= kslMinOmzet) {
					alStokis.add(stockistId);
				}
			}
			
			ArrayList alBagian = new ArrayList();
			for (int i=0; i<alStokis.size(); i++) {
				String stockistId = (String) alStokis.get(i);
				System.out.println("===Looping KSL Stockist " + stockistId + "===");
				double stockAwalBulan = 0;
				sb = new StringBuffer();
				sb.append("SELECT DISTINCT(product_code) product_code, SUM(quantity) quantity FROM stock ");
				sb.append("WHERE stock_date < '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND gudang_id = '" + stockistId + "' ");
				sb.append("GROUP BY product_code");
				rs = sqlAccess.executeQuery(sb.toString());
				
				while (rs.next()) {
					stockAwalBulan += (getProductPrice(rs.getString("product_code"), bulanHitung, dbConn) * rs.getInt("quantity"));
				}
				
				double stockTertinggi = stockAwalBulan;
				System.out.println("Stock awal bulan = " + stockAwalBulan);
				int jumlahHari = 0;
				MutableDateTime dayToDay = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), bulanHitung.getDayOfMonth(), 0, 0, 0, 0);
//				dayToDay.addDays(1);
				double jumlahStokTertinggi = 0;
				while (dayToDay.compareTo(beginOfDay) < 0) {
					jumlahHari++;
					double stockHariBerjalan = stockTertinggi;
					sb = new StringBuffer();
					sb.append("SELECT * FROM stock ");
//					sb.append("WHERE stock_date < '" + dayToDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND gudang_id = '" + stockistId + "' ");
					sb.append("WHERE DATE(stock_date) = '" + dayToDay.toString("yyyy-MM-dd") + "' AND gudang_id = '" + stockistId + "' ");
					sb.append("ORDER BY stock_date");
					rs = sqlAccess.executeQuery(sb.toString());
					while (rs.next()) {
						double perProduk = getProductPrice(rs.getString("product_code"), dayToDay, dbConn) * rs.getInt("quantity");
						stockHariBerjalan += perProduk;
						if ((perProduk > 0) && (stockHariBerjalan > stockTertinggi)) {
							stockTertinggi = stockHariBerjalan;
						}
					}
					System.out.println("Stock tertinggi hari ke-"+jumlahHari+" = "+stockTertinggi);
					jumlahStokTertinggi += stockTertinggi;
					stockTertinggi = stockHariBerjalan;
					dayToDay.addDays(1);
				}
				double bagianStockist = hitungBagianStockist(jumlahStokTertinggi, jumlahHari);
				alBagian.add(bagianStockist);
			}
			if (alBagian.size() > 0) {				
				double totalBagian = hitungTotalBagianKsl(alBagian);
				System.out.println("Total bagian KSL = " + totalBagian);
				double bagian = budgetSPlan * kslBudgetPct / (100 * totalBagian);
				System.out.println("1 bagian KSL = " + bagian);
				for (int i=0; i<alStokis.size(); i++) {
					String stockistId = (String) alStokis.get(i);
					double bagianStockist = Double.parseDouble(alBagian.get(i).toString());
					System.out.println("Bagian Stockist "+stockistId+" = " + bagianStockist);
					if (bagianStockist > 0) {
						double bonusKsl = NumberUtil.roundToDecimals((bagian * bagianStockist), 0);
						System.out.println("Bonus KSL Stockist "+stockistId+" = " + bonusKsl);
						
						BonusSc bonus = BonusScFcd.getBonusByStockistAndMonth(stockistId, bulanHitung.toDate(), dbConn);
						if (bonus == null) {
							bonus = new BonusSc();
							bonus.setStockistId(stockistId);
							bonus.setBulan(bulanHitung.toDate());
							bonus.setkStockLevel(bonusKsl);
							BonusScFcd bsFcd = new BonusScFcd(bonus);
							bsFcd.insertBonus(dbConn);
						} else {
							bonus.setkStockLevel(bonusKsl);
							BonusScFcd bsFcd = new BonusScFcd();
							bsFcd.updateBonus(bonus, dbConn);
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
//			if (dbConn != null) dbConn.close();
		}
	}
	
	private double hitungBagianStockist(double jmlStokTertinggi, int jmlHari) {
		double bagian = 0;
		double rataan = jmlStokTertinggi / jmlHari;
		System.out.println("Jumlah stok tertinggi="+jmlStokTertinggi+"; jumlah hari="+jmlHari);
//		double pctStokLevel = rataan * 100 / kslFullPct;
//		if (pctStokLevel >= kslGolfMin) {
//			bagian = kslGolfBagian;
//		} else if (pctStokLevel >= kslGoleMin) {
//			bagian = kslGoleBagian;
//		} else if (pctStokLevel >= kslGoldMin) {
//			bagian = kslGoldBagian;
//		} else if (pctStokLevel >= kslGolcMin) {
//			bagian = kslGolcBagian;
//		} else if (pctStokLevel >= kslGolbMin) {
//			bagian = kslGolbBagian;
//		} else if (pctStokLevel >= kslGolaMin) {
//			bagian = kslGolaBagian;
//		}
		if (rataan >= kslMinStokrata) {
			bagian = NumberUtil.roundToDecimals((rataan / kslStokPerbagian), 0);
		}
		return bagian;
	}
	
	@SuppressWarnings("unchecked")
	private double hitungTotalBagianKsl(ArrayList alBagian) {
		double totalBagian = 0;
		for (int i=0; i<alBagian.size(); i++) {
			double bagian = Double.parseDouble(alBagian.get(i).toString());
			totalBagian += bagian;
		}
		return totalBagian;
	}
	
	private double getProductPrice(String prodCode, MutableDateTime tanggal, DBConnection dbConn) {
		double harga = 0;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT harga1 FROM product_price WHERE product_code = '" + prodCode + "' ");
//		sb.append("AND STATUS = 'Berlaku' AND start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' ");
		sb.append("AND start_date < '" + tanggal.toString("yyyy-MM-dd") + "' ");
		sb.append("ORDER BY start_date DESC LIMIT 1");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			if (rs.next()) {
				harga = rs.getDouble("harga1");
			}
			rs.close();
			return harga;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
//	public void giveBonusLpku() {
	public void giveBonusLpku(DBConnection dbConn) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT stockist, SUM(jumlah) jumlah ");
		sb.append("FROM ( ");
		sb.append("SELECT COUNT(DISTINCT(customer_id)) jumlah, customer_id stockist FROM pesan_penjualan_sc ");
		sb.append("WHERE tgl_transaksi >= '" + bulanHitung.toString("yyyy-MM-dd HH:mm:ss") + "' AND tgl_transaksi < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND customer_id LIKE 'SC%' ");
		sb.append("AND (TIME(tgl_pesan) < '" + blpukMaxPoHour.toString() + "' AND TIME(tgl_transaksi) < '" + blpukMaxPaymentHour.toString() + "' AND DATE(tgl_pesan) = DATE(tgl_transaksi)) ");
		sb.append("AND using_kode_unik = 1 ");
		sb.append("GROUP BY customer_id, DATE(tgl_transaksi)");
		sb.append(") po ");
		sb.append("GROUP BY stockist");
		
		ArrayList alStokis = new ArrayList();
		ArrayList alJumlah = new ArrayList();
//		DBConnection dbConn = null;
		SQLAccess sqlAccess = null;
		try {
//			dbConn = new DBConnection();
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				String stockistId = rs.getString("stockist");
				int jumlah = rs.getInt("jumlah");
				if (jumlah >= blpukGolaMin) {
					alStokis.add(stockistId);
					alJumlah.add(jumlah);
				}
			}
			double bagian = budgetSPlan * blpukBudgetPct / (100 * getTotalBagianBlpuk(alJumlah));
			System.out.println("1 bagian BLPKU = " + bagian);
			for (int i=0; i<alStokis.size(); i++) {
				String stockistId = (String) alStokis.get(i);
				int jumlah = Integer.parseInt(alJumlah.get(i).toString());
				double bagianStockist = jumlah - blpukGolaMin + blpukGolaBagian;
				double bonusLpku = NumberUtil.roundToDecimals((bagianStockist * bagian), 0);
				
				BonusSc bonus = BonusScFcd.getBonusByStockistAndMonth(stockistId, bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusSc();
					bonus.setStockistId(stockistId);
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusKodeUnik(bonusLpku);
					BonusScFcd bsFcd = new BonusScFcd(bonus);
					bsFcd.insertBonus(dbConn);
				} else {
					bonus.setBonusKodeUnik(bonusLpku);
					BonusScFcd bsFcd = new BonusScFcd();
					bsFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
//			if (dbConn != null) dbConn.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	private double getTotalBagianBlpuk(ArrayList alJumlah) {
		double totalBagian = 0;
		for (int i=0; i<alJumlah.size(); i++) {
			int jumlah = Integer.parseInt(alJumlah.get(i).toString());
			double bagian = jumlah - blpukGolaMin + blpukGolaBagian;
			totalBagian += bagian;
		}
		System.out.println("Total Bagian LPKU = " + totalBagian);
		
		return totalBagian;
	}
	
	@SuppressWarnings("unchecked")
//	public void giveBonusTop5() {
	public void giveBonusTop5(DBConnection dbConn) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT stockist_id, (k_stockist_omzet + k_purchase_order + k_stock_level + bonus_kode_unik) bonus ");
		sb.append("FROM bonus_sc ");
		sb.append("ORDER BY bonus DESC LIMIT 5");
		
		ArrayList alStokis = new ArrayList();
		ArrayList alBagian = new ArrayList();
//		DBConnection dbConn = null;
		SQLAccess sqlAccess = null;
		try {
//			dbConn = new DBConnection();
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			int ranking = 0;
			double totalBagian = 0;
			
			while (rs.next()) {
				ranking++;
				String stockistId = rs.getString("stockist_id");
				System.out.println("Ranking "+ranking+" adalah "+stockistId);
				alStokis.add(stockistId);
				double bagianStockist = 0;
				switch (ranking) {
				case 1:
					bagianStockist = top5Rank1Bagian;
					break;
				case 2:
					bagianStockist = top5Rank2Bagian;
					break;
				case 3:
					bagianStockist = top5Rank3Bagian;
					break;
				case 4:
					bagianStockist = top5Rank4Bagian;
					break;
				case 5:
					bagianStockist = top5Rank5Bagian;
					break;
				default:
					break;
				}
				totalBagian += bagianStockist;
				alBagian.add(bagianStockist);
			}
			double bagian = budgetSPlan * top5BudgetPct / (100 * totalBagian);
			System.out.println("Total bagian Top 5 = " + totalBagian);
			System.out.println("1 bagian Top 5 = " + bagian);
			for (int i=0; i<alStokis.size(); i++) {
				String stockistId = (String) alStokis.get(i);
				double bagianStockist = Double.parseDouble(alBagian.get(i).toString());
				double bonusTop5 = NumberUtil.roundToDecimals((bagianStockist * bagian), 0);
				
				BonusSc bonus = BonusScFcd.getBonusByStockistAndMonth(stockistId, bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusSc();
					bonus.setStockistId(stockistId);
					bonus.setBulan(bulanHitung.toDate());
					bonus.setTop5(bonusTop5);
					BonusScFcd bsFcd = new BonusScFcd(bonus);
					bsFcd.insertBonus(dbConn);
				} else {
					bonus.setTop5(bonusTop5);
					BonusScFcd bsFcd = new BonusScFcd();
					bsFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
//			if (dbConn != null) dbConn.close();
		}
	}
}
