package id.co.birumerah.bsi.bonus.stockist;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SettingBonusScSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;

	@SuppressWarnings("unchecked")
	public SettingBonusScSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(SettingBonusSc dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public SettingBonusSc get(int index) {
		SettingBonusSc result = null;
		
		if ((index >= 0) && (index < length()))
			result = (SettingBonusSc) set.get(index);
		
		return result;
	}
}
