package id.co.birumerah.bsi.bonus.stockist;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusScFcd {

	private BonusSc bonusSc;
	
	public BonusScFcd() {}

	public BonusScFcd(BonusSc bonusSc) {
		this.bonusSc = bonusSc;
	}

	public BonusSc getBonusSc() {
		return bonusSc;
	}

	public void setBonusSc(BonusSc bonusSc) {
		this.bonusSc = bonusSc;
	}
	
	public BonusScSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		BonusScSet bsSet = null;
		
		try {
			dbConn = new DBConnection();
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			bsSet = bsDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return bsSet;
	}
	
	public BonusScSet search(String whereClause, DBConnection dbConn) throws Exception {
		BonusScSet bsSet = null;
		
		try {
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			bsSet = bsDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return bsSet;
	}
	
	public static BonusSc getBonusByStockistAndMonth(String stockistId, Date bulan) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusSc bonus = new BonusSc();
			bonus.setStockistId(stockistId);
			bonus.setBulan(bulan);
			
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			boolean found = bsDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus bulan ini tidak ditemukan untuk stockist ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static BonusSc getBonusByStockistAndMonth(String stockistId, Date bulan, DBConnection dbConn) throws Exception {
		try {
			BonusSc bonus = new BonusSc();
			bonus.setStockistId(stockistId);
			bonus.setBulan(bulan);
			
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			boolean found = bsDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus bulan ini tidak ditemukan untuk stockist ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertBonus() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			bsDAO.insert(bonusSc);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			bsDAO.insert(bonusSc);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateBonus(BonusSc bonus) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			if (bsDAO.update(bonus) < 1) {
				throw new Exception("Update bonus stockist unsuccessful.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateBonus(BonusSc bonus, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusScDAO bsDAO = new BonusScDAO(dbConn);
			if (bsDAO.update(bonus) < 1) {
				throw new Exception("Update bonus stockist unsuccessful.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
