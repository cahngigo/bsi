package id.co.birumerah.bsi.bonus.stockist;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class SettingBonusScFcd {

	private SettingBonusSc settings;
	
	public SettingBonusScFcd() {}

	public SettingBonusScFcd(SettingBonusSc settings) {
		this.settings = settings;
	}

	public SettingBonusSc getSettings() {
		return settings;
	}

	public void setSettings(SettingBonusSc settings) {
		this.settings = settings;
	}
	
	public SettingBonusScSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		SettingBonusScSet sbSet = null;
		
		try {
			dbConn = new DBConnection();
			SettingBonusScDAO sbDAO = new SettingBonusScDAO(dbConn);
			sbSet = sbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return sbSet;
	}
	
	public SettingBonusScSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		SettingBonusScSet sbSet = null;
		
		try {
//			dbConn = new DBConnection();
			SettingBonusScDAO sbDAO = new SettingBonusScDAO(dbConn);
			sbSet = sbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return sbSet;
	}
}
