package id.co.birumerah.bsi.bonus.stockist;

import java.sql.Time;

public class SettingBonusSc {

	private long id;
	private double budgetUsPct;
	private double ksoBudgetPct;
	private double ksoMinOmzet;
	private double ksoOmzetPerbagian;
	private double kpoBudgetPct;
	private Time kpoMaxPoHour;
	private Time kpoMaxPaymentHour;
	private int kpoGolaMin;
	private int kpoGolaBagian;
	private double kslBudgetPct;
	private double kslFullPct;
	private double kslMinOmzet;
	private double kslMinStokrata;
	private double kslStokPerbagian;
	private double blpukBudgetPct;
	private Time blpukMaxPoHour;
	private Time blpukMaxPaymentHour;
	private int blpukGolaMin;
	private int blpukGolaBagian;
	private double top5BudgetPct;
	private int top5Rank1Bagian;
	private int top5Rank2Bagian;
	private int top5Rank3Bagian;
	private int top5Rank4Bagian;
	private int top5Rank5Bagian;
	
	public SettingBonusSc() {}

	public SettingBonusSc(long id, double budgetUsPct, double ksoBudgetPct,
			double ksoMinOmzet, double ksoOmzetPerbagian, double kpoBudgetPct,
			Time kpoMaxPoHour, Time kpoMaxPaymentHour, int kpoGolaMin,
			int kpoGolaBagian, double kslBudgetPct, double kslFullPct,
			double kslMinOmzet, double kslMinStokrata, double kslStokPerbagian,
			double blpukBudgetPct, Time blpukMaxPoHour,
			Time blpukMaxPaymentHour, int blpukGolaMin, int blpukGolaBagian,
			double top5BudgetPct, int top5Rank1Bagian, int top5Rank2Bagian,
			int top5Rank3Bagian, int top5Rank4Bagian, int top5Rank5Bagian) {
		this.id = id;
		this.budgetUsPct = budgetUsPct;
		this.ksoBudgetPct = ksoBudgetPct;
		this.ksoMinOmzet = ksoMinOmzet;
		this.ksoOmzetPerbagian = ksoOmzetPerbagian;
		this.kpoBudgetPct = kpoBudgetPct;
		this.kpoMaxPoHour = kpoMaxPoHour;
		this.kpoMaxPaymentHour = kpoMaxPaymentHour;
		this.kpoGolaMin = kpoGolaMin;
		this.kpoGolaBagian = kpoGolaBagian;
		this.kslBudgetPct = kslBudgetPct;
		this.kslFullPct = kslFullPct;
		this.kslMinOmzet = kslMinOmzet;
		this.kslMinStokrata = kslMinStokrata;
		this.kslStokPerbagian = kslStokPerbagian;
		this.blpukBudgetPct = blpukBudgetPct;
		this.blpukMaxPoHour = blpukMaxPoHour;
		this.blpukMaxPaymentHour = blpukMaxPaymentHour;
		this.blpukGolaMin = blpukGolaMin;
		this.blpukGolaBagian = blpukGolaBagian;
		this.top5BudgetPct = top5BudgetPct;
		this.top5Rank1Bagian = top5Rank1Bagian;
		this.top5Rank2Bagian = top5Rank2Bagian;
		this.top5Rank3Bagian = top5Rank3Bagian;
		this.top5Rank4Bagian = top5Rank4Bagian;
		this.top5Rank5Bagian = top5Rank5Bagian;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getBudgetUsPct() {
		return budgetUsPct;
	}

	public void setBudgetUsPct(double budgetUsPct) {
		this.budgetUsPct = budgetUsPct;
	}

	public double getKsoBudgetPct() {
		return ksoBudgetPct;
	}

	public void setKsoBudgetPct(double ksoBudgetPct) {
		this.ksoBudgetPct = ksoBudgetPct;
	}

	public double getKsoMinOmzet() {
		return ksoMinOmzet;
	}

	public void setKsoMinOmzet(double ksoMinOmzet) {
		this.ksoMinOmzet = ksoMinOmzet;
	}

	public double getKsoOmzetPerbagian() {
		return ksoOmzetPerbagian;
	}

	public void setKsoOmzetPerbagian(double ksoOmzetPerbagian) {
		this.ksoOmzetPerbagian = ksoOmzetPerbagian;
	}

	public double getKpoBudgetPct() {
		return kpoBudgetPct;
	}

	public void setKpoBudgetPct(double kpoBudgetPct) {
		this.kpoBudgetPct = kpoBudgetPct;
	}

	public Time getKpoMaxPoHour() {
		return kpoMaxPoHour;
	}

	public void setKpoMaxPoHour(Time kpoMaxPoHour) {
		this.kpoMaxPoHour = kpoMaxPoHour;
	}

	public Time getKpoMaxPaymentHour() {
		return kpoMaxPaymentHour;
	}

	public void setKpoMaxPaymentHour(Time kpoMaxPaymentHour) {
		this.kpoMaxPaymentHour = kpoMaxPaymentHour;
	}

	public int getKpoGolaMin() {
		return kpoGolaMin;
	}

	public void setKpoGolaMin(int kpoGolaMin) {
		this.kpoGolaMin = kpoGolaMin;
	}

	public int getKpoGolaBagian() {
		return kpoGolaBagian;
	}

	public void setKpoGolaBagian(int kpoGolaBagian) {
		this.kpoGolaBagian = kpoGolaBagian;
	}

	public double getKslBudgetPct() {
		return kslBudgetPct;
	}

	public void setKslBudgetPct(double kslBudgetPct) {
		this.kslBudgetPct = kslBudgetPct;
	}

	public double getKslFullPct() {
		return kslFullPct;
	}

	public void setKslFullPct(double kslFullPct) {
		this.kslFullPct = kslFullPct;
	}

	public double getKslMinOmzet() {
		return kslMinOmzet;
	}

	public void setKslMinOmzet(double kslMinOmzet) {
		this.kslMinOmzet = kslMinOmzet;
	}

	public double getKslMinStokrata() {
		return kslMinStokrata;
	}

	public void setKslMinStokrata(double kslMinStokrata) {
		this.kslMinStokrata = kslMinStokrata;
	}

	public double getKslStokPerbagian() {
		return kslStokPerbagian;
	}

	public void setKslStokPerbagian(double kslStokPerbagian) {
		this.kslStokPerbagian = kslStokPerbagian;
	}

	public double getBlpukBudgetPct() {
		return blpukBudgetPct;
	}

	public void setBlpukBudgetPct(double blpukBudgetPct) {
		this.blpukBudgetPct = blpukBudgetPct;
	}

	public Time getBlpukMaxPoHour() {
		return blpukMaxPoHour;
	}

	public void setBlpukMaxPoHour(Time blpukMaxPoHour) {
		this.blpukMaxPoHour = blpukMaxPoHour;
	}

	public Time getBlpukMaxPaymentHour() {
		return blpukMaxPaymentHour;
	}

	public void setBlpukMaxPaymentHour(Time blpukMaxPaymentHour) {
		this.blpukMaxPaymentHour = blpukMaxPaymentHour;
	}

	public int getBlpukGolaMin() {
		return blpukGolaMin;
	}

	public void setBlpukGolaMin(int blpukGolaMin) {
		this.blpukGolaMin = blpukGolaMin;
	}

	public int getBlpukGolaBagian() {
		return blpukGolaBagian;
	}

	public void setBlpukGolaBagian(int blpukGolaBagian) {
		this.blpukGolaBagian = blpukGolaBagian;
	}

	public double getTop5BudgetPct() {
		return top5BudgetPct;
	}

	public void setTop5BudgetPct(double top5BudgetPct) {
		this.top5BudgetPct = top5BudgetPct;
	}

	public int getTop5Rank1Bagian() {
		return top5Rank1Bagian;
	}

	public void setTop5Rank1Bagian(int top5Rank1Bagian) {
		this.top5Rank1Bagian = top5Rank1Bagian;
	}

	public int getTop5Rank2Bagian() {
		return top5Rank2Bagian;
	}

	public void setTop5Rank2Bagian(int top5Rank2Bagian) {
		this.top5Rank2Bagian = top5Rank2Bagian;
	}

	public int getTop5Rank3Bagian() {
		return top5Rank3Bagian;
	}

	public void setTop5Rank3Bagian(int top5Rank3Bagian) {
		this.top5Rank3Bagian = top5Rank3Bagian;
	}

	public int getTop5Rank4Bagian() {
		return top5Rank4Bagian;
	}

	public void setTop5Rank4Bagian(int top5Rank4Bagian) {
		this.top5Rank4Bagian = top5Rank4Bagian;
	}

	public int getTop5Rank5Bagian() {
		return top5Rank5Bagian;
	}

	public void setTop5Rank5Bagian(int top5Rank5Bagian) {
		this.top5Rank5Bagian = top5Rank5Bagian;
	}
}
