package id.co.birumerah.bsi.bonus.stockist;

import java.util.Date;

public class BonusSc {

	private String stockistId;
	private Date bulan;
	private double kStockistOmzet;
	private double kPurchaseOrder;
	private double kStockLevel;
	private double bonusKodeUnik;
	private double top5;
	
	public BonusSc() {}

	public BonusSc(String stockistId, Date bulan, double kStockistOmzet,
			double kPurchaseOrder, double kStockLevel, double bonusKodeUnik,
			double top5) {
		this.stockistId = stockistId;
		this.bulan = bulan;
		this.kStockistOmzet = kStockistOmzet;
		this.kPurchaseOrder = kPurchaseOrder;
		this.kStockLevel = kStockLevel;
		this.bonusKodeUnik = bonusKodeUnik;
		this.top5 = top5;
	}

	public String getStockistId() {
		return stockistId;
	}

	public void setStockistId(String stockistId) {
		this.stockistId = stockistId;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public double getkStockistOmzet() {
		return kStockistOmzet;
	}

	public void setkStockistOmzet(double kStockistOmzet) {
		this.kStockistOmzet = kStockistOmzet;
	}

	public double getkPurchaseOrder() {
		return kPurchaseOrder;
	}

	public void setkPurchaseOrder(double kPurchaseOrder) {
		this.kPurchaseOrder = kPurchaseOrder;
	}

	public double getkStockLevel() {
		return kStockLevel;
	}

	public void setkStockLevel(double kStockLevel) {
		this.kStockLevel = kStockLevel;
	}

	public double getBonusKodeUnik() {
		return bonusKodeUnik;
	}

	public void setBonusKodeUnik(double bonusKodeUnik) {
		this.bonusKodeUnik = bonusKodeUnik;
	}

	public double getTop5() {
		return top5;
	}

	public void setTop5(double top5) {
		this.top5 = top5;
	}
}
