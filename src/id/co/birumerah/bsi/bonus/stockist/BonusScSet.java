package id.co.birumerah.bsi.bonus.stockist;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BonusScSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public BonusScSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BonusSc dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusSc get(int index) {
		BonusSc result = null;
		
		if ((index >= 0) && (index < length()))
			result = (BonusSc) set.get(index);
		
		return result;
	}
}
