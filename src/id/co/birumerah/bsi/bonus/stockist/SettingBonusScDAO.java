package id.co.birumerah.bsi.bonus.stockist;

import java.sql.ResultSet;
import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class SettingBonusScDAO {

	DBConnection dbConn;

	public SettingBonusScDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public int insert(SettingBonusSc dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO setting_bonus_sc (");
		sb.append("budget_us_pct,");
		sb.append("kso_budget_pct,");
		sb.append("kso_min_omzet,");
		sb.append("kso_omzet_perbagian,");
		sb.append("kpo_budget_pct,");
		sb.append("kpo_max_po_hour,");
		sb.append("kpo_max_payment_hour,");
		sb.append("kpo_gola_min,");
		sb.append("kpo_gola_bagian,");
		sb.append("ksl_budget_pct,");
		sb.append("ksl_full_pct,");
		sb.append("ksl_min_omzet,");
		sb.append("ksl_min_stokrata,");
		sb.append("ksl_stok_perbagian,");
		sb.append("blpuk_budget_pct,");
		sb.append("blpuk_max_po_hour,");
		sb.append("blpuk_max_payment_hour,");
		sb.append("blpuk_gola_min,");
		sb.append("blpuk_gola_bagian,");
		sb.append("top5_budget_pct,");
		sb.append("top5_rank1_bagian,");
		sb.append("top5_rank2_bagian,");
		sb.append("top5_rank3_bagian,");
		sb.append("top5_rank4_bagian,");
		sb.append("top5_rank5_bagian");
		sb.append(") VALUES (");
		sb.append("" + dataObject.getBudgetUsPct() + ",");
		sb.append("" + dataObject.getKsoBudgetPct() + ",");
		sb.append("" + dataObject.getKsoMinOmzet() + ",");
		sb.append("" + dataObject.getKsoOmzetPerbagian() + ",");
		sb.append("" + dataObject.getKpoBudgetPct() + ",");
		sb.append("" + dataObject.getKpoMaxPoHour().toString() + ",");
		sb.append("" + dataObject.getKpoMaxPaymentHour().toString() + ",");
		sb.append("" + dataObject.getKpoGolaMin() + ",");
		sb.append("" + dataObject.getKpoGolaBagian() + ",");
		sb.append("" + dataObject.getKslBudgetPct() + ",");
		sb.append("" + dataObject.getKslFullPct() + ",");
		sb.append("" + dataObject.getKslMinOmzet() + ",");
		sb.append("" + dataObject.getKslMinStokrata() + ",");
		sb.append("" + dataObject.getKslStokPerbagian() + ",");
		sb.append("" + dataObject.getBlpukBudgetPct() + ",");
		sb.append("" + dataObject.getBlpukMaxPoHour().toString() + ",");
		sb.append("" + dataObject.getBlpukMaxPaymentHour().toString() + ",");
		sb.append("" + dataObject.getBlpukGolaMin() + ",");
		sb.append("" + dataObject.getBlpukGolaBagian() + ",");
		sb.append("" + dataObject.getTop5BudgetPct() + ",");
		sb.append("" + dataObject.getTop5Rank1Bagian() + ",");
		sb.append("" + dataObject.getTop5Rank2Bagian() + ",");
		sb.append("" + dataObject.getTop5Rank3Bagian() + ",");
		sb.append("" + dataObject.getTop5Rank4Bagian() + ",");
		sb.append("" + dataObject.getTop5Rank5Bagian() );
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(SettingBonusSc dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE setting_bonus_sc ");
		sb.append("SET ");
		sb.append("budget_us_pct = " + dataObject.getBudgetUsPct() + ",");
		sb.append("kso_budget_pct = " + dataObject.getKsoBudgetPct() + ",");
		sb.append("kso_min_omzet = " + dataObject.getKsoMinOmzet() + ",");
		sb.append("kso_omzet_perbagian = " + dataObject.getKsoOmzetPerbagian() + ",");
		sb.append("kpo_budget_pct = " + dataObject.getKpoBudgetPct() + ",");
		sb.append("kpo_max_po_hour = " + dataObject.getKpoMaxPoHour().toString() + ",");
		sb.append("kpo_max_payment_hour = " + dataObject.getKpoMaxPaymentHour().toString() + ",");
		sb.append("kpo_gola_min = " + dataObject.getKpoGolaMin() + ",");
		sb.append("kpo_gola_bagian = " + dataObject.getKpoGolaBagian() + ",");
		sb.append("ksl_budget_pct = " + dataObject.getKslBudgetPct() + ",");
		sb.append("ksl_full_pct = " + dataObject.getKslFullPct() + ",");
		sb.append("ksl_min_omzet = " + dataObject.getKslMinOmzet() + ",");
		sb.append("ksl_min_stokrata = " + dataObject.getKslMinStokrata() + ",");
		sb.append("ksl_stok_perbagian = " + dataObject.getKslStokPerbagian() + ",");
		sb.append("blpuk_budget_pct = " + dataObject.getBlpukBudgetPct() + ",");
		sb.append("blpuk_max_po_hour = " + dataObject.getBlpukMaxPoHour().toString() + ",");
		sb.append("blpuk_max_payment_hour = " + dataObject.getBlpukMaxPaymentHour().toString() + ",");
		sb.append("blpuk_gola_min = " + dataObject.getBlpukGolaMin() + ",");
		sb.append("blpuk_gola_bagian = " + dataObject.getBlpukGolaBagian() + ",");
		sb.append("top5_budget_pct = " + dataObject.getTop5BudgetPct() + ",");
		sb.append("top5_rank1_bagian = " + dataObject.getTop5Rank1Bagian() + ",");
		sb.append("top5_rank2_bagian = " + dataObject.getTop5Rank2Bagian() + ",");
		sb.append("top5_rank3_bagian = " + dataObject.getTop5Rank3Bagian() + ",");
		sb.append("top5_rank4_bagian = " + dataObject.getTop5Rank4Bagian() + ",");
		sb.append("top5_rank5_bagian = " + dataObject.getTop5Rank5Bagian() + " ");
		sb.append("WHERE id = " + dataObject.getId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(SettingBonusSc dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("budget_us_pct,");
		sb.append("kso_budget_pct,");
		sb.append("kso_min_omzet,");
		sb.append("kso_omzet_perbagian,");
		sb.append("kpo_budget_pct,");
		sb.append("kpo_max_po_hour,");
		sb.append("kpo_max_payment_hour,");
		sb.append("kpo_gola_min,");
		sb.append("kpo_gola_bagian,");
		sb.append("ksl_budget_pct,");
		sb.append("ksl_full_pct,");
		sb.append("ksl_min_omzet,");
		sb.append("ksl_min_stokrata,");
		sb.append("ksl_stok_perbagian,");
		sb.append("blpuk_budget_pct,");
		sb.append("blpuk_max_po_hour,");
		sb.append("blpuk_max_payment_hour,");
		sb.append("blpuk_gola_min,");
		sb.append("blpuk_gola_bagian,");
		sb.append("top5_budget_pct,");
		sb.append("top5_rank1_bagian,");
		sb.append("top5_rank2_bagian,");
		sb.append("top5_rank3_bagian,");
		sb.append("top5_rank4_bagian,");
		sb.append("top5_rank5_bagian ");
		sb.append("FROM setting_bonus_sc ");
		sb.append("WHERE id = " + dataObject.getId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBudgetUsPct(rs.getDouble("budget_us_pct"));
				dataObject.setKsoBudgetPct(rs.getDouble("kso_budget_pct"));
				dataObject.setKsoMinOmzet(rs.getDouble("kso_min_omzet"));
				dataObject.setKsoOmzetPerbagian(rs.getDouble("kso_omzet_perbagian"));
				dataObject.setKpoBudgetPct(rs.getDouble("kpo_budget_pct"));
				dataObject.setKpoMaxPoHour(rs.getTime("kpo_max_po_hour"));
				dataObject.setKpoMaxPaymentHour(rs.getTime("kpo_max_payment_hour"));
				dataObject.setKpoGolaMin(rs.getInt("kpo_gola_min"));
				dataObject.setKpoGolaBagian(rs.getInt("kpo_gola_bagian"));
				dataObject.setKslBudgetPct(rs.getDouble("ksl_budget_pct"));
				dataObject.setKslFullPct(rs.getDouble("ksl_full_pct"));
				dataObject.setKslMinOmzet(rs.getDouble("ksl_min_omzet"));
				dataObject.setKslMinStokrata(rs.getDouble("ksl_min_stokrata"));
				dataObject.setKslStokPerbagian(rs.getDouble("ksl_stok_perbagian"));
				dataObject.setBlpukBudgetPct(rs.getDouble("blpuk_budget_pct"));
				dataObject.setBlpukMaxPoHour(rs.getTime("blpuk_max_po_hour"));
				dataObject.setBlpukMaxPaymentHour(rs.getTime("blpuk_max_payment_hour"));
				dataObject.setBlpukGolaMin(rs.getInt("blpuk_gola_min"));
				dataObject.setBlpukGolaBagian(rs.getInt("blpuk_gola_bagian"));
				dataObject.setTop5BudgetPct(rs.getDouble("top5_budget_pct"));
				dataObject.setTop5Rank1Bagian(rs.getInt("top5_rank1_bagian"));
				dataObject.setTop5Rank2Bagian(rs.getInt("top5_rank2_bagian"));
				dataObject.setTop5Rank3Bagian(rs.getInt("top5_rank3_bagian"));
				dataObject.setTop5Rank4Bagian(rs.getInt("top5_rank4_bagian"));
				dataObject.setTop5Rank5Bagian(rs.getInt("top5_rank5_bagian"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public SettingBonusScSet select(String whereClause) throws SQLException {
		SettingBonusScSet dataObjectSet = new SettingBonusScSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("budget_us_pct,");
		sb.append("kso_budget_pct,");
		sb.append("kso_min_omzet,");
		sb.append("kso_omzet_perbagian,");
		sb.append("kpo_budget_pct,");
		sb.append("kpo_max_po_hour,");
		sb.append("kpo_max_payment_hour,");
		sb.append("kpo_gola_min,");
		sb.append("kpo_gola_bagian,");
		sb.append("ksl_budget_pct,");
		sb.append("ksl_full_pct,");
		sb.append("ksl_min_omzet,");
		sb.append("ksl_min_stokrata,");
		sb.append("ksl_stok_perbagian,");
		sb.append("blpuk_budget_pct,");
		sb.append("blpuk_max_po_hour,");
		sb.append("blpuk_max_payment_hour,");
		sb.append("blpuk_gola_min,");
		sb.append("blpuk_gola_bagian,");
		sb.append("top5_budget_pct,");
		sb.append("top5_rank1_bagian,");
		sb.append("top5_rank2_bagian,");
		sb.append("top5_rank3_bagian,");
		sb.append("top5_rank4_bagian,");
		sb.append("top5_rank5_bagian ");
		sb.append("FROM setting_bonus_sc ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				SettingBonusSc dataObject = new SettingBonusSc();
				dataObject.setId(rs.getLong("id"));
				dataObject.setBudgetUsPct(rs.getDouble("budget_us_pct"));
				dataObject.setKsoBudgetPct(rs.getDouble("kso_budget_pct"));
				dataObject.setKsoMinOmzet(rs.getDouble("kso_min_omzet"));
				dataObject.setKsoOmzetPerbagian(rs.getDouble("kso_omzet_perbagian"));
				dataObject.setKpoBudgetPct(rs.getDouble("kpo_budget_pct"));
				dataObject.setKpoMaxPoHour(rs.getTime("kpo_max_po_hour"));
				dataObject.setKpoMaxPaymentHour(rs.getTime("kpo_max_payment_hour"));
				dataObject.setKpoGolaMin(rs.getInt("kpo_gola_min"));
				dataObject.setKpoGolaBagian(rs.getInt("kpo_gola_bagian"));
				dataObject.setKslBudgetPct(rs.getDouble("ksl_budget_pct"));
				dataObject.setKslFullPct(rs.getDouble("ksl_full_pct"));
				dataObject.setKslMinOmzet(rs.getDouble("ksl_min_omzet"));
				dataObject.setKslMinStokrata(rs.getDouble("ksl_min_stokrata"));
				dataObject.setKslStokPerbagian(rs.getDouble("ksl_stok_perbagian"));
				dataObject.setBlpukBudgetPct(rs.getDouble("blpuk_budget_pct"));
				dataObject.setBlpukMaxPoHour(rs.getTime("blpuk_max_po_hour"));
				dataObject.setBlpukMaxPaymentHour(rs.getTime("blpuk_max_payment_hour"));
				dataObject.setBlpukGolaMin(rs.getInt("blpuk_gola_min"));
				dataObject.setBlpukGolaBagian(rs.getInt("blpuk_gola_bagian"));
				dataObject.setTop5BudgetPct(rs.getDouble("top5_budget_pct"));
				dataObject.setTop5Rank1Bagian(rs.getInt("top5_rank1_bagian"));
				dataObject.setTop5Rank2Bagian(rs.getInt("top5_rank2_bagian"));
				dataObject.setTop5Rank3Bagian(rs.getInt("top5_rank3_bagian"));
				dataObject.setTop5Rank4Bagian(rs.getInt("top5_rank4_bagian"));
				dataObject.setTop5Rank5Bagian(rs.getInt("top5_rank5_bagian"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
