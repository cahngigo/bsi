package id.co.birumerah.bsi.bonus;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class TaxRateSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public TaxRateSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(TaxRate dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public TaxRate get(int index) {
		TaxRate result = null;
		
		if ((index >= 0) && (index < length()))
			result = (TaxRate) set.get(index);
		
		return result;
	}
}
