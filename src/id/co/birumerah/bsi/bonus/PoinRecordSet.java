package id.co.birumerah.bsi.bonus;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PoinRecordSet implements Serializable {
	
	@SuppressWarnings("rawtypes")
	ArrayList set = null;
	
	@SuppressWarnings("rawtypes")
	public PoinRecordSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(PoinRecord dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public PoinRecord get(int index) {
		PoinRecord result = null;
		
		if ((index >= 0) && (index < length())) 
			result = (PoinRecord) set.get(index);
		
		return result;
	}
}
