package id.co.birumerah.bsi.bonus;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PeringkatMemberSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public PeringkatMemberSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(PeringkatMember dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public PeringkatMember get(int index) {
		PeringkatMember result = null;
		
		if ((index >= 0) && (index < length()))
			result = (PeringkatMember) set.get(index);
		
		return result;
	}
}
