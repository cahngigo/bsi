package id.co.birumerah.bsi.bonus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class PoinRecordDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public PoinRecordDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(PoinRecord dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO poin_record (");
		sb.append("member_id,");
		sb.append("trx_id,");
		sb.append("tanggal,");
		sb.append("am,");
		sb.append("poin,");
		sb.append("bv");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + dataObject.getTrxId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("" + dataObject.getAm() + ",");
		sb.append("" + dataObject.getPoin() + ",");
		sb.append("" + dataObject.getBv());
		sb.append(")");
		System.out.println("Query insert poin_record: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public PoinRecordSet select(String whereClause) throws SQLException {
		PoinRecordSet dataObjectSet = new PoinRecordSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("member_id,");
		sb.append("trx_id,");
		sb.append("tanggal,");
		sb.append("am,");
		sb.append("poin,");
		sb.append("bv ");
		sb.append("FROM poin_record ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				PoinRecord dataObject = new PoinRecord();
				dataObject.setId(rs.getLong("id"));
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setTrxId(rs.getString("trx_id"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setAm(rs.getDouble("am"));
				dataObject.setPoin(rs.getFloat("poin"));
				dataObject.setBv(rs.getDouble("bv"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int delete(PoinRecord dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM poin_record ");
		sb.append("WHERE tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "' ");
		sb.append("AND member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
