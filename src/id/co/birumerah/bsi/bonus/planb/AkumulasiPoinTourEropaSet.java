package id.co.birumerah.bsi.bonus.planb;

import java.io.Serializable;
import java.util.ArrayList;

public class AkumulasiPoinTourEropaSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2806383214140539209L;
	
	@SuppressWarnings("rawtypes")
	ArrayList set;
	
	@SuppressWarnings("rawtypes")
	public AkumulasiPoinTourEropaSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(AkumulasiPoinTourEropa dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public AkumulasiPoinTourEropa get(int index) {
		AkumulasiPoinTourEropa result = null;
		
		if ((index >= 0) && (index < length()))
			result = (AkumulasiPoinTourEropa) set.get(index);
		
		return result;
	}

}
