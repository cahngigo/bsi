package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class AkumulasiPoinTourEropa {

	private String memberId;
	private Date bulan;
	private Date regDate;
	private float pvGroup;
	private float akumKiri;
	private float akumTengah;
	private float akumKanan;
	private float akumPv;
	private float poinBroken;
	private String brokenByKiri;
	private String brokenByTengah;
	private String brokenByKanan;
	
	public AkumulasiPoinTourEropa() {}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public float getPvGroup() {
		return pvGroup;
	}

	public void setPvGroup(float pvGroup) {
		this.pvGroup = pvGroup;
	}

	public float getAkumKiri() {
		return akumKiri;
	}

	public void setAkumKiri(float akumKiri) {
		this.akumKiri = akumKiri;
	}

	public float getAkumTengah() {
		return akumTengah;
	}

	public void setAkumTengah(float akumTengah) {
		this.akumTengah = akumTengah;
	}

	public float getAkumKanan() {
		return akumKanan;
	}

	public void setAkumKanan(float akumKanan) {
		this.akumKanan = akumKanan;
	}

	public float getAkumPv() {
		return akumPv;
	}

	public void setAkumPv(float akumPv) {
		this.akumPv = akumPv;
	}

	public float getPoinBroken() {
		return poinBroken;
	}

	public void setPoinBroken(float poinBroken) {
		this.poinBroken = poinBroken;
	}

	public String getBrokenByKiri() {
		return brokenByKiri;
	}

	public void setBrokenByKiri(String brokenByKiri) {
		this.brokenByKiri = brokenByKiri;
	}

	public String getBrokenByTengah() {
		return brokenByTengah;
	}

	public void setBrokenByTengah(String brokenByTengah) {
		this.brokenByTengah = brokenByTengah;
	}

	public String getBrokenByKanan() {
		return brokenByKanan;
	}

	public void setBrokenByKanan(String brokenByKanan) {
		this.brokenByKanan = brokenByKanan;
	}
}
