package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class BonusB {

	private String memberId;
	private Date bulan;
	private double bonusRo;
	private double bonusSi;
	private double bonusUs;
	private double bonusEarlybird;
	private double bonusOr;
	private double borAntrian;
	private double traveling;
	private double bonusRc;
	private float borAkumgk;
	private int borTingkat;
	private int borIsqualified;
	private int borIsclaimed;
	private int brcIsqualifiedCd;
	private int brcContinuityCd;
	private int brcIsqualifiedScd;
	private int brcContinuityScd;
	private double bwtAkum;
	private double potonganAm;
	private int ldpS1Continuity;
	private int ldpS1Isqualified;
	private int ldpS2Continuity;
	private int ldpS2Isqualified;
	private int ldpS3Continuity;
	private int ldpS3Isqualified;
	private int ldpS4Continuity;
	private int ldpS4Isqualified;
	private double pkpKumulatif;
	private double tax;
	
	public BonusB() {}

	public BonusB(String memberId, Date bulan, double bonusRo, double bonusSi,
			double bonusUs, double bonusEarlybird, double bonusOr,
			double borAntrian, double traveling, double bonusRc,
			float borAkumgk, int borTingkat, int borIsqualified,
			int borIsclaimed, int brcIsqualifiedCd, int brcContinuityCd,
			int brcIsqualifiedScd, int brcContinuityScd, double bwtAkum,
			double potonganAm, double pkpKumulatif, double tax) {
		this.memberId = memberId;
		this.bulan = bulan;
		this.bonusRo = bonusRo;
		this.bonusSi = bonusSi;
		this.bonusUs = bonusUs;
		this.bonusEarlybird = bonusEarlybird;
		this.bonusOr = bonusOr;
		this.borAntrian = borAntrian;
		this.traveling = traveling;
		this.bonusRc = bonusRc;
		this.borAkumgk = borAkumgk;
		this.borTingkat = borTingkat;
		this.borIsqualified = borIsqualified;
		this.borIsclaimed = borIsclaimed;
		this.brcIsqualifiedCd = brcIsqualifiedCd;
		this.brcContinuityCd = brcContinuityCd;
		this.brcIsqualifiedScd = brcIsqualifiedScd;
		this.brcContinuityScd = brcContinuityScd;
		this.bwtAkum = bwtAkum;
		this.potonganAm = potonganAm;
		this.pkpKumulatif = pkpKumulatif;
		this.tax = tax;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public double getBonusRo() {
		return bonusRo;
	}

	public void setBonusRo(double bonusRo) {
		this.bonusRo = bonusRo;
	}

	public double getBonusSi() {
		return bonusSi;
	}

	public void setBonusSi(double bonusSi) {
		this.bonusSi = bonusSi;
	}

	public double getBonusUs() {
		return bonusUs;
	}

	public void setBonusUs(double bonusUs) {
		this.bonusUs = bonusUs;
	}

	public double getBonusEarlybird() {
		return bonusEarlybird;
	}

	public void setBonusEarlybird(double bonusEarlybird) {
		this.bonusEarlybird = bonusEarlybird;
	}

	public double getBonusOr() {
		return bonusOr;
	}

	public void setBonusOr(double bonusOr) {
		this.bonusOr = bonusOr;
	}

	public double getBorAntrian() {
		return borAntrian;
	}

	public void setBorAntrian(double borAntrian) {
		this.borAntrian = borAntrian;
	}

	public double getTraveling() {
		return traveling;
	}

	public void setTraveling(double traveling) {
		this.traveling = traveling;
	}

	public double getBonusRc() {
		return bonusRc;
	}

	public void setBonusRc(double bonusRc) {
		this.bonusRc = bonusRc;
	}

	public float getBorAkumgk() {
		return borAkumgk;
	}

	public void setBorAkumgk(float borAkumgk) {
		this.borAkumgk = borAkumgk;
	}

	public int getBorTingkat() {
		return borTingkat;
	}

	public void setBorTingkat(int borTingkat) {
		this.borTingkat = borTingkat;
	}

	public int getBorIsqualified() {
		return borIsqualified;
	}

	public void setBorIsqualified(int borIsqualified) {
		this.borIsqualified = borIsqualified;
	}

	public int getBorIsclaimed() {
		return borIsclaimed;
	}

	public void setBorIsclaimed(int borIsclaimed) {
		this.borIsclaimed = borIsclaimed;
	}

	public int getBrcIsqualifiedCd() {
		return brcIsqualifiedCd;
	}

	public void setBrcIsqualifiedCd(int brcIsqualifiedCd) {
		this.brcIsqualifiedCd = brcIsqualifiedCd;
	}

	public int getBrcContinuityCd() {
		return brcContinuityCd;
	}

	public void setBrcContinuityCd(int brcContinuityCd) {
		this.brcContinuityCd = brcContinuityCd;
	}

	public int getBrcIsqualifiedScd() {
		return brcIsqualifiedScd;
	}

	public void setBrcIsqualifiedScd(int brcIsqualifiedScd) {
		this.brcIsqualifiedScd = brcIsqualifiedScd;
	}

	public int getBrcContinuityScd() {
		return brcContinuityScd;
	}

	public void setBrcContinuityScd(int brcContinuityScd) {
		this.brcContinuityScd = brcContinuityScd;
	}

	public double getBwtAkum() {
		return bwtAkum;
	}

	public void setBwtAkum(double bwtAkum) {
		this.bwtAkum = bwtAkum;
	}

	public double getPotonganAm() {
		return potonganAm;
	}

	public void setPotonganAm(double potonganAm) {
		this.potonganAm = potonganAm;
	}

	public double getPkpKumulatif() {
		return pkpKumulatif;
	}

	public void setPkpKumulatif(double pkpKumulatif) {
		this.pkpKumulatif = pkpKumulatif;
	}

	public int getLdpS1Continuity() {
		return ldpS1Continuity;
	}

	public void setLdpS1Continuity(int ldpS1Continuity) {
		this.ldpS1Continuity = ldpS1Continuity;
	}

	public int getLdpS1Isqualified() {
		return ldpS1Isqualified;
	}

	public void setLdpS1Isqualified(int ldpS1Isqualified) {
		this.ldpS1Isqualified = ldpS1Isqualified;
	}

	public int getLdpS2Continuity() {
		return ldpS2Continuity;
	}

	public void setLdpS2Continuity(int ldpS2Continuity) {
		this.ldpS2Continuity = ldpS2Continuity;
	}

	public int getLdpS2Isqualified() {
		return ldpS2Isqualified;
	}

	public void setLdpS2Isqualified(int ldpS2Isqualified) {
		this.ldpS2Isqualified = ldpS2Isqualified;
	}

	public int getLdpS3Continuity() {
		return ldpS3Continuity;
	}

	public void setLdpS3Continuity(int ldpS3Continuity) {
		this.ldpS3Continuity = ldpS3Continuity;
	}

	public int getLdpS3Isqualified() {
		return ldpS3Isqualified;
	}

	public void setLdpS3Isqualified(int ldpS3Isqualified) {
		this.ldpS3Isqualified = ldpS3Isqualified;
	}

	public int getLdpS4Continuity() {
		return ldpS4Continuity;
	}

	public void setLdpS4Continuity(int ldpS4Continuity) {
		this.ldpS4Continuity = ldpS4Continuity;
	}

	public int getLdpS4Isqualified() {
		return ldpS4Isqualified;
	}

	public void setLdpS4Isqualified(int ldpS4Isqualified) {
		this.ldpS4Isqualified = ldpS4Isqualified;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}
}
