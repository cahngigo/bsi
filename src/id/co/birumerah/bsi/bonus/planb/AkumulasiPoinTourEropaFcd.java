package id.co.birumerah.bsi.bonus.planb;

import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;
import java.util.Date;

public class AkumulasiPoinTourEropaFcd {

	private AkumulasiPoinTourEropa poinTour;
	
	public AkumulasiPoinTourEropaFcd() {}

	public AkumulasiPoinTourEropaFcd(AkumulasiPoinTourEropa poinTour) {
		this.poinTour = poinTour;
	}

	public AkumulasiPoinTourEropa getPoinTour() {
		return poinTour;
	}

	public void setPoinTour(AkumulasiPoinTourEropa poinTour) {
		this.poinTour = poinTour;
	}
	
	public AkumulasiPoinTourEropaSet search(String whereClause, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourEropaSet aptSet = null;
		
		try {
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			aptSet = aptDao.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return aptSet;
	}
	
	public static AkumulasiPoinTourEropa getAkumulasiByMemberAndMonth(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		try {
			AkumulasiPoinTourEropa akumPoin = new AkumulasiPoinTourEropa();
			akumPoin.setMemberId(memberId);
			akumPoin.setBulan(bulan);
			
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			boolean found = aptDao.select(akumPoin);
			
			if (found) {
				return akumPoin;
			} else {
				System.err.println("Akumulasi poin bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertAkumulasi(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			aptDao.insert(poinTour);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateAkumulasi(AkumulasiPoinTourEropa dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			if (aptDao.update(dataObject) < 1) {
				throw new Exception("Updating Akumulasi Point Tour Eropa failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public AkumulasiPoinTourEropaSet getMemberPass(Date bulan, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourEropaSet aptSet = null;
		
		try {
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			aptSet = aptDao.getMemberPass(bulan);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return aptSet;
	}
	
	public AkumulasiPoinTourEropa getMemberPassByMember(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourEropa apt = null;
		
		try {
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			apt = aptDao.getMemberPassByMember(memberId, bulan);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return apt;
	}
	
	public AkumulasiPoinTourEropaSet getWrongAkumulasiPv(Date bulan, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourEropaSet aptSet = null;
		
		try {
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			aptSet = aptDao.getWrongAkumulasiPv(bulan);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return aptSet;
	}
	
	public boolean searchAkumulasiByMemberAndBlockBy(String memberId, String blockBy, DBConnection dbConn) throws Exception {
		boolean found = false;
		try {
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			found = aptDao.searchAkumulasiByMemberAndBlockBy(memberId, blockBy);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return found;
	}
	
	public void deleteAkumulasiPoinByBulan(Date bulan, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			AkumulasiPoinTourEropaDAO aptDao = new AkumulasiPoinTourEropaDAO(dbConn);
			if (aptDao.deleteAkumulasiPoinByBulan(bulan) < 1) {
				throw new Exception("Deleting Akumulasi Point Tour Eropa failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
