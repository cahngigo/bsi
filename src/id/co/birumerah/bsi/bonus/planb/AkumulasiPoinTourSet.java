package id.co.birumerah.bsi.bonus.planb;

import java.io.Serializable;
import java.util.ArrayList;

public class AkumulasiPoinTourSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1639685078666582535L;

	@SuppressWarnings("unchecked")
	ArrayList set;
	
	public AkumulasiPoinTourSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(AkumulasiPoinTour dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public AkumulasiPoinTour get(int index) {
		AkumulasiPoinTour result = null;
		
		if ((index >= 0) && (index < length()))
			result = (AkumulasiPoinTour) set.get(index);
		
		return result;
	}
}
