package id.co.birumerah.bsi.bonus.planb;

import java.io.Serializable;
import java.util.ArrayList;

public class BonusBSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1200964958210755231L;

	@SuppressWarnings("unchecked")
	ArrayList set;
	
	@SuppressWarnings("unchecked")
	public BonusBSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BonusB dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusB get(int index) {
		BonusB result = null;
		
		if ((index >= 0) && (index < length()))
			result = (BonusB) set.get(index);
		
		return result;
	}
}
