package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class BonusPbr {

	private String memberId;
	private Date bulan;
	private int pbr50;
	private int pbr100;
	private int pbr200;
	
	public BonusPbr() {}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public int getPbr50() {
		return pbr50;
	}

	public void setPbr50(int pbr50) {
		this.pbr50 = pbr50;
	}

	public int getPbr100() {
		return pbr100;
	}

	public void setPbr100(int pbr100) {
		this.pbr100 = pbr100;
	}

	public int getPbr200() {
		return pbr200;
	}

	public void setPbr200(int pbr200) {
		this.pbr200 = pbr200;
	}
}
