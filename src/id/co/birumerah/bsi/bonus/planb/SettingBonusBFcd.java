package id.co.birumerah.bsi.bonus.planb;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class SettingBonusBFcd {

	private SettingBonusB settings;
	
	public SettingBonusBFcd() {}
	
	public SettingBonusBFcd(SettingBonusB settings) {
		this.settings = settings;
	}

	public SettingBonusB getSettings() {
		return settings;
	}

	public void setSettings(SettingBonusB settings) {
		this.settings = settings;
	}
	
	public SettingBonusBSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		SettingBonusBSet sbSet = null;
		
		try {
			dbConn = new DBConnection();
			SettingBonusBDAO sbDAO = new SettingBonusBDAO(dbConn);
			sbSet = sbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return sbSet;
	}
	
	public SettingBonusBSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		SettingBonusBSet sbSet = null;
		
		try {
//			dbConn = new DBConnection();
			SettingBonusBDAO sbDAO = new SettingBonusBDAO(dbConn);
			sbSet = sbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return sbSet;
	}
}
