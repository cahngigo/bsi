package id.co.birumerah.bsi.bonus.planb;

import java.io.Serializable;
import java.util.ArrayList;

public class BonusLdpSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8319402285676709087L;
	@SuppressWarnings("rawtypes")
	private ArrayList set;
	
	@SuppressWarnings("rawtypes")
	public BonusLdpSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BonusLdp dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusLdp get(int index) {
		BonusLdp result = null;
		
		if ((index >= 0) && (index < length())) {
			result = (BonusLdp) set.get(index);
		}
		return result;
	}
}
