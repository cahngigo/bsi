package id.co.birumerah.bsi.bonus.planb;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.bonus.KualifikasiMemberChecker;
import id.co.birumerah.bsi.bonus.PoinRecord;
import id.co.birumerah.bsi.bonus.PoinRecordFcd;
import id.co.birumerah.bsi.bonus.SaldoPtkp;
import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.bonus.plana.BonusA;
import id.co.birumerah.bsi.bonus.plana.BonusAFcd;
import id.co.birumerah.bsi.bonus.plana.BonusASet;
import id.co.birumerah.bsi.bonus.plana.KursAm;
import id.co.birumerah.bsi.bonus.plana.KursAmFcd;
import id.co.birumerah.bsi.bonus.plana.KursAmSet;
import id.co.birumerah.bsi.member.Member;
import id.co.birumerah.bsi.member.MemberFcd;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.bsi.member.MemberTreeSmall;
import id.co.birumerah.bsi.member.MemberTreeSmallBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeSmallHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSmallSet;
import id.co.birumerah.util.NumberUtil;
import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBCounter {

//	private Tree<MemberTree> mTree;
//	private MemberTreeSet memTreeSet;
	private MutableDateTime beginOfDay;
	private MutableDateTime bulanHitung;
	
	// setting-setting untuk Bonus Repeat Order
	private float minPvpribadi;
	private int pctRo1;
	private int pctRo2;
	private int pctRo3;
	private int pctRo4;
	private int pctRo5;
	private float grade1Pv;
	private float grade2Pv;
	private float grade3Pv;
	private float grade4Pv;
	private float grade5Pv;
	private int roPvPerbagian;
//	private static final int PCT_RO_1 = 3;
//	private static final int PCT_RO_2 = 3;
//	private static final int PCT_RO_3 = 2;
//	private static final int PCT_RO_4 = 1;
//	private static final int PCT_RO_5 = 1;
//	private static final float GRADE1_PV = 50;
//	private static final float GRADE2_PV = 200;
//	private static final float GRADE3_PV = 500;
//	private static final float GRADE4_PV = 1000;
//	private static final float GRADE5_PV = 2000;
//	private static final int PV_PER_BAGIAN_RO = 25;
	
	// setting-setting untuk Bonus Infinity Group
	private float minPvpribadiBig;
	private int bigMaxBagSedang;
    private int bigMaxBagKecil;
    private int bigSmPvgk;
	private int bigGmPvgk;
	private int bigRmPvgk;
	private int bigEmPvgk;
	private int bigPmPvgk;
	private int bigDmPvgk;
	private int bigCdPvgk;
	private int bigScdPvgk;
	private float bigPctBalance;
	private float bigSharePct1;
	private int bigMaxBag1;
	private float bigSharePct2;
	private int bigMaxBag2;
	private float bigSharePct3;
	private int bigMaxBag3;
	private float bigSharePct4;
	private int bigMaxBag4;
	private int bigMaxBag5;
	private int bigMaxgkBag1;
	private float bigGkPct1;
	private int bigMaxgsGk1;
	private float bigGsPct1;
	private int bigMaxgbGk1;
	private float bigGbPct1;
	private int bigPerbagian1;
	private float minPvpribadiBig1;
	private int bigMaxgkBag2;
	private float bigGkPct2;
	private int bigMaxgsGk2;
	private float bigGsPct2;
	private int bigMaxgbGk2;
	private float bigGbPct2;
	private int bigPerbagian2;
	private float minPvpribadiBig2;
	private int bigMaxgkBag3;
	private float bigGkPct3;
	private int bigMaxgsGk3;
	private float bigGsPct3;
	private int bigMaxgbGk3;
	private float bigGbPct3;
	private int bigPerbagian3;
	private float minPvpribadiBig3;
	private int bigMaxgkBag4;
	private float bigGkPct4;
	private int bigMaxgsGk4;
	private float bigGsPct4;
	private int bigMaxgbGk4;
	private float bigGbPct4;
	private int bigPerbagian4;
	private float minPvpribadiBig4;
	private int bigMaxgkBag5;
	private float bigGkPct5;
	private int bigMaxgsGk5;
	private float bigGsPct5;
	private int bigMaxgbGk5;
	private float bigGbPct5;
	private int bigPerbagian5;
	private float minPvpribadiBig5;
	private int bigMaxgkBag6;
	private float bigGkPct6;
	private int bigMaxgsGk6;
	private float bigGsPct6;
	private int bigMaxgbGk6;
	private float bigGbPct6;
	private int bigPerbagian6;
	private float minPvpribadiBig6;
	private int bigMaxgkBag7;
	private float bigGkPct7;
	private int bigMaxgsGk7;
	private float bigGsPct7;
	private int bigMaxgbGk7;
	private float bigGbPct7;
	private int bigPerbagian7;
	private float minPvpribadiBig7;
	private int bigMaxgkBag8;
	private float bigGkPct8;
	private int bigMaxgsGk8;
	private float bigGsPct8;
	private int bigMaxgbGk8;
	private float bigGbPct8;
	private int bigPerbagian8;
	private float minPvpribadiBig8;
	private float bigGkPct9;
	private float bigGsPct9;
	private float bigGbPct9;
	private int bigPerbagian9;
	private float minPvpribadiBig9;
	private int bigPvPerbagian;
	private float minPvpribadiBul;
	private int bulRmKualifikasi;
	private int bulEmKualifikasi;
	private int bulPmKualifikasi;
	private int bulDmKualifikasi;
	private int bulCdKualifikasi;
	private int bulScdKualifikasi;
	private int bulRmKualifikasi2Jalur;
	private int bulEmKualifikasi2Jalur;
	private int bulPmKualifikasi2Jalur;
	private int bulDmKualifikasi2Jalur;
	private int bulCdKualifikasi2Jalur;
	private int bulScdKualifikasi2Jalur;
	private float bulPct1;
	private float bulPct2;
	private float bulPct3;
	private float bulPct4;
	private float bulPct5;
	private float bulPct6;
	private float minPvpribadiBul1;
	private float minPvpribadiBul2;
	private float minPvpribadiBul3;
	private float minPvpribadiBul4;
	private float minPvpribadiBul5;
	private float minPvpribadiBul6;
	
	private float minPvpribadiBeb;
	private int bebMingk1;
	private int bebMaxgk1;
	private int bebPct1;
	private int bebTgl1;
	private int bebMingk2;
	private int bebMaxgk2;
	private int bebPct2;
	private int bebTgl2;
	private int bebPvPerbagian;
	
	private float minPvpribadiBor;
	private int borWisataValue;
	private int borWisataPct;
	private int borWisataAkumgk;
	private int borWisataTambahangk;
	private int borMotorValue;
	private int borMotorPct;
	private int borMotorAkumgk;
	private int borMotorTambahangk;
	private int borReligiValue;
	private int borReligiPct;
	private int borReligiAkumgk;
	private int borReligiTambahangk;
	private int borMobilValue;
	private int borMobilPct;
	private int borMobilAkumgk;
	private int borMobilTambahangk;
	private int borMobilMewahValue;
	private int borMobilMewahPct;
	private int borMobilMewahAkumgk;
	private int borMobilMewahTambahangk;
	private int borRumahMewahValue;
	private int borRumahMewahPct;
	private int borRumahMewahAkumgk;
	private int borRumahMewahTambahangk;
	private float borAntriWisataPct;
	private float borAntriMotorPct;
	private float borAntriReligiPct;
	private float borAntriMobilPct;
	
	private float minPvpribadiBrc;
	private int brcCdKualifikasi;
	private float brcCdPct;
	private int brcScdKualifikasi;
	private float brcScdPct;
	private int brcCdKualifikasi2Jalur;
	private int brcScdKualifikasi2Jalur;
	
	private float minPvpribadiBwt;
	private int bwtRmMingk;
	private int bwtRmBagian;
	private int bwtEmMingk;
	private int bwtEmBagian;
	private int bwtDmMingk;
	private int bwtDmBagian;
	private int bwtCdMingk;
	private int bwtCdBagian;
	private int bwtScdMingk;
	private int bwtScdBagian;
	private int bwtPvPerbagian;
	private float bwtBudgetPct;
	
	private float ldpS1BudgetPct;
	private double ldpS1AkumBudget;
	private int ldpS1BagianPertama;
	private int ldpS1BagianNext;
	private float ldpS2BudgetPct;
	private double ldpS2AkumBudget;
	private int ldpS2BagianPertama;
	private int ldpS2BagianNext;
	private float ldpS3BudgetPct;
	private double ldpS3AkumBudget;
	private int ldpS3BagianPertama;
	private int ldpS3BagianNext;
	private float ldpS4BudgetPct;
	private double ldpS4AkumBudget;
	private int ldpS4BagianPertama;
	private int ldpS4BagianNext;
	
	private double bvTotal;
	private static final int BRC_KUALIFIKASI_BERURUT = 6;
	private static final int LDP_KUALIFIKASI_BERURUT = 3;
	private static final float MINPV_SDM = 10000;
	private static final float MINPV_SEM = 3000;
	private static final float MINPV_SRM = 1000;
	private static final double MAX_TOTAL_BONUS_PBR = 500000;
	
	/**
	 * Constructor untuk kondisi production (real time)
	 */
	public BonusBCounter() {
		super();
		
		beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		bulanHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		bulanHitung.addMonths(-1);
//		getAllMember();
//		loadSettingBonusB();
//		hitungBvPerusahaan();
	}
	
	public BonusBCounter(DBConnection dbConn) {
		super();
		
		beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		beginOfDay.setDayOfMonth(1);
		bulanHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		bulanHitung.addMonths(-1);
//		getAllMember(dbConn);
		loadSettingBonusB(dbConn);
		hitungBvPerusahaan(dbConn);
		prepareMemberPv(dbConn);
	}
	
	/**
	 * Constructor untuk kondisi testing dengan bulan tertentu
	 * 
	 * @param beginOfDay tanggal 1 pada bulan tes
	 */
	public BonusBCounter(MutableDateTime beginOfDay, DBConnection dbConn) {
		this.beginOfDay = beginOfDay;
		bulanHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		bulanHitung.addMonths(-1);
		loadSettingBonusB(dbConn);
		hitungBvPerusahaan(dbConn);
		prepareMemberPv(dbConn);
	}
	
//	private void getAllMember() {
//		try {
////			memTreeSet = MemberTreeFcd.showAllMemberTreeBeforeToday();
//			memTreeSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
//	private void getAllMember(DBConnection dbConn) {
//		try {
//			memTreeSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday(beginOfDay, dbConn);
//			//production pake di bawah ini
////			memTreeSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday(dbConn);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
//	private void loadSettingBonusB() {
	private void loadSettingBonusB(DBConnection dbConn) {
		String whereClause = "status = 'Berlaku' AND start_date < '" 
			+ beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC";
		SettingBonusBFcd sbFcd = new SettingBonusBFcd();
		SettingBonusBSet sbSet = null;
		try {
//			sbSet = sbFcd.search(whereClause);
			sbSet = sbFcd.search(whereClause, dbConn);
			if (sbSet.length() < 1) {
				System.out.println("Belum ada setting bonus yang berlaku.");
				return;
			}
			SettingBonusB settings = sbSet.get(0);
			minPvpribadi = settings.getMinPvpribadi();
			pctRo1 = settings.getPctRo1();
			pctRo2 = settings.getPctRo2();
			pctRo3 = settings.getPctRo3();
			pctRo4 = settings.getPctRo4();
			pctRo5 = settings.getPctRo5();
			grade1Pv = settings.getGrade1Ro();
			grade2Pv = settings.getGrade2Ro();
			grade3Pv = settings.getGrade3Ro();
			grade4Pv = settings.getGrade4Ro();
			grade5Pv = settings.getGrade5Ro();
			roPvPerbagian = settings.getRoPvPerbagian();
			
			minPvpribadiBig = settings.getMinPvpribadiBig();
			bigMaxBagSedang = settings.getBigMaxBagSedang();
			bigMaxBagKecil = settings.getBigMaxBagKecil();
			bigSmPvgk = settings.getBigSmPvgk();
			bigGmPvgk = settings.getBigGmPvgk();
			bigRmPvgk = settings.getBigRmPvgk();
			bigEmPvgk = settings.getBigEmPvgk();
			bigPmPvgk = settings.getBigPmPvgk();
			bigDmPvgk = settings.getBigDmPvgk();
			bigCdPvgk = settings.getBigCdPvgk();
			bigScdPvgk = settings.getBigScdPvgk();
			bigPctBalance = settings.getBigPctBalance();
			bigSharePct1 = settings.getBigSharePct1();
			bigMaxBag1 = settings.getBigMaxBag1();
			bigSharePct2 = settings.getBigSharePct2();
			bigMaxBag2 = settings.getBigMaxBag2();
			bigSharePct3 = settings.getBigSharePct3();
			bigMaxBag3 = settings.getBigMaxBag3();
			bigSharePct4 = settings.getBigSharePct4();
			bigMaxBag4 = settings.getBigMaxgsGk4();
			bigMaxBag5 = settings.getBigMaxBag5();
			bigMaxgkBag1 = settings.getBigMaxgkBag1();
			bigGkPct1 = settings.getBigGkPct1();
			bigMaxgsGk1 = settings.getBigMaxgsGk1();
			bigGsPct1 = settings.getBigGsPct1();
			bigMaxgbGk1 = settings.getBigMaxgbGk1();
			bigGbPct1 = settings.getBigGbPct1();
			bigPerbagian1 = settings.getBigPerbagian1();
			minPvpribadiBig1 = settings.getMinPvpribadiBig1();
			bigMaxgkBag2 = settings.getBigMaxgkBag2();
			bigGkPct2 = settings.getBigGkPct2();
			bigMaxgsGk2 = settings.getBigMaxgsGk2();
			bigGsPct2 = settings.getBigGsPct2();
			bigMaxgbGk2 = settings.getBigMaxgbGk2();
			bigGbPct2 = settings.getBigGbPct2();
			bigPerbagian2 = settings.getBigPerbagian2();
			minPvpribadiBig2 = settings.getMinPvpribadiBig2();
			bigMaxgkBag3 = settings.getBigMaxgkBag3();
			bigGkPct3 = settings.getBigGkPct3();
			bigMaxgsGk3 = settings.getBigMaxgsGk3();
			bigGsPct3 = settings.getBigGsPct3();
			bigMaxgbGk3 = settings.getBigMaxgbGk3();
			bigGbPct3 = settings.getBigGbPct3();
			bigPerbagian3 = settings.getBigPerbagian3();
			minPvpribadiBig3 = settings.getMinPvpribadiBig3();
			bigMaxgkBag4 = settings.getBigMaxgkBag4();
			bigGkPct4 = settings.getBigGkPct4();
			bigMaxgsGk4 = settings.getBigMaxgsGk4();
			bigGsPct4 = settings.getBigGsPct4();
			bigMaxgbGk4 = settings.getBigMaxgbGk4();
			bigGbPct4 = settings.getBigGbPct4();
			bigPerbagian4 = settings.getBigPerbagian4();
			minPvpribadiBig4 = settings.getMinPvpribadiBig4();
			bigMaxgkBag5 = settings.getBigMaxgkBag5();
			bigGkPct5 = settings.getBigGkPct5();
			bigMaxgsGk5 = settings.getBigMaxgsGk5();
			bigGsPct5 = settings.getBigGsPct5();
			bigMaxgbGk5 = settings.getBigMaxgbGk5();
			bigGbPct5 = settings.getBigGbPct5();
			bigPerbagian5 = settings.getBigPerbagian5();
			minPvpribadiBig5 = settings.getMinPvpribadiBig5();
			bigMaxgkBag6 = settings.getBigMaxgkBag6();
			bigGkPct6 = settings.getBigGkPct6();
			bigMaxgsGk6 = settings.getBigMaxgsGk6();
			bigGsPct6 = settings.getBigGsPct6();
			bigMaxgbGk6 = settings.getBigMaxgbGk6();
			bigGbPct6 = settings.getBigGbPct6();
			bigPerbagian6 = settings.getBigPerbagian6();
			minPvpribadiBig6 = settings.getMinPvpribadiBig6();
			bigMaxgkBag7 = settings.getBigMaxgkBag7();
			bigGkPct7 = settings.getBigGkPct7();
			bigMaxgsGk7 = settings.getBigMaxgsGk7();
			bigGsPct7 = settings.getBigGsPct7();
			bigMaxgbGk7 = settings.getBigMaxgbGk7();
			bigGbPct7 = settings.getBigGbPct7();
			bigPerbagian7 = settings.getBigPerbagian7();
			minPvpribadiBig7 = settings.getMinPvpribadiBig7();
			bigMaxgkBag8 = settings.getBigMaxgkBag8();
			bigGkPct8 = settings.getBigGkPct8();
			bigMaxgsGk8 = settings.getBigMaxgsGk8();
			bigGsPct8 = settings.getBigGsPct8();
			bigMaxgbGk8 = settings.getBigMaxgbGk8();
			bigGbPct8 = settings.getBigGbPct8();
			bigPerbagian8 = settings.getBigPerbagian8();
			minPvpribadiBig8 = settings.getMinPvpribadiBig8();
			bigGkPct9 = settings.getBigGkPct9();
			bigGsPct9 = settings.getBigGsPct9();
			bigGbPct9 = settings.getBigGbPct9();
			bigPerbagian9 = settings.getBigPerbagian9();
			minPvpribadiBig9 = settings.getMinPvpribadiBig9();
			bigPvPerbagian = settings.getBigPvPerbagian();
			
			minPvpribadiBul = settings.getMinPvpribadiBul();
			bulRmKualifikasi = settings.getBulRmKualifikasi();
			bulEmKualifikasi = settings.getBulEmKualifikasi();
			bulPmKualifikasi = settings.getBulPmKualifikasi();
			bulDmKualifikasi = settings.getBulDmKualifikasi();
			bulCdKualifikasi = settings.getBulCdKualifikasi();
			bulScdKualifikasi = settings.getBulScdKualifikasi();
			bulRmKualifikasi2Jalur = settings.getBulRmKualifikasi2Jalur();
			bulEmKualifikasi2Jalur = settings.getBulEmKualifikasi2Jalur();
			bulPmKualifikasi2Jalur = settings.getBulPmKualifikasi2Jalur();
			bulDmKualifikasi2Jalur = settings.getBulDmKualifikasi2Jalur();
			bulCdKualifikasi2Jalur = settings.getBulCdKualifikasi2Jalur();
			bulScdKualifikasi2Jalur = settings.getBulScdKualifikasi2Jalur();
			bulPct1 = settings.getBulPct1();
			bulPct2 = settings.getBulPct2();
			bulPct3 = settings.getBulPct3();
			bulPct4 = settings.getBulPct4();
			bulPct5 = settings.getBulPct5();
			bulPct6 = settings.getBulPct6();
			minPvpribadiBul1 = settings.getMinPvpribadiBul1();
			minPvpribadiBul2 = settings.getMinPvpribadiBul2();
			minPvpribadiBul3 = settings.getMinPvpribadiBul3();
			minPvpribadiBul4 = settings.getMinPvpribadiBul4();
			minPvpribadiBul5 = settings.getMinPvpribadiBul5();
			minPvpribadiBul6 = settings.getMinPvpribadiBul6();
			
			minPvpribadiBeb = settings.getMinPvpribadiBeb();
			bebMingk1 = settings.getBebMingk1();
			bebMaxgk1 = settings.getBebMaxgk1();
			bebPct1 = settings.getBebPct1();
			bebTgl1 = settings.getBebTgl1();
			bebMingk2 = settings.getBebMingk2();
			bebMaxgk2 = settings.getBebMaxgk2();
			bebPct2 = settings.getBebPct2();
			bebTgl2 = settings.getBebTgl2();
			bebPvPerbagian = settings.getBebPvPerbagian();
			
			minPvpribadiBor = settings.getMinPvpribadiBor();
			borWisataValue = settings.getBorWisataValue();
			borWisataPct = settings.getBorWisataPct();
			borWisataAkumgk = settings.getBorWisataAkumgk();
			borWisataTambahangk = settings.getBorWisataTambahangk();
			borMotorValue = settings.getBorMotorValue();
			borMotorPct = settings.getBorMotorPct();
			borMotorAkumgk = settings.getBorMotorAkumgk();
			borMotorTambahangk = settings.getBorMotorTambahangk();
			borReligiValue = settings.getBorReligiValue();
			borReligiPct = settings.getBorReligiPct();
			borReligiAkumgk = settings.getBorReligiAkumgk();
			borReligiTambahangk = settings.getBorReligiTambahangk();
			borMobilValue = settings.getBorMobilValue();
			borMobilPct = settings.getBorMobilPct();
			borMobilAkumgk = settings.getBorMobilAkumgk();
			borMobilTambahangk = settings.getBorMobilTambahangk();
			borMobilMewahValue = settings.getBorMobilMewahValue();
			borMobilMewahPct = settings.getBorMobilMewahPct();
			borMobilMewahAkumgk = settings.getBorMobilMewahAkumgk();
			borMobilMewahTambahangk = settings.getBorMobilMewahTambahangk();
			borRumahMewahValue = settings.getBorRumahMewahValue();
			borRumahMewahPct = settings.getBorRumahMewahPct();
			borRumahMewahAkumgk = settings.getBorRumahMewahAkumgk();
			borRumahMewahTambahangk = settings.getBorRumahMewahTambahangk();
			borAntriWisataPct = settings.getBorAntriWisataPct();
			borAntriMotorPct = settings.getBorAntriMotorPct();
			borAntriReligiPct = settings.getBorAntriReligiPct();
			borAntriMobilPct = settings.getBorAntriMobilPct();
			
			minPvpribadiBrc = settings.getMinPvpribadiBrc();
			brcCdKualifikasi = settings.getBrcCdKualifikasi();
			brcCdPct = settings.getBrcCdPct();
			brcScdKualifikasi = settings.getBrcScdKualifikasi();
			brcScdPct = settings.getBrcScdPct();
			brcCdKualifikasi2Jalur = settings.getBrcCdKualifikasi2Jalur();
			brcScdKualifikasi2Jalur = settings.getBrcScdKualifikasi2Jalur();
			
			minPvpribadiBwt = settings.getMinPvpribadiBwt();
			bwtRmMingk = settings.getBwtRmMingk();
			bwtRmBagian = settings.getBwtRmBagian();
			bwtEmMingk = settings.getBwtEmMingk();
			bwtEmBagian = settings.getBwtEmBagian();
			bwtDmMingk = settings.getBwtDmMingk();
			bwtDmBagian = settings.getBwtDmBagian();
			bwtCdMingk = settings.getBwtCdMingk();
			bwtCdBagian = settings.getBwtCdBagian();
			bwtScdMingk = settings.getBwtScdMingk();
			bwtScdBagian = settings.getBwtScdBagian();
			bwtPvPerbagian = settings.getBwtPvPerbagian();
			bwtBudgetPct = settings.getBwtBudgetPct();
			
			ldpS1BudgetPct = settings.getLdpS1BudgetPct();
			ldpS1AkumBudget = settings.getLdpS1AkumBudget();
			ldpS1BagianPertama = settings.getLdpS1BagianPertama();
			ldpS1BagianNext = settings.getLdpS1BagianNext();
			ldpS2BudgetPct = settings.getLdpS2BudgetPct();
			ldpS2AkumBudget = settings.getLdpS2AkumBudget();
			ldpS2BagianPertama = settings.getLdpS2BagianPertama();
			ldpS2BagianNext = settings.getLdpS2BagianNext();
			ldpS3BudgetPct = settings.getLdpS3BudgetPct();
			ldpS3AkumBudget = settings.getLdpS3AkumBudget();
			ldpS3BagianPertama = settings.getLdpS3BagianPertama();
			ldpS3BagianNext = settings.getLdpS3BagianNext();
			ldpS4BudgetPct = settings.getLdpS4BudgetPct();
			ldpS4AkumBudget = settings.getLdpS4AkumBudget();
			ldpS4BagianPertama = settings.getLdpS4BagianPertama();
			ldpS4BagianNext = settings.getLdpS4BagianNext();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void prepareMemberPv(DBConnection dbConn) {
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND NOT (pv_bigfoot=0 AND pv_midfoot=0 AND pv_litfoot=0)";
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTreeSmall member = mtSet.get(i);
				float[] urutPv = sortPvMember(member);
				member.setPvBesar(urutPv[0]);
				member.setPvSedang(urutPv[1]);
				member.setPvKecil(urutPv[2]);
				if (urutPv[0] >= urutPv[1] + urutPv[2]) {
					member.setPvBesar2Jalur(urutPv[0]);
					member.setPvKecil2Jalur(urutPv[1] + urutPv[2]);
				}
				else {
					member.setPvBesar2Jalur(urutPv[1] + urutPv[2]);
					member.setPvKecil2Jalur(urutPv[0]);
				}
				mtFcd.updateMemberTree(member, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private float[] sortPvMember(MemberTreeSmall member) {
//		if ((member.getPvBigfoot()>=member.getPvMidfoot()) && (member.getPvBigfoot()>=member.getPvLitfoot())) {
//			member.setPvBesar(member.getPvBigfoot());
//			if (member.getPvMidfoot()>=member.getPvLitfoot()) {
//				member.setPvSedang(member.getPvMidfoot());
//				member.setPvKecil(member.getPvLitfoot());
//			} else {
//				member.setPvSedang(member.getPvLitfoot());
//				member.setPvKecil(member.getPvMidfoot());
//			}
//		}
		float[] urutPv = new float[3];
		urutPv[0] = member.getPvBigfoot();
		urutPv[1] = member.getPvMidfoot();
		urutPv[2] = member.getPvLitfoot();
		
		int n = 3;
		boolean doMore = true;
		while (doMore) {
			n--;
			doMore = false;
			for (int i=0; i<n; i++) {
				if (urutPv[i]<urutPv[i+1]) {
					float temp = urutPv[i];
					urutPv[i] = urutPv[i+1];
					urutPv[i+1] = temp;
					doMore = true;
				}
			}
		}
		return urutPv;
	}
	
	private void hitungBROPerGrade(int persenRo, float grade) {
//		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND saldo_poin >= " + grade;
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND saldo_poin >= " + grade;
//		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
//			mtSet = mtFcd.search(whereClause);
			int totalBagian = getTotalBagianPerTingkat(mtSet);
			double bagianPerTingkat = bvTotal * persenRo / (100 * totalBagian);
			System.out.println("1 bagian RO = " + bagianPerTingkat);
			for (int i=0; i<mtSet.length(); i++) {
//				MemberTree member = mtSet.get(i);
				MemberTreeSmall member = mtSet.get(i);
				float pvro = member.getSaldoPoin();
				int bagian = (int) (pvro/roPvPerbagian);
				double bonusRO = (double) bagian * bagianPerTingkat;
				bonusRO = NumberUtil.roundToDecimals(bonusRO, 0);
				System.out.println("Member " + member.getMemberId() + " dapat bonus RO tingkat = " + bonusRO);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusRo(bonusRO);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusRo = bonus.getBonusRo();
					curBonusRo += bonusRO;
					bonus.setBonusRo(curBonusRo);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBROPerGrade(int persenRo, float grade, int tingkat, DBConnection dbConn) {
//		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND saldo_poin >= " + grade;
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND pvpribadi >= " + grade +
				" AND is_basic > -1";
		if (persenRo==0 && grade==0) return;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			int totalBagian = getTotalBagianPerTingkat(mtSet);
			System.out.println("Total bagian per tingkat = " + totalBagian);
			double bagianPerTingkat = 0;
			if (totalBagian > 0) {
				bagianPerTingkat = bvTotal * persenRo / (100 * totalBagian);
			}
			System.out.println("1 bagian RO = " + bagianPerTingkat);
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			switch (tingkat) {
			case 1:
				bbLog.setBroT11Bagian(bagianPerTingkat);
				bbLog.setBroT1Totalbagian(totalBagian);
				bbLog.setBroT1Totalbonus(bagianPerTingkat * totalBagian);
				break;
			case 2:
				bbLog.setBroT21Bagian(bagianPerTingkat);
				bbLog.setBroT2Totalbagian(totalBagian);
				bbLog.setBroT2Totalbonus(bagianPerTingkat * totalBagian);
				break;
			case 3:
				bbLog.setBroT31Bagian(bagianPerTingkat);
				bbLog.setBroT3Totalbagian(totalBagian);
				bbLog.setBroT3Totalbonus(bagianPerTingkat * totalBagian);
				break;
			case 4:
				bbLog.setBroT41Bagian(bagianPerTingkat);
				bbLog.setBroT4Totalbagian(totalBagian);
				bbLog.setBroT4Totalbonus(bagianPerTingkat * totalBagian);
				break;
			case 5:
				bbLog.setBroT51Bagian(bagianPerTingkat);
				bbLog.setBroT5Totalbagian(totalBagian);
				bbLog.setBroT5Totalbonus(bagianPerTingkat * totalBagian);
				break;
			default:
				break;
			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bbLog, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTreeSmall member = mtSet.get(i);
//				float pvro = member.getSaldoPoin();
				float pvro = member.getPvpribadi();
				int bagian = (int)(pvro/roPvPerbagian);
				double bonusRO = (double) bagian * bagianPerTingkat;
				bonusRO = NumberUtil.roundToDecimals(bonusRO, 0);
				System.out.println("Member " + member.getMemberId() + " dapat bonus RO tingkat = " + bonusRO);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusRo(bonusRO);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus(dbConn);
					bonusFcd = null;
				} else {
					double curBonusRo = bonus.getBonusRo();
					curBonusRo += bonusRO;
					bonus.setBonusRo(curBonusRo);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus, dbConn);
					bonusFcd = null;
				}
				bonus = null;
			}
			bblFcd = null;
			bbLog = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusRO() {
		int[] persenRo = new int[] {pctRo1, pctRo2, pctRo3, pctRo4, pctRo5};
		float[] gradePv = new float[] {grade1Pv, grade2Pv, grade3Pv, grade4Pv, grade5Pv};
//		hitungBvPerusahaan();
		for (int i=0; i<gradePv.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBROPerGrade(persenRo[i], gradePv[i]);
		}
	}
	
	public void giveBonusRO(DBConnection dbConn) {
		int[] persenRo = new int[] {pctRo1, pctRo2, pctRo3, pctRo4, pctRo5};
		float[] gradePv = new float[] {grade1Pv, grade2Pv, grade3Pv, grade4Pv, grade5Pv};
		for (int i=0; i<gradePv.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBROPerGrade(persenRo[i], gradePv[i], (i+1), dbConn);
		}
		persenRo = null;
		gradePv = null;
	}
	
	private void hitungBvPerusahaan(DBConnection dbConn) {
		MemberTreeSmallSet memTreeSet = null;
		try {
			memTreeSet = MemberTreeSmallBulananFcd.showAllMemberTreeBeforeToday(beginOfDay, dbConn);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		bvTotal = 0;
		for (int i=0; i<memTreeSet.length(); i++) {
			MemberTreeSmall member = memTreeSet.get(i);
			bvTotal += member.getSaldoBv();
		}
		System.out.println("Total BV = " + bvTotal);
		memTreeSet = null;
		try {
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			if (bbLog == null) {
				bbLog = new BonusBLog();
				bbLog.setBulan(bulanHitung.toDate());
				bbLog.setBudgetAll(bvTotal);
				BonusBLogFcd bblFcd = new BonusBLogFcd(bbLog);
				bblFcd.insertLog(dbConn);
				bblFcd = null;
			} else {
				bbLog.setBulan(bulanHitung.toDate());
				bbLog.setBudgetAll(bvTotal);
				BonusBLogFcd bblFcd = new BonusBLogFcd();
				bblFcd.updateLog(bbLog, dbConn);
				bblFcd = null;
			}
			bbLog = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private int getTotalBagianPerTingkat(MemberTreeSmallSet mtSet) {
		int totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTreeSmall member = mtSet.get(i);
//			float pvro = member.getSaldoPoin();
			float pvro = member.getPvpribadi();
			int bagian = (int)(pvro/roPvPerbagian);
			totalBagian += bagian;
		}
		return totalBagian;
	}
	
	public void giveBonusIG() {
		float[] persenBigGb = new float[] {bigGbPct1, bigGbPct2, bigGbPct3, bigGbPct4, bigGbPct5, bigGbPct6, bigGbPct7, bigGbPct8, bigGbPct9};
		float[] persenBigGs = new float[] {bigGsPct1, bigGsPct2, bigGsPct3, bigGsPct4, bigGsPct5, bigGsPct6, bigGsPct7, bigGsPct8, bigGsPct9};
		float[] persenBigGk = new float[] {bigGkPct1, bigGkPct2, bigGkPct3, bigGkPct4, bigGkPct5, bigGkPct6, bigGkPct7, bigGkPct8, bigGkPct9};
		int[] maxBagianGb = new int[] {bigMaxgbGk1, bigMaxgbGk2, bigMaxgbGk3, bigMaxgbGk4, bigMaxgbGk5, bigMaxgbGk6, bigMaxgbGk7, bigMaxgbGk8, 0};
		int[] maxBagianGs = new int[] {bigMaxgsGk1, bigMaxgsGk2, bigMaxgsGk3, bigMaxgsGk4, bigMaxgsGk5, bigMaxgsGk6, bigMaxgsGk7, bigMaxgsGk8, 0};
		int[] maxBagianGk = new int[] {bigMaxgkBag1, bigMaxgkBag2, bigMaxgkBag3, bigMaxgkBag4, bigMaxgkBag5, bigMaxgkBag6, bigMaxgkBag7, bigMaxgkBag8, 0};
		int[] batasPvgk = new int[] {bigRmPvgk, bigEmPvgk, bigPmPvgk, bigDmPvgk, bigCdPvgk, bigScdPvgk, 0, 0, 0};
		
		for (int i=0; i<persenBigGb.length; i++) {
			float[] persenBig = new float[] {persenBigGb[i], persenBigGs[i], persenBigGk[i]};
			int[] maxBagian = new int[] {maxBagianGb[i], maxBagianGs[i], maxBagianGk[i]};
			System.out.println("=== Jenis " + (i+1) + " ===");
			hitungBIGPerJenis(persenBig, batasPvgk[i], maxBagian);
		}
	}
	
	public void giveBonusIG(DBConnection dbConn) {
		float[] persenBigGb = new float[] {bigGbPct1, bigGbPct2, bigGbPct3, bigGbPct4, bigGbPct5, bigGbPct6, bigGbPct7, bigGbPct8, bigGbPct9};
		float[] persenBigGs = new float[] {bigGsPct1, bigGsPct2, bigGsPct3, bigGsPct4, bigGsPct5, bigGsPct6, bigGsPct7, bigGsPct8, bigGsPct9};
		float[] persenBigGk = new float[] {bigGkPct1, bigGkPct2, bigGkPct3, bigGkPct4, bigGkPct5, bigGkPct6, bigGkPct7, bigGkPct8, bigGkPct9};
//		int[] maxBagianGb = new int[] {bigMaxgbGk1, bigMaxgbGk2, bigMaxgbGk3, bigMaxgbGk4, bigMaxgbGk5, bigMaxgbGk6, bigMaxgbGk7, bigMaxgbGk8, 0};
//		int[] maxBagianGs = new int[] {bigMaxgsGk1, bigMaxgsGk2, bigMaxgsGk3, bigMaxgsGk4, bigMaxgsGk5, bigMaxgsGk6, bigMaxgsGk7, bigMaxgsGk8, 0};
//		int[] maxBagianGk = new int[] {bigMaxgkBag1, bigMaxgkBag2, bigMaxgkBag3, bigMaxgkBag4, bigMaxgkBag5, bigMaxgkBag6, bigMaxgkBag7, bigMaxgkBag8, 0};
//		int[] batasPvgk = new int[] {bigRmPvgk, bigEmPvgk, bigPmPvgk, bigDmPvgk, bigCdPvgk, bigScdPvgk, 0, 0, 0};
		int[] pvPerbagianBig = new int[] {bigPerbagian1, bigPerbagian2, bigPerbagian3, bigPerbagian4, bigPerbagian5, bigPerbagian6, bigPerbagian7, bigPerbagian8, bigPerbagian9};
		float[] minPvpribadi = new float[] {minPvpribadiBig1, minPvpribadiBig2, minPvpribadiBig3, minPvpribadiBig4, minPvpribadiBig5, minPvpribadiBig6, minPvpribadiBig7, minPvpribadiBig8, minPvpribadiBig9};
		
		for (int i=0; i<persenBigGb.length; i++) {
			float[] persenBig = new float[] {persenBigGb[i], persenBigGs[i], persenBigGk[i]};
			//int[] maxBagian = new int[] {maxBagianGb[i], maxBagianGs[i], maxBagianGk[i]};
			System.out.println("=== Jenis " + (i+1) + " ===");
//			hitungBIGPerJenis(persenBig, pvPerbagianBig[i], (i+1), dbConn);
			hitungBIGPerJenis(persenBig, pvPerbagianBig[i], (i+1), minPvpribadi[i], dbConn);
		}
		persenBigGb = null;
		persenBigGs = null;
		persenBigGk = null;
//		maxBagianGb = null;
//		maxBagianGs = null;
//		maxBagianGk = null;
//		batasPvgk = null;
		pvPerbagianBig = null;
	}
	
	private void hitungBIGPerJenis(float[] persenBig, int batasPvgk, int[] maxBagian) {
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'" 
			+ " AND pvpribadi >= " + minPvpribadi
			+ " AND pv_litfoot >= " + bigSmPvgk;
		if (batasPvgk > 0) {
			whereClause += " AND pv_litfoot < " + batasPvgk;
		}
		System.out.println("whereClause = " + whereClause);
//		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
//			mtSet = mtFcd.search(whereClause);
//			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, maxBagian);
			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, maxBagian,0);
			double bagianGbPerJenis = bvTotal * persenBig[0] / (100 * totalBagian[0]);
			double bagianGsPerJenis = bvTotal * persenBig[1] / (100 * totalBagian[1]);
			double bagianGkPerJenis = bvTotal * persenBig[2] / (100 * totalBagian[2]);
			System.out.println("Persen GK="+persenBig[2]+"; TotalBagian GK="+totalBagian[2]);
			System.out.println("1B Besar="+bagianGbPerJenis+"; Sedang="+bagianGsPerJenis+"; Kecil="+bagianGkPerJenis);
			for (int i=0; i<mtSet.length(); i++) {
//				MemberTree member = mtSet.get(i);
				MemberTreeSmall member = mtSet.get(i);
				float pvGb = member.getPvBigfoot();
				float pvGs = member.getPvMidfoot();
				float pvGk = member.getPvLitfoot();
				double bagianGb = pvGb / bigPvPerbagian;
				double bagianGs = pvGs / bigPvPerbagian;
				double bagianGk = pvGk / bigPvPerbagian;
//				if ((bagianGb > (pvGk * maxBagian[0])) && (maxBagian[0] > 0)) {
//					bagianGb = pvGk * maxBagian[0];
//				}
//				if ((bagianGs > (pvGk * maxBagian[1])) && (maxBagian[1]) > 0) {
//					bagianGs = pvGk * maxBagian[1];
//				}
				if ((bagianGb > (maxBagian[2] * maxBagian[0])) && (maxBagian[0] > 0)) {
					bagianGb = maxBagian[2] * maxBagian[0];
				}
				if ((bagianGs > (maxBagian[2] * maxBagian[1])) && (maxBagian[1]) > 0) {
					bagianGs = maxBagian[2] * maxBagian[1];
				}
				if ((bagianGk > maxBagian[2]) && (maxBagian[2]) > 0) {
					bagianGk = maxBagian[2];
				}
				double bonusBigGb = NumberUtil.roundToDecimals((bagianGb * bagianGbPerJenis), 0);
				double bonusBigGs = NumberUtil.roundToDecimals((bagianGs * bagianGsPerJenis), 0);
				double bonusBigGk = NumberUtil.roundToDecimals((bagianGk * bagianGkPerJenis), 0);
				double bonusBig = bonusBigGb + bonusBigGs + bonusBigGk;
				System.out.println("Member "+member.getMemberId()+" dapat bonus GI tingkat "+bonusBig);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusSi(bonusBig);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusBig = bonus.getBonusSi();
					curBonusBig += bonusBig;
					bonus.setBonusSi(curBonusBig);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	private void hitungBIGPerJenis(float[] persenBig, int batasPvgk, int[] maxBagian, int jenisBig, DBConnection dbConn) {
	private void hitungBIGPerJenis(float[] persenBig, int pvPerbagian, int[] maxBagian, int jenisBig, DBConnection dbConn) {
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'" 
			+ " AND pvpribadi >= " + minPvpribadiBig
//			+ " AND pv_kecil >= " + pvPerbagian;
			+ " AND pv_kecil >= " + minPvpribadiBig;
//		if (batasPvgk > 0) {
////			whereClause += " AND pv_litfoot < " + batasPvgk;
//			whereClause += " AND pv_kecil < " + batasPvgk;
//		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
//			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, maxBagian);
			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, maxBagian, pvPerbagian);
			double bagianGbPerJenis = 0;
			if (totalBagian[0] > 0) {
				bagianGbPerJenis = bvTotal * persenBig[0] / (100 * totalBagian[0]);
			}
			double bagianGsPerJenis = 0;
			if (totalBagian[1] > 0) {
				bagianGsPerJenis = bvTotal * persenBig[1] / (100 * totalBagian[1]);
			}
			double bagianGkPerJenis = 0;
			if (totalBagian[2] > 0) {
				bagianGkPerJenis = bvTotal * persenBig[2] / (100 * totalBagian[2]);
			}
			System.out.println("Persen GK="+persenBig[2]+"; TotalBagian GK="+totalBagian[2]);
			System.out.println("1B Besar="+bagianGbPerJenis+"; Sedang="+bagianGsPerJenis+"; Kecil="+bagianGkPerJenis);
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			switch (jenisBig) {
			case 1:
				bbLog.setBigJ1Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ1GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ1GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ1Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ1GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ1GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ1Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ1GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ1GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 2:
				bbLog.setBigJ2Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ2GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ2GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ2Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ2GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ2GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ2Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ2GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ2GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 3:
				bbLog.setBigJ3Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ3GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ3GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ3Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ3GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ3GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ3Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ3GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ3GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 4:
				bbLog.setBigJ4Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ4GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ4GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ4Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ4GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ4GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ4Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ4GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ4GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 5:
				bbLog.setBigJ5Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ5GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ5GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ5Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ5GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ5GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ5Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ5GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ5GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 6:
				bbLog.setBigJ6Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ6GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ6GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ6Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ6GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ6GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ6Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ6GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ6GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 7:
				bbLog.setBigJ7Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ7GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ7GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ7Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ7GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ7GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ7Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ7GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ7GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 8:
				bbLog.setBigJ8Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ8GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ8GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ8Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ8GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ8GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ8Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ8GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ8GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 9:
				bbLog.setBigJ9Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ9GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ9GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ9Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ9GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ9GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ9Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ9GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ9GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			default:
				break;
			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bbLog, dbConn);
			bblFcd = null;
			bbLog = null;
			for (int i=0; i<mtSet.length(); i++) {
//				MemberTree member = mtSet.get(i);
				MemberTreeSmall member = mtSet.get(i);
//				float pvGb = member.getPvBigfoot();
				float pvGb = member.getPvBesar();
//				float pvGs = member.getPvMidfoot();
				float pvGs = member.getPvSedang();
//				float pvGk = member.getPvLitfoot();
				float pvGk = member.getPvKecil();
//				double bagianGb = pvGb / bigPvPerbagian;
//				double bagianGs = pvGs / bigPvPerbagian;
//				double bagianGk = pvGk / bigPvPerbagian;
				double bagianGb = NumberUtil.roundToDecimals((pvGb / pvPerbagian), 0);
				double bagianGs = NumberUtil.roundToDecimals((pvGs / pvPerbagian), 0);
				double bagianGk = NumberUtil.roundToDecimals((pvGk / pvPerbagian), 0);
				if ((bagianGk > maxBagian[2]) && (maxBagian[2]) > 0) {
					bagianGk = maxBagian[2];
				}
//				if ((bagianGs > (maxBagian[2] * maxBagian[1])) && (maxBagian[1]) > 0) {
//				if ((bagianGs > (bagianGk * maxBagian[1])) && (maxBagian[1]) > 0) {
				if ((pvGs > (pvGk * maxBagian[1])) && (maxBagian[1]) > 0) {
//					bagianGs = maxBagian[2] * maxBagian[1];
					bagianGs = NumberUtil.roundToDecimals((pvGk * maxBagian[1] / pvPerbagian), 0);
				}
//				if ((bagianGb > (maxBagian[2] * maxBagian[0])) && (maxBagian[0] > 0)) {
//				if ((bagianGb > (bagianGk * maxBagian[0])) && (maxBagian[0] > 0)) {
				if ((pvGb > (pvGk * maxBagian[0])) && (maxBagian[0] > 0)) {
//					bagianGb = bagianGk * maxBagian[0];
					bagianGb = NumberUtil.roundToDecimals((pvGk * maxBagian[0] / pvPerbagian), 0);
				}
				double bonusBigGb = NumberUtil.roundToDecimals((bagianGb * bagianGbPerJenis), 0);
				double bonusBigGs = NumberUtil.roundToDecimals((bagianGs * bagianGsPerJenis), 0);
				double bonusBigGk = NumberUtil.roundToDecimals((bagianGk * bagianGkPerJenis), 0);
				double bonusBig = bonusBigGb + bonusBigGs + bonusBigGk;
				System.out.println("Member "+member.getMemberId()+" BB="+bagianGb+"; BS="+bagianGs+"; BK="+bagianGk);
				System.out.println("Member "+member.getMemberId()+" dapat bonus GI tingkat "+bonusBig+"--> Bonus BB="+bonusBigGb+"; Bonus BS="+bonusBigGs+"; Bonus BK="+bonusBigGk);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
				if (bonusBig > 0) {
					BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
					if (bonus == null) {
						bonus = new BonusB();
						bonus.setMemberId(member.getMemberId());
						bonus.setBulan(bulanHitung.toDate());
						bonus.setBonusSi(bonusBig);
						BonusBFcd bonusFcd = new BonusBFcd(bonus);
						bonusFcd.insertBonus(dbConn);
						bonusFcd = null;
					} else {
						double curBonusBig = bonus.getBonusSi();
						curBonusBig += bonusBig;
						bonus.setBonusSi(curBonusBig);
						BonusBFcd bonusFcd = new BonusBFcd();
						bonusFcd.updateBonus(bonus, dbConn);
						bonusFcd = null;
					}
					bonus = null;
					member = null;
				}
			}
			mtFcd = null;
			mtSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	private void hitungBIGPerJenis(float[] persenBig, int pvPerbagian, int jenisBig, DBConnection dbConn) {
	private void hitungBIGPerJenis(float[] persenBig, int pvPerbagian, int jenisBig, float minPvpribadi, DBConnection dbConn) {
//		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'" 
//				+ " AND pvpribadi >= " + minPvpribadi;
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'" 
				+ " AND pvpribadi >= " + minPvpribadi
				+ " AND (pv_besar / (pv_besar+pv_sedang+pv_kecil)) <= (" + bigPctBalance
				+ "/100)";
		if (jenisBig > 3) {
			whereClause += " AND is_basic = 0 AND is_basic_new = 0";
		} else {
			whereClause += " AND is_basic > -1 AND is_basic_new > -1";
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, pvPerbagian, persenBig[0]);
			double bagianGbPerJenis = 0;
			if (totalBagian[0] > 0) {
				bagianGbPerJenis = bvTotal * persenBig[0] / (100 * totalBagian[0]);
			}
			double bagianGsPerJenis = 0;
			if (totalBagian[1] > 0) {
				bagianGsPerJenis = bvTotal * persenBig[1] / (100 * totalBagian[1]);
			}
			double bagianGkPerJenis = 0;
			if (totalBagian[2] > 0) {
				bagianGkPerJenis = bvTotal * persenBig[2] / (100 * totalBagian[2]);
			}
			System.out.println("Persen GK="+persenBig[2]+"; TotalBagian GK="+totalBagian[2]);
			System.out.println("1B Besar="+bagianGbPerJenis+"; Max 5 Lemah="+bagianGsPerJenis+"; Inf Lemah="+bagianGkPerJenis);
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			switch (jenisBig) {
			case 1:
				bbLog.setBigJ1Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ1GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ1GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ1Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ1GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ1GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ1Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ1GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ1GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 2:
				bbLog.setBigJ2Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ2GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ2GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ2Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ2GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ2GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ2Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ2GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ2GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 3:
				bbLog.setBigJ3Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ3GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ3GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ3Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ3GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ3GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ3Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ3GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ3GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 4:
				bbLog.setBigJ4Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ4GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ4GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ4Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ4GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ4GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ4Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ4GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ4GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 5:
				bbLog.setBigJ5Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ5GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ5GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ5Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ5GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ5GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ5Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ5GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ5GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 6:
				bbLog.setBigJ6Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ6GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ6GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ6Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ6GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ6GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ6Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ6GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ6GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 7:
				bbLog.setBigJ7Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ7GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ7GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ7Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ7GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ7GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ7Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ7GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ7GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 8:
				bbLog.setBigJ8Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ8GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ8GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ8Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ8GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ8GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ8Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ8GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ8GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			case 9:
				bbLog.setBigJ9Gb1Bag(bagianGbPerJenis);
				bbLog.setBigJ9GbTotalbag((int)totalBagian[0]);
				bbLog.setBigJ9GbTotalbonus(bagianGbPerJenis * totalBagian[0]);
				bbLog.setBigJ9Gs1Bag(bagianGsPerJenis);
				bbLog.setBigJ9GsTotalbag((int)totalBagian[1]);
				bbLog.setBigJ9GsTotalbonus(bagianGsPerJenis * totalBagian[1]);
				bbLog.setBigJ9Gk1Bag(bagianGkPerJenis);
				bbLog.setBigJ9GkTotalbag((int)totalBagian[2]);
				bbLog.setBigJ9GkTotalbonus(bagianGkPerJenis * totalBagian[2]);
				break;
			default:
				break;
			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bbLog, dbConn);
			bblFcd = null;
			bbLog = null;
			for (int i=0; i<mtSet.length(); i++) {
				MemberTreeSmall member = mtSet.get(i);
				float pvGb = member.getPvBesar();
				float pvGs = member.getPvSedang();
				float pvGk = member.getPvKecil();
				double bagianGb = NumberUtil.roundToDecimals((pvGb / pvPerbagian), 0);
				double bagianGs = NumberUtil.roundToDecimals((pvGs / pvPerbagian), 0);
				double bagianGk = NumberUtil.roundToDecimals((pvGk / pvPerbagian), 0);
				
				double maxGs = bagianGs;
				double maxGk = bagianGk;
//				if (persenBig[0] > 0) {
//					float share = pvGb * 100 / (pvGb + pvGs + pvGk);
//					double maxGb = 0;
//					if (share >= bigSharePct1) {
//						maxGb = bigMaxBag1;
//					} else if (share >= bigSharePct2) {
//						maxGb = bigMaxBag2;
//					} else if (share >= bigSharePct3) {
//						maxGb = bigMaxBag3;
//					} else if (share >= bigSharePct4) {
//						maxGb = bigMaxBag4;
//					} else {
//						if (bigMaxBag5 == 0) {
//							maxGb = bagianGb;
//						} else {
//							maxGb = bigMaxBag5;
//						}
//					}
//					if (bagianGb > maxGb) {
//						bagianGb = maxGb;
//					}
//				} else {
//					bagianGb = 0;
//				}
				if (persenBig[0] <= 0) {
					bagianGb = 0;
				}
				if (maxGs > bigMaxBagSedang) maxGs = bigMaxBagSedang;
				if (maxGk > bigMaxBagKecil) maxGk = bigMaxBagKecil;
				
				double bonusBigGb = NumberUtil.roundToDecimals((bagianGb * bagianGbPerJenis), 0);
				double bonusBigGs = NumberUtil.roundToDecimals(((maxGs+maxGk) * bagianGsPerJenis), 0);
				double bonusBigGk = NumberUtil.roundToDecimals(((bagianGk+bagianGs) * bagianGkPerJenis), 0);
				double bonusBig = bonusBigGb + bonusBigGs + bonusBigGk;
				System.out.println("Member "+member.getMemberId()+" BB="+bagianGb+"; Bag Max 5 Lemah="+bagianGs+"; Bag Inf="+bagianGk);
				System.out.println("Member "+member.getMemberId()+" dapat bonus GI tingkat "+bonusBig+"--> Bonus BB="+bonusBigGb+"; Bonus BS="+bonusBigGs+"; Bonus BK="+bonusBigGk);
				if (bonusBig > 0) {
					BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
					if (bonus == null) {
						bonus = new BonusB();
						bonus.setMemberId(member.getMemberId());
						bonus.setBulan(bulanHitung.toDate());
						bonus.setBonusSi(bonusBig);
						BonusBFcd bonusFcd = new BonusBFcd(bonus);
						bonusFcd.insertBonus(dbConn);
						bonusFcd = null;
					} else {
						double curBonusBig = bonus.getBonusSi();
						curBonusBig += bonusBig;
						bonus.setBonusSi(curBonusBig);
						BonusBFcd bonusFcd = new BonusBFcd();
						bonusFcd.updateBonus(bonus, dbConn);
						bonusFcd = null;
					}
					BonusBLogPeserta logPeserta = BonusBLogPesertaFcd.getBonusLogByMemberBulan(member.getMemberId(), bulanHitung.toDate(), dbConn);
					if (logPeserta == null) {
						logPeserta = new BonusBLogPeserta();
						logPeserta.setMemberId(member.getMemberId());
						logPeserta.setBulan(bulanHitung.toDate());
						switch (jenisBig) {
						case 1:
							logPeserta.setBigJ1(bonusBig);
							break;
						case 2:
							logPeserta.setBigJ2(bonusBig);
							break;
						case 3:
							logPeserta.setBigJ3(bonusBig);
							break;
						case 4:
							logPeserta.setBigJ4(bonusBig);
							break;
						case 5:
							logPeserta.setBigJ5(bonusBig);
							break;
						case 6:
							logPeserta.setBigJ6(bonusBig);
							break;
						case 7:
							logPeserta.setBigJ7(bonusBig);
							break;
						case 8:
							logPeserta.setBigJ8(bonusBig);
							break;
						case 9:
							logPeserta.setBigJ9(bonusBig);
							break;
						default:
							break;
						}
						BonusBLogPesertaFcd bblpFcd = new BonusBLogPesertaFcd(logPeserta);
						bblpFcd.insertLog(dbConn);
					} else {
						switch (jenisBig) {
						case 1:
							logPeserta.setBigJ1(bonusBig);
							break;
						case 2:
							logPeserta.setBigJ2(bonusBig);
							break;
						case 3:
							logPeserta.setBigJ3(bonusBig);
							break;
						case 4:
							logPeserta.setBigJ4(bonusBig);
							break;
						case 5:
							logPeserta.setBigJ5(bonusBig);
							break;
						case 6:
							logPeserta.setBigJ6(bonusBig);
							break;
						case 7:
							logPeserta.setBigJ7(bonusBig);
							break;
						case 8:
							logPeserta.setBigJ8(bonusBig);
							break;
						case 9:
							logPeserta.setBigJ9(bonusBig);
							break;
						default:
							break;
						}
						BonusBLogPesertaFcd bblpFcd = new BonusBLogPesertaFcd();
						bblpFcd.updateLog(logPeserta, dbConn);
					}
					bonus = null;
					member = null;
				}
			}
			mtFcd = null;
			mtSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	private double[] getTotalBagianPerJenisBig(MemberTreeSet mtSet, int[] maxBagian) {
//	private double[] getTotalBagianPerJenisBig(MemberTreeSmallSet mtSet, int[] maxBagian) {
	private double[] getTotalBagianPerJenisBig(MemberTreeSmallSet mtSet, int[] maxBagian, int pvPerbagian) {
		double totalBagianGb = 0;
		double totalBagianGs = 0;
		double totalBagianGk = 0;
		for (int i=0; i<mtSet.length(); i++) {
//			MemberTree member = mtSet.get(i);
			MemberTreeSmall member = mtSet.get(i);
//			float pvGb = member.getPvBigfoot();
//			float pvGs = member.getPvMidfoot();
//			float pvGk = member.getPvLitfoot();
			float pvGb = member.getPvBesar();
			float pvGs = member.getPvSedang();
			float pvGk = member.getPvKecil();
//			double bagianGb = pvGb/bigPvPerbagian;
//			double bagianGs = pvGs/bigPvPerbagian;
//			double bagianGk = pvGk/bigPvPerbagian;
			double bagianGb = NumberUtil.roundToDecimals(pvGb/pvPerbagian, 0);
			double bagianGs = NumberUtil.roundToDecimals(pvGs/pvPerbagian, 0);
			double bagianGk = NumberUtil.roundToDecimals(pvGk/pvPerbagian, 0);
			if ((bagianGk > maxBagian[2]) && (maxBagian[2] > 0)) {
				bagianGk = maxBagian[2];
			}
//			if ((bagianGs > (maxBagian[1] * maxBagian[2])) && (maxBagian[1] > 0)) {
//			if ((bagianGs > (maxBagian[1] * bagianGk)) && (maxBagian[1] > 0)) {
			if ((pvGs > (maxBagian[1] * pvGk)) && (maxBagian[1] > 0)) {
//				bagianGs = maxBagian[1] * maxBagian[2];
//				bagianGs = maxBagian[1] * bagianGk;
				bagianGs = NumberUtil.roundToDecimals((maxBagian[1] * pvGk / pvPerbagian), 0);
			}
//			if ((bagianGb > (maxBagian[0] * maxBagian[2])) && (maxBagian[0] > 0)) {
//			if ((bagianGb > (maxBagian[0] * bagianGk)) && (maxBagian[0] > 0)) {
			if ((pvGb > (maxBagian[0] * pvGk)) && (maxBagian[0] > 0)) {
//				bagianGb = maxBagian[0] * maxBagian[2];
//				bagianGb = maxBagian[0] * bagianGk;
				bagianGb = NumberUtil.roundToDecimals((maxBagian[0] * pvGk / pvPerbagian), 0);
			}
			totalBagianGb += bagianGb;
			totalBagianGs += bagianGs;
			totalBagianGk += bagianGk;
		}
		System.out.println("Total bagian GB="+totalBagianGb+"; GS="+totalBagianGs+"; GK="+totalBagianGk);
		double[] total = new double[] {totalBagianGb, totalBagianGs, totalBagianGk};
		return total;
	}
	
//	private double[] getTotalBagianPerJenisBig(MemberTreeSmallSet mtSet, int pvPerbagian) {
//		double totalBagianGb = 0;
//		double totalBagianGs = 0;
//		double totalBagianGk = 0;
//		for (int i=0; i<mtSet.length(); i++) {
//			MemberTreeSmall member = mtSet.get(i);
//			float pvGb = member.getPvBesar();
//			float pvGs = member.getPvSedang();
//			float pvGk = member.getPvKecil();
//			double bagianGb = NumberUtil.roundToDecimals(pvGb/pvPerbagian, 0);
//			double bagianGs = NumberUtil.roundToDecimals(pvGs/pvPerbagian, 0);
//			double bagianGk = NumberUtil.roundToDecimals(pvGk/pvPerbagian, 0);
//			totalBagianGb += bagianGb;
//			totalBagianGs += bagianGs;
//			totalBagianGk += bagianGk;
//		}
//		System.out.println("Total bagian GB="+totalBagianGb+"; GS="+totalBagianGs+"; GK="+totalBagianGk);
//		double[] total = new double[] {totalBagianGb, totalBagianGs, totalBagianGk};
//		return total;
//	}
	
	private double[] getTotalBagianPerJenisBig(MemberTreeSmallSet mtSet, int pvPerbagian, float persenGb) {
		double totalBagianGb = 0;
		double totalBagianGs = 0;
		double totalBagianGk = 0;
		System.out.println("Persen GB : " + persenGb);
		for (int i=0; i<mtSet.length(); i++) {
			MemberTreeSmall member = mtSet.get(i);
			float pvGb = member.getPvBesar();
			float pvGs = member.getPvSedang();
			float pvGk = member.getPvKecil();
			double bagianGb = NumberUtil.roundToDecimals(pvGb/pvPerbagian, 0);
			double bagianGs = NumberUtil.roundToDecimals(pvGs/pvPerbagian, 0);
			double bagianGk = NumberUtil.roundToDecimals(pvGk/pvPerbagian, 0);
			double maxGs = bagianGs;
			double maxGk = bagianGk;
//			if (persenGb > 0) {
//				float share = pvGb * 100 / (pvGb + pvGs + pvGk);
//				System.out.println("Member " + member.getMemberId() + " dengan share = " + share + "%");
//				System.out.println("bigSharePct1="+bigSharePct1+"; bigSharePct2="+bigSharePct2+"; bigSharePct3="+bigSharePct3+"; bigSharePct4="+bigSharePct4);
//				double maxGb = 0;
//				if (share >= bigSharePct1) {
//					maxGb = bigMaxBag1;
//				} else if (share >= bigSharePct2) {
//					maxGb = bigMaxBag2;
//				} else if (share >= bigSharePct3) {
//					maxGb = bigMaxBag3;
//				} else if (share >= bigSharePct4) {
//					maxGb = bigMaxBag4;
//				} else {
//					if (bigMaxBag5 == 0) {
//						maxGb = bagianGb;
//						System.out.println("@ bigMaxBag5==0; bagianGb="+bagianGb);
//					} else {
//						maxGb = bigMaxBag5;
//					}
//				}
//				if (bagianGb > maxGb) {
//					bagianGb = maxGb;
//				}
//				System.out.println("Member " + member.getMemberId() + " dengan bagian GB = " + bagianGb);
//			} else {
//				bagianGb = 0;
//			}
			if (persenGb <= 0) {
				bagianGb = 0;
			}
			if (maxGs > bigMaxBagSedang) maxGs = bigMaxBagSedang;
			if (maxGk > bigMaxBagKecil) maxGk = bigMaxBagKecil;
			totalBagianGb += bagianGb;
			totalBagianGs += maxGs + maxGk;
			totalBagianGk += bagianGs + bagianGk;
		}
		System.out.println("Total bagian GB="+totalBagianGb+"; GS="+totalBagianGs+"; GK="+totalBagianGk);
		double[] total = new double[] {totalBagianGb, totalBagianGs, totalBagianGk};
		return total;
	}
	
	private void hitungBULPerTingkat(float persenBul, int batasBawahPv, int batasAtasPv) {
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'" 
			+ " AND pv_litfoot >= " + batasBawahPv;
		if (batasAtasPv > 0) {
			whereClause += " AND pv_litfoot < " + batasAtasPv;
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause);
			double totalBagian = mtSet.length();
			System.out.println("Total Bagian="+ totalBagian);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				double bonusBul = bvTotal * persenBul / (100 * totalBagian);
				bonusBul = NumberUtil.roundToDecimals(bonusBul, 0);
				System.out.println("Member "+member.getMemberId()+" dapat bonus UL tingkat "+bonusBul);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusUs(bonusBul);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusBul = bonus.getBonusUs();
					curBonusBul += bonusBul;
					bonus.setBonusUs(curBonusBul);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	private void hitungBULPerTingkat(float persenBul, int batasBawahPv, int batasAtasPv, int tingkat, float minPvpribadi, DBConnection dbConn) {
	private void hitungBULPerTingkat(float persenBul, int batasBawahPv, int batasAtasPv, int tingkat, float minPvpribadi, int batasBawahPv2, int batasAtasPv2, DBConnection dbConn) {
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND pvpribadi >= " + minPvpribadi 
				+ " AND (pv_kecil >= " + batasBawahPv + " OR pv_kecil_2jalur >= " + batasBawahPv2 + ")";
		if (batasAtasPv > 0) {
			whereClause += " AND (pv_kecil < " + batasAtasPv + " OR pv_kecil_2jalur < " + batasAtasPv2 + ")";
		}
		if (tingkat > 1) {
			whereClause += " AND is_basic = 0 AND is_basic_new = 0";
		} else {
			whereClause += " AND is_basic > -1 AND is_basic_new > -1";
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			double totalBagian = mtSet.length();
			System.out.println("Total Bagian="+ totalBagian);
			double bonusBul = 0;
			if (totalBagian > 0) {
				bonusBul = bvTotal * persenBul / (100 * totalBagian);
			}
			bonusBul = NumberUtil.roundToDecimals(bonusBul, 0);
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			switch (tingkat) {
			case 1:
				bbLog.setBulT11Bag(bonusBul);
				bbLog.setBulT1Totalbag((int)totalBagian);
				bbLog.setBulT1Totalbonus(bonusBul * totalBagian);
				break;
			case 2:
				bbLog.setBulT21Bag(bonusBul);
				bbLog.setBulT2Totalbag((int)totalBagian);
				bbLog.setBulT2Totalbonus(bonusBul * totalBagian);
				break;
			case 3:
				bbLog.setBulT31Bag(bonusBul);
				bbLog.setBulT3Totalbag((int)totalBagian);
				bbLog.setBulT3Totalbonus(bonusBul * totalBagian);
				break;
			case 4:
				bbLog.setBulT41Bag(bonusBul);
				bbLog.setBulT4Totalbag((int)totalBagian);
				bbLog.setBulT4Totalbonus(bonusBul * totalBagian);
				break;
			case 5:
				bbLog.setBulT51Bag(bonusBul);
				bbLog.setBulT5Totalbag((int)totalBagian);
				bbLog.setBulT5Totalbonus(bonusBul * totalBagian);
				break;
			case 6:
				bbLog.setBulT61Bag(bonusBul);
				bbLog.setBulT6Totalbag((int)totalBagian);
				bbLog.setBulT6Totalbonus(bonusBul * totalBagian);
				break;
			default:
				break;
			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bbLog, dbConn);
			bblFcd = null;
			bbLog = null;
			for (int i=0; i<mtSet.length(); i++) {
//				MemberTree member = mtSet.get(i);
				MemberTreeSmall member = mtSet.get(i);
				System.out.println("Member "+member.getMemberId()+" dapat bonus UL tingkat "+bonusBul);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusUs(bonusBul);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus(dbConn);
					bonusFcd = null;
				} else {
					double curBonusBul = bonus.getBonusUs();
					curBonusBul += bonusBul;
					bonus.setBonusUs(curBonusBul);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus, dbConn);
					bonusFcd = null;
				}
				BonusBLogPeserta logPeserta = BonusBLogPesertaFcd.getBonusLogByMemberBulan(member.getMemberId(), bulanHitung.toDate(), dbConn);
				if (logPeserta == null) {
					logPeserta = new BonusBLogPeserta();
					logPeserta.setMemberId(member.getMemberId());
					logPeserta.setBulan(bulanHitung.toDate());
					switch (tingkat) {
					case 1:
						logPeserta.setBul1(bonusBul);
						break;
					case 2:
						logPeserta.setBul2(bonusBul);
						break;
					case 3:
						logPeserta.setBul3(bonusBul);
						break;
					case 4:
						logPeserta.setBul4(bonusBul);
						break;
					case 5:
						logPeserta.setBul5(bonusBul);
						break;
					case 6:
						logPeserta.setBul6(bonusBul);
						break;
					default:
						break;
					}
					BonusBLogPesertaFcd bblpFcd = new BonusBLogPesertaFcd(logPeserta);
					bblpFcd.insertLog(dbConn);
				} else {
					switch (tingkat) {
					case 1:
						logPeserta.setBul1(bonusBul);
						break;
					case 2:
						logPeserta.setBul2(bonusBul);
						break;
					case 3:
						logPeserta.setBul3(bonusBul);
						break;
					case 4:
						logPeserta.setBul4(bonusBul);
						break;
					case 5:
						logPeserta.setBul5(bonusBul);
						break;
					case 6:
						logPeserta.setBul6(bonusBul);
						break;
					default:
						break;
					}
					BonusBLogPesertaFcd bblpFcd = new BonusBLogPesertaFcd();
					bblpFcd.updateLog(logPeserta, dbConn);
				}
				bonus = null;
				member = null;
			}
			mtFcd = null;
			mtSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusUL() {
		float[] persenBul = new float[] {bulPct1, bulPct2, bulPct3, bulPct4, bulPct5, bulPct6};
		int[] batasBawah = new int[] {bulRmKualifikasi, bulEmKualifikasi, bulPmKualifikasi, bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi};
		int[] batasAtas = new int[] {bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi, 0, 0, 0};
		for (int i=0; i<persenBul.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBULPerTingkat(persenBul[i], batasBawah[i], batasAtas[i]);
		}
	}
	
	public void giveBonusUL(DBConnection dbConn) {
		float[] persenBul = new float[] {bulPct1, bulPct2, bulPct3, bulPct4, bulPct5, bulPct6};
		int[] batasBawah = new int[] {bulRmKualifikasi, bulEmKualifikasi, bulPmKualifikasi, bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi};
		int[] batasAtas = new int[] {bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi, 0, 0, 0};
		int[] batasBawah2 = new int[] {bulRmKualifikasi2Jalur, bulEmKualifikasi2Jalur, bulPmKualifikasi2Jalur, bulDmKualifikasi2Jalur, bulCdKualifikasi2Jalur, bulScdKualifikasi2Jalur};
		int[] batasAtas2 = new int[] {bulDmKualifikasi2Jalur, bulCdKualifikasi2Jalur, bulScdKualifikasi2Jalur, 0, 0, 0};
		float[] minPvpribadi = new float[] {minPvpribadiBul1, minPvpribadiBul2, minPvpribadiBul3, minPvpribadiBul4, minPvpribadiBul5, minPvpribadiBul6};
		for (int i=0; i<persenBul.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			//hitungBULPerTingkat(persenBul[i], batasBawah[i], batasAtas[i], (i+1), minPvpribadi[i], dbConn);
			hitungBULPerTingkat(persenBul[i], batasBawah[i], batasAtas[i], (i+1), minPvpribadi[i], batasBawah2[i], batasAtas2[i], dbConn);
		}
		persenBul = null;
		batasAtas = null;
		batasBawah = null;
		batasAtas2 = null;
		batasBawah2 = null;
	}
	
//	private double getTotalBagianPerTingkatBeb(MemberTreeSmallSet mtSet) {
	private double getTotalBagianPerTingkatBeb(MemberTreeSmallSet mtSet, DBConnection dbConn) throws Exception {
		double totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTreeSmall member = mtSet.get(i);
			float pvGk = member.getPvKecil();
//			float pvGk = member.getSaldoPoin();
			MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
			String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND member_id = '" + member.getMemberId() + "'";
			MemberTreeSmallSet mtss = mtsbFcd.search(whereClause, dbConn);
			if (mtss.get(0).getIsBasic() > -1) {
				double bagian = NumberUtil.roundToDecimals((pvGk / bebPvPerbagian), 0);
				totalBagian += bagian;
			}
		}
		System.out.println("Total bagian = " + totalBagian);
		return totalBagian;
	}
	
	/*
	private void hitungBEBPerTingkat(int batasBawah, int batasAtas, int persenBeb, int tanggal) {
		String whereClause = "pv_litfoot >= " + batasBawah; 
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), tanggal, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		whereClause += " AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "'";
		whereClause += " AND pv_litfoot <= " + batasAtas;
		System.out.println("whereClause = " + whereClause);
		MemberTreeHarianFcd mtFcd = new MemberTreeHarianFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause);
			double totalBagian = getTotalBagianPerTingkatBeb(mtSet);
			double bagianPerTingkat = bvTotal * persenBeb / (100 * totalBagian);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float bagian = member.getPvLitfoot() / bebPvPerbagian;
				double bonusBeb = NumberUtil.roundToDecimals((bagian * bagianPerTingkat), 0);
				System.out.println("Member "+member.getMemberId()+" dapat bonus EB tingkat "+bonusBeb);
				lastMonth.setDayOfMonth(beginOfDay.getDayOfMonth());
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(lastMonth.toDate());
					bonus.setBonusEarlybird(bonusBeb);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusBeb = bonus.getBonusUs();
					curBonusBeb += bonusBeb;
					bonus.setBonusEarlybird(curBonusBeb);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	private void hitungBEBPerTingkat(int batasBawah, int batasAtas, int persenBeb, int tanggal, int tingkat, DBConnection dbConn) {
//	private void hitungBEBPerTingkat(int batasBawah, int persenBeb, int tanggal, DBConnection dbConn) {
		String whereClause = "pv_kecil >= " + batasBawah;
//		String whereClause = "saldo_poin >= " + batasBawah;
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), tanggal, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		whereClause += " AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "'";
		 
		if (batasAtas > 0) whereClause += " AND pv_kecil <= " + batasAtas;
		whereClause += " AND pvpribadi >= " + minPvpribadiBeb + " AND saldo_poin > 0";
		System.out.println("whereClause = " + whereClause);
//		MemberTreeHarianFcd mtFcd = new MemberTreeHarianFcd();
		MemberTreeSmallHarianFcd mtFcd = new MemberTreeSmallHarianFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
//			double totalBagian = getTotalBagianPerTingkatBeb(mtSet);
			double totalBagian = getTotalBagianPerTingkatBeb(mtSet, dbConn);
			double bagianPerTingkat = 0;
			if (totalBagian > 0) {
				bagianPerTingkat = bvTotal * persenBeb / (100 * totalBagian);
			}
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
//			switch (tingkat) {
//			case 1:
//				bbLog.setBebT11Bag(bagianPerTingkat);
//				bbLog.setBebT1Totalbag((int)totalBagian);
//				bbLog.setBebT1Totalbonus(bagianPerTingkat * totalBagian);
			bbLog.setBebT11Bag(0);
			bbLog.setBebT1Totalbag(0);
			bbLog.setBebT1Totalbonus(0);
//				break;
//			case 2:
				bbLog.setBebT21Bag(bagianPerTingkat);
				bbLog.setBebT2Totalbag((int)totalBagian);
				bbLog.setBebT2Totalbonus(bagianPerTingkat * totalBagian);
//				break;
//			default:
//				break;
//			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bbLog, dbConn);
			bblFcd = null;
			bbLog = null;
			for (int i=0; i<mtSet.length(); i++) {
				MemberTreeSmall member = mtSet.get(i);
				MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
				whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND member_id = '" + member.getMemberId() + "'";
				MemberTreeSmallSet mtss = mtsbFcd.search(whereClause, dbConn);
				if (mtss.get(0).getIsBasic() > -1) {
					double bagian = NumberUtil.roundToDecimals((member.getPvKecil() / bebPvPerbagian), 0);
					double bonusBeb = NumberUtil.roundToDecimals((bagian * bagianPerTingkat), 0);
					System.out.println("Member "+member.getMemberId()+" dapat bonus EB tingkat "+bonusBeb);
					lastMonth.setDayOfMonth(beginOfDay.getDayOfMonth());
					BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
					if (bonus == null) {
						bonus = new BonusB();
						bonus.setMemberId(member.getMemberId());
						bonus.setBulan(lastMonth.toDate());
						bonus.setBonusEarlybird(bonusBeb);
						BonusBFcd bonusFcd = new BonusBFcd(bonus);
						bonusFcd.insertBonus(dbConn);
						bonusFcd = null;
					} else {
						double curBonusBeb = bonus.getBonusEarlybird();
						curBonusBeb += bonusBeb;
						bonus.setBonusEarlybird(curBonusBeb);
						BonusBFcd bonusFcd = new BonusBFcd();
						bonusFcd.updateBonus(bonus, dbConn);
						bonusFcd = null;
					}
					bonus = null;
					member = null;
				}
			}
			mtFcd = null;
			mtSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusEB() {
		int[] batasBawah = new int[] {bebMingk1, bebMaxgk1};
		int[] batasAtas = new int[] {bebMingk2, bebMaxgk2};
		int[] persenBeb = new int[] {bebPct1, bebPct2};
		int[] tanggal = new int[] {bebTgl1, bebTgl2};
		for (int i=0; i<batasBawah.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
//			hitungBEBPerTingkat(batasBawah[i], batasAtas[i], persenBeb[i], tanggal[i]);
		}
	}
	
	public void giveBonusEB(DBConnection dbConn) {
		int[] batasBawah = new int[] {bebMingk1, bebMingk2};
		int[] batasAtas = new int[] {bebMaxgk1, bebMaxgk2};
		int[] persenBeb = new int[] {bebPct1, bebPct2};
		int[] tanggal = new int[] {bebTgl1, bebTgl2};
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), bebTgl1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "'";
		whereClause += " AND NOT (pv_bigfoot=0 AND pv_midfoot=0 AND pv_litfoot=0)";
//		whereClause += " AND saldo_poin >= " + bebMingk2;
		MemberTreeSmallHarianFcd mtFcd = new MemberTreeSmallHarianFcd();
		try {
			MemberTreeSmallSet mtSet = mtFcd.search(whereClause, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTreeSmall member = mtSet.get(i);
				float[] urutPv = sortPvMember(member);
				member.setPvBesar(urutPv[0]);
				member.setPvSedang(urutPv[1]);
				member.setPvKecil(urutPv[2]);
				mtFcd.updateMemberTree(member, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int i=0; i<batasBawah.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			if (persenBeb[i] > 0)
				hitungBEBPerTingkat(batasBawah[i], batasAtas[i], persenBeb[i], tanggal[i], (i+1), dbConn);
//				hitungBEBPerTingkat(batasBawah[i], persenBeb[i], tanggal[i], dbConn);
		}
		batasAtas = null;
		batasBawah = null;
		persenBeb = null;
		tanggal = null;
	}
	
	public void giveBonusOR() {
		int[] akumulasi = new int[] {borWisataAkumgk, borMotorAkumgk, borReligiAkumgk, borMobilAkumgk, borMobilMewahAkumgk, borRumahMewahAkumgk};
		int[] syarat = new int[] {borWisataTambahangk, borMotorTambahangk, borReligiTambahangk, borMobilTambahangk, borMobilMewahTambahangk, borRumahMewahTambahangk};
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
					+ " AND pv_litfoot >= " + borWisataTambahangk;
		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
				lastMonth.addMonths(-1);
				float akumGk = 0;
				int tingkat = 0;
				BonusB bonusLastMonth = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate());
				if (bonusLastMonth == null) {
					tingkat = 1;
					akumGk = member.getPvLitfoot();
				} else {
					if (bonusLastMonth.getBorTingkat() == 0) {
						tingkat = 1;
						akumGk = member.getPvLitfoot();
					} else {
						if (bonusLastMonth.getBorIsclaimed() == 1 && bonusLastMonth.getBorTingkat() < 6) {
							tingkat = bonusLastMonth.getBorTingkat() + 1;
							if (member.getPvLitfoot() >= syarat[tingkat-1]) {
								akumGk = member.getPvLitfoot();
							}
						} else if (bonusLastMonth.getBorTingkat() < 6) {
							tingkat = bonusLastMonth.getBorTingkat();
							if (member.getPvLitfoot() >= syarat[tingkat-1]) {
								akumGk = bonusLastMonth.getBorAkumgk() + member.getPvLitfoot();
							}
						}
					}
				}
				if (akumulasi[tingkat-1] <= akumGk) {
					System.out.println("Member "+member.getMemberId()+" dengan akumulasi GK "+akumGk+" sudah kualifikasi untuk BOR Tingkat "+tingkat);
				}
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBorAkumgk(akumGk);
					bonus.setBorTingkat(tingkat);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					bonus.setBorAkumgk(akumGk);
					bonus.setBorTingkat(tingkat);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusOR(DBConnection dbConn) {
		int[] akumulasi = new int[] {borWisataAkumgk, borMotorAkumgk, borReligiAkumgk, borMobilAkumgk, borMobilMewahAkumgk, borRumahMewahAkumgk};
		int[] syarat = new int[] {borWisataTambahangk, borMotorTambahangk, borReligiTambahangk, borMobilTambahangk, borMobilMewahTambahangk, borRumahMewahTambahangk};
		int[] budgetPct = new int[] {borWisataPct, borMotorPct, borReligiPct, borMobilPct, borMobilMewahPct, borRumahMewahPct};
		int[] nilaiBor = new int[] {borWisataValue, borMotorValue, borReligiValue, borMobilValue, borMobilMewahValue, borRumahMewahValue};
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
//					+ " AND pv_litfoot >= " + borWisataTambahangk
					+ " AND pv_kecil >= " + borWisataTambahangk
					+ " AND pvpribadi >= " + minPvpribadiBor;
//		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
//				MemberTree member = mtSet.get(i);
				MemberTreeSmall member = mtSet.get(i);
				MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
				lastMonth.addMonths(-1);
				float akumGk = 0;
				int tingkat = 0;
				int isqualified = 0;
				whereClause = "member_id = '" + member.getMemberId() + "' AND bulan < '" + bulanHitung.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
				BonusBFcd bbFcd = new BonusBFcd();
				BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
//				BonusB bonusLastMonth = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
				BonusB bonusLastMonth = null;
				if (bbSet != null) bonusLastMonth = bbSet.get(0);
				if (bonusLastMonth == null) {
					tingkat = 1;
//					akumGk = member.getPvLitfoot();
					akumGk = member.getPvKecil();
				} else {
					if (bonusLastMonth.getBorTingkat() == 0) {
						tingkat = 1;
//						akumGk = member.getPvLitfoot();
						akumGk = member.getPvKecil();
					} else {
						if (bonusLastMonth.getBorIsclaimed() == 1 && bonusLastMonth.getBorTingkat() < 6) {
							tingkat = bonusLastMonth.getBorTingkat() + 1;
							isqualified = 0;
//							if (member.getPvLitfoot() >= syarat[tingkat-1]) {
							if (member.getPvKecil() >= syarat[tingkat-1]) {
//								akumGk = member.getPvLitfoot();
								akumGk = member.getPvKecil();
							}
						} else if (bonusLastMonth.getBorTingkat() < 6) {
							tingkat = bonusLastMonth.getBorTingkat();
//							if (member.getPvLitfoot() >= syarat[tingkat-1]) {
							if (member.getPvKecil() >= syarat[tingkat-1]) {
//								akumGk = bonusLastMonth.getBorAkumgk() + member.getPvLitfoot();
								akumGk = bonusLastMonth.getBorAkumgk() + member.getPvKecil();
							}
						}
					}
				}
				if (akumulasi[tingkat-1] <= akumGk) {
					System.out.println("Member "+member.getMemberId()+" dengan akumulasi GK "+akumGk+" sudah kualifikasi untuk BOR Tingkat "+tingkat);
					isqualified = 1;
				}
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBorAkumgk(akumGk);
					bonus.setBorTingkat(tingkat);
					bonus.setBorIsqualified(isqualified);
					BonusBFcd bonusFcd = new BonusBFcd(bonus);
					bonusFcd.insertBonus(dbConn);
 					bonusFcd = null;
				} else {
					bonus.setBorAkumgk(akumGk);
					bonus.setBorTingkat(tingkat);
					bonus.setBorIsqualified(isqualified);
					BonusBFcd bonusFcd = new BonusBFcd();
					bonusFcd.updateBonus(bonus, dbConn);
					bonusFcd = null;
				}
				bonus = null;
				bonusLastMonth = null;
				bbFcd = null;
				bbSet = null;
				member = null;
			}
			for (int i=0; i<budgetPct.length; i++) {
				getQualifiedBorMember(i+1, budgetPct[i], nilaiBor[i], dbConn);
			}
//			giveBonusAntrian(dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getQualifiedBorMember(int tingkat, int budgetPct, int nilaiBor, DBConnection dbConn) {
		double budgetTingkat = budgetPct * bvTotal / 100;
		BonusBLog bblNow = null;
		try {
			bblNow = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			MutableDateTime blnKemarin = bulanHitung.copy();
			blnKemarin.addMonths(-1);
			BonusBLog bblBefore = BonusBLogFcd.getLogByMonth(blnKemarin.toDate(), dbConn);
			if (bblBefore != null) {
				switch (tingkat) {
				case 1:
					budgetTingkat += bblBefore.getBorT1Saldo();
					break;
				case 2:
					budgetTingkat += bblBefore.getBorT2Saldo();
					break;
				case 3:
					budgetTingkat += bblBefore.getBorT3Saldo();
					break;
				case 4:
					budgetTingkat += bblBefore.getBorT4Saldo();
					break;
				case 5:
					budgetTingkat += bblBefore.getBorT5Saldo();
					break;
				case 6:
					budgetTingkat += bblBefore.getBorT6Saldo();
					break;
				default:
					break;
				}
			}
			double saldoNow = budgetTingkat;
		
			System.out.println("BOR Tingkat "+tingkat+" punya budget sebesar "+budgetTingkat);
//			int totalClaimed = (int) budgetTingkat / nilaiBor;
			int totalClaimed = (int) (budgetTingkat / nilaiBor);
			System.out.println("BOR Tingkat "+tingkat+" dapat diklaim sebanyak "+totalClaimed);
			if (totalClaimed > 0) {
				saldoNow = saldoNow - (totalClaimed * nilaiBor);
				String whereClause = "bor_isqualified=1 AND bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
					+ " AND bor_tingkat=" + tingkat + " AND bor_isclaimed=0"
					+ " ORDER BY bor_akumgk DESC";
				BonusBFcd bbFcd = new BonusBFcd();
				BonusBSet bbSet = null;
//				try {
				bbSet = bbFcd.search(whereClause, dbConn);
				int batas = totalClaimed;
				if (totalClaimed > bbSet.length()) batas = bbSet.length();
				for (int i=0; i<batas; i++) {
					BonusB bonus = bbSet.get(i);
					bonus.setBorIsclaimed(1);
					if (tingkat > 1) {
						bonus.setBonusOr(nilaiBor);
					}
					bbFcd.updateBonus(bonus, dbConn);
					bonus = null;
				}
				bbFcd = null;
				bbSet = null;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
			}
			switch (tingkat) {
			case 1:
				bblNow.setBorT1Budget(budgetTingkat);
				bblNow.setBorT1Totalklaim(totalClaimed);
				bblNow.setBorT1Saldo(saldoNow);
				break;
			case 2:
				bblNow.setBorT2Budget(budgetTingkat);
				bblNow.setBorT2Totalklaim(totalClaimed);
				bblNow.setBorT2Saldo(saldoNow);
				break;
			case 3:
				bblNow.setBorT3Budget(budgetTingkat);
				bblNow.setBorT3Totalklaim(totalClaimed);
				bblNow.setBorT3Saldo(saldoNow);
				break;
			case 4:
				bblNow.setBorT4Budget(budgetTingkat);
				bblNow.setBorT4Totalklaim(totalClaimed);
				bblNow.setBorT4Saldo(saldoNow);
				break;
			case 5:
				bblNow.setBorT5Budget(budgetTingkat);
				bblNow.setBorT5Totalklaim(totalClaimed);
				bblNow.setBorT5Saldo(saldoNow);
				break;
			case 6:
				bblNow.setBorT6Budget(budgetTingkat);
				bblNow.setBorT6Totalklaim(totalClaimed);
				bblNow.setBorT6Saldo(saldoNow);
				break;
			default:
				break;
			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bblNow, dbConn);
			bblFcd = null;
			bblBefore = null;
			bblNow = null;
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}
	
	private void giveBonusAntrian(DBConnection dbConn) {
		String whereClause = "bor_isqualified=1 AND bor_isclaimed=0"
			+ " AND bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "'";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		BonusBLog bblNow = null;
		BonusBLog bblBefore = null;
		try {
			bblNow = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			MutableDateTime before = bulanHitung.copy();
			before.addMonths(-1);
			bblBefore = BonusBLogFcd.getLogByMonth(before.toDate(), dbConn);
			double saldoBefore = 0;
			bbSet = bbFcd.search(whereClause, dbConn);
			int jmlWisata = 0;
			int jmlMotor = 0;
			int jmlReligi = 0;
			int jmlMobil = 0;
			for (int i=0; i<bbSet.length(); i++) {
				BonusB bonus = bbSet.get(i);
				switch (bonus.getBorTingkat()) {
				case 1:
					jmlWisata++;
					break;
				case 2:
					jmlMotor++;
					break;
				case 3:
					jmlReligi++;
					break;
				case 4:
					jmlMobil++;
					break;
				default:
					break;
				}
				bonus = null;
			}
			for (int i=0; i<bbSet.length(); i++) {
				BonusB bonus = bbSet.get(i);
				int pembagi = 0;
				float pctBonus = 0;
				switch (bonus.getBorTingkat()) {
				case 1:
					pembagi = jmlWisata;
					pctBonus = borAntriWisataPct;
					if (bblBefore != null) {
						saldoBefore = bblBefore.getBorantriT1Saldo();
					}
					break;
				case 2:
					pembagi = jmlMotor;
					pctBonus = borAntriMotorPct;
					if (bblBefore != null) {
						saldoBefore = bblBefore.getBorantriT2Saldo();
					}
					break;
				case 3:
					pembagi = jmlReligi;
					pctBonus = borAntriReligiPct;
					if (bblBefore != null) {
						saldoBefore = bblBefore.getBorantriT3Saldo();
					}
					break;
				case 4:
					pembagi = jmlMobil;
					pctBonus = borAntriMobilPct;
					if (bblBefore != null) {
						saldoBefore = bblBefore.getBorantriT4Saldo();
					}
					break;
				default:
					break;
				}
				if (pembagi > 0) {
					double bonusAntri = 0;
					if (saldoBefore > 0) {
						bonusAntri = (bvTotal * pctBonus / (100 * pembagi)) + (saldoBefore / pembagi);
					} else {
						bonusAntri = bvTotal * pctBonus / (100 * pembagi);
					}
					bonus.setBorAntrian(bonusAntri);
					bbFcd.updateBonus(bonus, dbConn);
				}
				bonus = null;
			}
			bblNow.setBorantriT1Acceptor(jmlWisata);
			double budgetAntrian = 0;
			budgetAntrian = bvTotal * borAntriWisataPct / 100;
			if (bblBefore != null) {
				budgetAntrian += bblBefore.getBorantriT1Saldo();
			}
			bblNow.setBorantriT1Budget(budgetAntrian);
			if (jmlWisata > 0) {
				bblNow.setBorantriT1Saldo(0);
			} else {
				bblNow.setBorantriT1Saldo(budgetAntrian);
			}
			
			bblNow.setBorantriT2Acceptor(jmlMotor);
			budgetAntrian = bvTotal * borAntriMotorPct / 100;
			if (bblBefore != null) {
				budgetAntrian += bblBefore.getBorantriT2Saldo();
			}
			bblNow.setBorantriT2Budget(budgetAntrian);
			if (jmlMotor > 0) {
				bblNow.setBorantriT2Saldo(0);
			} else {
				bblNow.setBorantriT2Saldo(budgetAntrian);
			}
			
			bblNow.setBorantriT3Acceptor(jmlReligi);
			budgetAntrian = bvTotal * borAntriReligiPct / 100;
			if (bblBefore != null) {
				budgetAntrian += bblBefore.getBorantriT3Saldo();
			}
			bblNow.setBorantriT3Budget(budgetAntrian);
			if (jmlReligi > 0) {
				bblNow.setBorantriT3Saldo(0);
			} else {
				bblNow.setBorantriT3Saldo(budgetAntrian);
			}
			
			bblNow.setBorantriT4Acceptor(jmlMobil);
			budgetAntrian = bvTotal * borAntriMobilPct / 100;
			if (bblBefore != null) {
				budgetAntrian += bblBefore.getBorantriT4Saldo();
			}
			bblNow.setBorantriT4Budget(budgetAntrian);
			if (jmlMobil > 0) {
				bblNow.setBorantriT4Saldo(0);
			} else {
				bblNow.setBorantriT4Saldo(budgetAntrian);
			}
			BonusBLogFcd bblFcd = new BonusBLogFcd();
			bblFcd.updateLog(bblNow, dbConn);
			bblFcd = null;
			bblBefore = null;
			bblNow = null;
			bbSet = null;
			bbFcd = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cekSyaratBRC() {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "bulan = '" + lastMonth.toString("yyyy-MM-dd") + "'"
			+ " AND (brc_continuity_cd > 0 OR brc_continuity_scd > 0)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause);
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonusLast = bbSet.get(i);
					if (bonusLast.getBrcIsqualifiedCd() == 0 && bonusLast.getBrcContinuityCd() > 0) {
						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
							+ " AND member_id = '" + bonusLast.getMemberId() + "'";
						MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
						MemberTreeSet mtSet = mtFcd.search(whereClause);
						MemberTree member = mtSet.get(0);
						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
						if (member.getPvLitfoot() >= brcCdKualifikasi) {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd() + 1);
							if (bonus.getBrcContinuityCd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedCd(1);
							}
							if (member.getPvLitfoot() >= brcScdKualifikasi) {
								bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
								if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
									bonus.setBrcIsqualifiedScd(1);
								}
							} else {
								bonus.setBrcContinuityScd(0);
							}
						} else {
							bonus.setBrcContinuityCd(0);
							bonus.setBrcContinuityScd(0);
						}
						bbFcd.updateBonus(bonus);
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 0 && bonusLast.getBrcContinuityScd() > 0) {
						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
							+ " AND member_id = '" + bonusLast.getMemberId() + "'";
						MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
						MemberTreeSet mtSet = mtFcd.search(whereClause);
						MemberTree member = mtSet.get(0);
						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
						bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
						bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
						if (member.getPvLitfoot() >= brcScdKualifikasi) {
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
							if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedScd(1);
							}
						} else {
							bonus.setBrcContinuityScd(0);
						}
						bbFcd.updateBonus(bonus);
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 1) {
						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(bonusLast.getMemberId(), bulanHitung.toDate());
						if (bonus == null) {
							bonus = new BonusB();
							bonus.setMemberId(bonusLast.getMemberId());
							bonus.setBulan(bulanHitung.toDate());
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
							BonusBFcd bonusFcd = new BonusBFcd(bonus);
							bonusFcd.insertBonus();
						} else {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
	//						BonusBFcd bonusFcd = new BonusBFcd();
							bbFcd.updateBonus(bonus);
						}
					}
				}
			}
			whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
				+ "AND pv_litfoot >= " + brcCdKualifikasi + " AND "
				+ "member_id IN (SELECT member_id FROM bonus_b "
				+ "WHERE brc_isqualified_cd = 0 AND brc_continuity_cd = 0 "
				+ "AND brc_isqualified_scd = 0 AND brc_continuity_scd = 0)";
			MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
			MemberTreeSet mtSet = mtFcd.search(whereClause);
			for (int j=0; j<mtSet.length(); j++) {
				MemberTree member = mtSet.get(j);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				if (member.getPvLitfoot() >= brcCdKualifikasi) {
					bonus.setBrcContinuityCd(1);
					if (member.getPvLitfoot() >= brcScdKualifikasi) {
						bonus.setBrcContinuityScd(1);
					}
					bbFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cekSyaratBRC(DBConnection dbConn) {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "bulan = '" + lastMonth.toString("yyyy-MM-dd") + "'"
			+ " AND (brc_continuity_cd > 0 OR brc_continuity_scd > 0)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause, dbConn);
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonusLast = bbSet.get(i);
					if (bonusLast.getBrcIsqualifiedCd() == 0 && bonusLast.getBrcContinuityCd() > 0) {
						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
							+ " AND member_id = '" + bonusLast.getMemberId() + "'" +
									" AND is_basic > -1";
						MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
						MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
						MemberTree member = mtSet.get(0);
						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
//						if (member.getPvLitfoot() >= brcCdKualifikasi) {
						if (member.getPvKecil() >= brcCdKualifikasi || member.getPvKecil2Jalur() >= brcCdKualifikasi2Jalur) {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd() + 1);
							if (bonus.getBrcContinuityCd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedCd(1);
							}
//							if (member.getPvLitfoot() >= brcScdKualifikasi) {
							if (member.getPvKecil() >= brcScdKualifikasi || member.getPvKecil2Jalur() >= brcScdKualifikasi2Jalur) {
								bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
								if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
									bonus.setBrcIsqualifiedScd(1);
								}
							} else {
								bonus.setBrcContinuityScd(0);
							}
						} else {
							bonus.setBrcContinuityCd(0);
							bonus.setBrcContinuityScd(0);
						}
						bbFcd.updateBonus(bonus, dbConn);
						mtFcd = null;
						mtSet = null;
						member = null;
						bonus = null;
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 0 && bonusLast.getBrcContinuityScd() > 0) {
						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
							+ " AND member_id = '" + bonusLast.getMemberId() + "' AND is_basic > -1";
						MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
						MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
						MemberTree member = mtSet.get(0);
						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
						bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
						bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
//						if (member.getPvLitfoot() >= brcScdKualifikasi) {
						if (member.getPvKecil() >= brcScdKualifikasi || member.getPvKecil2Jalur() >= brcScdKualifikasi2Jalur) {
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
							if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedScd(1);
							}
						} else {
							bonus.setBrcContinuityScd(0);
						}
						bbFcd.updateBonus(bonus, dbConn);
						mtFcd = null;
						mtSet = null;
						member = null;
						bonus = null;
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 1) {
						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(bonusLast.getMemberId(), bulanHitung.toDate(), dbConn);
						if (bonus == null) {
							bonus = new BonusB();
							bonus.setMemberId(bonusLast.getMemberId());
							bonus.setBulan(bulanHitung.toDate());
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
							BonusBFcd bonusFcd = new BonusBFcd(bonus);
							bonusFcd.insertBonus(dbConn);
						} else {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
	//						BonusBFcd bonusFcd = new BonusBFcd();
							bbFcd.updateBonus(bonus, dbConn);
						}
						bonus = null;
					}
					bonusLast = null;
				}
			}
			whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
				+ "AND (pv_kecil >= " + brcCdKualifikasi + " OR "
				+ " pv_kecil_2jalur >= " + brcCdKualifikasi2Jalur + ") AND "
				+ "member_id IN (SELECT member_id FROM bonus_b "
				+ "WHERE brc_isqualified_cd = 0 AND brc_continuity_cd = 0 "
				+ "AND brc_isqualified_scd = 0 AND brc_continuity_scd = 0 "
				+ "AND bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "') "
				+ "AND is_basic > -1";
			MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
			MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
			for (int j=0; j<mtSet.length(); j++) {
				MemberTree member = mtSet.get(j);
				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				if (member.getPvKecil() >= brcCdKualifikasi || member.getPvKecil2Jalur() >= brcCdKualifikasi2Jalur) {
					bonus.setBrcContinuityCd(1);
					if (member.getPvKecil() >= brcScdKualifikasi || member.getPvKecil2Jalur() >= brcScdKualifikasi2Jalur) {
						bonus.setBrcContinuityScd(1);
					}
					bbFcd.updateBonus(bonus, dbConn);
				}
				member = null;
				bonus = null;
			}
			bbSet = null;
			bbFcd = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusRC() {
		cekSyaratBRC();
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND (brc_isqualified_cd = 1 OR brc_isqualified_scd = 1)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause);
			if (bbSet != null) {
				double totalBagianCd = bbSet.length();
				if (totalBagianCd > 0) {
					double bagianCd = bvTotal * brcCdPct / (100 * totalBagianCd);
					double bagianScd = getBagianScd();
					for (int i=0; i<bbSet.length(); i++) {
						BonusB bonus = bbSet.get(i);
						bonus.setBonusRc(bagianCd);
						if (bonus.getBrcIsqualifiedScd() == 1) {
							bonus.setBonusRc(bagianCd + bagianScd);
						} else {
							bonus.setBonusRc(bagianCd);
						}
						bbFcd.updateBonus(bonus);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusRC(DBConnection dbConn) {
		cekSyaratBRC(dbConn);
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND (brc_isqualified_cd = 1 OR brc_isqualified_scd = 1)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause, dbConn);
			if (bbSet != null) {
				double totalBagianCd = bbSet.length();
				if (totalBagianCd > 0) {
					double bagianCd = bvTotal * brcCdPct / (100 * totalBagianCd);
					double bagianScd = getBagianScd(dbConn);
					for (int i=0; i<bbSet.length(); i++) {
						BonusB bonus = bbSet.get(i);
						bonus.setBonusRc(bagianCd);
						if (bonus.getBrcIsqualifiedScd() == 1) {
							bonus.setBonusRc(bagianCd + bagianScd);
						} else {
							bonus.setBonusRc(bagianCd);
						}
						bbFcd.updateBonus(bonus, dbConn);
						bonus = null;
					}
					BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
					bbLog.setBrcT11Bagian(bagianCd);
					bbLog.setBrcT1Acceptor((int)totalBagianCd);
					BonusBLogFcd bblFcd = new BonusBLogFcd();
					bblFcd.updateLog(bbLog, dbConn);
					bbLog = null;
					bblFcd = null;
				}
			}
			bbFcd = null;
			bbSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private double getBagianScd() {
		double bagianScd = 0;
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND brc_isqualified_scd = 1";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause);
			double totalBagianScd = bbSet.length();
			if (bagianScd > 0)
				bagianScd = bvTotal * brcScdPct / (100 * totalBagianScd);
			bbSet = null;
			bbFcd = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bagianScd;
	}
	
	private double getBagianScd(DBConnection dbConn) {
		double bagianScd = 0;
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND brc_isqualified_scd = 1";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause, dbConn);
			double totalBagianScd = bbSet.length();
			if (totalBagianScd > 0) {
				bagianScd = bvTotal * brcScdPct / (100 * totalBagianScd);
				BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
				bbLog.setBrcT21Bagian(bagianScd);
				bbLog.setBrcT2Acceptor((int)totalBagianScd);
				BonusBLogFcd bblFcd = new BonusBLogFcd();
				bblFcd.updateLog(bbLog, dbConn);
				bbLog = null;
				bblFcd = null;
			}
			bbSet = null;
			bbFcd = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bagianScd;
	}
	
	public void giveBonusWT() {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND pv_litfoot >= " + bwtRmMingk;
//		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
//			mtSet = mtFcd.search(whereClause);
			double totalBagian = getTotalBagianBwt(mtSet);
			if (totalBagian > 0) {
				double perBagian = bvTotal * bwtBudgetPct / (100 * totalBagian);
				for (int i=0; i<mtSet.length(); i++) {
//					MemberTree member = mtSet.get(i);
					MemberTreeSmall member = mtSet.get(i);
					double bagian = NumberUtil.roundToDecimals((member.getPvLitfoot() / bwtPvPerbagian), 0);
					double bonusWt = NumberUtil.roundToDecimals((bagian * perBagian), 0);
					BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
					if (bonus == null) {
						bonus = new BonusB();
						bonus.setMemberId(member.getMemberId());
						bonus.setBulan(bulanHitung.toDate());
						bonus.setTraveling(bonusWt);
						double akumTravel = 0;
						BonusB bonusLast = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate());
						if (bonusLast != null) {
							akumTravel = bonusLast.getBwtAkum();
						}
						akumTravel += bonusWt;
						bonus.setBwtAkum(akumTravel);
						BonusBFcd bonusFcd = new BonusBFcd(bonus);
						bonusFcd.insertBonus();
					} else {
						bonus.setTraveling(bonusWt);
						double akumTravel = 0;
						BonusB bonusLast = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate());
						if (bonusLast != null) {
							akumTravel = bonusLast.getBwtAkum();
						}
						akumTravel += bonusWt;
						bonus.setBwtAkum(akumTravel);
						BonusBFcd bonusFcd = new BonusBFcd();
						bonusFcd.updateBonus(bonus);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusWT(DBConnection dbConn) {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
//			+ "AND pv_litfoot >= " + bwtRmMingk;
				+ "AND pv_kecil >= " + bwtRmMingk + " AND pvpribadi >= " + minPvpribadiBwt;
//		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			double totalBagian = getTotalBagianBwt(mtSet);
			if (totalBagian > 0) {
				double perBagian = bvTotal * bwtBudgetPct / (100 * totalBagian);
				for (int i=0; i<mtSet.length(); i++) {
//					MemberTree member = mtSet.get(i);
					MemberTreeSmall member = mtSet.get(i);
//					double bagian = NumberUtil.roundToDecimals((member.getPvLitfoot() / bwtPvPerbagian), 0);
					double bagian = 0;
//					if (member.getPvLitfoot() >= bwtScdMingk) {
					if (member.getPvKecil() >= bwtScdMingk) {
						bagian = bwtScdBagian;
//					} else if (member.getPvLitfoot() >= bwtCdMingk) {
					} else if (member.getPvKecil() >= bwtCdMingk) {
						bagian = bwtCdBagian;
//					} else if (member.getPvLitfoot() >= bwtDmMingk) {
					} else if (member.getPvKecil() >= bwtDmMingk) {
						bagian = bwtDmBagian;
//					} else if (member.getPvLitfoot() >= bwtEmMingk) {
					} else if (member.getPvKecil() >= bwtEmMingk) {
						bagian = bwtEmBagian;
					} else {
						bagian = bwtRmBagian;
					}
					double bonusWt = NumberUtil.roundToDecimals((bagian * perBagian), 0);
					BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
					if (bonus == null) {
						bonus = new BonusB();
						bonus.setMemberId(member.getMemberId());
						bonus.setBulan(bulanHitung.toDate());
						bonus.setTraveling(bonusWt);
						double akumTravel = 0;
						whereClause = "member_id = '" + member.getMemberId() + "' AND bulan < '" + bulanHitung.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
						BonusBFcd bbFcd = new BonusBFcd();
						BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
//						BonusB bonusLast = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
						BonusB bonusLast = null;
						if (bbSet != null) bonusLast = bbSet.get(0);
						if (bonusLast != null) {
							akumTravel = bonusLast.getBwtAkum();
						}
						akumTravel += bonusWt;
						bonus.setBwtAkum(akumTravel);
						BonusBFcd bonusFcd = new BonusBFcd(bonus);
						bonusFcd.insertBonus(dbConn);
						bonusFcd = null;
						bbFcd = null;
						bbSet = null;
						bonusLast = null;
					} else {
						bonus.setTraveling(bonusWt);
						double akumTravel = 0;
						whereClause = "member_id = '" + member.getMemberId() + "' AND bulan < '" + bulanHitung.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
						BonusBFcd bbFcd = new BonusBFcd();
						BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
//						BonusB bonusLast = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
						BonusB bonusLast = null;
						if (bbSet != null) bonusLast = bbSet.get(0);
						if (bonusLast != null) {
							akumTravel = bonusLast.getBwtAkum();
						}
						akumTravel += bonusWt;
						bonus.setBwtAkum(akumTravel);
						bbFcd.updateBonus(bonus, dbConn);
						bbFcd = null;
						bbSet = null;
						bonusLast = null;
					}
					bonus = null;
					member = null;
				}
				BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
				bbLog.setBwt1Bagian(perBagian);
				bbLog.setBwtTotalbagian((int)totalBagian);
				BonusBLogFcd bblFcd = new BonusBLogFcd();
				bblFcd.updateLog(bbLog, dbConn);
				bblFcd = null;
				bbLog = null;
			}
			mtFcd = null;
			mtSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	private double getTotalBagianBwt(MemberTreeSet mtSet) {
	private double getTotalBagianBwt(MemberTreeSmallSet mtSet) {
		double totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
//			MemberTree member = mtSet.get(i);
			MemberTreeSmall member = mtSet.get(i);
//			double bagian = NumberUtil.roundToDecimals((member.getPvLitfoot() / bwtPvPerbagian), 0);
			double bagian = 0;
//			if (member.getPvLitfoot() >= bwtScdMingk) {
			if (member.getPvKecil() >= bwtScdMingk) {
				bagian = bwtScdBagian;
//			} else if (member.getPvLitfoot() >= bwtCdMingk) {
			} else if (member.getPvKecil() >= bwtCdMingk) {
				bagian = bwtCdBagian;
//			} else if (member.getPvLitfoot() >= bwtDmMingk) {
			} else if (member.getPvKecil() >= bwtDmMingk) {
				bagian = bwtDmBagian;
//			} else if (member.getPvLitfoot() >= bwtEmMingk) {
			} else if (member.getPvKecil() >= bwtEmMingk) {
				bagian = bwtEmBagian;
			} else {
				bagian = bwtRmBagian;
			}
			totalBagian += bagian;
		}
		return totalBagian;
	}
	
	public void giveLdp(DBConnection dbConn) {
		System.out.println("============ Start Counting Bonus LDP =============");
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND " +
				"(ldp_s1_isqualified = 1 OR ldp_s2_isqualified = 1 OR ldp_s3_isqualified = 1 " +
				"OR ldp_s4_isqualified = 1)";
//		float[] minPvKecil = new float[] {MINPV_SRM, MINPV_SEM, MINPV_SDM, MINPV_SDM};
		try {
			checkQualifiedLdp(dbConn);
			countLdpContinuity(dbConn);
			double totalBagianS1 = getTotalBagianLdp("S1", MINPV_SRM, dbConn);
			double totalBagianS2 = getTotalBagianLdp("S2", MINPV_SEM, dbConn);
			double totalBagianS3 = getTotalBagianLdp("S3", MINPV_SDM, dbConn);
			double totalBagianS4 = getTotalBagianLdp("S4", MINPV_SDM, dbConn);
			double budgetS1 = ldpS1BudgetPct / 100 * bvTotal;
			double budgetS2 = ldpS2BudgetPct / 100 * bvTotal;
			double budgetS3 = ldpS3BudgetPct / 100 * bvTotal;
			double budgetS4 = ldpS4BudgetPct / 100 * bvTotal;
			System.out.println("totalBag : S1="+totalBagianS1+"; S2="+totalBagianS2+"; S3="+totalBagianS3+"; S4="+totalBagianS4);
			System.out.println("budget : S1="+budgetS1+"; S2="+budgetS2+"; S3="+budgetS3+"; S4="+budgetS4);
			double satuBagianS1 = NumberUtil.roundToDecimals(budgetS1/totalBagianS1, 2);
			double satuBagianS2 = NumberUtil.roundToDecimals(budgetS2/totalBagianS2, 2);
			double satuBagianS3 = NumberUtil.roundToDecimals(budgetS3/totalBagianS3, 2);
			double satuBagianS4 = NumberUtil.roundToDecimals(budgetS4/totalBagianS4, 2);
			
			BonusBLog bbLog = BonusBLogFcd.getLogByMonth(bulanHitung.toDate(), dbConn);
			bbLog.setLdpS1BudgetPct(ldpS1BudgetPct);
			bbLog.setLdpS1Budget(budgetS1);
			bbLog.setLdpS1Totalbag((int)totalBagianS1);
			bbLog.setLdpS11Bagian(satuBagianS1);
			bbLog.setLdpS2BudgetPct(ldpS2BudgetPct);
			bbLog.setLdpS2Budget(budgetS2);
			bbLog.setLdpS2Totalbag((int)totalBagianS2);
			bbLog.setLdpS21Bagian(satuBagianS2);
			bbLog.setLdpS3BudgetPct(ldpS3BudgetPct);
			bbLog.setLdpS3Budget(budgetS3);
			bbLog.setLdpS3Totalbag((int)totalBagianS3);
			bbLog.setLdpS31Bagian(satuBagianS3);
			bbLog.setLdpS4BudgetPct(ldpS4BudgetPct);
			bbLog.setLdpS4Budget(budgetS4);
			bbLog.setLdpS4Totalbag((int)totalBagianS4);
			bbLog.setLdpS41Bagian(satuBagianS4);
			BonusBLogFcd bbLogFcd = new BonusBLogFcd();
			bbLogFcd.updateLog(bbLog, dbConn);
			
			BonusBFcd bbFcd = new BonusBFcd();
			BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
			MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
			BonusLdpFcd blFcd = new BonusLdpFcd();
			for (int i=0; i<bbSet.length(); i++) {
				BonusB bonus = bbSet.get(i);
				MemberTreeSmall mtsNow = mtsbFcd.getMemberTreeByIdAndBulan(bonus.getMemberId(), bulanHitung.toDate(), dbConn);
				if (bonus.getLdpS1Isqualified() == 1 && mtsNow.getPvKecil() >= MINPV_SRM && mtsNow.getIsBasic() > -1) {
					double akumulasi = blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), "S1", dbConn);
					double amountLdp = 0;
					if (bonus.getLdpS1Continuity() == LDP_KUALIFIKASI_BERURUT) {
						amountLdp = NumberUtil.roundToDecimals(ldpS1BagianPertama * satuBagianS1, 0);
					} else {
						amountLdp = NumberUtil.roundToDecimals(ldpS1BagianNext * satuBagianS1, 0);
					}
					if (akumulasi < ldpS1AkumBudget) {
						double akumLdp = amountLdp + akumulasi;
						if (akumLdp <= ldpS1AkumBudget) {
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S1");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp);
							bonusLdp.setAkum(akumLdp);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; S1Cont="+bonus.getLdpS1Continuity());
						} else {
							double sisaLdp = akumLdp - ldpS1AkumBudget;
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S1");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp-sisaLdp);
							bonusLdp.setAkum(ldpS1AkumBudget);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							
							bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S2");
							bonusLdp.setIsSisadana(1);
							bonusLdp.setBonus(sisaLdp);
							bonusLdp.setAkum(sisaLdp);
							blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; sisaS1keS2="+sisaLdp+"; S1Cont="+bonus.getLdpS1Continuity());
						}
					}
				}
				if (bonus.getLdpS2Isqualified() == 1 && mtsNow.getPvKecil() >= MINPV_SEM && mtsNow.getIsBasic() > -1) {
					double akumulasi = blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), "S2", dbConn);
					double amountLdp = 0;
					if (bonus.getLdpS2Continuity() == LDP_KUALIFIKASI_BERURUT) {
						amountLdp = NumberUtil.roundToDecimals(ldpS2BagianPertama * satuBagianS2, 0);
					} else {
						amountLdp = NumberUtil.roundToDecimals(ldpS2BagianNext * satuBagianS2, 0);
					}
					if (akumulasi < ldpS2AkumBudget) {
						double akumLdp = amountLdp + akumulasi;
						if (akumLdp <= ldpS2AkumBudget) {
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S2");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp);
							bonusLdp.setAkum(akumLdp);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; S2Cont="+bonus.getLdpS2Continuity());
						} else {
							double sisaLdp = akumLdp - ldpS2AkumBudget;
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S2");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp-sisaLdp);
							bonusLdp.setAkum(ldpS2AkumBudget);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							
							bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S3");
							bonusLdp.setIsSisadana(1);
							bonusLdp.setBonus(sisaLdp);
							bonusLdp.setAkum(sisaLdp);
							blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; sisaS2keS3="+sisaLdp+"; S2Cont="+bonus.getLdpS2Continuity());
						}
					}
				}
				if (bonus.getLdpS3Isqualified() == 1 && mtsNow.getPvKecil() >= MINPV_SDM && mtsNow.getIsBasic() > -1) {
					double akumulasi = blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), "S3", dbConn);
					double amountLdp = 0;
					if (bonus.getLdpS3Continuity() == LDP_KUALIFIKASI_BERURUT) {
						amountLdp = NumberUtil.roundToDecimals(ldpS3BagianPertama * satuBagianS3, 0);
					} else {
						amountLdp = NumberUtil.roundToDecimals(ldpS3BagianNext * satuBagianS3, 0);
					}
					if (akumulasi < ldpS3AkumBudget) {
						double akumLdp = amountLdp + akumulasi;
						if (akumLdp <= ldpS3AkumBudget) {
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S3");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp);
							bonusLdp.setAkum(akumLdp);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; S3Cont="+bonus.getLdpS3Continuity());
						} else {
							double sisaLdp = akumLdp - ldpS3AkumBudget;
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S3");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp-sisaLdp);
							bonusLdp.setAkum(ldpS3AkumBudget);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							
							bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S4");
							bonusLdp.setIsSisadana(1);
							bonusLdp.setBonus(sisaLdp);
							bonusLdp.setAkum(sisaLdp);
							blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; sisaS3keS4="+sisaLdp+"; S3Cont="+bonus.getLdpS3Continuity());
						}
					}
				}
				if (bonus.getLdpS4Isqualified() == 1 && mtsNow.getPvKecil() >= MINPV_SRM && mtsNow.getIsBasic() > -1) {
					double akumulasi = blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), "S4", dbConn);
					double amountLdp = 0;
					if (bonus.getLdpS4Continuity() == LDP_KUALIFIKASI_BERURUT) {
						amountLdp = NumberUtil.roundToDecimals(ldpS4BagianPertama * satuBagianS4, 0);
					} else {
						amountLdp = NumberUtil.roundToDecimals(ldpS4BagianNext * satuBagianS4, 0);
					}
					if (akumulasi < ldpS4AkumBudget) {
						double akumLdp = amountLdp + akumulasi;
						if (akumLdp <= ldpS4AkumBudget) {
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S4");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp);
							bonusLdp.setAkum(akumLdp);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; S4Cont="+bonus.getLdpS4Continuity());
						} else {
							double sisaLdp = akumLdp - ldpS4AkumBudget;
							BonusLdp bonusLdp = new BonusLdp();
							bonusLdp.setMemberId(bonus.getMemberId());
							bonusLdp.setPeriode(bulanHitung.toDate());
							bonusLdp.setTipe("S4");
							bonusLdp.setIsSisadana(0);
							bonusLdp.setBonus(amountLdp-sisaLdp);
							bonusLdp.setAkum(ldpS4AkumBudget);
							BonusLdpFcd blFcdNew = new BonusLdpFcd(bonusLdp);
							blFcdNew.insert(dbConn);
							System.out.println("**Member "+bonus.getMemberId()+": akumSblm="+akumulasi+"; bonusLdp="+amountLdp+"; akumSsdh="+akumLdp+"; sisaS4="+sisaLdp+"; S4Cont="+bonus.getLdpS4Continuity());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private double getTotalBagianLdp(String tipe, float minPvKecil, DBConnection dbConn) throws Exception {
		double totalBagian = 0;
		StringBuffer sb = new StringBuffer();
		sb.append("bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND ");
		if (tipe.equals("S1")) {
			sb.append("ldp_s1_isqualified = 1");
		} else if (tipe.equals("S2")) {
			sb.append("ldp_s2_isqualified = 1");
		} else if (tipe.equals("S3")) {
			sb.append("ldp_s3_isqualified = 1");
		} else if (tipe.equals("S4")) {
			sb.append("ldp_s4_isqualified = 1");
		}
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = bbFcd.search(sb.toString(), dbConn);
		MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
		for (int i=0; i<bbSet.length(); i++) {
			BonusB bonus = bbSet.get(i);
			MemberTreeSmall mtsNow = mtsbFcd.getMemberTreeByIdAndBulan(bonus.getMemberId(), bulanHitung.toDate(), dbConn);
			BonusLdpFcd blFcd = new BonusLdpFcd();
			if (mtsNow.getPvKecil() >= minPvKecil && mtsNow.getIsBasic() > -1) {
				if (tipe.equals("S1")) {
					if (bonus.getLdpS1Continuity() == LDP_KUALIFIKASI_BERURUT && blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), tipe, dbConn) < ldpS1AkumBudget) {
						totalBagian += ldpS1BagianPertama;
					} else {
						totalBagian += ldpS1BagianNext;
					}
				} else if (tipe.equals("S2")) {
					if (bonus.getLdpS2Continuity() == LDP_KUALIFIKASI_BERURUT && blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), tipe, dbConn) < ldpS2AkumBudget) {
						totalBagian += ldpS2BagianPertama;
					} else {
						totalBagian += ldpS2BagianNext;
					}
				} else if (tipe.equals("S3")) {
					if (bonus.getLdpS3Continuity() == LDP_KUALIFIKASI_BERURUT && blFcd.getAkumulasiByMemberAndType(bonus.getMemberId(), tipe, dbConn) < ldpS3AkumBudget) {
						totalBagian += ldpS3BagianPertama;
					} else {
						totalBagian += ldpS3BagianNext;
					}
				} else if (tipe.equals("S4")) {
					if (bonus.getLdpS4Continuity() == 1) {
						totalBagian += ldpS4BagianPertama;
					} else {
						totalBagian += ldpS4BagianNext;
					}
				}
			}
		}
		return totalBagian;
	}
	
	private void checkQualifiedLdp(DBConnection dbConn) throws Exception {
		MutableDateTime lastMonth = bulanHitung.copy();
		lastMonth.addMonths(-1);
//		String whereClause = "tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "' AND " +
//				"(ldp_s1_isqualified = 1 OR ldp_s2_isqualified = 1 OR ldp_s3_isqualified = 1)";
		String whereClause = "bulan = '" + lastMonth.toString("yyyy-MM-dd") + "' AND " +
				"(ldp_s1_continuity > 0 OR ldp_s2_continuity > 0 OR ldp_s3_continuity > 0 OR ldp_s4_isqualified = 1)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet =  bbFcd.search(whereClause, dbConn);
		for (int i=0; i<bbSet.length(); i++) {
			BonusB lastBonus = bbSet.get(i);
			BonusB bonusNow = BonusBFcd.getBonusByMemberAndMonth(lastBonus.getMemberId(), bulanHitung.toDate(), dbConn);
			MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
			MemberTreeSmall mtsNow = mtsbFcd.getMemberTreeByIdAndBulan(lastBonus.getMemberId(), bulanHitung.toDate(), dbConn);
			if (mtsNow.getIsBasic() > -1) {
				if (bonusNow != null) {
					if (lastBonus.getLdpS1Isqualified() == 1) {
						bonusNow.setLdpS1Isqualified(1);
						bonusNow.setLdpS1Continuity(lastBonus.getLdpS1Continuity());
					} else {
						if (lastBonus.getLdpS1Continuity() > 0 && mtsNow.getPvKecil() < MINPV_SRM) {
							bonusNow.setLdpS1Continuity(0);
						}
					}
					if (lastBonus.getLdpS2Isqualified() == 1) {
						bonusNow.setLdpS2Isqualified(1);
						bonusNow.setLdpS2Continuity(lastBonus.getLdpS2Continuity());
					} else {
						if (lastBonus.getLdpS2Continuity() > 0 && mtsNow.getPvKecil() < MINPV_SRM) {
							bonusNow.setLdpS2Continuity(0);
						}
					}
					if (lastBonus.getLdpS3Isqualified() == 1) {
						bonusNow.setLdpS3Isqualified(1);
						bonusNow.setLdpS3Continuity(lastBonus.getLdpS3Continuity());
					} else {
						if (lastBonus.getLdpS3Continuity() > 0 && mtsNow.getPvKecil() < MINPV_SRM) {
							bonusNow.setLdpS3Continuity(0);
						}
					}
					if (lastBonus.getLdpS4Isqualified() == 1) {
						bonusNow.setLdpS4Isqualified(1);
					}
					bbFcd.updateBonus(bonusNow, dbConn);
				} else {
					bonusNow = new BonusB();
					bonusNow.setMemberId(lastBonus.getMemberId());
					bonusNow.setBulan(bulanHitung.toDate());
					if (lastBonus.getLdpS1Isqualified() == 1) {
						bonusNow.setLdpS1Isqualified(1);
						bonusNow.setLdpS1Continuity(lastBonus.getLdpS1Continuity());
					} else {
						if (lastBonus.getLdpS1Continuity() > 0 && mtsNow.getPvKecil() < MINPV_SRM) {
							bonusNow.setLdpS1Continuity(0);
						}
					}
					if (lastBonus.getLdpS2Isqualified() == 1) {
						bonusNow.setLdpS2Isqualified(1);
						bonusNow.setLdpS2Continuity(lastBonus.getLdpS2Continuity());
					} else {
						if (lastBonus.getLdpS2Continuity() > 0 && mtsNow.getPvKecil() < MINPV_SRM) {
							bonusNow.setLdpS2Continuity(0);
						}
					}
					if (lastBonus.getLdpS3Isqualified() == 1) {
						bonusNow.setLdpS3Isqualified(1);
						bonusNow.setLdpS3Continuity(lastBonus.getLdpS3Continuity());
					} else {
						if (lastBonus.getLdpS3Continuity() > 0 && mtsNow.getPvKecil() < MINPV_SRM) {
							bonusNow.setLdpS3Continuity(0);
						}
					}
					if (lastBonus.getLdpS4Isqualified() == 1) {
						bonusNow.setLdpS4Isqualified(1);
						bonusNow.setLdpS4Continuity(lastBonus.getLdpS4Continuity());
					}
					BonusBFcd bbiFcd = new BonusBFcd(bonusNow);
					bbiFcd.insertBonus(dbConn);
				}
			}
		}
	}
	
	private void countLdpContinuity(DBConnection dbConn) throws Exception {
		MutableDateTime prevMonth = bulanHitung.copy();
		prevMonth.addMonths(-1);
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND " + 
								"pv_kecil >= " + MINPV_SRM + " AND is_basic > -1";
		MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtssCek = mtsbFcd.search(whereClause, dbConn);
		for (int i=0; i<mtssCek.length(); i++) {
			MemberTreeSmall mtsLdp = mtssCek.get(i);
			BonusB bonusNow = BonusBFcd.getBonusByMemberAndMonth(mtsLdp.getMemberId(), bulanHitung.toDate(), dbConn);
			boolean isNewBonus = false;
			if (bonusNow == null) {
				bonusNow = new BonusB();
				bonusNow.setMemberId(mtsLdp.getMemberId());
				bonusNow.setBulan(bulanHitung.toDate());
				isNewBonus = true;
			}
			BonusB lastBonus = BonusBFcd.getBonusByMemberAndMonth(mtsLdp.getMemberId(), prevMonth.toDate(), dbConn);
			int contS1 = 0;
			if (lastBonus != null) contS1 = lastBonus.getLdpS1Continuity();
			contS1++;
			bonusNow.setLdpS1Continuity(contS1);
			if (contS1 == LDP_KUALIFIKASI_BERURUT && bonusNow.getLdpS1Isqualified() != 1)
				bonusNow.setLdpS1Isqualified(1);
			int contS2 = 0;
			if (lastBonus != null) contS2 = lastBonus.getLdpS2Continuity();
			if (mtsLdp.getPvKecil() >= MINPV_SEM) {
				contS2++;
				bonusNow.setLdpS2Continuity(contS2);
				if (contS2 == LDP_KUALIFIKASI_BERURUT && bonusNow.getLdpS2Isqualified() != 1)
					bonusNow.setLdpS2Isqualified(1);
			} else {
				if (lastBonus != null) {
					if (bonusNow.getLdpS2Isqualified() != 1 && lastBonus.getLdpS2Continuity() > 0) {
						bonusNow.setLdpS2Continuity(0);
					}
				}
			}
			int contS3 = 0;
			if (lastBonus != null) contS3 = lastBonus.getLdpS3Continuity();
			if (mtsLdp.getPvKecil() >= MINPV_SDM) {
				contS3++;
				bonusNow.setLdpS3Continuity(contS3);
				if (contS3 == LDP_KUALIFIKASI_BERURUT && bonusNow.getLdpS3Isqualified() != 1) {
					bonusNow.setLdpS3Isqualified(1);
				}
			} else {
				if (lastBonus != null) {
					if (bonusNow.getLdpS3Isqualified() != 1 && lastBonus.getLdpS3Continuity() > 0) {
						bonusNow.setLdpS3Continuity(0);
					}
				}
			}
			BonusLdpFcd ldpFcd = new BonusLdpFcd();
			double akumS3 = ldpFcd.getAkumulasiByMemberAndType(mtsLdp.getMemberId(), "S3", dbConn);
			if ((bonusNow.getLdpS4Isqualified() == 0) && (bonusNow.getLdpS3Isqualified() == 1) && (akumS3 == ldpS3AkumBudget)) {
				bonusNow.setLdpS4Isqualified(1);
				bonusNow.setLdpS4Continuity(1);
			}
			if (isNewBonus) {
				BonusBFcd bonusFcd = new BonusBFcd(bonusNow);
				bonusFcd.insertBonus(dbConn);
			} else {
				BonusBFcd bonusFcd = new BonusBFcd();
				bonusFcd.updateBonus(bonusNow, dbConn);
			}
		}
	}
	
	public void countPbr(DBConnection dbConn) {
		int[] batasRo = new int[] {46, 100, 200};
		
		System.out.println("======== Perhitungan PBR =========");
		try {
			for (int i=0; i<batasRo.length; i++) {
				System.out.println("***PBR " + batasRo[i] + " Poin***");
				cekKualifikasiPbr(batasRo[i], dbConn);
			}
			System.out.println("======= End of Perhitungan PBR ========");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cekKualifikasiPbr(int batas, DBConnection dbConn) throws Exception {
		MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
		String whereClause = "tanggal='" + this.bulanHitung.toString("yyyy-MM-dd") + "' " +
				"AND saldo_poin >= " + batas + " AND is_basic > -1";
		MemberTreeSmallSet mtsSet = mtsbFcd.search(whereClause, dbConn);
		for (int i=0; i<mtsSet.length(); i++) {
			MemberTreeSmall mtsPbr = mtsSet.get(i);
			boolean proses = true;
			if (batas == 200) {
				BonusBFcd bonusFcd = new BonusBFcd();
				double totalBonus = bonusFcd.getTotalBonusByMember(mtsPbr.getMemberId(), bulanHitung.toDate(), dbConn);
				if (totalBonus > MAX_TOTAL_BONUS_PBR) proses = false; 
			}
			if (proses) {
				BonusPbr bonusPbr = BonusPbrFcd.getBonusPbrByMemberAndMonth(mtsPbr.getMemberId(), bulanHitung.toDate(), dbConn);
				if (bonusPbr != null) {
					switch (batas) {
					case 46:
						bonusPbr.setPbr50(1);
						break;
					case 100:
						bonusPbr.setPbr100(1);
						break;
					case 200:
						bonusPbr.setPbr200(1);
						break;
					default:
						break;
					}
					BonusPbrFcd pbrFcd = new BonusPbrFcd();
					pbrFcd.update(bonusPbr, dbConn);
				} else {
					bonusPbr = new BonusPbr();
					bonusPbr.setMemberId(mtsPbr.getMemberId());
					bonusPbr.setBulan(bulanHitung.toDate());
					switch (batas) {
					case 46:
						bonusPbr.setPbr50(1);
						break;
					case 100:
						bonusPbr.setPbr100(1);
						break;
					case 200:
						bonusPbr.setPbr200(1);
						break;
					default:
						break;
					}
					BonusPbrFcd pbrFcd = new BonusPbrFcd(bonusPbr);
					pbrFcd.insert(dbConn);
				}
			}
		}
	}
	
	public void potongPajak(DBConnection dbConn) {
		System.out.println("*** di potongPajak");
		String whereClause = "start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
		TaxRateFcd trFcd = new TaxRateFcd();
		TaxRateSet trSet = null;
		try {
//			trSet = trFcd.search(whereClause);
			trSet = trFcd.search(whereClause, dbConn);
			if (trSet.length() < 1)
				throw new Exception("Belum ada setting Rate Pajak yang berlaku.");
			TaxRate tax = trSet.get(0);
			whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "'";
			System.out.println("whereClause untuk BonusB = " + whereClause);
			BonusBFcd bbFcd = new BonusBFcd();
			BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
			MutableDateTime hariHitung = beginOfDay.copy();
			hariHitung.addDays(-1);
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonus = bbSet.get(i);
////					SaldoPtkp saldoPtkp = SaldoPtkpFcd.getSaldoPtkpByMemberId(bonus.getMemberId(), dbConn);
////					int sisaPtkp = saldoPtkp.getSisaPtkp();
////					int sisaPtkpLalu = sisaPtkp;
////					whereClause = "member_id = '" + bonus.getMemberId() + "' AND tanggal < '" + beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY tanggal DESC LIMIT 1";
//					whereClause = "member_id = '" + bonus.getMemberId() + "' AND bulan < '" + beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
////					BonusASet bakSet = baFcd.search(whereClause, dbConn);
////					BonusA bonusToday = null;
//					BonusBSet bakSet = bbFcd.search(whereClause, dbConn);
//					BonusB bonusToday = null;
//					if (bakSet != null) bonusToday = bakSet.get(0);
////					double totalBonus = bonus.getBonusRo() + bonus.getBonusSi() + bonus.getBonusUs() + bonus.getBonusEarlybird() + bonus.getBonusOr() + bonus.getTraveling() + bonus.getBonusRc();
					double totalBonus = bonus.getBonusRo() + bonus.getBonusSi() + bonus.getBonusUs() + bonus.getBonusEarlybird() + bonus.getBonusOr() + bonus.getBonusRc() - bonus.getPotonganAm();
//					double setengahBonus = totalBonus/2;
////					if (totalBonus >= sisaPtkp) {
////						sisaPtkp = 0;
////					} else {
////						sisaPtkp = (int) (sisaPtkp - totalBonus);
////					}
					boolean isNpwp = false;
					Member member = MemberFcd.getMemberById(bonus.getMemberId(), dbConn);
					if (member.getNpwp() != null && !member.getNpwp().equals("")) {
						isNpwp = true;
					}
//					if (!isNpwp) {
//						sisaPtkp = 0;
//					}
//					if (sisaPtkp == 0) {
					if (totalBonus > tax.getPtkp()) {
						double pkp = 0;
//						if (isNpwp) {
//							pkp = totalBonus - sisaPtkpLalu;
//						} else {
//							pkp = totalBonus;
						pkp = totalBonus - tax.getPtkp();
//						}
						double pkpKum = pkp;
						double pphTerutang = 0;
						int levelNow, levelToday = 0;
						long batasPkp = 0;
						float pctNpwpToday, pctNpwpNow = 0;
//						if (pkpKumToday > tax.getPkpFromLevel4()) {
//							levelToday = 4;
//							pctNpwpToday = tax.getPctNpwpLevel4();
//						} else if (pkpKumToday > tax.getPkpFromLevel3()) {
//							levelToday = 3;
//							pctNpwpToday = tax.getPctNpwpLevel3();
//						} else if (pkpKumToday > tax.getPkpFromLevel2()) {
//							levelToday = 2;
//							pctNpwpToday = tax.getPctNpwpLevel2();
//						} else {
							levelToday = 1;
							pctNpwpToday = tax.getPctNpwpLevel1();
//						}
						if (pkpKum > tax.getPkpFromLevel4()) {
							levelNow = 4;
							pctNpwpNow = tax.getPctNpwpLevel4();
							batasPkp = tax.getPkpFromLevel4();
						} else if (pkpKum > tax.getPkpFromLevel3()) {
							levelNow = 3;
							pctNpwpNow = tax.getPctNpwpLevel3();
							batasPkp = tax.getPkpFromLevel3();
						} else if (pkpKum > tax.getPkpFromLevel2()) {
							levelNow = 2;
							pctNpwpNow = tax.getPctNpwpLevel2();
							batasPkp = tax.getPkpFromLevel2();
						} else {
							levelNow = 1;
							pctNpwpNow = tax.getPctNpwpLevel1();
						}
						if (isNpwp) {
							if (levelNow == levelToday) {
								pphTerutang = pctNpwpNow * pkp / 100;
							} else {
//								if ((levelNow - levelToday) == 1) {
								if (levelNow == 2) {
									double pkpAtas = pkpKum - batasPkp;
									double pkpBawah = pkp - pkpAtas;
									pphTerutang = (pctNpwpToday * pkpBawah / 100) + (pctNpwpNow * pkpAtas / 100);
//								} else if ((levelNow - levelToday) == 2) {
								} else if (levelNow == 3) {
									long batasPkp2 = 0;
									float pctNpwpTengah = 0;
//									if (levelToday == 1) {
										batasPkp2 = tax.getPkpFromLevel2();
										pctNpwpTengah = tax.getPctNpwpLevel2();
//									} else {
//										batasPkp2 = tax.getPkpFromLevel3();
//										pctNpwpTengah = tax.getPctNpwpLevel3();
//									}
									double pkpAtas = pkpKum - batasPkp;
									double pkpTengah = batasPkp - batasPkp2;
									double pkpBawah = pkp - (pkpAtas + pkpTengah);
									pphTerutang = (pctNpwpToday * pkpBawah / 100) + (pctNpwpTengah * pkpTengah / 100) + (pctNpwpNow * pkpAtas / 100);
								} else {
									long batasPkp2 = tax.getPkpFromLevel2();
									long batasPkp3 = tax.getPkpFromLevel3();
									float pctNpwp2 = tax.getPctNpwpLevel2();
									float pctNpwp3 = tax.getPctNpwpLevel3();
									double pkpAtas = pkpKum - batasPkp;
									double pkpTengah3 = batasPkp - batasPkp3;
									double pkpTengah2 = batasPkp3- batasPkp2;
									double pkpBawah = pkp - (pkpAtas + pkpTengah3 + pkpTengah2);
									pphTerutang = (pctNpwpToday * pkpBawah / 100) + (pctNpwp2 * pkpTengah2 / 100) + (pctNpwp3 * pkpTengah3 / 100) + (pctNpwpNow * pkpAtas / 100);
								}
							}
						} else {
							if (levelNow == levelToday) {
								pphTerutang = pctNpwpNow * tax.getPctPenalti() * pkp / 10000; 
							} else {
//								if ((levelNow - levelToday) == 1) {
								if (levelNow == 2) {
									double pkpAtas = pkpKum - batasPkp;
									double pkpBawah = pkp - pkpAtas;
									pphTerutang = (pctNpwpToday * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
//								} else if ((levelNow - levelToday) == 2) {
								} else if (levelNow == 3) {
									long batasPkp2 = 0;
									float pctNpwpTengah = 0;
//									if (levelToday == 1) {
										batasPkp2 = tax.getPkpFromLevel2();
										pctNpwpTengah = tax.getPctNpwpLevel2();
//									} else {
//										batasPkp2 = tax.getPkpFromLevel3();
//										pctNpwpTengah = tax.getPctNpwpLevel3();
//									}
									double pkpAtas = pkpKum - batasPkp;
									double pkpTengah = batasPkp - batasPkp2;
									double pkpBawah = pkp - (pkpAtas + pkpTengah);
									pphTerutang = (pctNpwpToday * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwpTengah * tax.getPctPenalti() * pkpTengah / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
								} else {
									long batasPkp2 = tax.getPkpFromLevel2();
									long batasPkp3 = tax.getPkpFromLevel3();
									float pctNpwp2 = tax.getPctNpwpLevel2();
									float pctNpwp3 = tax.getPctNpwpLevel3();
									double pkpAtas = pkpKum - batasPkp;
									double pkpTengah3 = batasPkp - batasPkp3;
									double pkpTengah2 = batasPkp3 - batasPkp2;
									double pkpBawah = pkp - (pkpAtas + pkpTengah3 + pkpTengah2);
									pphTerutang = (pctNpwpToday * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwp2 * tax.getPctPenalti() * pkpTengah2 / 10000) + (pctNpwp3 * tax.getPctPenalti() * pkpTengah3 / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
								}
							}
						}
						bonus.setTax(pphTerutang);
						bonus.setPkpKumulatif(pkpKum);
						bbFcd.updateBonus(bonus, dbConn);
//						bonusToday.setPkpKumulatif(pkpKum);
//						baFcd.updateBonus(bonusToday, dbConn);
//						if (sisaPtkpLalu > 0) {
//							saldoPtkp.setSisaPtkp(sisaPtkp);
//							SaldoPtkpFcd spFcd = new SaldoPtkpFcd();
//							spFcd.updateSaldo(saldoPtkp, dbConn);
//						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void potongPajakOld(DBConnection dbConn) {
		System.out.println("*** di potongPajak");
		String whereClause = "start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
		TaxRateFcd trFcd = new TaxRateFcd();
		TaxRateSet trSet = null;
		try {
//			trSet = trFcd.search(whereClause);
			trSet = trFcd.search(whereClause, dbConn);
			if (trSet.length() < 1)
				throw new Exception("Belum ada setting Rate Pajak yang berlaku.");
			TaxRate tax = trSet.get(0);
			whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "'";
			System.out.println("whereClause untuk BonusB = " + whereClause);
			BonusBFcd bbFcd = new BonusBFcd();
			BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
			MutableDateTime hariHitung = beginOfDay.copy();
			hariHitung.addDays(-1);
//			whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "'";
//			BonusAFcd baFcd = new BonusAFcd();
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonus = bbSet.get(i);
//					SaldoPtkp saldoPtkp = SaldoPtkpFcd.getSaldoPtkpByMemberId(bonus.getMemberId(), dbConn);
//					int sisaPtkp = saldoPtkp.getSisaPtkp();
//					int sisaPtkpLalu = sisaPtkp;
//					whereClause = "member_id = '" + bonus.getMemberId() + "' AND tanggal < '" + beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY tanggal DESC LIMIT 1";
					whereClause = "member_id = '" + bonus.getMemberId() + "' AND bulan < '" + beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
//					BonusASet bakSet = baFcd.search(whereClause, dbConn);
//					BonusA bonusToday = null;
					BonusBSet bakSet = bbFcd.search(whereClause, dbConn);
					BonusB bonusToday = null;
					if (bakSet != null) bonusToday = bakSet.get(0);
//					double totalBonus = bonus.getBonusRo() + bonus.getBonusSi() + bonus.getBonusUs() + bonus.getBonusEarlybird() + bonus.getBonusOr() + bonus.getTraveling() + bonus.getBonusRc();
					double totalBonus = bonus.getBonusRo() + bonus.getBonusSi() + bonus.getBonusUs() + bonus.getBonusEarlybird() + bonus.getBonusOr() + bonus.getBonusRc() - bonus.getPotonganAm();
					double setengahBonus = totalBonus/2;
//					if (totalBonus >= sisaPtkp) {
//						sisaPtkp = 0;
//					} else {
//						sisaPtkp = (int) (sisaPtkp - totalBonus);
//					}
					boolean isNpwp = false;
					Member member = MemberFcd.getMemberById(bonus.getMemberId(), dbConn);
					if (member.getNpwp() != null && !member.getNpwp().equals("")) {
						isNpwp = true;
					}
//					if (!isNpwp) {
//						sisaPtkp = 0;
//					}
//					if (sisaPtkp == 0) {
						double pkp = 0;
//						if (isNpwp) {
//							pkp = totalBonus - sisaPtkpLalu;
//						} else {
//							pkp = totalBonus;
						pkp = setengahBonus;
//						}
						double pkpKumToday = 0;
						if (bonusToday != null) {
							pkpKumToday = bonusToday.getPkpKumulatif();
						}
						double pkpKum = pkpKumToday + pkp;
						double pphTerutang = 0;
						int levelNow, levelToday = 0;
						long batasPkp = 0;
						float pctNpwpToday, pctNpwpNow = 0;
						if (pkpKumToday > tax.getPkpFromLevel4()) {
							levelToday = 4;
							pctNpwpToday = tax.getPctNpwpLevel4();
						} else if (pkpKumToday > tax.getPkpFromLevel3()) {
							levelToday = 3;
							pctNpwpToday = tax.getPctNpwpLevel3();
						} else if (pkpKumToday > tax.getPkpFromLevel2()) {
							levelToday = 2;
							pctNpwpToday = tax.getPctNpwpLevel2();
						} else {
							levelToday = 1;
							pctNpwpToday = tax.getPctNpwpLevel1();
						}
						if (pkpKum > tax.getPkpFromLevel4()) {
							levelNow = 4;
							pctNpwpNow = tax.getPctNpwpLevel4();
							batasPkp = tax.getPkpFromLevel4();
						} else if (pkpKum > tax.getPkpFromLevel3()) {
							levelNow = 3;
							pctNpwpNow = tax.getPctNpwpLevel3();
							batasPkp = tax.getPkpFromLevel3();
						} else if (pkpKum > tax.getPkpFromLevel2()) {
							levelNow = 2;
							pctNpwpNow = tax.getPctNpwpLevel2();
							batasPkp = tax.getPkpFromLevel2();
						} else {
							levelNow = 1;
							pctNpwpNow = tax.getPctNpwpLevel1();
						}
						if (isNpwp) {
							if (levelNow == levelToday) {
								pphTerutang = pctNpwpNow * pkp / 100;
							} else {
								if ((levelNow - levelToday) == 1) {
									double pkpAtas = pkpKum - batasPkp;
									double pkpBawah = pkp - pkpAtas;
									pphTerutang = (pctNpwpToday * pkpBawah / 100) + (pctNpwpNow * pkpAtas / 100);
								} else if ((levelNow - levelToday) == 2) {
									long batasPkp2 = 0;
									float pctNpwpTengah = 0;
									if (levelToday == 1) {
										batasPkp2 = tax.getPkpFromLevel2();
										pctNpwpTengah = tax.getPctNpwpLevel2();
									} else {
										batasPkp2 = tax.getPkpFromLevel3();
										pctNpwpTengah = tax.getPctNpwpLevel3();
									}
									double pkpAtas = pkpKum - batasPkp;
									double pkpTengah = batasPkp - batasPkp2;
									double pkpBawah = pkp - (pkpAtas + pkpTengah);
									pphTerutang = (pctNpwpToday * pkpBawah / 100) + (pctNpwpTengah * pkpTengah / 100) + (pctNpwpNow * pkpAtas / 100);
								}
							}
						} else {
							if (levelNow == levelToday) {
								pphTerutang = pctNpwpNow * tax.getPctPenalti() * pkp / 10000; 
							} else {
								if ((levelNow - levelToday) == 1) {
									double pkpAtas = pkpKum - batasPkp;
									double pkpBawah = pkp - pkpAtas;
									pphTerutang = (pctNpwpToday * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
								} else if ((levelNow - levelToday) == 2) {
									long batasPkp2 = 0;
									float pctNpwpTengah = 0;
									if (levelToday == 1) {
										batasPkp2 = tax.getPkpFromLevel2();
										pctNpwpTengah = tax.getPctNpwpLevel2();
									} else {
										batasPkp2 = tax.getPkpFromLevel3();
										pctNpwpTengah = tax.getPctNpwpLevel3();
									}
									double pkpAtas = pkpKum - batasPkp;
									double pkpTengah = batasPkp - batasPkp2;
									double pkpBawah = pkp - (pkpAtas + pkpTengah);
									pphTerutang = (pctNpwpToday * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwpTengah * tax.getPctPenalti() * pkpTengah / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
								}
							}
						}
						bonus.setTax(pphTerutang);
						bonus.setPkpKumulatif(pkpKum);
						bbFcd.updateBonus(bonus, dbConn);
//						bonusToday.setPkpKumulatif(pkpKum);
//						baFcd.updateBonus(bonusToday, dbConn);
//						if (sisaPtkpLalu > 0) {
//							saldoPtkp.setSisaPtkp(sisaPtkp);
//							SaldoPtkpFcd spFcd = new SaldoPtkpFcd();
//							spFcd.updateSaldo(saldoPtkp, dbConn);
//						}
//					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void potongAm(DBConnection dbConn) {
		String whereClause = "start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
		KursAmFcd kaFcd = new KursAmFcd();
		KursAmSet kaSet = null;
		try {
			kaSet = kaFcd.search(whereClause, dbConn);
			if (kaSet.length() < 1)
				throw new Exception("Belum ada setting Automaintenance yang berlaku.");
			KursAm kAm = kaSet.get(0);
			double potongBulanan = kAm.getMaxPotongBulanan();
			int pctPotong = kAm.getPersenPotongHarian();
			whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "'";
			BonusBFcd bbFcd = new BonusBFcd();
			BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonus = bbSet.get(i);
					MemberTree member = MemberTreeFcd.getMemberTreeById(bonus.getMemberId(), dbConn);
					if (member.getPotonganAm() < potongBulanan) {
//						double totalBonus = bonus.getBonusRo() + bonus.getBonusSi() + bonus.getBonusUs() + bonus.getBonusEarlybird() + bonus.getBonusOr() + bonus.getTraveling() + bonus.getBonusRc() - bonus.getTax();
						double totalBonus = bonus.getBonusRo() + bonus.getBonusSi() + bonus.getBonusUs() + bonus.getBonusEarlybird() + bonus.getBonusOr() + bonus.getBorAntrian() + bonus.getBonusRc();
						double potong = (double)pctPotong / 100 * totalBonus;
						double saldoAm = member.getPotonganAm();
						if ((potong + saldoAm) > potongBulanan) {
							potong = potongBulanan - saldoAm;
						}
						saldoAm += potong;
						bonus.setPotonganAm(potong);
						bbFcd.updateBonus(bonus, dbConn);
						member.setPotonganAm(saldoAm);
						member.setSaldoAm(member.getSaldoAm() + potong);
						float pValue = kAm.getPointValue();
						float bValue = kAm.getBusinessValue();
						float curPv = (float) NumberUtil.roundToDecimals(potong / pValue, 1);
						float curBv = (float) NumberUtil.roundToDecimals((bValue * potong / 100), 0);
						float pvpribadi = member.getPvpribadi() + curPv;
						double saldoBv = member.getSaldoBv() + curBv;
						member.setPvpribadi(pvpribadi);
						member.setSaldoBv(saldoBv);
						MemberTreeFcd mtFcd = new MemberTreeFcd();
						mtFcd.updateMemberTree(member, dbConn);
						PoinRecord poin = new PoinRecord();
						poin.setMemberId(member.getMemberId());
						poin.setPoin(curPv);
						poin.setBv(curBv);
						MutableDateTime hariHitung = beginOfDay.copy();
						hariHitung.addDays(-1);
						poin.setTrxId("AM_B_" + hariHitung.toString("yyyyMMdd"));
						PoinRecordFcd prFcd = new PoinRecordFcd(poin);
						prFcd.insertPoin(dbConn);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getPeringkatMember(DBConnection dbConn) {
		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'";
//		MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//		MemberTreeSet mtSet = null;
		MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
//				MemberTree mtHistory = mtSet.get(i);
				MemberTreeSmall mtHistory = mtSet.get(i);
//				float pvgk = mtHistory.getPvLitfoot();
				float pvgk = mtHistory.getPvKecil();
//				String kualifikasi = KualifikasiMemberChecker.getKualifikasiMember(pvgk);
//				String peringkat = KualifikasiMemberChecker.getPeringkatMember(mtHistory.getMemberId(), pvgk, dbConn);
				KualifikasiMemberChecker checker = new KualifikasiMemberChecker();
				String kualifikasi = checker.getKualifikasiMember(pvgk);
				String peringkat = checker.getPeringkatMember(mtHistory.getMemberId(), pvgk, dbConn);
				System.out.println("Member "+mtHistory.getMemberId()+" dengan PVGK "+pvgk+" berkualifikasi "+kualifikasi);
				mtHistory.setKualifikasi(kualifikasi);
				mtHistory.setPeringkat(peringkat);
				mtFcd.updateMemberTree(mtHistory, dbConn);
				MemberTree member = MemberTreeFcd.getMemberTreeById(mtHistory.getMemberId(), dbConn);
				MemberTreeFcd mtrFcd = new MemberTreeFcd();
				member.setPeringkat(peringkat);
				member.setKualifikasi(kualifikasi);
				mtrFcd.updateMemberTree(member, dbConn);
				mtHistory = null;
				member = null;
				mtrFcd = null;
				checker = null;
			}
			mtFcd = null;
			mtSet = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void prepareTransfer(DBConnection dbConn) {
		BonusBFcd bbFcd = new BonusBFcd();
		try {
			bbFcd.insertTransfer(bulanHitung.toString("yyyy-MM-dd"), dbConn);
//			MutableDateTime hariBonus = beginOfDay.copy();
//			hariBonus.addDays(-1);
//			bbFcd.insertTransfer(hariBonus.toString("yyyy-MM-dd"), dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
