package id.co.birumerah.bsi.bonus.planb;

import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;
import java.util.Date;

public class BonusBLogPesertaFcd {

	private BonusBLogPeserta bblp;
	
	public BonusBLogPesertaFcd() {}

	public BonusBLogPesertaFcd(BonusBLogPeserta bblp) {
		this.bblp = bblp;
	}

	public BonusBLogPeserta getBblp() {
		return bblp;
	}

	public void setBblp(BonusBLogPeserta bblp) {
		this.bblp = bblp;
	}
	
	public static BonusBLogPeserta getBonusLogByMemberBulan(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		try {
			BonusBLogPeserta log = new BonusBLogPeserta();
			log.setBulan(bulan);
			log.setMemberId(memberId);
			
			BonusBLogPesertaDAO bblpDao = new BonusBLogPesertaDAO(dbConn);
			boolean found = bblpDao.select(log);
			
			if (found) {
				return log;
			} else {
				System.err.println("Log bonus bulanan untuk peserta "+memberId+" pada "+bulan+" tidak ditemukan.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertLog(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBLogPesertaDAO bblpDAO = new BonusBLogPesertaDAO(dbConn);
			bblpDAO.insert(bblp);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateLog(BonusBLogPeserta log, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBLogPesertaDAO logDAO = new BonusBLogPesertaDAO(dbConn);
			if (logDAO.update(log) < 1) {
				throw new Exception("Update log gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
