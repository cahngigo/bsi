package id.co.birumerah.bsi.bonus.planb;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusPbrFcd {

	private BonusPbr bonusPbr;
	
	public BonusPbrFcd() {}

	public BonusPbrFcd(BonusPbr bonusPbr) {
		this.bonusPbr = bonusPbr;
	}

	public BonusPbr getBonusPbr() {
		return bonusPbr;
	}

	public void setBonusPbr(BonusPbr bonusPbr) {
		this.bonusPbr = bonusPbr;
	}
	
	public void insert(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusPbrDAO bonusPbrDao = new BonusPbrDAO(dbConn);
			bonusPbrDao.insert(bonusPbr);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void update(BonusPbr bonusPbr, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusPbrDAO pbrDao = new BonusPbrDAO(dbConn);
			if (pbrDao.update(bonusPbr) < 1) {
				throw new Exception("Update BonusPbr failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public static BonusPbr getBonusPbrByMemberAndMonth(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		try {
			BonusPbr bonusPbr = new BonusPbr();
			bonusPbr.setMemberId(memberId);
			bonusPbr.setBulan(bulan);
			
			BonusPbrDAO pbrDao = new BonusPbrDAO(dbConn);
			boolean found = pbrDao.select(bonusPbr);
			
			if (found) {
				return bonusPbr;
			} else {
				System.err.println("BonusPBR bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
		return null;
	}
}
