package id.co.birumerah.bsi.bonus.planb;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class AkumulasiPoinTourFcd {

	private AkumulasiPoinTour poinTour;
	
	public AkumulasiPoinTourFcd() {}

	public AkumulasiPoinTourFcd(AkumulasiPoinTour poinTour) {
		this.poinTour = poinTour;
	}

	public AkumulasiPoinTour getPoinTour() {
		return poinTour;
	}

	public void setPoinTour(AkumulasiPoinTour poinTour) {
		this.poinTour = poinTour;
	}
	
	public AkumulasiPoinTourSet search(String whereClause, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourSet aptSet = null;
		
		try {
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			aptSet = aptDao.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return aptSet;
	}
	
	public static AkumulasiPoinTour getAkumulasiByMemberAndMonth(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		try {
			AkumulasiPoinTour akumPoin = new AkumulasiPoinTour();
			akumPoin.setMemberId(memberId);
			akumPoin.setBulan(bulan);
			
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			boolean found = aptDao.select(akumPoin);
			
			if (found) {
				return akumPoin;
			} else {
				System.err.println("Akumulasi poin bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertAkumulasi(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			aptDao.insert(poinTour);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateAkumulasi(AkumulasiPoinTour dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			if (aptDao.update(dataObject) < 1) {
				throw new Exception("Updating Akumulasi Point Tour failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public AkumulasiPoinTourSet getMemberPass(Date bulan, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourSet aptSet = null;
		
		try {
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			aptSet = aptDao.getMemberPass(bulan);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return aptSet;
	}
	
	public AkumulasiPoinTour getMemberPassByMember(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		AkumulasiPoinTour apt = null;
		
		try {
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			apt = aptDao.getMemberPassByMember(memberId, bulan);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return apt;
	}
	
	public AkumulasiPoinTourSet getWrongAkumulasiPv(Date bulan, DBConnection dbConn) throws Exception {
		AkumulasiPoinTourSet aptSet = null;
		
		try {
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			aptSet = aptDao.getWrongAkumulasiPv(bulan);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return aptSet;
	}
	
	public boolean searchAkumulasiByMemberAndBlockBy(String memberId, String blockBy, DBConnection dbConn) throws Exception {
		boolean found = false;
		try {
			AkumulasiPoinTourDAO aptDao = new AkumulasiPoinTourDAO(dbConn);
			found = aptDao.searchAkumulasiByMemberAndBlockBy(memberId, blockBy);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return found;
	}
}
