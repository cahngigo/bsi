package id.co.birumerah.bsi.bonus.planb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusBDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusBDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusB dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_b (");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("bonus_ro,");
		sb.append("bonus_si,");
		sb.append("bonus_us,");
		sb.append("bonus_earlybird,");
		sb.append("bonus_or,");
		sb.append("bor_antrian,");
		sb.append("traveling,");
		sb.append("bonus_rc,");
		sb.append("bor_akumgk,");
		sb.append("bor_tingkat,");
		sb.append("bor_isqualified,");
		sb.append("bor_isclaimed,");
		sb.append("brc_isqualified_cd,");
		sb.append("brc_continuity_cd,");
		sb.append("brc_isqualified_scd,");
		sb.append("brc_continuity_scd,");
		sb.append("bwt_akum,");
		sb.append("potongan_am,");
		sb.append("ldp_s1_continuity,");
		sb.append("ldp_s1_isqualified,");
		sb.append("ldp_s2_continuity,");
		sb.append("ldp_s2_isqualified,");
		sb.append("ldp_s3_continuity,");
		sb.append("ldp_s3_isqualified,");
		sb.append("ldp_s4_continuity,");
		sb.append("ldp_s4_isqualified,");
		sb.append("pkp_kumulatif,");
		sb.append("tax");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getBulan()) + "',");
		sb.append("" + dataObject.getBonusRo() + ",");
		sb.append("" + dataObject.getBonusSi() + ",");
		sb.append("" + dataObject.getBonusUs() + ",");
		sb.append("" + dataObject.getBonusEarlybird() + ",");
		sb.append("" + dataObject.getBonusOr() + ",");
		sb.append("" + dataObject.getBorAntrian() + ",");
		sb.append("" + dataObject.getTraveling() + ",");
		sb.append("" + dataObject.getBonusRc() + ",");
		sb.append("" + dataObject.getBorAkumgk() + ",");
		sb.append("" + dataObject.getBorTingkat() + ",");
		sb.append("" + dataObject.getBorIsqualified() + ",");
		sb.append("" + dataObject.getBorIsclaimed() + ",");
		sb.append("" + dataObject.getBrcIsqualifiedCd() + ",");
		sb.append("" + dataObject.getBrcContinuityCd() + ",");
		sb.append("" + dataObject.getBrcIsqualifiedScd() + ",");
		sb.append("" + dataObject.getBrcContinuityScd() + ",");
		sb.append("" + dataObject.getBwtAkum() + ",");
		sb.append("" + dataObject.getPotonganAm() + ",");
		sb.append("" + dataObject.getLdpS1Continuity() + ",");
		sb.append("" + dataObject.getLdpS1Isqualified() + ",");
		sb.append("" + dataObject.getLdpS2Continuity() + ",");
		sb.append("" + dataObject.getLdpS2Isqualified() + ",");
		sb.append("" + dataObject.getLdpS3Continuity() + ",");
		sb.append("" + dataObject.getLdpS3Isqualified() + ",");
		sb.append("" + dataObject.getLdpS4Continuity() + ",");
		sb.append("" + dataObject.getLdpS4Isqualified() + ",");
		sb.append("" + dataObject.getPkpKumulatif() + ",");
		sb.append("" + dataObject.getTax());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusB dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_b ");
		sb.append("SET ");
		sb.append("bonus_ro = " + dataObject.getBonusRo() + ",");
		sb.append("bonus_si = " + dataObject.getBonusSi() + ",");
		sb.append("bonus_us = " + dataObject.getBonusUs() + ",");
		sb.append("bonus_earlybird = " + dataObject.getBonusEarlybird() + ",");
		sb.append("bonus_or = " + dataObject.getBonusOr() + ",");
		sb.append("bor_antrian = " + dataObject.getBorAntrian() + ",");
		sb.append("traveling = " + dataObject.getTraveling() + ",");
		sb.append("bonus_rc = " + dataObject.getBonusRc() + ",");
		sb.append("bor_akumgk = " + dataObject.getBorAkumgk() + ",");
		sb.append("bor_tingkat = " + dataObject.getBorTingkat() + ",");
		sb.append("bor_isqualified = " + dataObject.getBorIsqualified() + ",");
		sb.append("bor_isclaimed = " + dataObject.getBorIsclaimed() + ",");
		sb.append("brc_isqualified_cd = " + dataObject.getBrcIsqualifiedCd() + ",");
		sb.append("brc_continuity_cd = " + dataObject.getBrcContinuityCd() + ",");
		sb.append("brc_isqualified_scd = " + dataObject.getBrcIsqualifiedScd() + ",");
		sb.append("brc_continuity_scd = " + dataObject.getBrcContinuityScd() + ",");
		sb.append("bwt_akum = " + dataObject.getBwtAkum() + ",");
		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
		sb.append("ldp_s1_continuity = " + dataObject.getLdpS1Continuity() + ",");
		sb.append("ldp_s1_isqualified = " + dataObject.getLdpS1Isqualified() + ",");
		sb.append("ldp_s2_continuity = " + dataObject.getLdpS2Continuity() + ",");
		sb.append("ldp_s2_isqualified = " + dataObject.getLdpS2Isqualified() + ",");
		sb.append("ldp_s3_continuity = " + dataObject.getLdpS3Continuity() + ",");
		sb.append("ldp_s3_isqualified = " + dataObject.getLdpS3Isqualified() + ",");
		sb.append("ldp_s4_continuity = " + dataObject.getLdpS4Continuity() + ",");
		sb.append("ldp_s4_isqualified = " + dataObject.getLdpS4Isqualified() + ",");
		sb.append("pkp_kumulatif = " + dataObject.getPkpKumulatif() + ",");
		sb.append("tax = " + dataObject.getTax() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(BonusB dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM bonus_b WHERE ");
		sb.append("member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusB dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("bonus_ro,");
		sb.append("bonus_si,");
		sb.append("bonus_us,");
		sb.append("bonus_earlybird,");
		sb.append("bonus_or,");
		sb.append("bor_antrian,");
		sb.append("traveling,");
		sb.append("bonus_rc,");
		sb.append("bor_akumgk,");
		sb.append("bor_tingkat,");
		sb.append("bor_isqualified,");
		sb.append("bor_isclaimed,");
		sb.append("brc_isqualified_cd,");
		sb.append("brc_continuity_cd,");
		sb.append("brc_isqualified_scd,");
		sb.append("brc_continuity_scd,");
		sb.append("bwt_akum,");
		sb.append("potongan_am,");
		sb.append("ldp_s1_continuity,");
		sb.append("ldp_s1_isqualified,");
		sb.append("ldp_s2_continuity,");
		sb.append("ldp_s2_isqualified,");
		sb.append("ldp_s3_continuity,");
		sb.append("ldp_s3_isqualified,");
		sb.append("ldp_s4_continuity,");
		sb.append("ldp_s4_isqualified,");
		sb.append("pkp_kumulatif,");
		sb.append("tax ");
		sb.append("FROM bonus_b ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBonusRo(rs.getDouble("bonus_ro"));
				dataObject.setBonusSi(rs.getDouble("bonus_si"));
				dataObject.setBonusUs(rs.getDouble("bonus_us"));
				dataObject.setBonusEarlybird(rs.getDouble("bonus_earlybird"));
				dataObject.setBonusOr(rs.getDouble("bonus_or"));
				dataObject.setBorAntrian(rs.getDouble("bor_antrian"));
				dataObject.setTraveling(rs.getDouble("traveling"));
				dataObject.setBonusRc(rs.getDouble("bonus_rc"));
				dataObject.setBorAkumgk(rs.getFloat("bor_akumgk"));
				dataObject.setBorTingkat(rs.getInt("bor_tingkat"));
				dataObject.setBorIsqualified(rs.getInt("bor_isqualified"));
				dataObject.setBorIsclaimed(rs.getInt("bor_isclaimed"));
				dataObject.setBrcIsqualifiedCd(rs.getInt("brc_isqualified_cd"));
				dataObject.setBrcContinuityCd(rs.getInt("brc_continuity_cd"));
				dataObject.setBrcIsqualifiedScd(rs.getInt("brc_isqualified_scd"));
				dataObject.setBrcContinuityScd(rs.getInt("brc_continuity_scd"));
				dataObject.setBwtAkum(rs.getDouble("bwt_akum"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setLdpS1Continuity(rs.getInt("ldp_s1_continuity"));
				dataObject.setLdpS1Isqualified(rs.getInt("ldp_s1_isqualified"));
				dataObject.setLdpS2Continuity(rs.getInt("ldp_s2_continuity"));
				dataObject.setLdpS2Isqualified(rs.getInt("ldp_s2_isqualified"));
				dataObject.setLdpS3Continuity(rs.getInt("ldp_s3_continuity"));
				dataObject.setLdpS3Isqualified(rs.getInt("ldp_s3_isqualified"));
				dataObject.setLdpS4Continuity(rs.getInt("ldp_s4_continuity"));
				dataObject.setLdpS4Isqualified(rs.getInt("ldp_s4_isqualified"));
				dataObject.setPkpKumulatif(rs.getDouble("pkp_kumulatif"));
				dataObject.setTax(rs.getDouble("tax"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BonusBSet select(String whereClause) throws SQLException {
		BonusBSet dataObjectSet = new BonusBSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("bonus_ro,");
		sb.append("bonus_si,");
		sb.append("bonus_us,");
		sb.append("bonus_earlybird,");
		sb.append("bonus_or,");
		sb.append("bor_antrian,");
		sb.append("traveling,");
		sb.append("bonus_rc,");
		sb.append("bor_akumgk,");
		sb.append("bor_tingkat,");
		sb.append("bor_isqualified,");
		sb.append("bor_isclaimed,");
		sb.append("brc_isqualified_cd,");
		sb.append("brc_continuity_cd,");
		sb.append("brc_isqualified_scd,");
		sb.append("brc_continuity_scd,");
		sb.append("bwt_akum,");
		sb.append("potongan_am,");
		sb.append("ldp_s1_continuity,");
		sb.append("ldp_s1_isqualified,");
		sb.append("ldp_s2_continuity,");
		sb.append("ldp_s2_isqualified,");
		sb.append("ldp_s3_continuity,");
		sb.append("ldp_s3_isqualified,");
		sb.append("ldp_s4_continuity,");
		sb.append("ldp_s4_isqualified,");
		sb.append("pkp_kumulatif,");
		sb.append("tax ");
		sb.append("FROM bonus_b ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BonusB dataObject = new BonusB();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setBulan(rs.getDate("bulan"));
				dataObject.setBonusRo(rs.getDouble("bonus_ro"));
				dataObject.setBonusSi(rs.getDouble("bonus_si"));
				dataObject.setBonusUs(rs.getDouble("bonus_us"));
				dataObject.setBonusEarlybird(rs.getDouble("bonus_earlybird"));
				dataObject.setBonusOr(rs.getDouble("bonus_or"));
				dataObject.setBorAntrian(rs.getDouble("bor_antrian"));
				dataObject.setTraveling(rs.getDouble("traveling"));
				dataObject.setBonusRc(rs.getDouble("bonus_rc"));
				dataObject.setBorAkumgk(rs.getFloat("bor_akumgk"));
				dataObject.setBorTingkat(rs.getInt("bor_tingkat"));
				dataObject.setBorIsqualified(rs.getInt("bor_isqualified"));
				dataObject.setBorIsclaimed(rs.getInt("bor_isclaimed"));
				dataObject.setBrcIsqualifiedCd(rs.getInt("brc_isqualified_cd"));
				dataObject.setBrcContinuityCd(rs.getInt("brc_continuity_cd"));
				dataObject.setBrcIsqualifiedScd(rs.getInt("brc_isqualified_scd"));
				dataObject.setBrcContinuityScd(rs.getInt("brc_continuity_scd"));
				dataObject.setBwtAkum(rs.getDouble("bwt_akum"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setLdpS1Continuity(rs.getInt("ldp_s1_continuity"));
				dataObject.setLdpS1Isqualified(rs.getInt("ldp_s1_isqualified"));
				dataObject.setLdpS2Continuity(rs.getInt("ldp_s2_continuity"));
				dataObject.setLdpS2Isqualified(rs.getInt("ldp_s2_isqualified"));
				dataObject.setLdpS3Continuity(rs.getInt("ldp_s3_continuity"));
				dataObject.setLdpS3Isqualified(rs.getInt("ldp_s3_isqualified"));
				dataObject.setLdpS4Continuity(rs.getInt("ldp_s4_continuity"));
				dataObject.setLdpS4Isqualified(rs.getInt("ldp_s4_isqualified"));
				dataObject.setPkpKumulatif(rs.getDouble("pkp_kumulatif"));
				dataObject.setTax(rs.getDouble("tax"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int insertBonusTransfer(String tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("CALL filltransfer_b('" + tanggal + "')");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public double getTotalBonusByMember(String memberId, Date bulan) throws SQLException {
		double total = 0;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT (bb.bonus_or + bb.bonus_si + bb.bonus_us + bb.bonus_earlybird) total_bonus ");
		sb.append("FROM bonus_b bb ");
		sb.append("WHERE bulan = '" + shortFormat.format(bulan) + "' ");
		sb.append("AND bb.member_id = '" + memberId + "'");
		System.out.println("QUERY: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			if (rs.next()) {
				total = rs.getDouble("total_bonus");
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return total;
	}
}
