package id.co.birumerah.bsi.bonus.planb;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusLdpFcd {

	private BonusLdp bonusLdp;
	
	public BonusLdpFcd() {}
	
	public BonusLdpFcd(BonusLdp bonusLdp) {
		this.bonusLdp = bonusLdp;
	}

	public BonusLdp getBonusLdp() {
		return bonusLdp;
	}

	public void setBonusLdp(BonusLdp bonusLdp) {
		this.bonusLdp = bonusLdp;
	}
	
	public void insert(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusLdpDAO ldpDao = new BonusLdpDAO(dbConn);
			ldpDao.insert(bonusLdp);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void update(BonusLdp bonusLdp, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusLdpDAO ldpDao = new BonusLdpDAO(dbConn);
			if (ldpDao.update(bonusLdp) < 1) {
				throw new Exception("Update Bonus LDP failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public double getAkumulasiByMemberAndType(String memberId, String tipe, DBConnection dbConn) throws Exception {
		double akum = 0;
		
		BonusLdpDAO ldpDao = new BonusLdpDAO(dbConn);
		akum = ldpDao.getAkumulasiByMemberAndType(memberId, tipe);
		
		return akum;
	}
}
