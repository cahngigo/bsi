package id.co.birumerah.bsi.bonus.planb;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SettingBonusBSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public SettingBonusBSet() {
		this.set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(SettingBonusB dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public SettingBonusB get(int index) {
		SettingBonusB result = null;
		
		if ((index >= 0) && (index < length()))
			result = (SettingBonusB) set.get(index);
		
		return result;
	}
}
