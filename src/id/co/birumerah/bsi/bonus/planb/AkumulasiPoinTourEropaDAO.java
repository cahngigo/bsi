package id.co.birumerah.bsi.bonus.planb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class AkumulasiPoinTourEropaDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	DateFormat longFormat;
	
	public AkumulasiPoinTourEropaDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		this.longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int insert(AkumulasiPoinTourEropa dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO akumulasi_poin_tour_eropa (");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("reg_date,");
		sb.append("pv_group,");
		sb.append("akum_kiri,");
		sb.append("akum_tengah,");
		sb.append("akum_kanan,");
		sb.append("akum_pv,");
		sb.append("poin_broken,");
		sb.append("broken_by_kiri,");
		sb.append("broken_by_tengah,");
		sb.append("broken_by_kanan");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getBulan()) + "',");
		sb.append("'" + longFormat.format(dataObject.getRegDate()) + "',");
		sb.append("" + dataObject.getPvGroup() + ",");
		sb.append("" + dataObject.getAkumKiri() + ",");
		sb.append("" + dataObject.getAkumTengah() + ",");
		sb.append("" + dataObject.getAkumKanan() + ",");
		sb.append("" + dataObject.getAkumPv() + ",");
		sb.append("" + dataObject.getPoinBroken() + ",");
		sb.append("'" + dataObject.getBrokenByKiri() + "',");
		sb.append("'" + dataObject.getBrokenByTengah() + "',");
		sb.append("'" + dataObject.getBrokenByKanan() + "'");
		sb.append(")");
		System.out.println("Query Insert : " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(AkumulasiPoinTourEropa dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE akumulasi_poin_tour_eropa SET ");
		sb.append("pv_group = " + dataObject.getPvGroup() + ",");
		sb.append("akum_kiri = " + dataObject.getAkumKiri() + ",");
		sb.append("akum_tengah = " + dataObject.getAkumTengah() + ",");
		sb.append("akum_kanan = " + dataObject.getAkumKanan() + ",");
		sb.append("akum_pv = " + dataObject.getAkumPv() + ",");
		sb.append("poin_broken = " + dataObject.getPoinBroken() + ",");
		sb.append("broken_by_kiri = '" + dataObject.getBrokenByKiri() + "', ");
		sb.append("broken_by_tengah = '" + dataObject.getBrokenByTengah() + "', ");
		sb.append("broken_by_kanan = '" + dataObject.getBrokenByKanan() + "' ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		System.out.println("Query Update : " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(AkumulasiPoinTourEropa dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("reg_date,");
		sb.append("pv_group,");
		sb.append("akum_kiri,");
		sb.append("akum_tengah,");
		sb.append("akum_kanan,");
		sb.append("akum_pv,");
		sb.append("poin_broken,");
		sb.append("broken_by_kiri,");
		sb.append("broken_by_tengah,");
		sb.append("broken_by_kanan ");
		sb.append("FROM akumulasi_poin_tour_eropa ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvGroup(rs.getFloat("pv_group"));
				dataObject.setAkumKiri(rs.getFloat("akum_kiri"));
				dataObject.setAkumTengah(rs.getFloat("akum_tengah"));
				dataObject.setAkumKanan(rs.getFloat("akum_kanan"));
				dataObject.setAkumPv(rs.getFloat("akum_pv"));
				dataObject.setPoinBroken(rs.getFloat("poin_broken"));
				dataObject.setBrokenByKiri(rs.getString("broken_by_kiri"));
				dataObject.setBrokenByTengah(rs.getString("broken_by_tengah"));
				dataObject.setBrokenByKanan(rs.getString("broken_by_kanan"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public AkumulasiPoinTourEropaSet select(String whereClause) throws SQLException {
		AkumulasiPoinTourEropaSet dataObjectSet = new AkumulasiPoinTourEropaSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("reg_date,");
		sb.append("pv_group,");
		sb.append("akum_kiri,");
		sb.append("akum_tengah,");
		sb.append("akum_kanan,");
		sb.append("akum_pv,");
		sb.append("poin_broken,");
		sb.append("broken_by_kiri,");
		sb.append("broken_by_tengah,");
		sb.append("broken_by_kanan ");
		sb.append("FROM akumulasi_poin_tour_eropa ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				AkumulasiPoinTourEropa dataObject = new AkumulasiPoinTourEropa();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setBulan(rs.getDate("bulan"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvGroup(rs.getFloat("pv_group"));
				dataObject.setAkumKiri(rs.getFloat("akum_kiri"));
				dataObject.setAkumTengah(rs.getFloat("akum_tengah"));
				dataObject.setAkumKanan(rs.getFloat("akum_kanan"));
				dataObject.setAkumPv(rs.getFloat("akum_pv"));
				dataObject.setPoinBroken(rs.getFloat("poin_broken"));
				dataObject.setBrokenByKiri(rs.getString("broken_by_kiri"));
				dataObject.setBrokenByTengah(rs.getString("broken_by_tengah"));
				dataObject.setBrokenByKanan(rs.getString("broken_by_kanan"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public AkumulasiPoinTourEropaSet getMemberPass(Date bulan) throws SQLException {
		AkumulasiPoinTourEropaSet dataObjectSet = new AkumulasiPoinTourEropaSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT * FROM ( ");
		sb.append("SELECT apt.member_id, reg_date, SUM(pv_group) pv_group, 0 akum_kiri, 0 akum_tengah, 0 akum_kanan, 0 akum_pv, 0 poin_broken, '' broken_by_kiri, '' broken_by_tengah, '' broken_by_kanan ");
		sb.append("FROM akumulasi_poin_tour_eropa apt, ");
		sb.append("(SELECT member_id FROM akumulasi_poin_tour_eropa WHERE bulan='" + shortFormat.format(bulan) + "') apt2 ");
		sb.append("WHERE apt.member_id=apt2.member_id ");
		sb.append("GROUP BY apt.member_id, reg_date) tes ");
		sb.append("WHERE (pv_group-poin_broken) >= 200000 ");
		sb.append("ORDER BY reg_date DESC");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				AkumulasiPoinTourEropa dataObject = new AkumulasiPoinTourEropa();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setBulan(bulan);
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvGroup(rs.getFloat("pv_group"));
				dataObject.setAkumKiri(rs.getFloat("akum_kiri"));
				dataObject.setAkumTengah(rs.getFloat("akum_tengah"));
				dataObject.setAkumKanan(rs.getFloat("akum_kanan"));
				dataObject.setAkumPv(rs.getFloat("akum_pv"));
				dataObject.setPoinBroken(rs.getFloat("poin_broken"));
				dataObject.setBrokenByKiri(rs.getString("broken_by_kiri"));
				dataObject.setBrokenByTengah(rs.getString("broken_by_tengah"));
				dataObject.setBrokenByKanan(rs.getString("broken_by_kanan"));
				
				//passMember.put(dataObject.getMemberId(), dataObject.getMemberId());
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public AkumulasiPoinTourEropa getMemberPassByMember(String memberId, Date bulan) throws SQLException {
		AkumulasiPoinTourEropa dataObject = null;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT * FROM ( ");
		sb.append("SELECT apt.member_id, reg_date, SUM(pv_group) pv_group, 0 akum_kiri, 0 akum_tengah, 0 akum_kanan, 0 akum_pv, 0 poin_broken, '' broken_by_kiri, '' broken_by_tengah, '' broken_by_kanan ");
		sb.append("FROM akumulasi_poin_tour_eropa apt, ");
		sb.append("(SELECT member_id FROM akumulasi_poin_tour_eropa WHERE bulan='" + shortFormat.format(bulan) + "') apt2 ");
		sb.append("WHERE apt.member_id=apt2.member_id AND apt.member_id='" + memberId + "' ");
		sb.append("GROUP BY apt.member_id, reg_date) tes ");
		sb.append("WHERE (pv_group-poin_broken) >= 200000 ");
		sb.append("ORDER BY reg_date DESC");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			if (rs.next()) {
				dataObject = new AkumulasiPoinTourEropa();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setBulan(bulan);
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvGroup(rs.getFloat("pv_group"));
				dataObject.setAkumKiri(rs.getFloat("akum_kiri"));
				dataObject.setAkumTengah(rs.getFloat("akum_tengah"));
				dataObject.setAkumKanan(rs.getFloat("akum_kanan"));
				dataObject.setAkumPv(rs.getFloat("akum_pv"));
				dataObject.setPoinBroken(rs.getFloat("poin_broken"));
				dataObject.setBrokenByKiri(rs.getString("broken_by_kiri"));
				dataObject.setBrokenByTengah(rs.getString("broken_by_tengah"));
				dataObject.setBrokenByKanan(rs.getString("broken_by_kanan"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObject;
	}
	
	public AkumulasiPoinTourEropaSet getWrongAkumulasiPv(Date bulan) throws SQLException {
		AkumulasiPoinTourEropaSet dataObjectSet = new AkumulasiPoinTourEropaSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT apt.member_id, apt.bulan, apt.reg_date, apt2.pv_group, apt.akum_kiri, ");
		sb.append("apt.akum_tengah, apt.akum_kanan, apt.akum_pv, apt.poin_broken, ");
		sb.append("apt.broken_by_kiri, apt.broken_by_tengah, apt.broken_by_kanan ");
		sb.append("FROM akumulasi_poin_tour_eropa apt, ");
		sb.append("(SELECT apt2.member_id, SUM(apt2.pv_group) pv_group ");
		sb.append("FROM akumulasi_poin_tour_eropa apt2 ");
		sb.append("WHERE apt2.bulan <= '" + shortFormat.format(bulan) + "' ");
		sb.append("GROUP BY apt2.member_id) apt2 ");
		sb.append("WHERE  apt.member_id=apt2.member_id AND ");
		sb.append("apt2.pv_group - apt.poin_broken <> apt.akum_pv ");
		sb.append("AND apt.bulan = '" + shortFormat.format(bulan) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				AkumulasiPoinTourEropa dataObject = new AkumulasiPoinTourEropa();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setBulan(bulan);
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvGroup(rs.getFloat("pv_group"));
				dataObject.setAkumKiri(rs.getFloat("akum_kiri"));
				dataObject.setAkumTengah(rs.getFloat("akum_tengah"));
				dataObject.setAkumKanan(rs.getFloat("akum_kanan"));
				dataObject.setAkumPv(rs.getFloat("akum_pv"));
				dataObject.setPoinBroken(rs.getFloat("poin_broken"));
				dataObject.setBrokenByKiri(rs.getString("broken_by_kiri"));
				dataObject.setBrokenByTengah(rs.getString("broken_by_tengah"));
				dataObject.setBrokenByKanan(rs.getString("broken_by_kanan"));
				
				//passMember.put(dataObject.getMemberId(), dataObject.getMemberId());
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public boolean searchAkumulasiByMemberAndBlockBy(String memberId, String blockBy) throws SQLException {
		boolean found = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT member_id, bulan, reg_date, pv_group, akum_kiri, akum_tengah, akum_kanan, akum_pv, poin_broken, broken_by_kiri, broken_by_tengah, broken_by_kanan ");
		sb.append("FROM akumulasi_poin_tour_eropa ");
		sb.append("WHERE member_id = '" + memberId + "' ");
		sb.append("AND broken_by LIKE '%" + blockBy + "%' ");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			if (rs.next()) {
				found = true;
			}
		} catch (SQLException e) {
			throw e;
		}
		return found;
	}
	
	public int deleteAkumulasiPoinByBulan(Date bulan) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM akumulasi_poin_tour_eropa WHERE bulan = '" + shortFormat.format(bulan) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
