package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class BonusLdp {
	
	private long id;
	private String memberId;
//	private Date periodeS1;
//	private double bonusS1;
//	private double akumS1;
//	private Date periodeS2;
//	private int isSisadanaS1;
//	private double bonusS2;
//	private double akumS2;
//	private Date periodeS3;
//	private int isSisadanaS2;
//	private double bonusS3;
//	private double akumS3;
//	private Date periodeS4;
//	private int isSisadanaS3;
//	private double bonusS4;
//	private double akumS4;
	private String tipe;
	private Date periode;
	private int isSisadana;
	private double bonus;
	private double akum;
	
	public BonusLdp() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public Date getPeriode() {
		return periode;
	}

	public void setPeriode(Date periode) {
		this.periode = periode;
	}

	public int getIsSisadana() {
		return isSisadana;
	}

	public void setIsSisadana(int isSisadana) {
		this.isSisadana = isSisadana;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	public double getAkum() {
		return akum;
	}

	public void setAkum(double akum) {
		this.akum = akum;
	}

//	public Date getPeriodeS1() {
//		return periodeS1;
//	}
//
//	public void setPeriodeS1(Date periodeS1) {
//		this.periodeS1 = periodeS1;
//	}
//
//	public double getBonusS1() {
//		return bonusS1;
//	}
//
//	public void setBonusS1(double bonusS1) {
//		this.bonusS1 = bonusS1;
//	}
//
//	public double getAkumS1() {
//		return akumS1;
//	}
//
//	public void setAkumS1(double akumS1) {
//		this.akumS1 = akumS1;
//	}
//
//	public Date getPeriodeS2() {
//		return periodeS2;
//	}
//
//	public void setPeriodeS2(Date periodeS2) {
//		this.periodeS2 = periodeS2;
//	}
//
//	public int getIsSisadanaS1() {
//		return isSisadanaS1;
//	}
//
//	public void setIsSisadanaS1(int isSisadanaS1) {
//		this.isSisadanaS1 = isSisadanaS1;
//	}
//
//	public double getBonusS2() {
//		return bonusS2;
//	}
//
//	public void setBonusS2(double bonusS2) {
//		this.bonusS2 = bonusS2;
//	}
//
//	public double getAkumS2() {
//		return akumS2;
//	}
//
//	public void setAkumS2(double akumS2) {
//		this.akumS2 = akumS2;
//	}
//
//	public Date getPeriodeS3() {
//		return periodeS3;
//	}
//
//	public void setPeriodeS3(Date periodeS3) {
//		this.periodeS3 = periodeS3;
//	}
//
//	public int getIsSisadanaS2() {
//		return isSisadanaS2;
//	}
//
//	public void setIsSisadanaS2(int isSisadanaS2) {
//		this.isSisadanaS2 = isSisadanaS2;
//	}
//
//	public double getBonusS3() {
//		return bonusS3;
//	}
//
//	public void setBonusS3(double bonusS3) {
//		this.bonusS3 = bonusS3;
//	}
//
//	public double getAkumS3() {
//		return akumS3;
//	}
//
//	public void setAkumS3(double akumS3) {
//		this.akumS3 = akumS3;
//	}
//
//	public Date getPeriodeS4() {
//		return periodeS4;
//	}
//
//	public void setPeriodeS4(Date periodeS4) {
//		this.periodeS4 = periodeS4;
//	}
//
//	public int getIsSisadanaS3() {
//		return isSisadanaS3;
//	}
//
//	public void setIsSisadanaS3(int isSisadanaS3) {
//		this.isSisadanaS3 = isSisadanaS3;
//	}
//
//	public double getBonusS4() {
//		return bonusS4;
//	}
//
//	public void setBonusS4(double bonusS4) {
//		this.bonusS4 = bonusS4;
//	}
//
//	public double getAkumS4() {
//		return akumS4;
//	}
//
//	public void setAkumS4(double akumS4) {
//		this.akumS4 = akumS4;
//	}
}
