package id.co.birumerah.bsi.bonus.planb;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class BonusBTempDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusBTempDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusB dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_b_temp (");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("bonus_ro,");
		sb.append("bonus_si,");
		sb.append("bonus_us,");
		sb.append("bonus_earlybird,");
		sb.append("bonus_or,");
		sb.append("bor_antrian,");
		sb.append("traveling,");
		sb.append("bonus_rc,");
		sb.append("bor_akumgk,");
		sb.append("bor_tingkat,");
		sb.append("bor_isqualified,");
		sb.append("bor_isclaimed,");
		sb.append("brc_isqualified_cd,");
		sb.append("brc_continuity_cd,");
		sb.append("brc_isqualified_scd,");
		sb.append("brc_continuity_scd,");
		sb.append("bwt_akum,");
		sb.append("potongan_am,");
		sb.append("tax");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getBulan()) + "',");
		sb.append("" + dataObject.getBonusRo() + ",");
		sb.append("" + dataObject.getBonusSi() + ",");
		sb.append("" + dataObject.getBonusUs() + ",");
		sb.append("" + dataObject.getBonusEarlybird() + ",");
		sb.append("" + dataObject.getBonusOr() + ",");
		sb.append("" + dataObject.getBorAntrian() + ",");
		sb.append("" + dataObject.getTraveling() + ",");
		sb.append("" + dataObject.getBonusRc() + ",");
		sb.append("" + dataObject.getBorAkumgk() + ",");
		sb.append("" + dataObject.getBorTingkat() + ",");
		sb.append("" + dataObject.getBorIsqualified() + ",");
		sb.append("" + dataObject.getBorIsclaimed() + ",");
		sb.append("" + dataObject.getBrcIsqualifiedCd() + ",");
		sb.append("" + dataObject.getBrcContinuityCd() + ",");
		sb.append("" + dataObject.getBrcIsqualifiedScd() + ",");
		sb.append("" + dataObject.getBrcContinuityScd() + ",");
		sb.append("" + dataObject.getBwtAkum() + ",");
		sb.append("" + dataObject.getPotonganAm() + ",");
		sb.append("" + dataObject.getTax());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusB dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_b_temp ");
		sb.append("SET ");
		sb.append("bonus_ro = " + dataObject.getBonusRo() + ",");
		sb.append("bonus_si = " + dataObject.getBonusSi() + ",");
		sb.append("bonus_us = " + dataObject.getBonusUs() + ",");
		sb.append("bonus_earlybird = " + dataObject.getBonusEarlybird() + ",");
		sb.append("bonus_or = " + dataObject.getBonusOr() + ",");
		sb.append("bor_antrian = " + dataObject.getBorAntrian() + ",");
		sb.append("traveling = " + dataObject.getTraveling() + ",");
		sb.append("bonus_rc = " + dataObject.getBonusRc() + ",");
		sb.append("bor_akumgk = " + dataObject.getBorAkumgk() + ",");
		sb.append("bor_tingkat = " + dataObject.getBorTingkat() + ",");
		sb.append("bor_isqualified = " + dataObject.getBorIsqualified() + ",");
		sb.append("bor_isclaimed = " + dataObject.getBorIsclaimed() + ",");
		sb.append("brc_isqualified_cd = " + dataObject.getBrcIsqualifiedCd() + ",");
		sb.append("brc_continuity_cd = " + dataObject.getBrcContinuityCd() + ",");
		sb.append("brc_isqualified_scd = " + dataObject.getBrcIsqualifiedScd() + ",");
		sb.append("brc_continuity_scd = " + dataObject.getBrcContinuityScd() + ",");
		sb.append("bwt_akum = " + dataObject.getBwtAkum() + ",");
		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
		sb.append("tax = " + dataObject.getTax() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
//		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(BonusB dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM bonus_b_temp WHERE ");
		sb.append("member_id = '" + dataObject.getMemberId() + "'");
//		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusB dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("bonus_ro,");
		sb.append("bonus_si,");
		sb.append("bonus_us,");
		sb.append("bonus_earlybird,");
		sb.append("bonus_or,");
		sb.append("bor_antrian,");
		sb.append("traveling,");
		sb.append("bonus_rc,");
		sb.append("bor_akumgk,");
		sb.append("bor_tingkat,");
		sb.append("bor_isqualified,");
		sb.append("bor_isclaimed,");
		sb.append("brc_isqualified_cd,");
		sb.append("brc_continuity_cd,");
		sb.append("brc_isqualified_scd,");
		sb.append("brc_continuity_scd,");
		sb.append("bwt_akum,");
		sb.append("potongan_am,");
		sb.append("tax ");
		sb.append("FROM bonus_b_temp ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
//		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBulan(rs.getDate("bulan"));
				dataObject.setBonusRo(rs.getDouble("bonus_ro"));
				dataObject.setBonusSi(rs.getDouble("bonus_si"));
				dataObject.setBonusUs(rs.getDouble("bonus_us"));
				dataObject.setBonusEarlybird(rs.getDouble("bonus_earlybird"));
				dataObject.setBonusOr(rs.getDouble("bonus_or"));
				dataObject.setBorAntrian(rs.getDouble("bor_antrian"));
				dataObject.setTraveling(rs.getDouble("traveling"));
				dataObject.setBonusRc(rs.getDouble("bonus_rc"));
				dataObject.setBorAkumgk(rs.getFloat("bor_akumgk"));
				dataObject.setBorTingkat(rs.getInt("bor_tingkat"));
				dataObject.setBorIsqualified(rs.getInt("bor_isqualified"));
				dataObject.setBorIsclaimed(rs.getInt("bor_isclaimed"));
				dataObject.setBrcIsqualifiedCd(rs.getInt("brc_isqualified_cd"));
				dataObject.setBrcContinuityCd(rs.getInt("brc_continuity_cd"));
				dataObject.setBrcIsqualifiedScd(rs.getInt("brc_isqualified_scd"));
				dataObject.setBrcContinuityScd(rs.getInt("brc_continuity_scd"));
				dataObject.setBwtAkum(rs.getDouble("bwt_akum"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setTax(rs.getDouble("tax"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BonusBSet select(String whereClause) throws SQLException {
		BonusBSet dataObjectSet = new BonusBSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("bonus_ro,");
		sb.append("bonus_si,");
		sb.append("bonus_us,");
		sb.append("bonus_earlybird,");
		sb.append("bonus_or,");
		sb.append("bor_antrian,");
		sb.append("traveling,");
		sb.append("bonus_rc,");
		sb.append("bor_akumgk,");
		sb.append("bor_tingkat,");
		sb.append("bor_isqualified,");
		sb.append("bor_isclaimed,");
		sb.append("brc_isqualified_cd,");
		sb.append("brc_continuity_cd,");
		sb.append("brc_isqualified_scd,");
		sb.append("brc_continuity_scd,");
		sb.append("bwt_akum,");
		sb.append("potongan_am,");
		sb.append("tax ");
		sb.append("FROM bonus_b_temp ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BonusB dataObject = new BonusB();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setBulan(rs.getDate("bulan"));
				dataObject.setBonusRo(rs.getDouble("bonus_ro"));
				dataObject.setBonusSi(rs.getDouble("bonus_si"));
				dataObject.setBonusUs(rs.getDouble("bonus_us"));
				dataObject.setBonusEarlybird(rs.getDouble("bonus_earlybird"));
				dataObject.setBonusOr(rs.getDouble("bonus_or"));
				dataObject.setBorAntrian(rs.getDouble("bor_antrian"));
				dataObject.setTraveling(rs.getDouble("traveling"));
				dataObject.setBonusRc(rs.getDouble("bonus_rc"));
				dataObject.setBorAkumgk(rs.getFloat("bor_akumgk"));
				dataObject.setBorTingkat(rs.getInt("bor_tingkat"));
				dataObject.setBorIsqualified(rs.getInt("bor_isqualified"));
				dataObject.setBorIsclaimed(rs.getInt("bor_isclaimed"));
				dataObject.setBrcIsqualifiedCd(rs.getInt("brc_isqualified_cd"));
				dataObject.setBrcContinuityCd(rs.getInt("brc_continuity_cd"));
				dataObject.setBrcIsqualifiedScd(rs.getInt("brc_isqualified_scd"));
				dataObject.setBrcContinuityScd(rs.getInt("brc_continuity_scd"));
				dataObject.setBwtAkum(rs.getDouble("bwt_akum"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setTax(rs.getDouble("tax"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int cleanTemporary() throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM bonus_b_temp");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
