package id.co.birumerah.bsi.bonus.planb;

import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;
import java.util.Date;

public class BonusBLogFcd {

	private BonusBLog bbLog;
	
	public BonusBLogFcd () {}

	public BonusBLogFcd(BonusBLog bbLog) {
		this.bbLog = bbLog;
	}

	public BonusBLog getBbLog() {
		return bbLog;
	}

	public void setBbLog(BonusBLog bbLog) {
		this.bbLog = bbLog;
	}
	
	public static BonusBLog getLogByMonth(Date bulan) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusBLog log = new BonusBLog();
			log.setBulan(bulan);
			
			BonusBLogDAO bblDAO = new BonusBLogDAO(dbConn);
			boolean found = bblDAO.select(log);
			
			if (found) {
				return log;
			} else {
				System.err.println("Log bonus bulan ini tidak ditemukan.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static BonusBLog getLogByMonth(Date bulan, DBConnection dbConn) throws Exception {
		try {
			BonusBLog log = new BonusBLog();
			log.setBulan(bulan);
			
			BonusBLogDAO bblDAO = new BonusBLogDAO(dbConn);
			boolean found = bblDAO.select(log);
			
			if (found) {
				return log;
			} else {
				System.err.println("Log bonus bulan ini tidak ditemukan.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertLog() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBLogDAO bblDAO = new BonusBLogDAO(dbConn);
			bblDAO.insert(bbLog);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertLog(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBLogDAO bblDAO = new BonusBLogDAO(dbConn);
			bblDAO.insert(bbLog);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateLog(BonusBLog log) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBLogDAO bblDAO = new BonusBLogDAO(dbConn);
			if (bblDAO.update(log) < 1) {
				throw new Exception("Update log gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateLog(BonusBLog log, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBLogDAO bblDAO = new BonusBLogDAO(dbConn);
			if (bblDAO.update(log) < 1) {
				throw new Exception("Update log gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
