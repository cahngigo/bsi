package id.co.birumerah.bsi.bonus.planb;

import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.NumberUtil;
import id.co.birumerah.util.dbaccess.DBConnection;

import org.joda.time.MutableDateTime;

public class BonusBTempCounter {

//	private Tree<MemberTree> mTree;
	private MemberTreeSet memTreeSet;
	private MutableDateTime beginOfDay;
	private MutableDateTime bulanHitung;
	
	private float minPvpribadi;
	
	// setting-setting untuk Bonus Repeat Order
	private int pctRo1;
	private int pctRo2;
	private int pctRo3;
	private int pctRo4;
	private int pctRo5;
	private float grade1Pv;
	private float grade2Pv;
	private float grade3Pv;
	private float grade4Pv;
	private float grade5Pv;
	private int roPvPerbagian;
	
	// setting-setting untuk Bonus Infinity Group
	private int bigSmPvgk;
	private int bigGmPvgk;
	private int bigRmPvgk;
	private int bigEmPvgk;
	private int bigPmPvgk;
	private int bigDmPvgk;
	private int bigCdPvgk;
	private int bigScdPvgk;
	private int bigMaxgkBag1;
	private float bigGkPct1;
	private int bigMaxgsGk1;
	private float bigGsPct1;
	private int bigMaxgbGk1;
	private float bigGbPct1;
	private int bigMaxgkBag2;
	private float bigGkPct2;
	private int bigMaxgsGk2;
	private float bigGsPct2;
	private int bigMaxgbGk2;
	private float bigGbPct2;
	private int bigMaxgkBag3;
	private float bigGkPct3;
	private int bigMaxgsGk3;
	private float bigGsPct3;
	private int bigMaxgbGk3;
	private float bigGbPct3;
	private int bigMaxgkBag4;
	private float bigGkPct4;
	private int bigMaxgsGk4;
	private float bigGsPct4;
	private int bigMaxgbGk4;
	private float bigGbPct4;
	private int bigMaxgkBag5;
	private float bigGkPct5;
	private int bigMaxgsGk5;
	private float bigGsPct5;
	private int bigMaxgbGk5;
	private float bigGbPct5;
	private int bigMaxgkBag6;
	private float bigGkPct6;
	private int bigMaxgsGk6;
	private float bigGsPct6;
	private int bigMaxgbGk6;
	private float bigGbPct6;
	private int bigMaxgkBag7;
	private float bigGkPct7;
	private int bigMaxgsGk7;
	private float bigGsPct7;
	private int bigMaxgbGk7;
	private float bigGbPct7;
	private int bigMaxgkBag8;
	private float bigGkPct8;
	private int bigMaxgsGk8;
	private float bigGsPct8;
	private int bigMaxgbGk8;
	private float bigGbPct8;
	private float bigGkPct9;
	private float bigGsPct9;
	private float bigGbPct9;
	private int bigPvPerbagian;
	private int bulRmKualifikasi;
	private int bulEmKualifikasi;
	private int bulPmKualifikasi;
	private int bulDmKualifikasi;
	private int bulCdKualifikasi;
	private int bulScdKualifikasi;
	private float bulPct1;
	private float bulPct2;
	private float bulPct3;
	private float bulPct4;
	private float bulPct5;
	private float bulPct6;
	
	private int bebMingk1;
	private int bebMaxgk1;
	private int bebPct1;
	private int bebTgl1;
	private int bebMingk2;
	private int bebMaxgk2;
	private int bebPct2;
	private int bebTgl2;
	private int bebPvPerbagian;
	
	private int borWisataValue;
	private int borWisataPct;
	private int borWisataAkumgk;
	private int borWisataTambahangk;
	private int borMotorValue;
	private int borMotorPct;
	private int borMotorAkumgk;
	private int borMotorTambahangk;
	private int borReligiValue;
	private int borReligiPct;
	private int borReligiAkumgk;
	private int borReligiTambahangk;
	private int borMobilValue;
	private int borMobilPct;
	private int borMobilAkumgk;
	private int borMobilTambahangk;
	private int borMobilMewahValue;
	private int borMobilMewahPct;
	private int borMobilMewahAkumgk;
	private int borMobilMewahTambahangk;
	private int borRumahMewahValue;
	private int borRumahMewahPct;
	private int borRumahMewahAkumgk;
	private int borRumahMewahTambahangk;
	private float borAntriWisataPct;
	private float borAntriMotorPct;
	private float borAntriReligiPct;
	private float borAntriMobilPct;
	
	private int brcCdKualifikasi;
	private float brcCdPct;
	private int brcScdKualifikasi;
	private float brcScdPct;
	
	private int bwtRmMingk;
	private int bwtRmBagian;
	private int bwtEmMingk;
	private int bwtEmBagian;
	private int bwtDmMingk;
	private int bwtDmBagian;
	private int bwtCdMingk;
	private int bwtCdBagian;
	private int bwtScdMingk;
	private int bwtScdBagian;
	private int bwtPvPerbagian;
	private float bwtBudgetPct;
	
	private double bvTotal;
	private static final int BRC_KUALIFIKASI_BERURUT = 6;
	
	public BonusBTempCounter() {
		super();
		
		beginOfDay = new MutableDateTime();
//		beginOfDay.setHourOfDay(0);
//		beginOfDay.setMinuteOfHour(0);
//		beginOfDay.setSecondOfMinute(0);
		bulanHitung = beginOfDay.copy();
		bulanHitung.setDayOfMonth(1);
		getAllMember();
//		loadSettingBonusB();
		hitungBvPerusahaan();
	}
	
	public BonusBTempCounter(DBConnection dbConn) {
		super();
		
		beginOfDay = new MutableDateTime();
//		beginOfDay.setHourOfDay(0);
//		beginOfDay.setMinuteOfHour(0);
//		beginOfDay.setSecondOfMinute(0);
		bulanHitung = beginOfDay.copy();
		bulanHitung.setDayOfMonth(1);
		getAllMember(dbConn);
		loadSettingBonusB(dbConn);
		hitungBvPerusahaan();
	}
	
	private void getAllMember() {
		try {
			memTreeSet = MemberTreeFcd.showAllMemberTree();
//			memTreeSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getAllMember(DBConnection dbConn) {
		try {
//			memTreeSet = MemberTreeFcd.showAllMemberTreeBeforeToday();
//			memTreeSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday(beginOfDay, dbConn);
			//production pake di bawah ini
//			memTreeSet = MemberTreeBulananFcd.showAllMemberTreeBeforeToday(dbConn);
			memTreeSet = MemberTreeFcd.showAllMemberTree(dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadSettingBonusB(DBConnection dbConn) {
		String whereClause = "status = 'Berlaku' AND start_date < '" 
			+ beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC";
		SettingBonusBFcd sbFcd = new SettingBonusBFcd();
		SettingBonusBSet sbSet = null;
		try {
//			sbSet = sbFcd.search(whereClause);
			sbSet = sbFcd.search(whereClause, dbConn);
			if (sbSet.length() < 1) {
				System.out.println("Belum ada setting bonus yang berlaku.");
				return;
			}
			SettingBonusB settings = sbSet.get(0);
			minPvpribadi = settings.getMinPvpribadi();
			pctRo1 = settings.getPctRo1();
			pctRo2 = settings.getPctRo2();
			pctRo3 = settings.getPctRo3();
			pctRo4 = settings.getPctRo4();
			pctRo5 = settings.getPctRo5();
			grade1Pv = settings.getGrade1Ro();
			grade2Pv = settings.getGrade2Ro();
			grade3Pv = settings.getGrade3Ro();
			grade4Pv = settings.getGrade4Ro();
			grade5Pv = settings.getGrade5Ro();
			roPvPerbagian = settings.getRoPvPerbagian();
			
			bigSmPvgk = settings.getBigSmPvgk();
			bigGmPvgk = settings.getBigGmPvgk();
			bigRmPvgk = settings.getBigRmPvgk();
			bigEmPvgk = settings.getBigEmPvgk();
			bigPmPvgk = settings.getBigPmPvgk();
			bigDmPvgk = settings.getBigDmPvgk();
			bigCdPvgk = settings.getBigCdPvgk();
			bigScdPvgk = settings.getBigScdPvgk();
			bigMaxgkBag1 = settings.getBigMaxgkBag1();
			bigGkPct1 = settings.getBigGkPct1();
			bigMaxgsGk1 = settings.getBigMaxgsGk1();
			bigGsPct1 = settings.getBigGsPct1();
			bigMaxgbGk1 = settings.getBigMaxgbGk1();
			bigGbPct1 = settings.getBigGbPct1();
			bigMaxgkBag2 = settings.getBigMaxgkBag2();
			bigGkPct2 = settings.getBigGkPct2();
			bigMaxgsGk2 = settings.getBigMaxgsGk2();
			bigGsPct2 = settings.getBigGsPct2();
			bigMaxgbGk2 = settings.getBigMaxgbGk2();
			bigGbPct2 = settings.getBigGbPct2();
			bigMaxgkBag3 = settings.getBigMaxgkBag3();
			bigGkPct3 = settings.getBigGkPct3();
			bigMaxgsGk3 = settings.getBigMaxgsGk3();
			bigGsPct3 = settings.getBigGsPct3();
			bigMaxgbGk3 = settings.getBigMaxgbGk3();
			bigGbPct3 = settings.getBigGbPct3();
			bigMaxgkBag4 = settings.getBigMaxgkBag4();
			bigGkPct4 = settings.getBigGkPct4();
			bigMaxgsGk4 = settings.getBigMaxgsGk4();
			bigGsPct4 = settings.getBigGsPct4();
			bigMaxgbGk4 = settings.getBigMaxgbGk4();
			bigGbPct4 = settings.getBigGbPct4();
			bigMaxgkBag5 = settings.getBigMaxgkBag5();
			bigGkPct5 = settings.getBigGkPct5();
			bigMaxgsGk5 = settings.getBigMaxgsGk5();
			bigGsPct5 = settings.getBigGsPct5();
			bigMaxgbGk5 = settings.getBigMaxgbGk5();
			bigGbPct5 = settings.getBigGbPct5();
			bigMaxgkBag6 = settings.getBigMaxgkBag6();
			bigGkPct6 = settings.getBigGkPct6();
			bigMaxgsGk6 = settings.getBigMaxgsGk6();
			bigGsPct6 = settings.getBigGsPct6();
			bigMaxgbGk6 = settings.getBigMaxgbGk6();
			bigGbPct6 = settings.getBigGbPct6();
			bigMaxgkBag7 = settings.getBigMaxgkBag7();
			bigGkPct7 = settings.getBigGkPct7();
			bigMaxgsGk7 = settings.getBigMaxgsGk7();
			bigGsPct7 = settings.getBigGsPct7();
			bigMaxgbGk7 = settings.getBigMaxgbGk7();
			bigGbPct7 = settings.getBigGbPct7();
			bigMaxgkBag8 = settings.getBigMaxgkBag8();
			bigGkPct8 = settings.getBigGkPct8();
			bigMaxgsGk8 = settings.getBigMaxgsGk8();
			bigGsPct8 = settings.getBigGsPct8();
			bigMaxgbGk8 = settings.getBigMaxgbGk8();
			bigGbPct8 = settings.getBigGbPct8();
			bigGkPct9 = settings.getBigGkPct9();
			bigGsPct9 = settings.getBigGsPct9();
			bigGbPct9 = settings.getBigGbPct9();
			bigPvPerbagian = settings.getBigPvPerbagian();
			
			bulRmKualifikasi = settings.getBulRmKualifikasi();
			bulEmKualifikasi = settings.getBulEmKualifikasi();
			bulPmKualifikasi = settings.getBulPmKualifikasi();
			bulDmKualifikasi = settings.getBulDmKualifikasi();
			bulCdKualifikasi = settings.getBulCdKualifikasi();
			bulScdKualifikasi = settings.getBulScdKualifikasi();
			bulPct1 = settings.getBulPct1();
			bulPct2 = settings.getBulPct2();
			bulPct3 = settings.getBulPct3();
			bulPct4 = settings.getBulPct4();
			bulPct5 = settings.getBulPct5();
			bulPct6 = settings.getBulPct6();
			
			bebMingk1 = settings.getBebMingk1();
			bebMaxgk1 = settings.getBebMaxgk1();
			bebPct1 = settings.getBebPct1();
			bebTgl1 = settings.getBebTgl1();
			bebMingk2 = settings.getBebMingk2();
			bebMaxgk2 = settings.getBebMaxgk2();
			bebPct2 = settings.getBebPct2();
			bebTgl2 = settings.getBebTgl2();
			bebPvPerbagian = settings.getBebPvPerbagian();
			
			borWisataValue = settings.getBorWisataValue();
			borWisataPct = settings.getBorWisataPct();
			borWisataAkumgk = settings.getBorWisataAkumgk();
			borWisataTambahangk = settings.getBorWisataTambahangk();
			borMotorValue = settings.getBorMotorValue();
			borMotorPct = settings.getBorMotorPct();
			borMotorAkumgk = settings.getBorMotorAkumgk();
			borMotorTambahangk = settings.getBorMotorTambahangk();
			borReligiValue = settings.getBorReligiValue();
			borReligiPct = settings.getBorReligiPct();
			borReligiAkumgk = settings.getBorReligiAkumgk();
			borReligiTambahangk = settings.getBorReligiTambahangk();
			borMobilValue = settings.getBorMobilValue();
			borMobilPct = settings.getBorMobilPct();
			borMobilAkumgk = settings.getBorMobilAkumgk();
			borMobilTambahangk = settings.getBorMobilTambahangk();
			borMobilMewahValue = settings.getBorMobilMewahValue();
			borMobilMewahPct = settings.getBorMobilMewahPct();
			borMobilMewahAkumgk = settings.getBorMobilMewahAkumgk();
			borMobilMewahTambahangk = settings.getBorMobilMewahTambahangk();
			borRumahMewahValue = settings.getBorRumahMewahValue();
			borRumahMewahPct = settings.getBorRumahMewahPct();
			borRumahMewahAkumgk = settings.getBorRumahMewahAkumgk();
			borRumahMewahTambahangk = settings.getBorRumahMewahTambahangk();
			borAntriWisataPct = settings.getBorAntriWisataPct();
			borAntriMotorPct = settings.getBorAntriMotorPct();
			borAntriReligiPct = settings.getBorAntriReligiPct();
			borAntriMobilPct = settings.getBorAntriMobilPct();
			
			brcCdKualifikasi = settings.getBrcCdKualifikasi();
			brcCdPct = settings.getBrcCdPct();
			brcScdKualifikasi = settings.getBrcScdKualifikasi();
			brcScdPct = settings.getBrcScdPct();
			
			bwtRmMingk = settings.getBwtRmMingk();
			bwtRmBagian = settings.getBwtRmBagian();
			bwtEmMingk = settings.getBwtEmMingk();
			bwtEmBagian = settings.getBwtEmBagian();
			bwtDmMingk = settings.getBwtDmMingk();
			bwtDmBagian = settings.getBwtDmBagian();
			bwtCdMingk = settings.getBwtCdMingk();
			bwtCdBagian = settings.getBwtCdBagian();
			bwtScdMingk = settings.getBwtScdMingk();
			bwtScdBagian = settings.getBwtScdBagian();
			bwtPvPerbagian = settings.getBwtPvPerbagian();
			bwtBudgetPct = settings.getBwtBudgetPct();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cleanTemporary(DBConnection dbConn) {
		BonusBTempFcd bbtFcd = new BonusBTempFcd();
		try {
			bbtFcd.clearTemporary(dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBROPerGrade(int persenRo, float grade) {
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND status=1 AND saldo_poin >= " + grade;
//		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND saldo_poin >= " + grade;
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause);
			int totalBagian = getTotalBagianPerTingkat(mtSet);
			double bagianPerTingkat = bvTotal * persenRo / (100 * totalBagian);
			System.out.println("1 bagian RO = " + bagianPerTingkat);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float pvro = member.getSaldoPoin();
				int bagian = (int)pvro/roPvPerbagian;
				double bonusRO = (double) bagian * bagianPerTingkat;
				bonusRO = NumberUtil.roundToDecimals(bonusRO, 0);
				System.out.println("Member " + member.getMemberId() + " dapat bonus RO tingkat = " + bonusRO);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusRo(bonusRO);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusRo = bonus.getBonusRo();
					curBonusRo += bonusRO;
					bonus.setBonusRo(curBonusRo);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBROPerGrade(int persenRo, float grade, DBConnection dbConn) {
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' AND status=1 AND saldo_poin >= " + grade;
//		String whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "' AND saldo_poin >= " + grade;
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			int totalBagian = getTotalBagianPerTingkat(mtSet);
			double bagianPerTingkat = bvTotal * persenRo / (100 * totalBagian);
			System.out.println("1 bagian RO = " + bagianPerTingkat);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float pvro = member.getSaldoPoin();
				int bagian = (int)pvro/roPvPerbagian;
				double bonusRO = (double) bagian * bagianPerTingkat;
				bonusRO = NumberUtil.roundToDecimals(bonusRO, 0);
				System.out.println("Member " + member.getMemberId() + " dapat bonus RO tingkat = " + bonusRO);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusRo(bonusRO);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus(dbConn);
				} else {
					double curBonusRo = bonus.getBonusRo();
					curBonusRo += bonusRO;
					bonus.setBonusRo(curBonusRo);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusRO() {
		int[] persenRo = new int[] {pctRo1, pctRo2, pctRo3, pctRo4, pctRo5};
		float[] gradePv = new float[] {grade1Pv, grade2Pv, grade3Pv, grade4Pv, grade5Pv};
//		hitungBvPerusahaan();
		for (int i=0; i<gradePv.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBROPerGrade(persenRo[i], gradePv[i]);
		}
	}
	
	public void giveBonusRO(DBConnection dbConn) {
		int[] persenRo = new int[] {pctRo1, pctRo2, pctRo3, pctRo4, pctRo5};
		float[] gradePv = new float[] {grade1Pv, grade2Pv, grade3Pv, grade4Pv, grade5Pv};
		for (int i=0; i<gradePv.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBROPerGrade(persenRo[i], gradePv[i], dbConn);
		}
	}
	
	private void hitungBvPerusahaan() {
		bvTotal = 0;
		for (int i=0; i<memTreeSet.length(); i++) {
			MemberTree member = memTreeSet.get(i);
			bvTotal += member.getSaldoBv();
		}
		System.out.println("Total BV = " + bvTotal);
	}
	
	private int getTotalBagianPerTingkat(MemberTreeSet mtSet) {
		int totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			float pvro = member.getSaldoPoin();
			int bagian = (int)pvro/roPvPerbagian;
			totalBagian += bagian;
		}
		return totalBagian;
	}
	
	public void giveBonusIG() {
		float[] persenBigGb = new float[] {bigGbPct1, bigGbPct2, bigGbPct3, bigGbPct4, bigGbPct5, bigGbPct6, bigGbPct7, bigGbPct8, bigGbPct9};
		float[] persenBigGs = new float[] {bigGsPct1, bigGsPct2, bigGsPct3, bigGsPct4, bigGsPct5, bigGsPct6, bigGsPct7, bigGsPct8, bigGsPct9};
		float[] persenBigGk = new float[] {bigGkPct1, bigGkPct2, bigGkPct3, bigGkPct4, bigGkPct5, bigGkPct6, bigGkPct7, bigGkPct8, bigGkPct9};
		int[] maxBagianGb = new int[] {bigMaxgbGk1, bigMaxgbGk2, bigMaxgbGk3, bigMaxgbGk4, bigMaxgbGk5, bigMaxgbGk6, bigMaxgbGk7, bigMaxgbGk8, 0};
		int[] maxBagianGs = new int[] {bigMaxgsGk1, bigMaxgsGk2, bigMaxgsGk3, bigMaxgsGk4, bigMaxgsGk5, bigMaxgsGk6, bigMaxgsGk7, bigMaxgsGk8, 0};
		int[] maxBagianGk = new int[] {bigMaxgkBag1, bigMaxgkBag2, bigMaxgkBag3, bigMaxgkBag4, bigMaxgkBag5, bigMaxgkBag6, bigMaxgkBag7, bigMaxgkBag8, 0};
		int[] batasPvgk = new int[] {bigRmPvgk, bigEmPvgk, bigPmPvgk, bigDmPvgk, bigCdPvgk, bigScdPvgk, 0, 0, 0};
		
		for (int i=0; i<persenBigGb.length; i++) {
			float[] persenBig = new float[] {persenBigGb[i], persenBigGs[i], persenBigGk[i]};
			int[] maxBagian = new int[] {maxBagianGb[i], maxBagianGs[i], maxBagianGk[i]};
			System.out.println("=== Jenis " + (i+1) + " ===");
			hitungBIGPerJenis(persenBig, batasPvgk[i], maxBagian);
		}
	}
	
	public void giveBonusIG(DBConnection dbConn) {
		float[] persenBigGb = new float[] {bigGbPct1, bigGbPct2, bigGbPct3, bigGbPct4, bigGbPct5, bigGbPct6, bigGbPct7, bigGbPct8, bigGbPct9};
		float[] persenBigGs = new float[] {bigGsPct1, bigGsPct2, bigGsPct3, bigGsPct4, bigGsPct5, bigGsPct6, bigGsPct7, bigGsPct8, bigGsPct9};
		float[] persenBigGk = new float[] {bigGkPct1, bigGkPct2, bigGkPct3, bigGkPct4, bigGkPct5, bigGkPct6, bigGkPct7, bigGkPct8, bigGkPct9};
		int[] maxBagianGb = new int[] {bigMaxgbGk1, bigMaxgbGk2, bigMaxgbGk3, bigMaxgbGk4, bigMaxgbGk5, bigMaxgbGk6, bigMaxgbGk7, bigMaxgbGk8, 0};
		int[] maxBagianGs = new int[] {bigMaxgsGk1, bigMaxgsGk2, bigMaxgsGk3, bigMaxgsGk4, bigMaxgsGk5, bigMaxgsGk6, bigMaxgsGk7, bigMaxgsGk8, 0};
		int[] maxBagianGk = new int[] {bigMaxgkBag1, bigMaxgkBag2, bigMaxgkBag3, bigMaxgkBag4, bigMaxgkBag5, bigMaxgkBag6, bigMaxgkBag7, bigMaxgkBag8, 0};
		int[] batasPvgk = new int[] {bigRmPvgk, bigEmPvgk, bigPmPvgk, bigDmPvgk, bigCdPvgk, bigScdPvgk, 0, 0, 0};
		
		for (int i=0; i<persenBigGb.length; i++) {
			float[] persenBig = new float[] {persenBigGb[i], persenBigGs[i], persenBigGk[i]};
			int[] maxBagian = new int[] {maxBagianGb[i], maxBagianGs[i], maxBagianGk[i]};
			System.out.println("=== Jenis " + (i+1) + " ===");
			hitungBIGPerJenis(persenBig, batasPvgk[i], maxBagian, dbConn);
		}
	}
	
	private void hitungBIGPerJenis(float[] persenBig, int batasPvgk, int[] maxBagian) {
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'" 
			+ " AND pvpribadi >= " + minPvpribadi
			+ " AND pv_litfoot >= " + bigSmPvgk;
		if (batasPvgk > 0) {
			whereClause += " AND pv_litfoot < " + batasPvgk;
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause);
			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, maxBagian);
			double bagianGbPerJenis = bvTotal * persenBig[0] / (100 * totalBagian[0]);
			double bagianGsPerJenis = bvTotal * persenBig[1] / (100 * totalBagian[1]);
			double bagianGkPerJenis = bvTotal * persenBig[2] / (100 * totalBagian[2]);
			System.out.println("Persen GK="+persenBig[2]+"; TotalBagian GK="+totalBagian[2]);
			System.out.println("1B Besar="+bagianGbPerJenis+"; Sedang="+bagianGsPerJenis+"; Kecil="+bagianGkPerJenis);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float pvGb = member.getPvBigfoot();
				float pvGs = member.getPvMidfoot();
				float pvGk = member.getPvLitfoot();
				double bagianGb = pvGb / bigPvPerbagian;
				double bagianGs = pvGs / bigPvPerbagian;
				double bagianGk = pvGk / bigPvPerbagian;
//				if ((bagianGb > (pvGk * maxBagian[0])) && (maxBagian[0] > 0)) {
//					bagianGb = pvGk * maxBagian[0];
//				}
//				if ((bagianGs > (pvGk * maxBagian[1])) && (maxBagian[1]) > 0) {
//					bagianGs = pvGk * maxBagian[1];
//				}
				if ((bagianGb > (maxBagian[2] * maxBagian[0])) && (maxBagian[0] > 0)) {
					bagianGb = maxBagian[2] * maxBagian[0];
				}
				if ((bagianGs > (maxBagian[2] * maxBagian[1])) && (maxBagian[1]) > 0) {
					bagianGs = maxBagian[2] * maxBagian[1];
				}
				if ((bagianGk > maxBagian[2]) && (maxBagian[2]) > 0) {
					bagianGk = maxBagian[2];
				}
				double bonusBigGb = NumberUtil.roundToDecimals((bagianGb * bagianGbPerJenis), 0);
				double bonusBigGs = NumberUtil.roundToDecimals((bagianGs * bagianGsPerJenis), 0);
				double bonusBigGk = NumberUtil.roundToDecimals((bagianGk * bagianGkPerJenis), 0);
				double bonusBig = bonusBigGb + bonusBigGs + bonusBigGk;
				System.out.println("Member "+member.getMemberId()+" dapat bonus GI tingkat "+bonusBig);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusSi(bonusBig);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusBig = bonus.getBonusSi();
					curBonusBig += bonusBig;
					bonus.setBonusSi(curBonusBig);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBIGPerJenis(float[] persenBig, int batasPvgk, int[] maxBagian, DBConnection dbConn) {
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'" 
			+ " AND pvpribadi >= " + minPvpribadi
			+ " AND pv_litfoot >= " + bigSmPvgk;
		if (batasPvgk > 0) {
			whereClause += " AND pv_litfoot < " + batasPvgk;
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			double[] totalBagian = getTotalBagianPerJenisBig(mtSet, maxBagian);
			double bagianGbPerJenis = bvTotal * persenBig[0] / (100 * totalBagian[0]);
			double bagianGsPerJenis = bvTotal * persenBig[1] / (100 * totalBagian[1]);
			double bagianGkPerJenis = bvTotal * persenBig[2] / (100 * totalBagian[2]);
			System.out.println("Persen GK="+persenBig[2]+"; TotalBagian GK="+totalBagian[2]);
			System.out.println("1B Besar="+bagianGbPerJenis+"; Sedang="+bagianGsPerJenis+"; Kecil="+bagianGkPerJenis);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float pvGb = member.getPvBigfoot();
				float pvGs = member.getPvMidfoot();
				float pvGk = member.getPvLitfoot();
				double bagianGb = pvGb / bigPvPerbagian;
				double bagianGs = pvGs / bigPvPerbagian;
				double bagianGk = pvGk / bigPvPerbagian;
//				if ((bagianGb > (pvGk * maxBagian[0])) && (maxBagian[0] > 0)) {
//					bagianGb = pvGk * maxBagian[0];
//				}
//				if ((bagianGs > (pvGk * maxBagian[1])) && (maxBagian[1]) > 0) {
//					bagianGs = pvGk * maxBagian[1];
//				}
				if ((bagianGb > (maxBagian[2] * maxBagian[0])) && (maxBagian[0] > 0)) {
					bagianGb = maxBagian[2] * maxBagian[0];
				}
				if ((bagianGs > (maxBagian[2] * maxBagian[1])) && (maxBagian[1]) > 0) {
					bagianGs = maxBagian[2] * maxBagian[1];
				}
				if ((bagianGk > maxBagian[2]) && (maxBagian[2]) > 0) {
					bagianGk = maxBagian[2];
				}
				double bonusBigGb = NumberUtil.roundToDecimals((bagianGb * bagianGbPerJenis), 0);
				double bonusBigGs = NumberUtil.roundToDecimals((bagianGs * bagianGsPerJenis), 0);
				double bonusBigGk = NumberUtil.roundToDecimals((bagianGk * bagianGkPerJenis), 0);
				double bonusBig = bonusBigGb + bonusBigGs + bonusBigGk;
				System.out.println("Member "+member.getMemberId()+" dapat bonus GI tingkat "+bonusBig);
//				MutableDateTime blnTrx = beginOfDay;
//				blnTrx.addMonths(-1);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusSi(bonusBig);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus(dbConn);
				} else {
					double curBonusBig = bonus.getBonusSi();
					curBonusBig += bonusBig;
					bonus.setBonusSi(curBonusBig);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private double[] getTotalBagianPerJenisBig(MemberTreeSet mtSet, int[] maxBagian) {
		double totalBagianGb = 0;
		double totalBagianGs = 0;
		double totalBagianGk = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			float pvGb = member.getPvBigfoot();
			float pvGs = member.getPvMidfoot();
			float pvGk = member.getPvLitfoot();
			double bagianGb = pvGb/bigPvPerbagian;
			double bagianGs = pvGs/bigPvPerbagian;
			double bagianGk = pvGk/bigPvPerbagian;
			if ((bagianGb > (maxBagian[0] * maxBagian[2])) && (maxBagian[0] > 0)) {
				bagianGb = maxBagian[0];
			}
			if ((bagianGs > (maxBagian[1] * maxBagian[2])) && (maxBagian[1] > 0)) {
				bagianGs = maxBagian[1];
			}
			if ((bagianGk > maxBagian[2]) && (maxBagian[2] > 0)) {
				bagianGk = maxBagian[2];
			}
			totalBagianGb += bagianGb;
			totalBagianGs += bagianGs;
			totalBagianGk += bagianGk;
		}
		System.out.println("Total bagian GB="+totalBagianGb+"; GS="+totalBagianGs+"; GK="+totalBagianGk);
		double[] total = new double[] {totalBagianGb, totalBagianGs, totalBagianGk};
		return total;
	}
	
	private void hitungBULPerTingkat(float persenBul, int batasBawahPv, int batasAtasPv) {
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'" 
			+ " AND pv_litfoot >= " + batasBawahPv;
		if (batasAtasPv > 0) {
			whereClause += " AND pv_litfoot < " + batasAtasPv;
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause);
			double totalBagian = mtSet.length();
			System.out.println("Total Bagian="+ totalBagian);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				double bonusBul = bvTotal * persenBul / (100 * totalBagian);
				bonusBul = NumberUtil.roundToDecimals(bonusBul, 0);
				System.out.println("Member "+member.getMemberId()+" dapat bonus UL tingkat "+bonusBul);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusUs(bonusBul);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusBul = bonus.getBonusUs();
					curBonusBul += bonusBul;
					bonus.setBonusUs(curBonusBul);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBULPerTingkat(float persenBul, int batasBawahPv, int batasAtasPv, DBConnection dbConn) {
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'" 
			+ " AND pv_litfoot >= " + batasBawahPv;
		if (batasAtasPv > 0) {
			whereClause += " AND pv_litfoot < " + batasAtasPv;
		}
		System.out.println("whereClause = " + whereClause);
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			double totalBagian = mtSet.length();
			System.out.println("Total Bagian="+ totalBagian);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				double bonusBul = bvTotal * persenBul / (100 * totalBagian);
				bonusBul = NumberUtil.roundToDecimals(bonusBul, 0);
				System.out.println("Member "+member.getMemberId()+" dapat bonus UL tingkat "+bonusBul);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBonusUs(bonusBul);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus(dbConn);
				} else {
					double curBonusBul = bonus.getBonusUs();
					curBonusBul += bonusBul;
					bonus.setBonusUs(curBonusBul);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusUL() {
		float[] persenBul = new float[] {bulPct1, bulPct2, bulPct3, bulPct4, bulPct5, bulPct6};
		int[] batasBawah = new int[] {bulRmKualifikasi, bulEmKualifikasi, bulPmKualifikasi, bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi};
		int[] batasAtas = new int[] {bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi, 0, 0, 0};
		for (int i=0; i<persenBul.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBULPerTingkat(persenBul[i], batasBawah[i], batasAtas[i]);
		}
	}
	
	public void giveBonusUL(DBConnection dbConn) {
		float[] persenBul = new float[] {bulPct1, bulPct2, bulPct3, bulPct4, bulPct5, bulPct6};
		int[] batasBawah = new int[] {bulRmKualifikasi, bulEmKualifikasi, bulPmKualifikasi, bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi};
		int[] batasAtas = new int[] {bulDmKualifikasi, bulCdKualifikasi, bulScdKualifikasi, 0, 0, 0};
		for (int i=0; i<persenBul.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBULPerTingkat(persenBul[i], batasBawah[i], batasAtas[i], dbConn);
		}
	}
	
	private double getTotalBagianPerTingkatBeb(MemberTreeSet mtSet) {
		double totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			float pvGk = member.getPvLitfoot();
			double bagian = pvGk / bebPvPerbagian;
			totalBagian += bagian;
		}
		System.out.println("Total bagian = " + totalBagian);
		return totalBagian;
	}
	
	private void hitungBEBPerTingkat(int batasBawah, int batasAtas, int persenBeb, int tanggal) {
		String whereClause = "pv_litfoot >= " + batasBawah; 
//		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, tanggal, 0, 0, 0, 0);
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), tanggal, 0, 0, 0, 0);
//		lastMonth.addMonths(-1);
		boolean biggerThanLimit = false;
		if (beginOfDay.isAfter(lastMonth)) {
			whereClause += " AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "'";
			biggerThanLimit = true;
		} else {
			whereClause += " AND reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'";
		}
		whereClause += " AND pv_litfoot <= " + batasAtas;
		System.out.println("whereClause = " + whereClause);
		MemberTreeHarianFcd mthFcd = null;
		MemberTreeFcd mtFcd = null;
		MemberTreeSet mtSet = null;
		try {
			if (biggerThanLimit) {
				mthFcd = new MemberTreeHarianFcd();
				mtSet = mthFcd.search(whereClause);
			} else {
				mtFcd = new MemberTreeFcd();
				mtSet = mtFcd.search(whereClause);
			}
			double totalBagian = getTotalBagianPerTingkatBeb(mtSet);
			double bagianPerTingkat = bvTotal * persenBeb / (100 * totalBagian);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float bagian = member.getPvLitfoot() / bebPvPerbagian;
				double bonusBeb = NumberUtil.roundToDecimals((bagian * bagianPerTingkat), 0);
				System.out.println("Member "+member.getMemberId()+" dapat bonus EB tingkat "+bonusBeb);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				lastMonth.setDayOfMonth(beginOfDay.getDayOfMonth());
				lastMonth.setDayOfMonth(1);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate());
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId());
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(lastMonth.toDate());
					bonus.setBonusEarlybird(bonusBeb);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					double curBonusBeb = bonus.getBonusUs();
					curBonusBeb += bonusBeb;
					bonus.setBonusEarlybird(curBonusBeb);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBEBPerTingkat(int batasBawah, int batasAtas, int persenBeb, int tanggal, DBConnection dbConn) {
		String whereClause = "pv_litfoot >= " + batasBawah; 
//		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, tanggal, 0, 0, 0, 0);
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), tanggal, 0, 0, 0, 0);
//		lastMonth.addMonths(-1);
		boolean biggerThanLimit = false;
		if (beginOfDay.isAfter(lastMonth)) {
			whereClause += " AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "'";
			biggerThanLimit = true;
		} else {
			whereClause += " AND reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'";
		}
		whereClause += " AND pv_litfoot <= " + batasAtas;
		System.out.println("whereClause = " + whereClause);
		MemberTreeHarianFcd mthFcd = null;
		MemberTreeFcd mtFcd = null;
		MemberTreeSet mtSet = null;
		try {
			if (biggerThanLimit) {
				mthFcd = new MemberTreeHarianFcd();
				mtSet = mthFcd.search(whereClause, dbConn);
			} else {
				mtFcd = new MemberTreeFcd();
				mtSet = mtFcd.search(whereClause, dbConn);
			}
			double totalBagian = getTotalBagianPerTingkatBeb(mtSet);
			double bagianPerTingkat = bvTotal * persenBeb / (100 * totalBagian);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				float bagian = member.getPvLitfoot() / bebPvPerbagian;
				double bonusBeb = NumberUtil.roundToDecimals((bagian * bagianPerTingkat), 0);
				System.out.println("Member "+member.getMemberId()+" dapat bonus EB tingkat "+bonusBeb);
//				MutableDateTime blnTrx = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear()-1, beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
//				lastMonth.setDayOfMonth(beginOfDay.getDayOfMonth());
				lastMonth.setDayOfMonth(1);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(lastMonth.toDate());
					bonus.setBonusEarlybird(bonusBeb);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus(dbConn);
				} else {
					double curBonusBeb = bonus.getBonusUs();
					curBonusBeb += bonusBeb;
					bonus.setBonusEarlybird(curBonusBeb);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusEB() {
		int[] batasBawah = new int[] {bebMingk1, bebMaxgk1};
		int[] batasAtas = new int[] {bebMingk2, bebMaxgk2};
		int[] persenBeb = new int[] {bebPct1, bebPct2};
		int[] tanggal = new int[] {bebTgl1, bebTgl2};
		for (int i=0; i<batasBawah.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBEBPerTingkat(batasBawah[i], batasAtas[i], persenBeb[i], tanggal[i]);
		}
	}
	
	public void giveBonusEB(DBConnection dbConn) {
		int[] batasBawah = new int[] {bebMingk1, bebMaxgk1};
		int[] batasAtas = new int[] {bebMingk2, bebMaxgk2};
		int[] persenBeb = new int[] {bebPct1, bebPct2};
		int[] tanggal = new int[] {bebTgl1, bebTgl2};
		for (int i=0; i<batasBawah.length; i++) {
			System.out.println("=== Tingkat " + (i+1) + " ===");
			hitungBEBPerTingkat(batasBawah[i], batasAtas[i], persenBeb[i], tanggal[i], dbConn);
		}
	}
	
	public void giveBonusOR(DBConnection dbConn) {
		int[] akumulasi = new int[] {borWisataAkumgk, borMotorAkumgk, borReligiAkumgk, borMobilAkumgk, borMobilMewahAkumgk, borRumahMewahAkumgk};
		int[] syarat = new int[] {borWisataTambahangk, borMotorTambahangk, borReligiTambahangk, borMobilTambahangk, borMobilMewahTambahangk, borRumahMewahTambahangk};
		int[] budgetPct = new int[] {borWisataPct, borMotorPct, borReligiPct, borMobilPct, borMobilMewahPct, borRumahMewahPct};
		int[] nilaiBor = new int[] {borWisataValue, borMotorValue, borReligiValue, borMobilValue, borMobilMewahValue, borRumahMewahValue};
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "'"
					+ " AND pv_litfoot >= " + borWisataTambahangk
					+ " AND pvpribadi >= " + minPvpribadi;
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
				lastMonth.addMonths(-1);
				float akumGk = 0;
				int tingkat = 0;
				int isqualified = 0;
				whereClause = "member_id = '" + member.getMemberId() + "' AND bulan < '" + bulanHitung.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
				BonusBFcd bbFcd = new BonusBFcd();
				BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
//				BonusB bonusLastMonth = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
				BonusB bonusLastMonth = null;
				if (bbSet != null) bonusLastMonth = bbSet.get(0);
				if (bonusLastMonth == null) {
					tingkat = 1;
					akumGk = member.getPvLitfoot();
				} else {
					if (bonusLastMonth.getBorTingkat() == 0) {
						tingkat = 1;
						akumGk = member.getPvLitfoot();
					} else {
						if (bonusLastMonth.getBorIsclaimed() == 1 && bonusLastMonth.getBorTingkat() < 6) {
							tingkat = bonusLastMonth.getBorTingkat() + 1;
							isqualified = 0;
							if (member.getPvLitfoot() >= syarat[tingkat-1]) {
								akumGk = member.getPvLitfoot();
							}
						} else if (bonusLastMonth.getBorTingkat() < 6) {
							tingkat = bonusLastMonth.getBorTingkat();
							if (member.getPvLitfoot() >= syarat[tingkat-1]) {
								akumGk = bonusLastMonth.getBorAkumgk() + member.getPvLitfoot();
							}
						}
					}
				}
				if (akumulasi[tingkat-1] <= akumGk) {
					System.out.println("Member "+member.getMemberId()+" dengan akumulasi GK "+akumGk+" sudah kualifikasi untuk BOR Tingkat "+tingkat);
					isqualified = 1;
				}
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
				if (bonus == null) {
					bonus = new BonusB();
					bonus.setMemberId(member.getMemberId());
					bonus.setBulan(bulanHitung.toDate());
					bonus.setBorAkumgk(akumGk);
					bonus.setBorTingkat(tingkat);
					bonus.setBorIsqualified(isqualified);
					BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
					bonusFcd.insertBonus(dbConn);
				} else {
					bonus.setBorAkumgk(akumGk);
					bonus.setBorTingkat(tingkat);
					bonus.setBorIsqualified(isqualified);
					BonusBTempFcd bonusFcd = new BonusBTempFcd();
					bonusFcd.updateBonus(bonus, dbConn);
				}
			}
			for (int i=0; i<budgetPct.length; i++) {
				getQualifiedBorMember(i+1, budgetPct[i], nilaiBor[i], dbConn);
			}
//			giveBonusAntrian(dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getQualifiedBorMember(int tingkat, int budgetPct, int nilaiBor, DBConnection dbConn) {
		double budgetTingkat = budgetPct * bvTotal / 100;
		System.out.println("BOR Tingkat "+tingkat+" punya budget sebesar "+budgetTingkat);
		int totalClaimed = (int) budgetTingkat / nilaiBor;
		System.out.println("BOR Tingkat "+tingkat+" dapat diklaim sebanyak "+totalClaimed);
		if (totalClaimed > 0) {
			String whereClause = "bor_isqualified=1 AND bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
				+ " AND bor_tingkat=" + tingkat + " AND bor_isclaimed=0"
				+ " ORDER BY bor_akumgk DESC";
			BonusBTempFcd bbFcd = new BonusBTempFcd();
			BonusBSet bbSet = null;
			try {
				bbSet = bbFcd.search(whereClause, dbConn);
				int batas = totalClaimed;
				if (totalClaimed > bbSet.length()) batas = bbSet.length();
				for (int i=0; i<batas; i++) {
					BonusB bonus = bbSet.get(i);
					bonus.setBorIsclaimed(1);
					bbFcd.updateBonus(bonus, dbConn);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void cekSyaratBRC() {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "bulan = '" + lastMonth.toString("yyyy-MM-dd") + "'"
			+ " AND (brc_continuity_cd > 0 OR brc_continuity_scd > 0)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		BonusBTempFcd bTempFcd = new BonusBTempFcd();
		try {
			bbSet = bbFcd.search(whereClause);
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonusLast = bbSet.get(i);
					if (bonusLast.getBrcIsqualifiedCd() == 0 && bonusLast.getBrcContinuityCd() > 0) {
//						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
//							+ " AND member_id = '" + bonusLast.getMemberId() + "'";
//						MemberTreeFcd mtFcd = new MemberTreeFcd();
//						MemberTreeSet mtSet = mtFcd.search(whereClause);
//						MemberTree member = mtSet.get(0);
						MemberTree member = MemberTreeFcd.getMemberTreeById(bonusLast.getMemberId());
//						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
						BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId());
						if (member.getPvLitfoot() >= brcCdKualifikasi) {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd() + 1);
							if (bonus.getBrcContinuityCd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedCd(1);
							}
							if (member.getPvLitfoot() >= brcScdKualifikasi) {
								bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
								if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
									bonus.setBrcIsqualifiedScd(1);
								}
							} else {
								bonus.setBrcContinuityScd(0);
							}
						} else {
							bonus.setBrcContinuityCd(0);
							bonus.setBrcContinuityScd(0);
						}
//						bbFcd.updateBonus(bonus);
						bTempFcd.updateBonus(bonus);
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 0 && bonusLast.getBrcContinuityScd() > 0) {
//						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
//							+ " AND member_id = '" + bonusLast.getMemberId() + "'";
//						MemberTreeFcd mtFcd = new MemberTreeFcd();
//						MemberTreeSet mtSet = mtFcd.search(whereClause);
//						MemberTree member = mtSet.get(0);
						MemberTree member = MemberTreeFcd.getMemberTreeById(bonusLast.getMemberId());
//						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
						BonusB bonus = BonusBTempFcd.getBonusByMember(bonusLast.getMemberId());
						bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
						bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
						if (member.getPvLitfoot() >= brcScdKualifikasi) {
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
							if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedScd(1);
							}
						} else {
							bonus.setBrcContinuityScd(0);
						}
//						bbFcd.updateBonus(bonus);
						bTempFcd.updateBonus(bonus);
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 1) {
//						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(bonusLast.getMemberId(), bulanHitung.toDate());
						BonusB bonus = BonusBTempFcd.getBonusByMember(bonusLast.getMemberId());
						if (bonus == null) {
							bonus = new BonusB();
							bonus.setMemberId(bonusLast.getMemberId());
							bonus.setBulan(bulanHitung.toDate());
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
							BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
							bonusFcd.insertBonus();
						} else {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
							bTempFcd.updateBonus(bonus);
						}
					}
				}
			}
			whereClause = "pv_litfoot >= " + brcCdKualifikasi + " AND "
				+ "member_id IN (SELECT member_id FROM bonus_b "
				+ "WHERE brc_isqualified_cd = 0 AND brc_continuity_cd = 0 "
				+ "AND brc_isqualified_scd = 0 AND brc_continuity_scd = 0)";
			MemberTreeFcd mtFcd = new MemberTreeFcd();
			MemberTreeSet mtSet = mtFcd.search(whereClause);
			for (int j=0; j<mtSet.length(); j++) {
				MemberTree member = mtSet.get(j);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate());
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId());
				if (member.getPvLitfoot() >= brcCdKualifikasi) {
					bonus.setBrcContinuityCd(1);
					if (member.getPvLitfoot() >= brcScdKualifikasi) {
						bonus.setBrcContinuityScd(1);
					}
//					bbFcd.updateBonus(bonus);
					bTempFcd.updateBonus(bonus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cekSyaratBRC(DBConnection dbConn) {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "bulan = '" + lastMonth.toString("yyyy-MM-dd") + "'"
			+ " AND (brc_continuity_cd > 0 OR brc_continuity_scd > 0)";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		BonusBTempFcd bTempFcd = new BonusBTempFcd();
		try {
			bbSet = bbFcd.search(whereClause, dbConn);
			if (bbSet != null) {
				for (int i=0; i<bbSet.length(); i++) {
					BonusB bonusLast = bbSet.get(i);
					if (bonusLast.getBrcIsqualifiedCd() == 0 && bonusLast.getBrcContinuityCd() > 0) {
//						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
//							+ " AND member_id = '" + bonusLast.getMemberId() + "'";
//						MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
//						MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
//						MemberTree member = mtSet.get(0);
						MemberTree member = MemberTreeFcd.getMemberTreeById(bonusLast.getMemberId());
//						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
						BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
						if (member.getPvLitfoot() >= brcCdKualifikasi) {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd() + 1);
							if (bonus.getBrcContinuityCd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedCd(1);
							}
							if (member.getPvLitfoot() >= brcScdKualifikasi) {
								bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
								if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
									bonus.setBrcIsqualifiedScd(1);
								}
							} else {
								bonus.setBrcContinuityScd(0);
							}
						} else {
							bonus.setBrcContinuityCd(0);
							bonus.setBrcContinuityScd(0);
						}
//						bbFcd.updateBonus(bonus, dbConn);
						bTempFcd.updateBonus(bonus, dbConn);
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 0 && bonusLast.getBrcContinuityScd() > 0) {
//						whereClause = "tanggal = '" + bulanHitung.toString("yyyy-MM-dd") + "'"
//							+ " AND member_id = '" + bonusLast.getMemberId() + "'";
//						MemberTreeFcd mtFcd = new MemberTreeFcd();
//						MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
//						MemberTree member = mtSet.get(0);
						MemberTree member = MemberTreeFcd.getMemberTreeById(bonusLast.getMemberId());
//						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
						BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
						bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
						bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
						if (member.getPvLitfoot() >= brcScdKualifikasi) {
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd() + 1);
							if (bonus.getBrcContinuityScd() == BRC_KUALIFIKASI_BERURUT) {
								bonus.setBrcIsqualifiedScd(1);
							}
						} else {
							bonus.setBrcContinuityScd(0);
						}
//						bbFcd.updateBonus(bonus, dbConn);
						bTempFcd.updateBonus(bonus, dbConn);
					} else if (bonusLast.getBrcIsqualifiedCd() == 1 && bonusLast.getBrcIsqualifiedScd() == 1) {
//						BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(bonusLast.getMemberId(), bulanHitung.toDate(), dbConn);
						BonusB bonus = BonusBTempFcd.getBonusByMember(bonusLast.getMemberId(), dbConn);
						if (bonus == null) {
							bonus = new BonusB();
							bonus.setMemberId(bonusLast.getMemberId());
							bonus.setBulan(bulanHitung.toDate());
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
							BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
							bonusFcd.insertBonus(dbConn);
						} else {
							bonus.setBrcContinuityCd(bonusLast.getBrcContinuityCd());
							bonus.setBrcIsqualifiedCd(bonusLast.getBrcIsqualifiedCd());
							bonus.setBrcContinuityScd(bonusLast.getBrcContinuityScd());
							bonus.setBrcIsqualifiedScd(bonusLast.getBrcIsqualifiedScd());
//							bbFcd.updateBonus(bonus, dbConn);
							bTempFcd.updateBonus(bonus, dbConn);
						}
					}
				}
			}
			whereClause = "pv_litfoot >= " + brcCdKualifikasi + " AND "
				+ "member_id IN (SELECT member_id FROM bonus_b "
				+ "WHERE brc_isqualified_cd = 0 AND brc_continuity_cd = 0 "
				+ "AND brc_isqualified_scd = 0 AND brc_continuity_scd = 0)";
			MemberTreeFcd mtFcd = new MemberTreeFcd();
			MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
			for (int j=0; j<mtSet.length(); j++) {
				MemberTree member = mtSet.get(j);
//				BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
				BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
				if (member.getPvLitfoot() >= brcCdKualifikasi) {
					bonus.setBrcContinuityCd(1);
					if (member.getPvLitfoot() >= brcScdKualifikasi) {
						bonus.setBrcContinuityScd(1);
					}
//					bbFcd.updateBonus(bonus, dbConn);
					bTempFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusRC() {
		cekSyaratBRC();
//		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
//			+ "AND (brc_isqualified_cd = 1 OR brc_isqualified_scd = 1)";
		String whereClause = "brc_isqualified_cd = 1 OR brc_isqualified_scd = 1";
		BonusBTempFcd bbFcd = new BonusBTempFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause);
			if (bbSet != null) {
				double totalBagianCd = bbSet.length();
				if (totalBagianCd > 0) {
					double bagianCd = bvTotal * brcCdPct / (100 * totalBagianCd);
					double bagianScd = getBagianScd();
					for (int i=0; i<bbSet.length(); i++) {
						BonusB bonus = bbSet.get(i);
						bonus.setBonusRc(bagianCd);
						if (bonus.getBrcIsqualifiedScd() == 1) {
							bonus.setBonusRc(bagianCd + bagianScd);
						} else {
							bonus.setBonusRc(bagianCd);
						}
						bbFcd.updateBonus(bonus);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusRC(DBConnection dbConn) {
		cekSyaratBRC(dbConn);
//		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
//			+ "AND (brc_isqualified_cd = 1 OR brc_isqualified_scd = 1)";
		String whereClause = "brc_isqualified_cd = 1 OR brc_isqualified_scd = 1";
		BonusBTempFcd bbFcd = new BonusBTempFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause, dbConn);
			if (bbSet != null) {
				double totalBagianCd = bbSet.length();
				if (totalBagianCd > 0) {
					double bagianCd = bvTotal * brcCdPct / (100 * totalBagianCd);
					double bagianScd = getBagianScd(dbConn);
					for (int i=0; i<bbSet.length(); i++) {
						BonusB bonus = bbSet.get(i);
						bonus.setBonusRc(bagianCd);
						if (bonus.getBrcIsqualifiedScd() == 1) {
							bonus.setBonusRc(bagianCd + bagianScd);
						} else {
							bonus.setBonusRc(bagianCd);
						}
						bbFcd.updateBonus(bonus, dbConn);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private double getBagianScd() {
		double bagianScd = 0;
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND brc_isqualified_scd = 1";
		BonusBTempFcd bbFcd = new BonusBTempFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause);
			double totalBagianScd = bbSet.length();
			if (bagianScd > 0)
				bagianScd = bvTotal * brcScdPct / (100 * totalBagianScd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bagianScd;
	}
	
	private double getBagianScd(DBConnection dbConn) {
		double bagianScd = 0;
		String whereClause = "bulan = '" + bulanHitung.toString("yyyy-MM-dd") + "' "
			+ "AND brc_isqualified_scd = 1";
		BonusBFcd bbFcd = new BonusBFcd();
		BonusBSet bbSet = null;
		try {
			bbSet = bbFcd.search(whereClause, dbConn);
			double totalBagianScd = bbSet.length();
			if (bagianScd > 0)
				bagianScd = bvTotal * brcScdPct / (100 * totalBagianScd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bagianScd;
	}
	
	public void giveBonusWT(DBConnection dbConn) {
		MutableDateTime lastMonth = new MutableDateTime(bulanHitung.getYear(), bulanHitung.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		String whereClause = "pv_litfoot >= " + bwtRmMingk;
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSet mtSet = null;
		try {
			mtSet = mtFcd.search(whereClause, dbConn);
			double totalBagian = getTotalBagianBwt(mtSet);
			if (totalBagian > 0) {
				double perBagian = bvTotal * bwtBudgetPct / (100 * totalBagian);
				for (int i=0; i<mtSet.length(); i++) {
					MemberTree member = mtSet.get(i);
//					double bagian = NumberUtil.roundToDecimals((member.getPvLitfoot() / bwtPvPerbagian), 0);
					double bagian = 0;
					if (member.getPvLitfoot() >= bwtScdMingk) {
						bagian = bwtScdBagian;
					} else if (member.getPvLitfoot() >= bwtCdMingk) {
						bagian = bwtCdBagian;
					} else if (member.getPvLitfoot() >= bwtDmMingk) {
						bagian = bwtDmBagian;
					} else if (member.getPvLitfoot() >= bwtEmMingk) {
						bagian = bwtEmBagian;
					} else {
						bagian = bwtRmBagian;
					}
					double bonusWt = NumberUtil.roundToDecimals((bagian * perBagian), 0);
//					BonusB bonus = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), bulanHitung.toDate(), dbConn);
					BonusB bonus = BonusBTempFcd.getBonusByMember(member.getMemberId(), dbConn);
					if (bonus == null) {
						bonus = new BonusB();
						bonus.setMemberId(member.getMemberId());
						bonus.setBulan(bulanHitung.toDate());
						bonus.setTraveling(bonusWt);
						double akumTravel = 0;
						whereClause = "member_id = '" + member.getMemberId() + "' AND bulan < '" + bulanHitung.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
						BonusBFcd bbFcd = new BonusBFcd();
						BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
//						BonusB bonusLast = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
						BonusB bonusLast = null;
						if (bbSet != null) bonusLast = bbSet.get(0);
						if (bonusLast != null) {
							akumTravel = bonusLast.getBwtAkum();
						}
						akumTravel += bonusWt;
						bonus.setBwtAkum(akumTravel);
						BonusBTempFcd bonusFcd = new BonusBTempFcd(bonus);
						bonusFcd.insertBonus(dbConn);
					} else {
						bonus.setTraveling(bonusWt);
						double akumTravel = 0;
						whereClause = "member_id = '" + member.getMemberId() + "' AND bulan < '" + bulanHitung.toString("yyyy-MM-dd") + "' ORDER BY bulan DESC LIMIT 1";
						BonusBFcd bbFcd = new BonusBFcd();
						BonusBSet bbSet = bbFcd.search(whereClause, dbConn);
//						BonusB bonusLast = BonusBFcd.getBonusByMemberAndMonth(member.getMemberId(), lastMonth.toDate(), dbConn);
						BonusB bonusLast = null;
						if (bbSet != null) bonusLast = bbSet.get(0);
						if (bonusLast != null) {
							akumTravel = bonusLast.getBwtAkum();
						}
						akumTravel += bonusWt;
						bonus.setBwtAkum(akumTravel);
//						bbFcd.updateBonus(bonus, dbConn);
						BonusBTempFcd bTempFcd = new BonusBTempFcd();
						bTempFcd.updateBonus(bonus, dbConn);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private double getTotalBagianBwt(MemberTreeSet mtSet) {
		double totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
//			double bagian = NumberUtil.roundToDecimals((member.getPvLitfoot() / bwtPvPerbagian), 0);
			double bagian = 0;
			if (member.getPvLitfoot() >= bwtScdMingk) {
				bagian = bwtScdBagian;
			} else if (member.getPvLitfoot() >= bwtCdMingk) {
				bagian = bwtCdBagian;
			} else if (member.getPvLitfoot() >= bwtDmMingk) {
				bagian = bwtDmBagian;
			} else if (member.getPvLitfoot() >= bwtEmMingk) {
				bagian = bwtEmBagian;
			} else {
				bagian = bwtRmBagian;
			}
			totalBagian += bagian;
		}
		return totalBagian;
	}
}
