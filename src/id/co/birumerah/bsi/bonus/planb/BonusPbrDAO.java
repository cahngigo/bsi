package id.co.birumerah.bsi.bonus.planb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusPbrDAO {

	private DBConnection dbConn;
	private DateFormat shortFormat;
	
	public BonusPbrDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusPbr dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_pbr (");
		sb.append("member_id,");
		sb.append("bulan,");
		sb.append("pbr50,");
		sb.append("pbr100,");
		sb.append("pbr200");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getBulan()) + "',");
		sb.append("" + dataObject.getPbr50() + ",");
		sb.append("" + dataObject.getPbr100() + ",");
		sb.append("" + dataObject.getPbr200());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusPbr dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_pbr SET ");
		sb.append("pbr50 = " + dataObject.getPbr50() + ",");
		sb.append("pbr100 = " + dataObject.getPbr100() + ",");
		sb.append("pbr200 = " + dataObject.getPbr200() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusPbr dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("pbr50,");
		sb.append("pbr100,");
		sb.append("pbr200 ");
		sb.append("FROM bonus_pbr ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND bulan = '" + shortFormat.format(dataObject.getBulan()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setPbr50(rs.getInt("pbr50"));
				dataObject.setPbr100(rs.getInt("pbr100"));
				dataObject.setPbr200(rs.getInt("pbr200"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
