package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class BonusBLogPeserta {

	private long id;
	private Date bulan;
	private String memberId;
	private double bigJ1;
	private double bigJ2;
	private double bigJ3;
	private double bigJ4;
	private double bigJ5;
	private double bigJ6;
	private double bigJ7;
	private double bigJ8;
	private double bigJ9;
	private double bul1;
	private double bul2;
	private double bul3;
	private double bul4;
	private double bul5;
	private double bul6;
	
	public BonusBLogPeserta(){}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public double getBigJ1() {
		return bigJ1;
	}

	public void setBigJ1(double bigJ1) {
		this.bigJ1 = bigJ1;
	}

	public double getBigJ2() {
		return bigJ2;
	}

	public void setBigJ2(double bigJ2) {
		this.bigJ2 = bigJ2;
	}

	public double getBigJ3() {
		return bigJ3;
	}

	public void setBigJ3(double bigJ3) {
		this.bigJ3 = bigJ3;
	}

	public double getBigJ4() {
		return bigJ4;
	}

	public void setBigJ4(double bigJ4) {
		this.bigJ4 = bigJ4;
	}

	public double getBigJ5() {
		return bigJ5;
	}

	public void setBigJ5(double bigJ5) {
		this.bigJ5 = bigJ5;
	}

	public double getBigJ6() {
		return bigJ6;
	}

	public void setBigJ6(double bigJ6) {
		this.bigJ6 = bigJ6;
	}

	public double getBigJ7() {
		return bigJ7;
	}

	public void setBigJ7(double bigJ7) {
		this.bigJ7 = bigJ7;
	}

	public double getBigJ8() {
		return bigJ8;
	}

	public void setBigJ8(double bigJ8) {
		this.bigJ8 = bigJ8;
	}

	public double getBigJ9() {
		return bigJ9;
	}

	public void setBigJ9(double bigJ9) {
		this.bigJ9 = bigJ9;
	}

	public double getBul1() {
		return bul1;
	}

	public void setBul1(double bul1) {
		this.bul1 = bul1;
	}

	public double getBul2() {
		return bul2;
	}

	public void setBul2(double bul2) {
		this.bul2 = bul2;
	}

	public double getBul3() {
		return bul3;
	}

	public void setBul3(double bul3) {
		this.bul3 = bul3;
	}

	public double getBul4() {
		return bul4;
	}

	public void setBul4(double bul4) {
		this.bul4 = bul4;
	}

	public double getBul5() {
		return bul5;
	}

	public void setBul5(double bul5) {
		this.bul5 = bul5;
	}

	public double getBul6() {
		return bul6;
	}

	public void setBul6(double bul6) {
		this.bul6 = bul6;
	}
	
	
}
