package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class BonusBLog {

	private long id;
	private Date bulan;
	private double budgetAll;
	private int broT1Totalbagian;
	private double broT11Bagian;
	private double broT1Totalbonus;
	private int broT2Totalbagian;
	private double broT21Bagian;
	private double broT2Totalbonus;
	private int broT3Totalbagian;
	private double broT31Bagian;
	private double broT3Totalbonus;
	private int broT4Totalbagian;
	private double broT41Bagian;
	private double broT4Totalbonus;
	private int broT5Totalbagian;
	private double broT51Bagian;
	private double broT5Totalbonus;
	private int bigJ1GbTotalbag;
	private double bigJ1Gb1Bag;
	private double bigJ1GbTotalbonus;
	private int bigJ1GsTotalbag;
	private double bigJ1Gs1Bag;
	private double bigJ1GsTotalbonus;
	private int bigJ1GkTotalbag;
	private double bigJ1Gk1Bag;
	private double bigJ1GkTotalbonus;
	private int bigJ2GbTotalbag;
	private double bigJ2Gb1Bag;
	private double bigJ2GbTotalbonus;
	private int bigJ2GsTotalbag;
	private double bigJ2Gs1Bag;
	private double bigJ2GsTotalbonus;
	private int bigJ2GkTotalbag;
	private double bigJ2Gk1Bag;
	private double bigJ2GkTotalbonus;
	private int bigJ3GbTotalbag;
	private double bigJ3Gb1Bag;
	private double bigJ3GbTotalbonus;
	private int bigJ3GsTotalbag;
	private double bigJ3Gs1Bag;
	private double bigJ3GsTotalbonus;
	private int bigJ3GkTotalbag;
	private double bigJ3Gk1Bag;
	private double bigJ3GkTotalbonus;
	private int bigJ4GbTotalbag;
	private double bigJ4Gb1Bag;
	private double bigJ4GbTotalbonus;
	private int bigJ4GsTotalbag;
	private double bigJ4Gs1Bag;
	private double bigJ4GsTotalbonus;
	private int bigJ4GkTotalbag;
	private double bigJ4Gk1Bag;
	private double bigJ4GkTotalbonus;
	private int bigJ5GbTotalbag;
	private double bigJ5Gb1Bag;
	private double bigJ5GbTotalbonus;
	private int bigJ5GsTotalbag;
	private double bigJ5Gs1Bag;
	private double bigJ5GsTotalbonus;
	private int bigJ5GkTotalbag;
	private double bigJ5Gk1Bag;
	private double bigJ5GkTotalbonus;
	private int bigJ6GbTotalbag;
	private double bigJ6Gb1Bag;
	private double bigJ6GbTotalbonus;
	private int bigJ6GsTotalbag;
	private double bigJ6Gs1Bag;
	private double bigJ6GsTotalbonus;
	private int bigJ6GkTotalbag;
	private double bigJ6Gk1Bag;
	private double bigJ6GkTotalbonus;
	private int bigJ7GbTotalbag;
	private double bigJ7Gb1Bag;
	private double bigJ7GbTotalbonus;
	private int bigJ7GsTotalbag;
	private double bigJ7Gs1Bag;
	private double bigJ7GsTotalbonus;
	private int bigJ7GkTotalbag;
	private double bigJ7Gk1Bag;
	private double bigJ7GkTotalbonus;
	private int bigJ8GbTotalbag;
	private double bigJ8Gb1Bag;
	private double bigJ8GbTotalbonus;
	private int bigJ8GsTotalbag;
	private double bigJ8Gs1Bag;
	private double bigJ8GsTotalbonus;
	private int bigJ8GkTotalbag;
	private double bigJ8Gk1Bag;
	private double bigJ8GkTotalbonus;
	private int bigJ9GbTotalbag;
	private double bigJ9Gb1Bag;
	private double bigJ9GbTotalbonus;
	private int bigJ9GsTotalbag;
	private double bigJ9Gs1Bag;
	private double bigJ9GsTotalbonus;
	private int bigJ9GkTotalbag;
	private double bigJ9Gk1Bag;
	private double bigJ9GkTotalbonus;
	private int bulT1Totalbag;
	private double bulT11Bag;
	private double bulT1Totalbonus;
	private int bulT2Totalbag;
	private double bulT21Bag;
	private double bulT2Totalbonus;
	private int bulT3Totalbag;
	private double bulT31Bag;
	private double bulT3Totalbonus;
	private int bulT4Totalbag;
	private double bulT41Bag;
	private double bulT4Totalbonus;
	private int bulT5Totalbag;
	private double bulT51Bag;
	private double bulT5Totalbonus;
	private int bulT6Totalbag;
	private double bulT61Bag;
	private double bulT6Totalbonus;
	private int bebT1Totalbag;
	private double bebT11Bag;
	private double bebT1Totalbonus;
	private int bebT2Totalbag;
	private double bebT21Bag;
	private double bebT2Totalbonus;
	private double borT1Budget;
	private int borT1Totalklaim;
	private double borT1Saldo;
	private double borT2Budget;
	private int borT2Totalklaim;
	private double borT2Saldo;
	private double borT3Budget;
	private int borT3Totalklaim;
	private double borT3Saldo;
	private double borT4Budget;
	private int borT4Totalklaim;
	private double borT4Saldo;
	private double borT5Budget;
	private int borT5Totalklaim;
	private double borT5Saldo;
	private double borT6Budget;
	private int borT6Totalklaim;
	private double borT6Saldo;
	private int borantriT1Acceptor;
	private double borantriT1Budget;
	private double borantriT1Saldo;
	private int borantriT2Acceptor;
	private double borantriT2Budget;
	private double borantriT2Saldo;
	private int borantriT3Acceptor;
	private double borantriT3Budget;
	private double borantriT3Saldo;
	private int borantriT4Acceptor;
	private double borantriT4Budget;
	private double borantriT4Saldo;
	private int brcT1Acceptor;
	private double brcT11Bagian;
	private int brcT2Acceptor;
	private double brcT21Bagian;
	private double bwt1Bagian;
	private int bwtTotalbagian;
	private float ldpS1BudgetPct;
	private double ldpS1Budget;
	private int ldpS1Totalbag;
	private double ldpS11Bagian;
	private float ldpS2BudgetPct;
	private double ldpS2Budget;
	private int ldpS2Totalbag;
	private double ldpS21Bagian;
	private float ldpS3BudgetPct;
	private double ldpS3Budget;
	private int ldpS3Totalbag;
	private double ldpS31Bagian;
	private float ldpS4BudgetPct;
	private double ldpS4Budget;
	private int ldpS4Totalbag;
	private double ldpS41Bagian;
	
	public BonusBLog() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public double getBudgetAll() {
		return budgetAll;
	}

	public void setBudgetAll(double budgetAll) {
		this.budgetAll = budgetAll;
	}

	public int getBroT1Totalbagian() {
		return broT1Totalbagian;
	}

	public void setBroT1Totalbagian(int broT1Totalbagian) {
		this.broT1Totalbagian = broT1Totalbagian;
	}

	public double getBroT11Bagian() {
		return broT11Bagian;
	}

	public void setBroT11Bagian(double broT11Bagian) {
		this.broT11Bagian = broT11Bagian;
	}

	public double getBroT1Totalbonus() {
		return broT1Totalbonus;
	}

	public void setBroT1Totalbonus(double broT1Totalbonus) {
		this.broT1Totalbonus = broT1Totalbonus;
	}

	public int getBroT2Totalbagian() {
		return broT2Totalbagian;
	}

	public void setBroT2Totalbagian(int broT2Totalbagian) {
		this.broT2Totalbagian = broT2Totalbagian;
	}

	public double getBroT21Bagian() {
		return broT21Bagian;
	}

	public void setBroT21Bagian(double broT21Bagian) {
		this.broT21Bagian = broT21Bagian;
	}

	public double getBroT2Totalbonus() {
		return broT2Totalbonus;
	}

	public void setBroT2Totalbonus(double broT2Totalbonus) {
		this.broT2Totalbonus = broT2Totalbonus;
	}

	public int getBroT3Totalbagian() {
		return broT3Totalbagian;
	}

	public void setBroT3Totalbagian(int broT3Totalbagian) {
		this.broT3Totalbagian = broT3Totalbagian;
	}

	public double getBroT31Bagian() {
		return broT31Bagian;
	}

	public void setBroT31Bagian(double broT31Bagian) {
		this.broT31Bagian = broT31Bagian;
	}

	public double getBroT3Totalbonus() {
		return broT3Totalbonus;
	}

	public void setBroT3Totalbonus(double broT3Totalbonus) {
		this.broT3Totalbonus = broT3Totalbonus;
	}

	public int getBroT4Totalbagian() {
		return broT4Totalbagian;
	}

	public void setBroT4Totalbagian(int broT4Totalbagian) {
		this.broT4Totalbagian = broT4Totalbagian;
	}

	public double getBroT41Bagian() {
		return broT41Bagian;
	}

	public void setBroT41Bagian(double broT41Bagian) {
		this.broT41Bagian = broT41Bagian;
	}

	public double getBroT4Totalbonus() {
		return broT4Totalbonus;
	}

	public void setBroT4Totalbonus(double broT4Totalbonus) {
		this.broT4Totalbonus = broT4Totalbonus;
	}

	public int getBroT5Totalbagian() {
		return broT5Totalbagian;
	}

	public void setBroT5Totalbagian(int broT5Totalbagian) {
		this.broT5Totalbagian = broT5Totalbagian;
	}

	public double getBroT51Bagian() {
		return broT51Bagian;
	}

	public void setBroT51Bagian(double broT51Bagian) {
		this.broT51Bagian = broT51Bagian;
	}

	public double getBroT5Totalbonus() {
		return broT5Totalbonus;
	}

	public void setBroT5Totalbonus(double broT5Totalbonus) {
		this.broT5Totalbonus = broT5Totalbonus;
	}

	public int getBigJ1GbTotalbag() {
		return bigJ1GbTotalbag;
	}

	public void setBigJ1GbTotalbag(int bigJ1GbTotalbag) {
		this.bigJ1GbTotalbag = bigJ1GbTotalbag;
	}

	public double getBigJ1Gb1Bag() {
		return bigJ1Gb1Bag;
	}

	public void setBigJ1Gb1Bag(double bigJ1Gb1Bag) {
		this.bigJ1Gb1Bag = bigJ1Gb1Bag;
	}

	public double getBigJ1GbTotalbonus() {
		return bigJ1GbTotalbonus;
	}

	public void setBigJ1GbTotalbonus(double bigJ1GbTotalbonus) {
		this.bigJ1GbTotalbonus = bigJ1GbTotalbonus;
	}

	public int getBigJ1GsTotalbag() {
		return bigJ1GsTotalbag;
	}

	public void setBigJ1GsTotalbag(int bigJ1GsTotalbag) {
		this.bigJ1GsTotalbag = bigJ1GsTotalbag;
	}

	public double getBigJ1Gs1Bag() {
		return bigJ1Gs1Bag;
	}

	public void setBigJ1Gs1Bag(double bigJ1Gs1Bag) {
		this.bigJ1Gs1Bag = bigJ1Gs1Bag;
	}

	public double getBigJ1GsTotalbonus() {
		return bigJ1GsTotalbonus;
	}

	public void setBigJ1GsTotalbonus(double bigJ1GsTotalbonus) {
		this.bigJ1GsTotalbonus = bigJ1GsTotalbonus;
	}

	public int getBigJ1GkTotalbag() {
		return bigJ1GkTotalbag;
	}

	public void setBigJ1GkTotalbag(int bigJ1GkTotalbag) {
		this.bigJ1GkTotalbag = bigJ1GkTotalbag;
	}

	public double getBigJ1Gk1Bag() {
		return bigJ1Gk1Bag;
	}

	public void setBigJ1Gk1Bag(double bigJ1Gk1Bag) {
		this.bigJ1Gk1Bag = bigJ1Gk1Bag;
	}

	public double getBigJ1GkTotalbonus() {
		return bigJ1GkTotalbonus;
	}

	public void setBigJ1GkTotalbonus(double bigJ1GkTotalbonus) {
		this.bigJ1GkTotalbonus = bigJ1GkTotalbonus;
	}

	public int getBigJ2GbTotalbag() {
		return bigJ2GbTotalbag;
	}

	public void setBigJ2GbTotalbag(int bigJ2GbTotalbag) {
		this.bigJ2GbTotalbag = bigJ2GbTotalbag;
	}

	public double getBigJ2Gb1Bag() {
		return bigJ2Gb1Bag;
	}

	public void setBigJ2Gb1Bag(double bigJ2Gb1Bag) {
		this.bigJ2Gb1Bag = bigJ2Gb1Bag;
	}

	public double getBigJ2GbTotalbonus() {
		return bigJ2GbTotalbonus;
	}

	public void setBigJ2GbTotalbonus(double bigJ2GbTotalbonus) {
		this.bigJ2GbTotalbonus = bigJ2GbTotalbonus;
	}

	public int getBigJ2GsTotalbag() {
		return bigJ2GsTotalbag;
	}

	public void setBigJ2GsTotalbag(int bigJ2GsTotalbag) {
		this.bigJ2GsTotalbag = bigJ2GsTotalbag;
	}

	public double getBigJ2Gs1Bag() {
		return bigJ2Gs1Bag;
	}

	public void setBigJ2Gs1Bag(double bigJ2Gs1Bag) {
		this.bigJ2Gs1Bag = bigJ2Gs1Bag;
	}

	public double getBigJ2GsTotalbonus() {
		return bigJ2GsTotalbonus;
	}

	public void setBigJ2GsTotalbonus(double bigJ2GsTotalbonus) {
		this.bigJ2GsTotalbonus = bigJ2GsTotalbonus;
	}

	public int getBigJ2GkTotalbag() {
		return bigJ2GkTotalbag;
	}

	public void setBigJ2GkTotalbag(int bigJ2GkTotalbag) {
		this.bigJ2GkTotalbag = bigJ2GkTotalbag;
	}

	public double getBigJ2Gk1Bag() {
		return bigJ2Gk1Bag;
	}

	public void setBigJ2Gk1Bag(double bigJ2Gk1Bag) {
		this.bigJ2Gk1Bag = bigJ2Gk1Bag;
	}

	public double getBigJ2GkTotalbonus() {
		return bigJ2GkTotalbonus;
	}

	public void setBigJ2GkTotalbonus(double bigJ2GkTotalbonus) {
		this.bigJ2GkTotalbonus = bigJ2GkTotalbonus;
	}

	public int getBigJ3GbTotalbag() {
		return bigJ3GbTotalbag;
	}

	public void setBigJ3GbTotalbag(int bigJ3GbTotalbag) {
		this.bigJ3GbTotalbag = bigJ3GbTotalbag;
	}

	public double getBigJ3Gb1Bag() {
		return bigJ3Gb1Bag;
	}

	public void setBigJ3Gb1Bag(double bigJ3Gb1Bag) {
		this.bigJ3Gb1Bag = bigJ3Gb1Bag;
	}

	public double getBigJ3GbTotalbonus() {
		return bigJ3GbTotalbonus;
	}

	public void setBigJ3GbTotalbonus(double bigJ3GbTotalbonus) {
		this.bigJ3GbTotalbonus = bigJ3GbTotalbonus;
	}

	public int getBigJ3GsTotalbag() {
		return bigJ3GsTotalbag;
	}

	public void setBigJ3GsTotalbag(int bigJ3GsTotalbag) {
		this.bigJ3GsTotalbag = bigJ3GsTotalbag;
	}

	public double getBigJ3Gs1Bag() {
		return bigJ3Gs1Bag;
	}

	public void setBigJ3Gs1Bag(double bigJ3Gs1Bag) {
		this.bigJ3Gs1Bag = bigJ3Gs1Bag;
	}

	public double getBigJ3GsTotalbonus() {
		return bigJ3GsTotalbonus;
	}

	public void setBigJ3GsTotalbonus(double bigJ3GsTotalbonus) {
		this.bigJ3GsTotalbonus = bigJ3GsTotalbonus;
	}

	public int getBigJ3GkTotalbag() {
		return bigJ3GkTotalbag;
	}

	public void setBigJ3GkTotalbag(int bigJ3GkTotalbag) {
		this.bigJ3GkTotalbag = bigJ3GkTotalbag;
	}

	public double getBigJ3Gk1Bag() {
		return bigJ3Gk1Bag;
	}

	public void setBigJ3Gk1Bag(double bigJ3Gk1Bag) {
		this.bigJ3Gk1Bag = bigJ3Gk1Bag;
	}

	public double getBigJ3GkTotalbonus() {
		return bigJ3GkTotalbonus;
	}

	public void setBigJ3GkTotalbonus(double bigJ3GkTotalbonus) {
		this.bigJ3GkTotalbonus = bigJ3GkTotalbonus;
	}

	public int getBigJ4GbTotalbag() {
		return bigJ4GbTotalbag;
	}

	public void setBigJ4GbTotalbag(int bigJ4GbTotalbag) {
		this.bigJ4GbTotalbag = bigJ4GbTotalbag;
	}

	public double getBigJ4Gb1Bag() {
		return bigJ4Gb1Bag;
	}

	public void setBigJ4Gb1Bag(double bigJ4Gb1Bag) {
		this.bigJ4Gb1Bag = bigJ4Gb1Bag;
	}

	public double getBigJ4GbTotalbonus() {
		return bigJ4GbTotalbonus;
	}

	public void setBigJ4GbTotalbonus(double bigJ4GbTotalbonus) {
		this.bigJ4GbTotalbonus = bigJ4GbTotalbonus;
	}

	public int getBigJ4GsTotalbag() {
		return bigJ4GsTotalbag;
	}

	public void setBigJ4GsTotalbag(int bigJ4GsTotalbag) {
		this.bigJ4GsTotalbag = bigJ4GsTotalbag;
	}

	public double getBigJ4Gs1Bag() {
		return bigJ4Gs1Bag;
	}

	public void setBigJ4Gs1Bag(double bigJ4Gs1Bag) {
		this.bigJ4Gs1Bag = bigJ4Gs1Bag;
	}

	public double getBigJ4GsTotalbonus() {
		return bigJ4GsTotalbonus;
	}

	public void setBigJ4GsTotalbonus(double bigJ4GsTotalbonus) {
		this.bigJ4GsTotalbonus = bigJ4GsTotalbonus;
	}

	public int getBigJ4GkTotalbag() {
		return bigJ4GkTotalbag;
	}

	public void setBigJ4GkTotalbag(int bigJ4GkTotalbag) {
		this.bigJ4GkTotalbag = bigJ4GkTotalbag;
	}

	public double getBigJ4Gk1Bag() {
		return bigJ4Gk1Bag;
	}

	public void setBigJ4Gk1Bag(double bigJ4Gk1Bag) {
		this.bigJ4Gk1Bag = bigJ4Gk1Bag;
	}

	public double getBigJ4GkTotalbonus() {
		return bigJ4GkTotalbonus;
	}

	public void setBigJ4GkTotalbonus(double bigJ4GkTotalbonus) {
		this.bigJ4GkTotalbonus = bigJ4GkTotalbonus;
	}

	public int getBigJ5GbTotalbag() {
		return bigJ5GbTotalbag;
	}

	public void setBigJ5GbTotalbag(int bigJ5GbTotalbag) {
		this.bigJ5GbTotalbag = bigJ5GbTotalbag;
	}

	public double getBigJ5Gb1Bag() {
		return bigJ5Gb1Bag;
	}

	public void setBigJ5Gb1Bag(double bigJ5Gb1Bag) {
		this.bigJ5Gb1Bag = bigJ5Gb1Bag;
	}

	public double getBigJ5GbTotalbonus() {
		return bigJ5GbTotalbonus;
	}

	public void setBigJ5GbTotalbonus(double bigJ5GbTotalbonus) {
		this.bigJ5GbTotalbonus = bigJ5GbTotalbonus;
	}

	public int getBigJ5GsTotalbag() {
		return bigJ5GsTotalbag;
	}

	public void setBigJ5GsTotalbag(int bigJ5GsTotalbag) {
		this.bigJ5GsTotalbag = bigJ5GsTotalbag;
	}

	public double getBigJ5Gs1Bag() {
		return bigJ5Gs1Bag;
	}

	public void setBigJ5Gs1Bag(double bigJ5Gs1Bag) {
		this.bigJ5Gs1Bag = bigJ5Gs1Bag;
	}

	public double getBigJ5GsTotalbonus() {
		return bigJ5GsTotalbonus;
	}

	public void setBigJ5GsTotalbonus(double bigJ5GsTotalbonus) {
		this.bigJ5GsTotalbonus = bigJ5GsTotalbonus;
	}

	public int getBigJ5GkTotalbag() {
		return bigJ5GkTotalbag;
	}

	public void setBigJ5GkTotalbag(int bigJ5GkTotalbag) {
		this.bigJ5GkTotalbag = bigJ5GkTotalbag;
	}

	public double getBigJ5Gk1Bag() {
		return bigJ5Gk1Bag;
	}

	public void setBigJ5Gk1Bag(double bigJ5Gk1Bag) {
		this.bigJ5Gk1Bag = bigJ5Gk1Bag;
	}

	public double getBigJ5GkTotalbonus() {
		return bigJ5GkTotalbonus;
	}

	public void setBigJ5GkTotalbonus(double bigJ5GkTotalbonus) {
		this.bigJ5GkTotalbonus = bigJ5GkTotalbonus;
	}

	public int getBigJ6GbTotalbag() {
		return bigJ6GbTotalbag;
	}

	public void setBigJ6GbTotalbag(int bigJ6GbTotalbag) {
		this.bigJ6GbTotalbag = bigJ6GbTotalbag;
	}

	public double getBigJ6Gb1Bag() {
		return bigJ6Gb1Bag;
	}

	public void setBigJ6Gb1Bag(double bigJ6Gb1Bag) {
		this.bigJ6Gb1Bag = bigJ6Gb1Bag;
	}

	public double getBigJ6GbTotalbonus() {
		return bigJ6GbTotalbonus;
	}

	public void setBigJ6GbTotalbonus(double bigJ6GbTotalbonus) {
		this.bigJ6GbTotalbonus = bigJ6GbTotalbonus;
	}

	public int getBigJ6GsTotalbag() {
		return bigJ6GsTotalbag;
	}

	public void setBigJ6GsTotalbag(int bigJ6GsTotalbag) {
		this.bigJ6GsTotalbag = bigJ6GsTotalbag;
	}

	public double getBigJ6Gs1Bag() {
		return bigJ6Gs1Bag;
	}

	public void setBigJ6Gs1Bag(double bigJ6Gs1Bag) {
		this.bigJ6Gs1Bag = bigJ6Gs1Bag;
	}

	public double getBigJ6GsTotalbonus() {
		return bigJ6GsTotalbonus;
	}

	public void setBigJ6GsTotalbonus(double bigJ6GsTotalbonus) {
		this.bigJ6GsTotalbonus = bigJ6GsTotalbonus;
	}

	public int getBigJ6GkTotalbag() {
		return bigJ6GkTotalbag;
	}

	public void setBigJ6GkTotalbag(int bigJ6GkTotalbag) {
		this.bigJ6GkTotalbag = bigJ6GkTotalbag;
	}

	public double getBigJ6Gk1Bag() {
		return bigJ6Gk1Bag;
	}

	public void setBigJ6Gk1Bag(double bigJ6Gk1Bag) {
		this.bigJ6Gk1Bag = bigJ6Gk1Bag;
	}

	public double getBigJ6GkTotalbonus() {
		return bigJ6GkTotalbonus;
	}

	public void setBigJ6GkTotalbonus(double bigJ6GkTotalbonus) {
		this.bigJ6GkTotalbonus = bigJ6GkTotalbonus;
	}

	public int getBigJ7GbTotalbag() {
		return bigJ7GbTotalbag;
	}

	public void setBigJ7GbTotalbag(int bigJ7GbTotalbag) {
		this.bigJ7GbTotalbag = bigJ7GbTotalbag;
	}

	public double getBigJ7Gb1Bag() {
		return bigJ7Gb1Bag;
	}

	public void setBigJ7Gb1Bag(double bigJ7Gb1Bag) {
		this.bigJ7Gb1Bag = bigJ7Gb1Bag;
	}

	public double getBigJ7GbTotalbonus() {
		return bigJ7GbTotalbonus;
	}

	public void setBigJ7GbTotalbonus(double bigJ7GbTotalbonus) {
		this.bigJ7GbTotalbonus = bigJ7GbTotalbonus;
	}

	public int getBigJ7GsTotalbag() {
		return bigJ7GsTotalbag;
	}

	public void setBigJ7GsTotalbag(int bigJ7GsTotalbag) {
		this.bigJ7GsTotalbag = bigJ7GsTotalbag;
	}

	public double getBigJ7Gs1Bag() {
		return bigJ7Gs1Bag;
	}

	public void setBigJ7Gs1Bag(double bigJ7Gs1Bag) {
		this.bigJ7Gs1Bag = bigJ7Gs1Bag;
	}

	public double getBigJ7GsTotalbonus() {
		return bigJ7GsTotalbonus;
	}

	public void setBigJ7GsTotalbonus(double bigJ7GsTotalbonus) {
		this.bigJ7GsTotalbonus = bigJ7GsTotalbonus;
	}

	public int getBigJ7GkTotalbag() {
		return bigJ7GkTotalbag;
	}

	public void setBigJ7GkTotalbag(int bigJ7GkTotalbag) {
		this.bigJ7GkTotalbag = bigJ7GkTotalbag;
	}

	public double getBigJ7Gk1Bag() {
		return bigJ7Gk1Bag;
	}

	public void setBigJ7Gk1Bag(double bigJ7Gk1Bag) {
		this.bigJ7Gk1Bag = bigJ7Gk1Bag;
	}

	public double getBigJ7GkTotalbonus() {
		return bigJ7GkTotalbonus;
	}

	public void setBigJ7GkTotalbonus(double bigJ7GkTotalbonus) {
		this.bigJ7GkTotalbonus = bigJ7GkTotalbonus;
	}

	public int getBigJ8GbTotalbag() {
		return bigJ8GbTotalbag;
	}

	public void setBigJ8GbTotalbag(int bigJ8GbTotalbag) {
		this.bigJ8GbTotalbag = bigJ8GbTotalbag;
	}

	public double getBigJ8Gb1Bag() {
		return bigJ8Gb1Bag;
	}

	public void setBigJ8Gb1Bag(double bigJ8Gb1Bag) {
		this.bigJ8Gb1Bag = bigJ8Gb1Bag;
	}

	public double getBigJ8GbTotalbonus() {
		return bigJ8GbTotalbonus;
	}

	public void setBigJ8GbTotalbonus(double bigJ8GbTotalbonus) {
		this.bigJ8GbTotalbonus = bigJ8GbTotalbonus;
	}

	public int getBigJ8GsTotalbag() {
		return bigJ8GsTotalbag;
	}

	public void setBigJ8GsTotalbag(int bigJ8GsTotalbag) {
		this.bigJ8GsTotalbag = bigJ8GsTotalbag;
	}

	public double getBigJ8Gs1Bag() {
		return bigJ8Gs1Bag;
	}

	public void setBigJ8Gs1Bag(double bigJ8Gs1Bag) {
		this.bigJ8Gs1Bag = bigJ8Gs1Bag;
	}

	public double getBigJ8GsTotalbonus() {
		return bigJ8GsTotalbonus;
	}

	public void setBigJ8GsTotalbonus(double bigJ8GsTotalbonus) {
		this.bigJ8GsTotalbonus = bigJ8GsTotalbonus;
	}

	public int getBigJ8GkTotalbag() {
		return bigJ8GkTotalbag;
	}

	public void setBigJ8GkTotalbag(int bigJ8GkTotalbag) {
		this.bigJ8GkTotalbag = bigJ8GkTotalbag;
	}

	public double getBigJ8Gk1Bag() {
		return bigJ8Gk1Bag;
	}

	public void setBigJ8Gk1Bag(double bigJ8Gk1Bag) {
		this.bigJ8Gk1Bag = bigJ8Gk1Bag;
	}

	public double getBigJ8GkTotalbonus() {
		return bigJ8GkTotalbonus;
	}

	public void setBigJ8GkTotalbonus(double bigJ8GkTotalbonus) {
		this.bigJ8GkTotalbonus = bigJ8GkTotalbonus;
	}

	public int getBigJ9GbTotalbag() {
		return bigJ9GbTotalbag;
	}

	public void setBigJ9GbTotalbag(int bigJ9GbTotalbag) {
		this.bigJ9GbTotalbag = bigJ9GbTotalbag;
	}

	public double getBigJ9Gb1Bag() {
		return bigJ9Gb1Bag;
	}

	public void setBigJ9Gb1Bag(double bigJ9Gb1Bag) {
		this.bigJ9Gb1Bag = bigJ9Gb1Bag;
	}

	public double getBigJ9GbTotalbonus() {
		return bigJ9GbTotalbonus;
	}

	public void setBigJ9GbTotalbonus(double bigJ9GbTotalbonus) {
		this.bigJ9GbTotalbonus = bigJ9GbTotalbonus;
	}

	public int getBigJ9GsTotalbag() {
		return bigJ9GsTotalbag;
	}

	public void setBigJ9GsTotalbag(int bigJ9GsTotalbag) {
		this.bigJ9GsTotalbag = bigJ9GsTotalbag;
	}

	public double getBigJ9Gs1Bag() {
		return bigJ9Gs1Bag;
	}

	public void setBigJ9Gs1Bag(double bigJ9Gs1Bag) {
		this.bigJ9Gs1Bag = bigJ9Gs1Bag;
	}

	public double getBigJ9GsTotalbonus() {
		return bigJ9GsTotalbonus;
	}

	public void setBigJ9GsTotalbonus(double bigJ9GsTotalbonus) {
		this.bigJ9GsTotalbonus = bigJ9GsTotalbonus;
	}

	public int getBigJ9GkTotalbag() {
		return bigJ9GkTotalbag;
	}

	public void setBigJ9GkTotalbag(int bigJ9GkTotalbag) {
		this.bigJ9GkTotalbag = bigJ9GkTotalbag;
	}

	public double getBigJ9Gk1Bag() {
		return bigJ9Gk1Bag;
	}

	public void setBigJ9Gk1Bag(double bigJ9Gk1Bag) {
		this.bigJ9Gk1Bag = bigJ9Gk1Bag;
	}

	public double getBigJ9GkTotalbonus() {
		return bigJ9GkTotalbonus;
	}

	public void setBigJ9GkTotalbonus(double bigJ9GkTotalbonus) {
		this.bigJ9GkTotalbonus = bigJ9GkTotalbonus;
	}

	public int getBulT1Totalbag() {
		return bulT1Totalbag;
	}

	public void setBulT1Totalbag(int bulT1Totalbag) {
		this.bulT1Totalbag = bulT1Totalbag;
	}

	public double getBulT11Bag() {
		return bulT11Bag;
	}

	public void setBulT11Bag(double bulT11Bag) {
		this.bulT11Bag = bulT11Bag;
	}

	public double getBulT1Totalbonus() {
		return bulT1Totalbonus;
	}

	public void setBulT1Totalbonus(double bulT1Totalbonus) {
		this.bulT1Totalbonus = bulT1Totalbonus;
	}

	public int getBulT2Totalbag() {
		return bulT2Totalbag;
	}

	public void setBulT2Totalbag(int bulT2Totalbag) {
		this.bulT2Totalbag = bulT2Totalbag;
	}

	public double getBulT21Bag() {
		return bulT21Bag;
	}

	public void setBulT21Bag(double bulT21Bag) {
		this.bulT21Bag = bulT21Bag;
	}

	public double getBulT2Totalbonus() {
		return bulT2Totalbonus;
	}

	public void setBulT2Totalbonus(double bulT2Totalbonus) {
		this.bulT2Totalbonus = bulT2Totalbonus;
	}

	public int getBulT3Totalbag() {
		return bulT3Totalbag;
	}

	public void setBulT3Totalbag(int bulT3Totalbag) {
		this.bulT3Totalbag = bulT3Totalbag;
	}

	public double getBulT31Bag() {
		return bulT31Bag;
	}

	public void setBulT31Bag(double bulT31Bag) {
		this.bulT31Bag = bulT31Bag;
	}

	public double getBulT3Totalbonus() {
		return bulT3Totalbonus;
	}

	public void setBulT3Totalbonus(double bulT3Totalbonus) {
		this.bulT3Totalbonus = bulT3Totalbonus;
	}

	public int getBulT4Totalbag() {
		return bulT4Totalbag;
	}

	public void setBulT4Totalbag(int bulT4Totalbag) {
		this.bulT4Totalbag = bulT4Totalbag;
	}

	public double getBulT41Bag() {
		return bulT41Bag;
	}

	public void setBulT41Bag(double bulT41Bag) {
		this.bulT41Bag = bulT41Bag;
	}

	public double getBulT4Totalbonus() {
		return bulT4Totalbonus;
	}

	public void setBulT4Totalbonus(double bulT4Totalbonus) {
		this.bulT4Totalbonus = bulT4Totalbonus;
	}

	public int getBulT5Totalbag() {
		return bulT5Totalbag;
	}

	public void setBulT5Totalbag(int bulT5Totalbag) {
		this.bulT5Totalbag = bulT5Totalbag;
	}

	public double getBulT51Bag() {
		return bulT51Bag;
	}

	public void setBulT51Bag(double bulT51Bag) {
		this.bulT51Bag = bulT51Bag;
	}

	public double getBulT5Totalbonus() {
		return bulT5Totalbonus;
	}

	public void setBulT5Totalbonus(double bulT5Totalbonus) {
		this.bulT5Totalbonus = bulT5Totalbonus;
	}

	public int getBulT6Totalbag() {
		return bulT6Totalbag;
	}

	public void setBulT6Totalbag(int bulT6Totalbag) {
		this.bulT6Totalbag = bulT6Totalbag;
	}

	public double getBulT61Bag() {
		return bulT61Bag;
	}

	public void setBulT61Bag(double bulT61Bag) {
		this.bulT61Bag = bulT61Bag;
	}

	public double getBulT6Totalbonus() {
		return bulT6Totalbonus;
	}

	public void setBulT6Totalbonus(double bulT6Totalbonus) {
		this.bulT6Totalbonus = bulT6Totalbonus;
	}

	public int getBebT1Totalbag() {
		return bebT1Totalbag;
	}

	public void setBebT1Totalbag(int bebT1Totalbag) {
		this.bebT1Totalbag = bebT1Totalbag;
	}

	public double getBebT11Bag() {
		return bebT11Bag;
	}

	public void setBebT11Bag(double bebT11Bag) {
		this.bebT11Bag = bebT11Bag;
	}

	public double getBebT1Totalbonus() {
		return bebT1Totalbonus;
	}

	public void setBebT1Totalbonus(double bebT1Totalbonus) {
		this.bebT1Totalbonus = bebT1Totalbonus;
	}

	public int getBebT2Totalbag() {
		return bebT2Totalbag;
	}

	public void setBebT2Totalbag(int bebT2Totalbag) {
		this.bebT2Totalbag = bebT2Totalbag;
	}

	public double getBebT21Bag() {
		return bebT21Bag;
	}

	public void setBebT21Bag(double bebT21Bag) {
		this.bebT21Bag = bebT21Bag;
	}

	public double getBebT2Totalbonus() {
		return bebT2Totalbonus;
	}

	public void setBebT2Totalbonus(double bebT2Totalbonus) {
		this.bebT2Totalbonus = bebT2Totalbonus;
	}

	public double getBorT1Budget() {
		return borT1Budget;
	}

	public void setBorT1Budget(double borT1Budget) {
		this.borT1Budget = borT1Budget;
	}

	public int getBorT1Totalklaim() {
		return borT1Totalklaim;
	}

	public void setBorT1Totalklaim(int borT1Totalklaim) {
		this.borT1Totalklaim = borT1Totalklaim;
	}

	public double getBorT1Saldo() {
		return borT1Saldo;
	}

	public void setBorT1Saldo(double borT1Saldo) {
		this.borT1Saldo = borT1Saldo;
	}

	public double getBorT2Budget() {
		return borT2Budget;
	}

	public void setBorT2Budget(double borT2Budget) {
		this.borT2Budget = borT2Budget;
	}

	public int getBorT2Totalklaim() {
		return borT2Totalklaim;
	}

	public void setBorT2Totalklaim(int borT2Totalklaim) {
		this.borT2Totalklaim = borT2Totalklaim;
	}

	public double getBorT2Saldo() {
		return borT2Saldo;
	}

	public void setBorT2Saldo(double borT2Saldo) {
		this.borT2Saldo = borT2Saldo;
	}

	public double getBorT3Budget() {
		return borT3Budget;
	}

	public void setBorT3Budget(double borT3Budget) {
		this.borT3Budget = borT3Budget;
	}

	public int getBorT3Totalklaim() {
		return borT3Totalklaim;
	}

	public void setBorT3Totalklaim(int borT3Totalklaim) {
		this.borT3Totalklaim = borT3Totalklaim;
	}

	public double getBorT3Saldo() {
		return borT3Saldo;
	}

	public void setBorT3Saldo(double borT3Saldo) {
		this.borT3Saldo = borT3Saldo;
	}

	public double getBorT4Budget() {
		return borT4Budget;
	}

	public void setBorT4Budget(double borT4Budget) {
		this.borT4Budget = borT4Budget;
	}

	public int getBorT4Totalklaim() {
		return borT4Totalklaim;
	}

	public void setBorT4Totalklaim(int borT4Totalklaim) {
		this.borT4Totalklaim = borT4Totalklaim;
	}

	public double getBorT4Saldo() {
		return borT4Saldo;
	}

	public void setBorT4Saldo(double borT4Saldo) {
		this.borT4Saldo = borT4Saldo;
	}

	public double getBorT5Budget() {
		return borT5Budget;
	}

	public void setBorT5Budget(double borT5Budget) {
		this.borT5Budget = borT5Budget;
	}

	public int getBorT5Totalklaim() {
		return borT5Totalklaim;
	}

	public void setBorT5Totalklaim(int borT5Totalklaim) {
		this.borT5Totalklaim = borT5Totalklaim;
	}

	public double getBorT5Saldo() {
		return borT5Saldo;
	}

	public void setBorT5Saldo(double borT5Saldo) {
		this.borT5Saldo = borT5Saldo;
	}

	public double getBorT6Budget() {
		return borT6Budget;
	}

	public void setBorT6Budget(double borT6Budget) {
		this.borT6Budget = borT6Budget;
	}

	public int getBorT6Totalklaim() {
		return borT6Totalklaim;
	}

	public void setBorT6Totalklaim(int borT6Totalklaim) {
		this.borT6Totalklaim = borT6Totalklaim;
	}

	public double getBorT6Saldo() {
		return borT6Saldo;
	}

	public void setBorT6Saldo(double borT6Saldo) {
		this.borT6Saldo = borT6Saldo;
	}

	public int getBorantriT1Acceptor() {
		return borantriT1Acceptor;
	}

	public void setBorantriT1Acceptor(int borantriT1Acceptor) {
		this.borantriT1Acceptor = borantriT1Acceptor;
	}

	public double getBorantriT1Budget() {
		return borantriT1Budget;
	}

	public void setBorantriT1Budget(double borantriT1Budget) {
		this.borantriT1Budget = borantriT1Budget;
	}

	public double getBorantriT1Saldo() {
		return borantriT1Saldo;
	}

	public void setBorantriT1Saldo(double borantriT1Saldo) {
		this.borantriT1Saldo = borantriT1Saldo;
	}

	public int getBorantriT2Acceptor() {
		return borantriT2Acceptor;
	}

	public void setBorantriT2Acceptor(int borantriT2Acceptor) {
		this.borantriT2Acceptor = borantriT2Acceptor;
	}

	public double getBorantriT2Budget() {
		return borantriT2Budget;
	}

	public void setBorantriT2Budget(double borantriT2Budget) {
		this.borantriT2Budget = borantriT2Budget;
	}

	public double getBorantriT2Saldo() {
		return borantriT2Saldo;
	}

	public void setBorantriT2Saldo(double borantriT2Saldo) {
		this.borantriT2Saldo = borantriT2Saldo;
	}

	public int getBorantriT3Acceptor() {
		return borantriT3Acceptor;
	}

	public void setBorantriT3Acceptor(int borantriT3Acceptor) {
		this.borantriT3Acceptor = borantriT3Acceptor;
	}

	public double getBorantriT3Budget() {
		return borantriT3Budget;
	}

	public void setBorantriT3Budget(double borantriT3Budget) {
		this.borantriT3Budget = borantriT3Budget;
	}

	public double getBorantriT3Saldo() {
		return borantriT3Saldo;
	}

	public void setBorantriT3Saldo(double borantriT3Saldo) {
		this.borantriT3Saldo = borantriT3Saldo;
	}

	public int getBorantriT4Acceptor() {
		return borantriT4Acceptor;
	}

	public void setBorantriT4Acceptor(int borantriT4Acceptor) {
		this.borantriT4Acceptor = borantriT4Acceptor;
	}

	public double getBorantriT4Budget() {
		return borantriT4Budget;
	}

	public void setBorantriT4Budget(double borantriT4Budget) {
		this.borantriT4Budget = borantriT4Budget;
	}

	public double getBorantriT4Saldo() {
		return borantriT4Saldo;
	}

	public void setBorantriT4Saldo(double borantriT4Saldo) {
		this.borantriT4Saldo = borantriT4Saldo;
	}

	public int getBrcT1Acceptor() {
		return brcT1Acceptor;
	}

	public void setBrcT1Acceptor(int brcT1Acceptor) {
		this.brcT1Acceptor = brcT1Acceptor;
	}

	public int getBrcT2Acceptor() {
		return brcT2Acceptor;
	}

	public void setBrcT2Acceptor(int brcT2Acceptor) {
		this.brcT2Acceptor = brcT2Acceptor;
	}

	public double getBrcT11Bagian() {
		return brcT11Bagian;
	}

	public void setBrcT11Bagian(double brcT11Bagian) {
		this.brcT11Bagian = brcT11Bagian;
	}

	public double getBrcT21Bagian() {
		return brcT21Bagian;
	}

	public void setBrcT21Bagian(double brcT21Bagian) {
		this.brcT21Bagian = brcT21Bagian;
	}

	public double getBwt1Bagian() {
		return bwt1Bagian;
	}

	public void setBwt1Bagian(double bwt1Bagian) {
		this.bwt1Bagian = bwt1Bagian;
	}

	public int getBwtTotalbagian() {
		return bwtTotalbagian;
	}

	public void setBwtTotalbagian(int bwtTotalbagian) {
		this.bwtTotalbagian = bwtTotalbagian;
	}

	public float getLdpS1BudgetPct() {
		return ldpS1BudgetPct;
	}

	public void setLdpS1BudgetPct(float ldpS1BudgetPct) {
		this.ldpS1BudgetPct = ldpS1BudgetPct;
	}

	public double getLdpS1Budget() {
		return ldpS1Budget;
	}

	public void setLdpS1Budget(double ldpS1Budget) {
		this.ldpS1Budget = ldpS1Budget;
	}

	public int getLdpS1Totalbag() {
		return ldpS1Totalbag;
	}

	public void setLdpS1Totalbag(int ldpS1Totalbag) {
		this.ldpS1Totalbag = ldpS1Totalbag;
	}

	public double getLdpS11Bagian() {
		return ldpS11Bagian;
	}

	public void setLdpS11Bagian(double ldpS11Bagian) {
		this.ldpS11Bagian = ldpS11Bagian;
	}

	public float getLdpS2BudgetPct() {
		return ldpS2BudgetPct;
	}

	public void setLdpS2BudgetPct(float ldpS2BudgetPct) {
		this.ldpS2BudgetPct = ldpS2BudgetPct;
	}

	public double getLdpS2Budget() {
		return ldpS2Budget;
	}

	public void setLdpS2Budget(double ldpS2Budget) {
		this.ldpS2Budget = ldpS2Budget;
	}

	public int getLdpS2Totalbag() {
		return ldpS2Totalbag;
	}

	public void setLdpS2Totalbag(int ldpS2Totalbag) {
		this.ldpS2Totalbag = ldpS2Totalbag;
	}

	public double getLdpS21Bagian() {
		return ldpS21Bagian;
	}

	public void setLdpS21Bagian(double ldpS21Bagian) {
		this.ldpS21Bagian = ldpS21Bagian;
	}

	public float getLdpS3BudgetPct() {
		return ldpS3BudgetPct;
	}

	public void setLdpS3BudgetPct(float ldpS3BudgetPct) {
		this.ldpS3BudgetPct = ldpS3BudgetPct;
	}

	public double getLdpS3Budget() {
		return ldpS3Budget;
	}

	public void setLdpS3Budget(double ldpS3Budget) {
		this.ldpS3Budget = ldpS3Budget;
	}

	public int getLdpS3Totalbag() {
		return ldpS3Totalbag;
	}

	public void setLdpS3Totalbag(int ldpS3Totalbag) {
		this.ldpS3Totalbag = ldpS3Totalbag;
	}

	public double getLdpS31Bagian() {
		return ldpS31Bagian;
	}

	public void setLdpS31Bagian(double ldpS31Bagian) {
		this.ldpS31Bagian = ldpS31Bagian;
	}

	public float getLdpS4BudgetPct() {
		return ldpS4BudgetPct;
	}

	public void setLdpS4BudgetPct(float ldpS4BudgetPct) {
		this.ldpS4BudgetPct = ldpS4BudgetPct;
	}

	public double getLdpS4Budget() {
		return ldpS4Budget;
	}

	public void setLdpS4Budget(double ldpS4Budget) {
		this.ldpS4Budget = ldpS4Budget;
	}

	public int getLdpS4Totalbag() {
		return ldpS4Totalbag;
	}

	public void setLdpS4Totalbag(int ldpS4Totalbag) {
		this.ldpS4Totalbag = ldpS4Totalbag;
	}

	public double getLdpS41Bagian() {
		return ldpS41Bagian;
	}

	public void setLdpS41Bagian(double ldpS41Bagian) {
		this.ldpS41Bagian = ldpS41Bagian;
	}
}