package id.co.birumerah.bsi.bonus.planb;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBFcd {

	private BonusB bonusB;
	
	public BonusBFcd() {}
	
	public BonusBFcd(BonusB bonusB) {
		this.bonusB = bonusB;
	}

	public BonusB getBonusB() {
		return bonusB;
	}

	public void setBonusB(BonusB bonusB) {
		this.bonusB = bonusB;
	}
	
	public BonusBSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		BonusBSet bbSet = null;
		
		try {
			dbConn = new DBConnection();
			BonusBDAO bbDAO = new BonusBDAO(dbConn);
			bbSet = bbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return bbSet;
	}
	
	public BonusBSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		BonusBSet bbSet = null;
		
		try {
//			dbConn = new DBConnection();
			BonusBDAO bbDAO = new BonusBDAO(dbConn);
			bbSet = bbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return bbSet;
	}
	
	public static BonusB getBonusByMemberAndMonth(String memberId, Date bulan)
			throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusB bonus = new BonusB();
			bonus.setMemberId(memberId);
			bonus.setBulan(bulan);
			
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			boolean found = bonusDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static BonusB getBonusByMemberAndMonth(String memberId, Date bulan, DBConnection dbConn)
			throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			BonusB bonus = new BonusB();
			bonus.setMemberId(memberId);
			bonus.setBulan(bulan);
			
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			boolean found = bonusDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public void insertBonus() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			bonusDAO.insert(bonusB);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			bonusDAO.insert(bonusB);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void updateBonus(BonusB bonus) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			if (bonusDAO.update(bonus) < 1) {
				throw new Exception("Update bonus gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateBonus(BonusB bonus, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			if (bonusDAO.update(bonus) < 1) {
				throw new Exception("Update Bonus B failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void insertTransfer(String tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBDAO bonusDAO = new BonusBDAO(dbConn);
			if (bonusDAO.insertBonusTransfer(tanggal) < 1) {
				throw new Exception("Insert ke bonus_transfer gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public double getTotalBonusByMember(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		double total = 0;
		try {
			BonusBDAO bonusDao = new BonusBDAO(dbConn);
			total = bonusDao.getTotalBonusByMember(memberId, bulan);
		}  catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return total;
	}
}
