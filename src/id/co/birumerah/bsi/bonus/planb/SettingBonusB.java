package id.co.birumerah.bsi.bonus.planb;

import java.util.Date;

public class SettingBonusB {

	private long id;
	private float minPvpribadi;
	private int pctRo1;
	private int pctRo2;
	private int pctRo3;
	private int pctRo4;
	private int pctRo5;
	private float grade1Ro;
	private float grade2Ro;
	private float grade3Ro;
	private float grade4Ro;
	private float grade5Ro;
	private int roPvPerbagian;
	private float minPvpribadiBig;
	private int bigMaxBagSedang;
    private int bigMaxBagKecil;
    private int bigSmPvgk;
	private int bigGmPvgk;
	private int bigRmPvgk;
	private int bigEmPvgk;
	private int bigPmPvgk;
	private int bigDmPvgk;
	private int bigCdPvgk;
	private int bigScdPvgk;
	private float bigPctBalance;
	private float bigSharePct1;
	private int bigMaxBag1;
	private float bigSharePct2;
	private int bigMaxBag2;
	private float bigSharePct3;
	private int bigMaxBag3;
	private float bigSharePct4;
	private int bigMaxBag4;
	private int bigMaxBag5;
	private int bigMaxgkBag1;
	private float bigGkPct1;
	private int bigMaxgsGk1;
	private float bigGsPct1;
	private int bigMaxgbGk1;
	private float bigGbPct1;
	private int bigPerbagian1;
	private float minPvpribadiBig1;
	private int bigMaxgkBag2;
	private float bigGkPct2;
	private int bigMaxgsGk2;
	private float bigGsPct2;
	private int bigMaxgbGk2;
	private float bigGbPct2;
	private int bigPerbagian2;
	private float minPvpribadiBig2;
	private int bigMaxgkBag3;
	private float bigGkPct3;
	private int bigMaxgsGk3;
	private float bigGsPct3;
	private int bigMaxgbGk3;
	private float bigGbPct3;
	private int bigPerbagian3;
	private float minPvpribadiBig3;
	private int bigMaxgkBag4;
	private float bigGkPct4;
	private int bigMaxgsGk4;
	private float bigGsPct4;
	private int bigMaxgbGk4;
	private float bigGbPct4;
	private int bigPerbagian4;
	private float minPvpribadiBig4;
	private int bigMaxgkBag5;
	private float bigGkPct5;
	private int bigMaxgsGk5;
	private float bigGsPct5;
	private int bigMaxgbGk5;
	private float bigGbPct5;
	private int bigPerbagian5;
	private float minPvpribadiBig5;
	private int bigMaxgkBag6;
	private float bigGkPct6;
	private int bigMaxgsGk6;
	private float bigGsPct6;
	private int bigMaxgbGk6;
	private float bigGbPct6;
	private int bigPerbagian6;
	private float minPvpribadiBig6;
	private int bigMaxgkBag7;
	private float bigGkPct7;
	private int bigMaxgsGk7;
	private float bigGsPct7;
	private int bigMaxgbGk7;
	private float bigGbPct7;
	private int bigPerbagian7;
	private float minPvpribadiBig7;
	private int bigMaxgkBag8;
	private float bigGkPct8;
	private int bigMaxgsGk8;
	private float bigGsPct8;
	private int bigMaxgbGk8;
	private float bigGbPct8;
	private int bigPerbagian8;
	private float minPvpribadiBig8;
	private float bigGkPct9;
	private float bigGsPct9;
	private float bigGbPct9;
	private int bigPerbagian9;
	private float minPvpribadiBig9;
	private int bigPvPerbagian;
	private float minPvpribadiBul;
	private int bulRmKualifikasi;
	private int bulEmKualifikasi;
	private int bulPmKualifikasi;
	private int bulDmKualifikasi;
	private int bulCdKualifikasi;
	private int bulScdKualifikasi;
	private int bulRmKualifikasi2Jalur;
	private int bulEmKualifikasi2Jalur;
	private int bulPmKualifikasi2Jalur;
	private int bulDmKualifikasi2Jalur;
	private int bulCdKualifikasi2Jalur;
	private int bulScdKualifikasi2Jalur;
	private float bulPct1;
	private float bulPct2;
	private float bulPct3;
	private float bulPct4;
	private float bulPct5;
	private float bulPct6;
	private float minPvpribadiBul1;
	private float minPvpribadiBul2;
	private float minPvpribadiBul3;
	private float minPvpribadiBul4;
	private float minPvpribadiBul5;
	private float minPvpribadiBul6;
	private float minPvpribadiBeb;
	private int bebMingk1;
	private int bebMaxgk1;
	private int bebPct1;
	private int bebTgl1;
	private int bebMingk2;
	private int bebMaxgk2;
	private int bebPct2;
	private int bebTgl2;
	private int bebPvPerbagian;
	private float minPvpribadiBor;
	private int borWisataValue;
	private int borWisataPct;
	private int borWisataAkumgk;
	private int borWisataTambahangk;
	private int borMotorValue;
	private int borMotorPct;
	private int borMotorAkumgk;
	private int borMotorTambahangk;
	private int borReligiValue;
	private int borReligiPct;
	private int borReligiAkumgk;
	private int borReligiTambahangk;
	private int borMobilValue;
	private int borMobilPct;
	private int borMobilAkumgk;
	private int borMobilTambahangk;
	private int borMobilMewahValue;
	private int borMobilMewahPct;
	private int borMobilMewahAkumgk;
	private int borMobilMewahTambahangk;
	private int borRumahMewahValue;
	private int borRumahMewahPct;
	private int borRumahMewahAkumgk;
	private int borRumahMewahTambahangk;
	private float borAntriWisataPct;
	private float borAntriMotorPct;
	private float borAntriReligiPct;
	private float borAntriMobilPct;
	private float minPvpribadiBrc;
	private int brcCdKualifikasi;
	private float brcCdPct;
	private int brcScdKualifikasi;
	private float brcScdPct;
	private int brcCdKualifikasi2Jalur;
	private int brcScdKualifikasi2Jalur;
	private float minPvpribadiBwt;
	private int bwtRmMingk;
	private int bwtRmBagian;
	private int bwtEmMingk;
	private int bwtEmBagian;
	private int bwtDmMingk;
	private int bwtDmBagian;
	private int bwtCdMingk;
	private int bwtCdBagian;
	private int bwtScdMingk;
	private int bwtScdBagian;
	private int bwtPvPerbagian;
	private float bwtBudgetPct;
	private float ldpS1BudgetPct;
	private double ldpS1AkumBudget;
	private int ldpS1BagianPertama;
	private int ldpS1BagianNext;
	private float ldpS2BudgetPct;
	private double ldpS2AkumBudget;
	private int ldpS2BagianPertama;
	private int ldpS2BagianNext;
	private float ldpS3BudgetPct;
	private double ldpS3AkumBudget;
	private int ldpS3BagianPertama;
	private int ldpS3BagianNext;
	private float ldpS4BudgetPct;
	private double ldpS4AkumBudget;
	private int ldpS4BagianPertama;
	private int ldpS4BagianNext;
	private Date startDate;
	private String status;
	
	public SettingBonusB() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getMinPvpribadi() {
		return minPvpribadi;
	}

	public void setMinPvpribadi(float minPvpribadi) {
		this.minPvpribadi = minPvpribadi;
	}

	public int getPctRo1() {
		return pctRo1;
	}

	public void setPctRo1(int pctRo1) {
		this.pctRo1 = pctRo1;
	}

	public int getPctRo2() {
		return pctRo2;
	}

	public void setPctRo2(int pctRo2) {
		this.pctRo2 = pctRo2;
	}

	public int getPctRo3() {
		return pctRo3;
	}

	public void setPctRo3(int pctRo3) {
		this.pctRo3 = pctRo3;
	}

	public int getPctRo4() {
		return pctRo4;
	}

	public void setPctRo4(int pctRo4) {
		this.pctRo4 = pctRo4;
	}

	public int getPctRo5() {
		return pctRo5;
	}

	public void setPctRo5(int pctRo5) {
		this.pctRo5 = pctRo5;
	}

	public float getGrade1Ro() {
		return grade1Ro;
	}

	public void setGrade1Ro(float grade1Ro) {
		this.grade1Ro = grade1Ro;
	}

	public float getGrade2Ro() {
		return grade2Ro;
	}

	public void setGrade2Ro(float grade2Ro) {
		this.grade2Ro = grade2Ro;
	}

	public float getGrade3Ro() {
		return grade3Ro;
	}

	public void setGrade3Ro(float grade3Ro) {
		this.grade3Ro = grade3Ro;
	}

	public float getGrade4Ro() {
		return grade4Ro;
	}

	public void setGrade4Ro(float grade4Ro) {
		this.grade4Ro = grade4Ro;
	}

	public float getGrade5Ro() {
		return grade5Ro;
	}

	public void setGrade5Ro(float grade5Ro) {
		this.grade5Ro = grade5Ro;
	}

	public int getRoPvPerbagian() {
		return roPvPerbagian;
	}

	public void setRoPvPerbagian(int roPvPerbagian) {
		this.roPvPerbagian = roPvPerbagian;
	}

	public int getBigMaxBagSedang() {
		return bigMaxBagSedang;
	}

	public void setBigMaxBagSedang(int bigMaxBagSedang) {
		this.bigMaxBagSedang = bigMaxBagSedang;
	}

	public int getBigMaxBagKecil() {
		return bigMaxBagKecil;
	}

	public void setBigMaxBagKecil(int bigMaxBagKecil) {
		this.bigMaxBagKecil = bigMaxBagKecil;
	}

	public int getBigSmPvgk() {
		return bigSmPvgk;
	}

	public void setBigSmPvgk(int bigSmPvgk) {
		this.bigSmPvgk = bigSmPvgk;
	}

	public int getBigGmPvgk() {
		return bigGmPvgk;
	}

	public void setBigGmPvgk(int bigGmPvgk) {
		this.bigGmPvgk = bigGmPvgk;
	}

	public int getBigRmPvgk() {
		return bigRmPvgk;
	}

	public void setBigRmPvgk(int bigRmPvgk) {
		this.bigRmPvgk = bigRmPvgk;
	}

	public int getBigEmPvgk() {
		return bigEmPvgk;
	}

	public void setBigEmPvgk(int bigEmPvgk) {
		this.bigEmPvgk = bigEmPvgk;
	}

	public int getBigPmPvgk() {
		return bigPmPvgk;
	}

	public void setBigPmPvgk(int bigPmPvgk) {
		this.bigPmPvgk = bigPmPvgk;
	}

	public int getBigDmPvgk() {
		return bigDmPvgk;
	}

	public void setBigDmPvgk(int bigDmPvgk) {
		this.bigDmPvgk = bigDmPvgk;
	}

	public int getBigCdPvgk() {
		return bigCdPvgk;
	}

	public void setBigCdPvgk(int bigCdPvgk) {
		this.bigCdPvgk = bigCdPvgk;
	}

	public int getBigScdPvgk() {
		return bigScdPvgk;
	}

	public void setBigScdPvgk(int bigScdPvgk) {
		this.bigScdPvgk = bigScdPvgk;
	}

	public float getBigPctBalance() {
		return bigPctBalance;
	}

	public void setBigPctBalance(float bigPctBalance) {
		this.bigPctBalance = bigPctBalance;
	}

	public int getBigMaxgkBag1() {
		return bigMaxgkBag1;
	}

	public void setBigMaxgkBag1(int bigMaxgkBag1) {
		this.bigMaxgkBag1 = bigMaxgkBag1;
	}

	public float getBigGkPct1() {
		return bigGkPct1;
	}

	public void setBigGkPct1(float bigGkPct1) {
		this.bigGkPct1 = bigGkPct1;
	}

	public int getBigMaxgsGk1() {
		return bigMaxgsGk1;
	}

	public void setBigMaxgsGk1(int bigMaxgsGk1) {
		this.bigMaxgsGk1 = bigMaxgsGk1;
	}

	public float getBigGsPct1() {
		return bigGsPct1;
	}

	public void setBigGsPct1(float bigGsPct1) {
		this.bigGsPct1 = bigGsPct1;
	}

	public int getBigMaxgbGk1() {
		return bigMaxgbGk1;
	}

	public void setBigMaxgbGk1(int bigMaxgbGk1) {
		this.bigMaxgbGk1 = bigMaxgbGk1;
	}

	public float getBigGbPct1() {
		return bigGbPct1;
	}

	public void setBigGbPct1(float bigGbPct1) {
		this.bigGbPct1 = bigGbPct1;
	}

	public int getBigMaxgkBag2() {
		return bigMaxgkBag2;
	}

	public void setBigMaxgkBag2(int bigMaxgkBag2) {
		this.bigMaxgkBag2 = bigMaxgkBag2;
	}

	public float getBigGkPct2() {
		return bigGkPct2;
	}

	public void setBigGkPct2(float bigGkPct2) {
		this.bigGkPct2 = bigGkPct2;
	}

	public int getBigMaxgsGk2() {
		return bigMaxgsGk2;
	}

	public void setBigMaxgsGk2(int bigMaxgsGk2) {
		this.bigMaxgsGk2 = bigMaxgsGk2;
	}

	public float getBigGsPct2() {
		return bigGsPct2;
	}

	public void setBigGsPct2(float bigGsPct2) {
		this.bigGsPct2 = bigGsPct2;
	}

	public int getBigMaxgbGk2() {
		return bigMaxgbGk2;
	}

	public void setBigMaxgbGk2(int bigMaxgbGk2) {
		this.bigMaxgbGk2 = bigMaxgbGk2;
	}

	public float getBigGbPct2() {
		return bigGbPct2;
	}

	public void setBigGbPct2(float bigGbPct2) {
		this.bigGbPct2 = bigGbPct2;
	}

	public int getBigMaxgkBag3() {
		return bigMaxgkBag3;
	}

	public void setBigMaxgkBag3(int bigMaxgkBag3) {
		this.bigMaxgkBag3 = bigMaxgkBag3;
	}

	public float getBigGkPct3() {
		return bigGkPct3;
	}

	public void setBigGkPct3(float bigGkPct3) {
		this.bigGkPct3 = bigGkPct3;
	}

	public int getBigMaxgsGk3() {
		return bigMaxgsGk3;
	}

	public void setBigMaxgsGk3(int bigMaxgsGk3) {
		this.bigMaxgsGk3 = bigMaxgsGk3;
	}

	public float getBigGsPct3() {
		return bigGsPct3;
	}

	public void setBigGsPct3(float bigGsPct3) {
		this.bigGsPct3 = bigGsPct3;
	}

	public int getBigMaxgbGk3() {
		return bigMaxgbGk3;
	}

	public void setBigMaxgbGk3(int bigMaxgbGk3) {
		this.bigMaxgbGk3 = bigMaxgbGk3;
	}

	public float getBigGbPct3() {
		return bigGbPct3;
	}

	public void setBigGbPct3(float bigGbPct3) {
		this.bigGbPct3 = bigGbPct3;
	}

	public int getBigMaxgkBag4() {
		return bigMaxgkBag4;
	}

	public void setBigMaxgkBag4(int bigMaxgkBag4) {
		this.bigMaxgkBag4 = bigMaxgkBag4;
	}

	public float getBigGkPct4() {
		return bigGkPct4;
	}

	public void setBigGkPct4(float bigGkPct4) {
		this.bigGkPct4 = bigGkPct4;
	}

	public int getBigMaxgsGk4() {
		return bigMaxgsGk4;
	}

	public void setBigMaxgsGk4(int bigMaxgsGk4) {
		this.bigMaxgsGk4 = bigMaxgsGk4;
	}

	public float getBigGsPct4() {
		return bigGsPct4;
	}

	public void setBigGsPct4(float bigGsPct4) {
		this.bigGsPct4 = bigGsPct4;
	}

	public int getBigMaxgbGk4() {
		return bigMaxgbGk4;
	}

	public void setBigMaxgbGk4(int bigMaxgbGk4) {
		this.bigMaxgbGk4 = bigMaxgbGk4;
	}

	public float getBigGbPct4() {
		return bigGbPct4;
	}

	public void setBigGbPct4(float bigGbPct4) {
		this.bigGbPct4 = bigGbPct4;
	}

	public int getBigMaxgkBag5() {
		return bigMaxgkBag5;
	}

	public void setBigMaxgkBag5(int bigMaxgkBag5) {
		this.bigMaxgkBag5 = bigMaxgkBag5;
	}

	public float getBigGkPct5() {
		return bigGkPct5;
	}

	public void setBigGkPct5(float bigGkPct5) {
		this.bigGkPct5 = bigGkPct5;
	}

	public int getBigMaxgsGk5() {
		return bigMaxgsGk5;
	}

	public void setBigMaxgsGk5(int bigMaxgsGk5) {
		this.bigMaxgsGk5 = bigMaxgsGk5;
	}

	public float getBigGsPct5() {
		return bigGsPct5;
	}

	public void setBigGsPct5(float bigGsPct5) {
		this.bigGsPct5 = bigGsPct5;
	}

	public int getBigMaxgbGk5() {
		return bigMaxgbGk5;
	}

	public void setBigMaxgbGk5(int bigMaxgbGk5) {
		this.bigMaxgbGk5 = bigMaxgbGk5;
	}

	public float getBigGbPct5() {
		return bigGbPct5;
	}

	public void setBigGbPct5(float bigGbPct5) {
		this.bigGbPct5 = bigGbPct5;
	}

	public int getBigMaxgkBag6() {
		return bigMaxgkBag6;
	}

	public void setBigMaxgkBag6(int bigMaxgkBag6) {
		this.bigMaxgkBag6 = bigMaxgkBag6;
	}

	public float getBigGkPct6() {
		return bigGkPct6;
	}

	public void setBigGkPct6(float bigGkPct6) {
		this.bigGkPct6 = bigGkPct6;
	}

	public int getBigMaxgsGk6() {
		return bigMaxgsGk6;
	}

	public void setBigMaxgsGk6(int bigMaxgsGk6) {
		this.bigMaxgsGk6 = bigMaxgsGk6;
	}

	public float getBigGsPct6() {
		return bigGsPct6;
	}

	public void setBigGsPct6(float bigGsPct6) {
		this.bigGsPct6 = bigGsPct6;
	}

	public int getBigMaxgbGk6() {
		return bigMaxgbGk6;
	}

	public void setBigMaxgbGk6(int bigMaxgbGk6) {
		this.bigMaxgbGk6 = bigMaxgbGk6;
	}

	public float getBigGbPct6() {
		return bigGbPct6;
	}

	public void setBigGbPct6(float bigGbPct6) {
		this.bigGbPct6 = bigGbPct6;
	}

	public int getBigMaxgkBag7() {
		return bigMaxgkBag7;
	}

	public void setBigMaxgkBag7(int bigMaxgkBag7) {
		this.bigMaxgkBag7 = bigMaxgkBag7;
	}

	public float getBigGkPct7() {
		return bigGkPct7;
	}

	public void setBigGkPct7(float bigGkPct7) {
		this.bigGkPct7 = bigGkPct7;
	}

	public int getBigMaxgsGk7() {
		return bigMaxgsGk7;
	}

	public void setBigMaxgsGk7(int bigMaxgsGk7) {
		this.bigMaxgsGk7 = bigMaxgsGk7;
	}

	public float getBigGsPct7() {
		return bigGsPct7;
	}

	public void setBigGsPct7(float bigGsPct7) {
		this.bigGsPct7 = bigGsPct7;
	}

	public int getBigMaxgbGk7() {
		return bigMaxgbGk7;
	}

	public void setBigMaxgbGk7(int bigMaxgbGk7) {
		this.bigMaxgbGk7 = bigMaxgbGk7;
	}

	public float getBigGbPct7() {
		return bigGbPct7;
	}

	public void setBigGbPct7(float bigGbPct7) {
		this.bigGbPct7 = bigGbPct7;
	}

	public int getBigMaxgkBag8() {
		return bigMaxgkBag8;
	}

	public void setBigMaxgkBag8(int bigMaxgkBag8) {
		this.bigMaxgkBag8 = bigMaxgkBag8;
	}

	public float getBigGkPct8() {
		return bigGkPct8;
	}

	public void setBigGkPct8(float bigGkPct8) {
		this.bigGkPct8 = bigGkPct8;
	}

	public int getBigMaxgsGk8() {
		return bigMaxgsGk8;
	}

	public void setBigMaxgsGk8(int bigMaxgsGk8) {
		this.bigMaxgsGk8 = bigMaxgsGk8;
	}

	public float getBigGsPct8() {
		return bigGsPct8;
	}

	public void setBigGsPct8(float bigGsPct8) {
		this.bigGsPct8 = bigGsPct8;
	}

	public int getBigMaxgbGk8() {
		return bigMaxgbGk8;
	}

	public void setBigMaxgbGk8(int bigMaxgbGk8) {
		this.bigMaxgbGk8 = bigMaxgbGk8;
	}

	public float getBigGbPct8() {
		return bigGbPct8;
	}

	public void setBigGbPct8(float bigGbPct8) {
		this.bigGbPct8 = bigGbPct8;
	}

	public float getBigGkPct9() {
		return bigGkPct9;
	}

	public void setBigGkPct9(float bigGkPct9) {
		this.bigGkPct9 = bigGkPct9;
	}

	public float getBigGsPct9() {
		return bigGsPct9;
	}

	public void setBigGsPct9(float bigGsPct9) {
		this.bigGsPct9 = bigGsPct9;
	}

	public float getBigGbPct9() {
		return bigGbPct9;
	}

	public void setBigGbPct9(float bigGbPct9) {
		this.bigGbPct9 = bigGbPct9;
	}

	public int getBigPvPerbagian() {
		return bigPvPerbagian;
	}

	public void setBigPvPerbagian(int bigPvPerbagian) {
		this.bigPvPerbagian = bigPvPerbagian;
	}

	public int getBulRmKualifikasi() {
		return bulRmKualifikasi;
	}

	public void setBulRmKualifikasi(int bulRmKualifikasi) {
		this.bulRmKualifikasi = bulRmKualifikasi;
	}

	public int getBulEmKualifikasi() {
		return bulEmKualifikasi;
	}

	public void setBulEmKualifikasi(int bulEmKualifikasi) {
		this.bulEmKualifikasi = bulEmKualifikasi;
	}

	public int getBulPmKualifikasi() {
		return bulPmKualifikasi;
	}

	public void setBulPmKualifikasi(int bulPmKualifikasi) {
		this.bulPmKualifikasi = bulPmKualifikasi;
	}

	public int getBulDmKualifikasi() {
		return bulDmKualifikasi;
	}

	public void setBulDmKualifikasi(int bulDmKualifikasi) {
		this.bulDmKualifikasi = bulDmKualifikasi;
	}

	public int getBulCdKualifikasi() {
		return bulCdKualifikasi;
	}

	public void setBulCdKualifikasi(int bulCdKualifikasi) {
		this.bulCdKualifikasi = bulCdKualifikasi;
	}

	public int getBulScdKualifikasi() {
		return bulScdKualifikasi;
	}

	public void setBulScdKualifikasi(int bulScdKualifikasi) {
		this.bulScdKualifikasi = bulScdKualifikasi;
	}

	public int getBulRmKualifikasi2Jalur() {
		return bulRmKualifikasi2Jalur;
	}

	public void setBulRmKualifikasi2Jalur(int bulRmKualifikasi2Jalur) {
		this.bulRmKualifikasi2Jalur = bulRmKualifikasi2Jalur;
	}

	public int getBulEmKualifikasi2Jalur() {
		return bulEmKualifikasi2Jalur;
	}

	public void setBulEmKualifikasi2Jalur(int bulEmKualifikasi2Jalur) {
		this.bulEmKualifikasi2Jalur = bulEmKualifikasi2Jalur;
	}

	public int getBulPmKualifikasi2Jalur() {
		return bulPmKualifikasi2Jalur;
	}

	public void setBulPmKualifikasi2Jalur(int bulPmKualifikasi2Jalur) {
		this.bulPmKualifikasi2Jalur = bulPmKualifikasi2Jalur;
	}

	public int getBulDmKualifikasi2Jalur() {
		return bulDmKualifikasi2Jalur;
	}

	public void setBulDmKualifikasi2Jalur(int bulDmKualifikasi2Jalur) {
		this.bulDmKualifikasi2Jalur = bulDmKualifikasi2Jalur;
	}

	public int getBulCdKualifikasi2Jalur() {
		return bulCdKualifikasi2Jalur;
	}

	public void setBulCdKualifikasi2Jalur(int bulCdKualifikasi2Jalur) {
		this.bulCdKualifikasi2Jalur = bulCdKualifikasi2Jalur;
	}

	public int getBulScdKualifikasi2Jalur() {
		return bulScdKualifikasi2Jalur;
	}

	public void setBulScdKualifikasi2Jalur(int bulScdKualifikasi2Jalur) {
		this.bulScdKualifikasi2Jalur = bulScdKualifikasi2Jalur;
	}

	public float getBulPct1() {
		return bulPct1;
	}

	public void setBulPct1(float bulPct1) {
		this.bulPct1 = bulPct1;
	}

	public float getBulPct2() {
		return bulPct2;
	}

	public void setBulPct2(float bulPct2) {
		this.bulPct2 = bulPct2;
	}

	public float getBulPct3() {
		return bulPct3;
	}

	public void setBulPct3(float bulPct3) {
		this.bulPct3 = bulPct3;
	}

	public float getBulPct4() {
		return bulPct4;
	}

	public void setBulPct4(float bulPct4) {
		this.bulPct4 = bulPct4;
	}

	public float getBulPct5() {
		return bulPct5;
	}

	public void setBulPct5(float bulPct5) {
		this.bulPct5 = bulPct5;
	}

	public float getBulPct6() {
		return bulPct6;
	}

	public void setBulPct6(float bulPct6) {
		this.bulPct6 = bulPct6;
	}

	public int getBebMingk1() {
		return bebMingk1;
	}

	public void setBebMingk1(int bebMingk1) {
		this.bebMingk1 = bebMingk1;
	}

	public int getBebPct1() {
		return bebPct1;
	}

	public void setBebPct1(int bebPct1) {
		this.bebPct1 = bebPct1;
	}

	public int getBebTgl1() {
		return bebTgl1;
	}

	public void setBebTgl1(int bebTgl1) {
		this.bebTgl1 = bebTgl1;
	}

	public int getBebMingk2() {
		return bebMingk2;
	}

	public void setBebMingk2(int bebMingk2) {
		this.bebMingk2 = bebMingk2;
	}

	public int getBebPct2() {
		return bebPct2;
	}

	public void setBebPct2(int bebPct2) {
		this.bebPct2 = bebPct2;
	}

	public int getBebTgl2() {
		return bebTgl2;
	}

	public void setBebTgl2(int bebTgl2) {
		this.bebTgl2 = bebTgl2;
	}

	public int getBebPvPerbagian() {
		return bebPvPerbagian;
	}

	public void setBebPvPerbagian(int bebPvPerbagian) {
		this.bebPvPerbagian = bebPvPerbagian;
	}

	public int getBebMaxgk1() {
		return bebMaxgk1;
	}

	public void setBebMaxgk1(int bebMaxgk1) {
		this.bebMaxgk1 = bebMaxgk1;
	}

	public int getBebMaxgk2() {
		return bebMaxgk2;
	}

	public void setBebMaxgk2(int bebMaxgk2) {
		this.bebMaxgk2 = bebMaxgk2;
	}

	public int getBorWisataValue() {
		return borWisataValue;
	}

	public void setBorWisataValue(int borWisataValue) {
		this.borWisataValue = borWisataValue;
	}

	public int getBorWisataPct() {
		return borWisataPct;
	}

	public void setBorWisataPct(int borWisataPct) {
		this.borWisataPct = borWisataPct;
	}

	public int getBorWisataAkumgk() {
		return borWisataAkumgk;
	}

	public void setBorWisataAkumgk(int borWisataAkumgk) {
		this.borWisataAkumgk = borWisataAkumgk;
	}

	public int getBorWisataTambahangk() {
		return borWisataTambahangk;
	}

	public void setBorWisataTambahangk(int borWisataTambahangk) {
		this.borWisataTambahangk = borWisataTambahangk;
	}

	public int getBorMotorValue() {
		return borMotorValue;
	}

	public void setBorMotorValue(int borMotorValue) {
		this.borMotorValue = borMotorValue;
	}

	public int getBorMotorPct() {
		return borMotorPct;
	}

	public void setBorMotorPct(int borMotorPct) {
		this.borMotorPct = borMotorPct;
	}

	public int getBorMotorAkumgk() {
		return borMotorAkumgk;
	}

	public void setBorMotorAkumgk(int borMotorAkumgk) {
		this.borMotorAkumgk = borMotorAkumgk;
	}

	public int getBorMotorTambahangk() {
		return borMotorTambahangk;
	}

	public void setBorMotorTambahangk(int borMotorTambahangk) {
		this.borMotorTambahangk = borMotorTambahangk;
	}

	public int getBorReligiValue() {
		return borReligiValue;
	}

	public void setBorReligiValue(int borReligiValue) {
		this.borReligiValue = borReligiValue;
	}

	public int getBorReligiPct() {
		return borReligiPct;
	}

	public void setBorReligiPct(int borReligiPct) {
		this.borReligiPct = borReligiPct;
	}

	public int getBorReligiAkumgk() {
		return borReligiAkumgk;
	}

	public void setBorReligiAkumgk(int borReligiAkumgk) {
		this.borReligiAkumgk = borReligiAkumgk;
	}

	public int getBorReligiTambahangk() {
		return borReligiTambahangk;
	}

	public void setBorReligiTambahangk(int borReligiTambahangk) {
		this.borReligiTambahangk = borReligiTambahangk;
	}

	public int getBorMobilValue() {
		return borMobilValue;
	}

	public void setBorMobilValue(int borMobilValue) {
		this.borMobilValue = borMobilValue;
	}

	public int getBorMobilPct() {
		return borMobilPct;
	}

	public void setBorMobilPct(int borMobilPct) {
		this.borMobilPct = borMobilPct;
	}

	public int getBorMobilAkumgk() {
		return borMobilAkumgk;
	}

	public void setBorMobilAkumgk(int borMobilAkumgk) {
		this.borMobilAkumgk = borMobilAkumgk;
	}

	public int getBorMobilTambahangk() {
		return borMobilTambahangk;
	}

	public void setBorMobilTambahangk(int borMobilTambahangk) {
		this.borMobilTambahangk = borMobilTambahangk;
	}

	public int getBorMobilMewahValue() {
		return borMobilMewahValue;
	}

	public void setBorMobilMewahValue(int borMobilMewahValue) {
		this.borMobilMewahValue = borMobilMewahValue;
	}

	public int getBorMobilMewahPct() {
		return borMobilMewahPct;
	}

	public void setBorMobilMewahPct(int borMobilMewahPct) {
		this.borMobilMewahPct = borMobilMewahPct;
	}

	public int getBorMobilMewahAkumgk() {
		return borMobilMewahAkumgk;
	}

	public void setBorMobilMewahAkumgk(int borMobilMewahAkumgk) {
		this.borMobilMewahAkumgk = borMobilMewahAkumgk;
	}

	public int getBorMobilMewahTambahangk() {
		return borMobilMewahTambahangk;
	}

	public void setBorMobilMewahTambahangk(int borMobilMewahTambahangk) {
		this.borMobilMewahTambahangk = borMobilMewahTambahangk;
	}

	public int getBorRumahMewahValue() {
		return borRumahMewahValue;
	}

	public void setBorRumahMewahValue(int borRumahMewahValue) {
		this.borRumahMewahValue = borRumahMewahValue;
	}

	public int getBorRumahMewahPct() {
		return borRumahMewahPct;
	}

	public void setBorRumahMewahPct(int borRumahMewahPct) {
		this.borRumahMewahPct = borRumahMewahPct;
	}

	public int getBorRumahMewahAkumgk() {
		return borRumahMewahAkumgk;
	}

	public void setBorRumahMewahAkumgk(int borRumahMewahAkumgk) {
		this.borRumahMewahAkumgk = borRumahMewahAkumgk;
	}

	public int getBorRumahMewahTambahangk() {
		return borRumahMewahTambahangk;
	}

	public void setBorRumahMewahTambahangk(int borRumahMewahTambahangk) {
		this.borRumahMewahTambahangk = borRumahMewahTambahangk;
	}

	public float getBorAntriWisataPct() {
		return borAntriWisataPct;
	}

	public void setBorAntriWisataPct(float borAntriWisataPct) {
		this.borAntriWisataPct = borAntriWisataPct;
	}

	public float getBorAntriMotorPct() {
		return borAntriMotorPct;
	}

	public void setBorAntriMotorPct(float borAntriMotorPct) {
		this.borAntriMotorPct = borAntriMotorPct;
	}

	public float getBorAntriReligiPct() {
		return borAntriReligiPct;
	}

	public void setBorAntriReligiPct(float borAntriReligiPct) {
		this.borAntriReligiPct = borAntriReligiPct;
	}

	public float getBorAntriMobilPct() {
		return borAntriMobilPct;
	}

	public void setBorAntriMobilPct(float borAntriMobilPct) {
		this.borAntriMobilPct = borAntriMobilPct;
	}

	public int getBrcCdKualifikasi() {
		return brcCdKualifikasi;
	}

	public void setBrcCdKualifikasi(int brcCdKualifikasi) {
		this.brcCdKualifikasi = brcCdKualifikasi;
	}

	public float getBrcCdPct() {
		return brcCdPct;
	}

	public void setBrcCdPct(float brcCdPct) {
		this.brcCdPct = brcCdPct;
	}

	public int getBrcScdKualifikasi() {
		return brcScdKualifikasi;
	}

	public void setBrcScdKualifikasi(int brcScdKualifikasi) {
		this.brcScdKualifikasi = brcScdKualifikasi;
	}

	public float getBrcScdPct() {
		return brcScdPct;
	}

	public void setBrcScdPct(float brcScdPct) {
		this.brcScdPct = brcScdPct;
	}

	public int getBrcCdKualifikasi2Jalur() {
		return brcCdKualifikasi2Jalur;
	}

	public void setBrcCdKualifikasi2Jalur(int brcCdKualifikasi2Jalur) {
		this.brcCdKualifikasi2Jalur = brcCdKualifikasi2Jalur;
	}

	public int getBrcScdKualifikasi2Jalur() {
		return brcScdKualifikasi2Jalur;
	}

	public void setBrcScdKualifikasi2Jalur(int brcScdKualifikasi2Jalur) {
		this.brcScdKualifikasi2Jalur = brcScdKualifikasi2Jalur;
	}

	public int getBwtRmMingk() {
		return bwtRmMingk;
	}

	public void setBwtRmMingk(int bwtRmMingk) {
		this.bwtRmMingk = bwtRmMingk;
	}

	public int getBwtRmBagian() {
		return bwtRmBagian;
	}

	public void setBwtRmBagian(int bwtRmBagian) {
		this.bwtRmBagian = bwtRmBagian;
	}

	public int getBwtEmMingk() {
		return bwtEmMingk;
	}

	public void setBwtEmMingk(int bwtEmMingk) {
		this.bwtEmMingk = bwtEmMingk;
	}

	public int getBwtEmBagian() {
		return bwtEmBagian;
	}

	public void setBwtEmBagian(int bwtEmBagian) {
		this.bwtEmBagian = bwtEmBagian;
	}

	public int getBwtDmMingk() {
		return bwtDmMingk;
	}

	public void setBwtDmMingk(int bwtDmMingk) {
		this.bwtDmMingk = bwtDmMingk;
	}

	public int getBwtDmBagian() {
		return bwtDmBagian;
	}

	public void setBwtDmBagian(int bwtDmBagian) {
		this.bwtDmBagian = bwtDmBagian;
	}

	public int getBwtCdMingk() {
		return bwtCdMingk;
	}

	public void setBwtCdMingk(int bwtCdMingk) {
		this.bwtCdMingk = bwtCdMingk;
	}

	public int getBwtCdBagian() {
		return bwtCdBagian;
	}

	public void setBwtCdBagian(int bwtCdBagian) {
		this.bwtCdBagian = bwtCdBagian;
	}

	public int getBwtScdMingk() {
		return bwtScdMingk;
	}

	public void setBwtScdMingk(int bwtScdMingk) {
		this.bwtScdMingk = bwtScdMingk;
	}

	public int getBwtScdBagian() {
		return bwtScdBagian;
	}

	public void setBwtScdBagian(int bwtScdBagian) {
		this.bwtScdBagian = bwtScdBagian;
	}

	public int getBwtPvPerbagian() {
		return bwtPvPerbagian;
	}

	public void setBwtPvPerbagian(int bwtPvPerbagian) {
		this.bwtPvPerbagian = bwtPvPerbagian;
	}

	public float getBwtBudgetPct() {
		return bwtBudgetPct;
	}

	public void setBwtBudgetPct(float bwtBudgetPct) {
		this.bwtBudgetPct = bwtBudgetPct;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public float getMinPvpribadiBig() {
		return minPvpribadiBig;
	}

	public void setMinPvpribadiBig(float minPvpribadiBig) {
		this.minPvpribadiBig = minPvpribadiBig;
	}

	public float getMinPvpribadiBul() {
		return minPvpribadiBul;
	}

	public void setMinPvpribadiBul(float minPvpribadiBul) {
		this.minPvpribadiBul = minPvpribadiBul;
	}

	public float getMinPvpribadiBeb() {
		return minPvpribadiBeb;
	}

	public void setMinPvpribadiBeb(float minPvpribadiBeb) {
		this.minPvpribadiBeb = minPvpribadiBeb;
	}

	public float getMinPvpribadiBor() {
		return minPvpribadiBor;
	}

	public void setMinPvpribadiBor(float minPvpribadiBor) {
		this.minPvpribadiBor = minPvpribadiBor;
	}

	public float getMinPvpribadiBrc() {
		return minPvpribadiBrc;
	}

	public void setMinPvpribadiBrc(float minPvpribadiBrc) {
		this.minPvpribadiBrc = minPvpribadiBrc;
	}

	public float getMinPvpribadiBwt() {
		return minPvpribadiBwt;
	}

	public void setMinPvpribadiBwt(float minPvpribadiBwt) {
		this.minPvpribadiBwt = minPvpribadiBwt;
	}

	public int getBigPerbagian1() {
		return bigPerbagian1;
	}

	public void setBigPerbagian1(int bigPerbagian1) {
		this.bigPerbagian1 = bigPerbagian1;
	}

	public int getBigPerbagian2() {
		return bigPerbagian2;
	}

	public void setBigPerbagian2(int bigPerbagian2) {
		this.bigPerbagian2 = bigPerbagian2;
	}

	public int getBigPerbagian3() {
		return bigPerbagian3;
	}

	public void setBigPerbagian3(int bigPerbagian3) {
		this.bigPerbagian3 = bigPerbagian3;
	}

	public int getBigPerbagian4() {
		return bigPerbagian4;
	}

	public void setBigPerbagian4(int bigPerbagian4) {
		this.bigPerbagian4 = bigPerbagian4;
	}

	public int getBigPerbagian5() {
		return bigPerbagian5;
	}

	public void setBigPerbagian5(int bigPerbagian5) {
		this.bigPerbagian5 = bigPerbagian5;
	}

	public int getBigPerbagian6() {
		return bigPerbagian6;
	}

	public void setBigPerbagian6(int bigPerbagian6) {
		this.bigPerbagian6 = bigPerbagian6;
	}

	public int getBigPerbagian7() {
		return bigPerbagian7;
	}

	public void setBigPerbagian7(int bigPerbagian7) {
		this.bigPerbagian7 = bigPerbagian7;
	}

	public int getBigPerbagian8() {
		return bigPerbagian8;
	}

	public void setBigPerbagian8(int bigPerbagian8) {
		this.bigPerbagian8 = bigPerbagian8;
	}

	public int getBigPerbagian9() {
		return bigPerbagian9;
	}

	public void setBigPerbagian9(int bigPerbagian9) {
		this.bigPerbagian9 = bigPerbagian9;
	}

	public float getBigSharePct1() {
		return bigSharePct1;
	}

	public void setBigSharePct1(float bigSharePct1) {
		this.bigSharePct1 = bigSharePct1;
	}

	public int getBigMaxBag1() {
		return bigMaxBag1;
	}

	public void setBigMaxBag1(int bigMaxBag1) {
		this.bigMaxBag1 = bigMaxBag1;
	}

	public float getBigSharePct2() {
		return bigSharePct2;
	}

	public void setBigSharePct2(float bigSharePct2) {
		this.bigSharePct2 = bigSharePct2;
	}

	public int getBigMaxBag2() {
		return bigMaxBag2;
	}

	public void setBigMaxBag2(int bigMaxBag2) {
		this.bigMaxBag2 = bigMaxBag2;
	}

	public float getBigSharePct3() {
		return bigSharePct3;
	}

	public void setBigSharePct3(float bigSharePct3) {
		this.bigSharePct3 = bigSharePct3;
	}

	public int getBigMaxBag3() {
		return bigMaxBag3;
	}

	public void setBigMaxBag3(int bigMaxBag3) {
		this.bigMaxBag3 = bigMaxBag3;
	}

	public float getBigSharePct4() {
		return bigSharePct4;
	}

	public void setBigSharePct4(float bigSharePct4) {
		this.bigSharePct4 = bigSharePct4;
	}

	public int getBigMaxBag4() {
		return bigMaxBag4;
	}

	public void setBigMaxBag4(int bigMaxBag4) {
		this.bigMaxBag4 = bigMaxBag4;
	}

	public int getBigMaxBag5() {
		return bigMaxBag5;
	}

	public void setBigMaxBag5(int bigMaxBag5) {
		this.bigMaxBag5 = bigMaxBag5;
	}

	public float getLdpS1BudgetPct() {
		return ldpS1BudgetPct;
	}

	public void setLdpS1BudgetPct(float ldpS1BudgetPct) {
		this.ldpS1BudgetPct = ldpS1BudgetPct;
	}

	public double getLdpS1AkumBudget() {
		return ldpS1AkumBudget;
	}

	public void setLdpS1AkumBudget(double ldpS1AkumBudget) {
		this.ldpS1AkumBudget = ldpS1AkumBudget;
	}

	public int getLdpS1BagianPertama() {
		return ldpS1BagianPertama;
	}

	public void setLdpS1BagianPertama(int ldpS1BagianPertama) {
		this.ldpS1BagianPertama = ldpS1BagianPertama;
	}

	public int getLdpS1BagianNext() {
		return ldpS1BagianNext;
	}

	public void setLdpS1BagianNext(int ldpS1BagianNext) {
		this.ldpS1BagianNext = ldpS1BagianNext;
	}

	public float getLdpS2BudgetPct() {
		return ldpS2BudgetPct;
	}

	public void setLdpS2BudgetPct(float ldpS2BudgetPct) {
		this.ldpS2BudgetPct = ldpS2BudgetPct;
	}

	public double getLdpS2AkumBudget() {
		return ldpS2AkumBudget;
	}

	public void setLdpS2AkumBudget(double ldpS2AkumBudget) {
		this.ldpS2AkumBudget = ldpS2AkumBudget;
	}

	public int getLdpS2BagianPertama() {
		return ldpS2BagianPertama;
	}

	public void setLdpS2BagianPertama(int ldpS2BagianPertama) {
		this.ldpS2BagianPertama = ldpS2BagianPertama;
	}

	public int getLdpS2BagianNext() {
		return ldpS2BagianNext;
	}

	public void setLdpS2BagianNext(int ldpS2BagianNext) {
		this.ldpS2BagianNext = ldpS2BagianNext;
	}

	public float getLdpS3BudgetPct() {
		return ldpS3BudgetPct;
	}

	public void setLdpS3BudgetPct(float ldpS3BudgetPct) {
		this.ldpS3BudgetPct = ldpS3BudgetPct;
	}

	public double getLdpS3AkumBudget() {
		return ldpS3AkumBudget;
	}

	public void setLdpS3AkumBudget(double ldpS3AkumBudget) {
		this.ldpS3AkumBudget = ldpS3AkumBudget;
	}

	public int getLdpS3BagianPertama() {
		return ldpS3BagianPertama;
	}

	public void setLdpS3BagianPertama(int ldpS3BagianPertama) {
		this.ldpS3BagianPertama = ldpS3BagianPertama;
	}

	public int getLdpS3BagianNext() {
		return ldpS3BagianNext;
	}

	public void setLdpS3BagianNext(int ldpS3BagianNext) {
		this.ldpS3BagianNext = ldpS3BagianNext;
	}

	public float getLdpS4BudgetPct() {
		return ldpS4BudgetPct;
	}

	public void setLdpS4BudgetPct(float ldpS4BudgetPct) {
		this.ldpS4BudgetPct = ldpS4BudgetPct;
	}

	public double getLdpS4AkumBudget() {
		return ldpS4AkumBudget;
	}

	public void setLdpS4AkumBudget(double ldpS4AkumBudget) {
		this.ldpS4AkumBudget = ldpS4AkumBudget;
	}

	public int getLdpS4BagianPertama() {
		return ldpS4BagianPertama;
	}

	public void setLdpS4BagianPertama(int ldpS4BagianPertama) {
		this.ldpS4BagianPertama = ldpS4BagianPertama;
	}

	public int getLdpS4BagianNext() {
		return ldpS4BagianNext;
	}

	public void setLdpS4BagianNext(int ldpS4BagianNext) {
		this.ldpS4BagianNext = ldpS4BagianNext;
	}

	public float getMinPvpribadiBig1() {
		return minPvpribadiBig1;
	}

	public void setMinPvpribadiBig1(float minPvpribadiBig1) {
		this.minPvpribadiBig1 = minPvpribadiBig1;
	}

	public float getMinPvpribadiBig2() {
		return minPvpribadiBig2;
	}

	public void setMinPvpribadiBig2(float minPvpribadiBig2) {
		this.minPvpribadiBig2 = minPvpribadiBig2;
	}

	public float getMinPvpribadiBig3() {
		return minPvpribadiBig3;
	}

	public void setMinPvpribadiBig3(float minPvpribadiBig3) {
		this.minPvpribadiBig3 = minPvpribadiBig3;
	}

	public float getMinPvpribadiBig4() {
		return minPvpribadiBig4;
	}

	public void setMinPvpribadiBig4(float minPvpribadiBig4) {
		this.minPvpribadiBig4 = minPvpribadiBig4;
	}

	public float getMinPvpribadiBig5() {
		return minPvpribadiBig5;
	}

	public void setMinPvpribadiBig5(float minPvpribadiBig5) {
		this.minPvpribadiBig5 = minPvpribadiBig5;
	}

	public float getMinPvpribadiBig6() {
		return minPvpribadiBig6;
	}

	public void setMinPvpribadiBig6(float minPvpribadiBig6) {
		this.minPvpribadiBig6 = minPvpribadiBig6;
	}

	public float getMinPvpribadiBig7() {
		return minPvpribadiBig7;
	}

	public void setMinPvpribadiBig7(float minPvpribadiBig7) {
		this.minPvpribadiBig7 = minPvpribadiBig7;
	}

	public float getMinPvpribadiBig8() {
		return minPvpribadiBig8;
	}

	public void setMinPvpribadiBig8(float minPvpribadiBig8) {
		this.minPvpribadiBig8 = minPvpribadiBig8;
	}

	public float getMinPvpribadiBig9() {
		return minPvpribadiBig9;
	}

	public void setMinPvpribadiBig9(float minPvpribadiBig9) {
		this.minPvpribadiBig9 = minPvpribadiBig9;
	}

	public float getMinPvpribadiBul1() {
		return minPvpribadiBul1;
	}

	public void setMinPvpribadiBul1(float minPvpribadiBul1) {
		this.minPvpribadiBul1 = minPvpribadiBul1;
	}

	public float getMinPvpribadiBul2() {
		return minPvpribadiBul2;
	}

	public void setMinPvpribadiBul2(float minPvpribadiBul2) {
		this.minPvpribadiBul2 = minPvpribadiBul2;
	}

	public float getMinPvpribadiBul3() {
		return minPvpribadiBul3;
	}

	public void setMinPvpribadiBul3(float minPvpribadiBul3) {
		this.minPvpribadiBul3 = minPvpribadiBul3;
	}

	public float getMinPvpribadiBul4() {
		return minPvpribadiBul4;
	}

	public void setMinPvpribadiBul4(float minPvpribadiBul4) {
		this.minPvpribadiBul4 = minPvpribadiBul4;
	}

	public float getMinPvpribadiBul5() {
		return minPvpribadiBul5;
	}

	public void setMinPvpribadiBul5(float minPvpribadiBul5) {
		this.minPvpribadiBul5 = minPvpribadiBul5;
	}

	public float getMinPvpribadiBul6() {
		return minPvpribadiBul6;
	}

	public void setMinPvpribadiBul6(float minPvpribadiBul6) {
		this.minPvpribadiBul6 = minPvpribadiBul6;
	}
}
