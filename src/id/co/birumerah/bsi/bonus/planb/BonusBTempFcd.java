package id.co.birumerah.bsi.bonus.planb;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBTempFcd {

	private BonusB bonusB;
	
	public BonusBTempFcd() {}

	public BonusBTempFcd(BonusB bonusB) {
		this.bonusB = bonusB;
	}

	public BonusB getBonusB() {
		return bonusB;
	}

	public void setBonusB(BonusB bonusB) {
		this.bonusB = bonusB;
	}
	
	public BonusBSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		BonusBSet bbSet = null;
		
		try {
			dbConn = new DBConnection();
			BonusBTempDAO bbDAO = new BonusBTempDAO(dbConn);
			bbSet = bbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return bbSet;
	}
	
	public BonusBSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		BonusBSet bbSet = null;
		
		try {
//			dbConn = new DBConnection();
			BonusBTempDAO bbDAO = new BonusBTempDAO(dbConn);
			bbSet = bbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return bbSet;
	}
	
	public static BonusB getBonusByMember(String memberId) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusB bonus = new BonusB();
			bonus.setMemberId(memberId);
//			bonus.setBulan(bulan);
			
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			boolean found = bonusDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static BonusB getBonusByMember(String memberId, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			BonusB bonus = new BonusB();
			bonus.setMemberId(memberId);
//			bonus.setBulan(bulan);
			
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			boolean found = bonusDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus bulan ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public void insertBonus() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			bonusDAO.insert(bonusB);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			bonusDAO.insert(bonusB);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void updateBonus(BonusB bonus) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			if (bonusDAO.update(bonus) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateBonus(BonusB bonus, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			if (bonusDAO.update(bonus) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void clearTemporary(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBTempDAO bonusDAO = new BonusBTempDAO(dbConn);
			if (bonusDAO.cleanTemporary() < 1) {
				throw new Exception("Cleaning table temporary failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
