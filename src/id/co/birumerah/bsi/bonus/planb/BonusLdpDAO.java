package id.co.birumerah.bsi.bonus.planb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusLdpDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusLdpDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusLdp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_ldp (");
		sb.append("member_id,");
//		sb.append("periode_s1,");
//		sb.append("bonus_s1,");
//		sb.append("akum_s1,");
//		sb.append("periode_s2,");
//		sb.append("is_sisadana_s1,");
//		sb.append("bonus_s2,");
//		sb.append("akum_s2,");
//		sb.append("periode_s3,");
//		sb.append("is_sisadana_s2,");
//		sb.append("bonus_s3,");
//		sb.append("akum_s3,");
//		sb.append("periode_s4,");
//		sb.append("is_sisadana_s3,");
//		sb.append("bonus_s4,");
//		sb.append("akum_s4");
		sb.append("tipe,");
		sb.append("periode,");
		sb.append("is_sisadana,");
		sb.append("bonus,");
		sb.append("akum");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
//		sb.append("'" + shortFormat.format(dataObject.getPeriodeS1()) + "',");
//		sb.append("" + dataObject.getBonusS1() + ",");
//		sb.append("" + dataObject.getAkumS1() + ",");
//		sb.append("'" + shortFormat.format(dataObject.getPeriodeS2()) + "',");
//		sb.append("" + dataObject.getIsSisadanaS1() + ",");
//		sb.append("" + dataObject.getBonusS2() + ",");
//		sb.append("" + dataObject.getAkumS2() + ",");
//		sb.append("'" + shortFormat.format(dataObject.getPeriodeS3()) + "',");
//		sb.append("" + dataObject.getIsSisadanaS2() + ",");
//		sb.append("" + dataObject.getBonusS3() + ",");
//		sb.append("" + dataObject.getAkumS3() + ",");
//		sb.append("'" + shortFormat.format(dataObject.getPeriodeS4()) + "',");
//		sb.append("" + dataObject.getIsSisadanaS3() + ",");
//		sb.append("" + dataObject.getBonusS4() + ",");
//		sb.append("" + dataObject.getAkumS4());
		sb.append("'" + dataObject.getTipe() + "',");
		sb.append("'" + shortFormat.format(dataObject.getPeriode()) + "',");
		sb.append("" + dataObject.getIsSisadana() + ",");
		sb.append("" + dataObject.getBonus() + ",");
		sb.append("" + dataObject.getAkum());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusLdp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_ldp SET ");
		sb.append("is_sisadana = " + dataObject.getIsSisadana() + ",");
		sb.append("bonus = " + dataObject.getBonus() + ",");
		sb.append("akum = " + dataObject.getAkum() + " ");
		sb.append("WHERE id = " + dataObject.getId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusLdp dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("is_sisadana,");
		sb.append("bonus,");
		sb.append("akum ");
		sb.append("FROM bonus_ldp ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tipe = '" + dataObject.getTipe() + "' ");
		sb.append("AND periode = '" + shortFormat.format(dataObject.getPeriode()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setId(rs.getLong("id"));
				dataObject.setIsSisadana(rs.getInt("is_sisadana"));
				dataObject.setBonus(rs.getDouble("bonus"));
				dataObject.setAkum(rs.getDouble("akum"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public double getAkumulasiByMemberAndType(String memberId, String tipe) throws SQLException {
		double akumulasi = 0;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT akum FROM bonus_ldp ");
		sb.append("WHERE member_id = '" + memberId + "' AND tipe = '" + tipe + "' ");
		sb.append("ORDER BY periode DESC LIMIT 1");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				akumulasi = rs.getDouble("akum");
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return akumulasi;
	}
}
