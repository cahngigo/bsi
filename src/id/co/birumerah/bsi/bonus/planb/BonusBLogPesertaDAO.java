package id.co.birumerah.bsi.bonus.planb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusBLogPesertaDAO {

	private DBConnection dbConn;
	private DateFormat shortFormat;
	
	public BonusBLogPesertaDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusBLogPeserta dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_b_log_peserta (");
		sb.append("bulan,");
		sb.append("member_id,");
		sb.append("big_j1,");
		sb.append("big_j2,");
		sb.append("big_j3,");
		sb.append("big_j4,");
		sb.append("big_j5,");
		sb.append("big_j6,");
		sb.append("big_j7,");
		sb.append("big_j8,");
		sb.append("big_j9,");
		sb.append("bul1,");
		sb.append("bul2,");
		sb.append("bul3,");
		sb.append("bul4,");
		sb.append("bul5,");
		sb.append("bul6");
		sb.append(") VALUES (");
		sb.append("'" + shortFormat.format(dataObject.getBulan()) + "',");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("" + dataObject.getBigJ1() + ",");
		sb.append("" + dataObject.getBigJ2() + ",");
		sb.append("" + dataObject.getBigJ3() + ",");
		sb.append("" + dataObject.getBigJ4() + ",");
		sb.append("" + dataObject.getBigJ5() + ",");
		sb.append("" + dataObject.getBigJ6() + ",");
		sb.append("" + dataObject.getBigJ7() + ",");
		sb.append("" + dataObject.getBigJ8() + ",");
		sb.append("" + dataObject.getBigJ9() + ",");
		sb.append("" + dataObject.getBul1() + ",");
		sb.append("" + dataObject.getBul2() + ",");
		sb.append("" + dataObject.getBul3() + ",");
		sb.append("" + dataObject.getBul4() + ",");
		sb.append("" + dataObject.getBul5() + ",");
		sb.append("" + dataObject.getBul6());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusBLogPeserta dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_b_log_peserta SET ");
//		sb.append("bulan = 'bulan',");
//		sb.append("member_id = 'member_id',");
		sb.append("big_j1 = " + dataObject.getBigJ1() + ",");
		sb.append("big_j2 = " + dataObject.getBigJ2() + ",");
		sb.append("big_j3 = " + dataObject.getBigJ3() + ",");
		sb.append("big_j4 = " + dataObject.getBigJ4() + ",");
		sb.append("big_j5 = " + dataObject.getBigJ5() + ",");
		sb.append("big_j6 = " + dataObject.getBigJ6() + ",");
		sb.append("big_j7 = " + dataObject.getBigJ7() + ",");
		sb.append("big_j8 = " + dataObject.getBigJ8() + ",");
		sb.append("big_j9 = " + dataObject.getBigJ9() + ",");
		sb.append("bul1 = " + dataObject.getBul1() + ",");
		sb.append("bul2 = " + dataObject.getBul2() + ",");
		sb.append("bul3 = " + dataObject.getBul3() + ",");
		sb.append("bul4 = " + dataObject.getBul4() + ",");
		sb.append("bul5 = " + dataObject.getBul5() + ",");
		sb.append("bul6 = " + dataObject.getBul6() + " ");
		sb.append("WHERE bulan = '" + shortFormat.format(dataObject.getBulan()) + "' ");
		sb.append("AND member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusBLogPeserta dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("bulan,");
		sb.append("member_id,");
		sb.append("big_j1,");
		sb.append("big_j2,");
		sb.append("big_j3,");
		sb.append("big_j4,");
		sb.append("big_j5,");
		sb.append("big_j6,");
		sb.append("big_j7,");
		sb.append("big_j8,");
		sb.append("big_j9,");
		sb.append("bul1,");
		sb.append("bul2,");
		sb.append("bul3,");
		sb.append("bul4,");
		sb.append("bul5,");
		sb.append("bul6 ");
		sb.append("FROM bonus_b_log_peserta ");
		sb.append("WHERE bulan = '" + shortFormat.format(dataObject.getBulan()) + "' ");
		sb.append("AND member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setId(rs.getLong("id"));
//				dataObject.setBulan(rs.getDate("bulan"));
//				dataObject member_id
				dataObject.setBigJ1(rs.getDouble("big_j1"));
				dataObject.setBigJ2(rs.getDouble("big_j2"));
				dataObject.setBigJ3(rs.getDouble("big_j3"));
				dataObject.setBigJ4(rs.getDouble("big_j4"));
				dataObject.setBigJ5(rs.getDouble("big_j5"));
				dataObject.setBigJ6(rs.getDouble("big_j6"));
				dataObject.setBigJ7(rs.getDouble("big_j7"));
				dataObject.setBigJ8(rs.getDouble("big_j8"));
				dataObject.setBigJ9(rs.getDouble("big_j9"));
				dataObject.setBul1(rs.getDouble("bul1"));
				dataObject.setBul2(rs.getDouble("bul2"));
				dataObject.setBul3(rs.getDouble("bul3"));
				dataObject.setBul4(rs.getDouble("bul4"));
				dataObject.setBul5(rs.getDouble("bul5"));
				dataObject.setBul6(rs.getDouble("bul6"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
