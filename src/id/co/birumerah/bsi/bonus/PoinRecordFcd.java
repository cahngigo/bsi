package id.co.birumerah.bsi.bonus;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class PoinRecordFcd {

	private PoinRecord poin;
	
	public PoinRecordFcd() {}

	public PoinRecordFcd(PoinRecord poin) {
		this.poin = poin;
	}

	public PoinRecord getPoin() {
		return poin;
	}

	public void setPoin(PoinRecord poin) {
		this.poin = poin;
	}
	
	public void insertPoin() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			PoinRecordDAO prDAO = new PoinRecordDAO(dbConn);
			prDAO.insert(poin);
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertPoin(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			PoinRecordDAO prDAO = new PoinRecordDAO(dbConn);
			prDAO.insert(poin);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public PoinRecordSet search(String whereClause, DBConnection dbConn) throws Exception {
		PoinRecordSet prSet = null;
		try {
			PoinRecordDAO prDAO = new PoinRecordDAO(dbConn);
			prSet = prDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return prSet;
	}
	
	public void deleteRecord(PoinRecord dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			PoinRecordDAO prDAO = new PoinRecordDAO(dbConn);
			prDAO.delete(dataObject);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
