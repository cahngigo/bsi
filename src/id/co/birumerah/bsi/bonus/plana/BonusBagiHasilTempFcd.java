package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBagiHasilTempFcd {

	private BonusBagiHasilTemp bbhTemp;
	
	public BonusBagiHasilTempFcd() {}

	public BonusBagiHasilTempFcd(BonusBagiHasilTemp bbhTemp) {
		this.bbhTemp = bbhTemp;
	}

	public BonusBagiHasilTemp getBbhTemp() {
		return bbhTemp;
	}

	public void setBbhTemp(BonusBagiHasilTemp bbhTemp) {
		this.bbhTemp = bbhTemp;
	}
	
	public BonusBagiHasilTempSet search(String whereClause, DBConnection dbConn) throws Exception {
		BonusBagiHasilTempSet bbhtSet = null;
		
		BonusBagiHasilTempDAO tempDao = new BonusBagiHasilTempDAO(dbConn);
		bbhtSet = tempDao.select(whereClause);
		
		return bbhtSet;
	}
	
	public static BonusBagiHasilTemp getBonusByMemberAndDate(String memberId, Date tanggal, DBConnection dbConn) throws Exception {
		BonusBagiHasilTemp dataObject = new BonusBagiHasilTemp();
		dataObject.setMemberId(memberId);
		dataObject.setTanggal(tanggal);
		
		BonusBagiHasilTempDAO tempDao = new BonusBagiHasilTempDAO(dbConn);
		boolean found = tempDao.select(dataObject);
		
		if (found) {
			return dataObject;
		} else {
			System.err.println("Bonus bagi hasil temp hari ini tidak ditemukan untuk member ybs.");
		}
		
		return null;
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilTempDAO tempDao = new BonusBagiHasilTempDAO(dbConn);
			tempDao.insert(bbhTemp);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateBonus(BonusBagiHasilTemp bonus, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilTempDAO tempDao = new BonusBagiHasilTempDAO(dbConn);
			if (tempDao.update(bonus) < 1) {
				throw new Exception("Update bonus bagi hasil temp failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteByMember(BonusBagiHasilTemp dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilTempDAO tempDao = new BonusBagiHasilTempDAO(dbConn);
			if (tempDao.deleteByMember(dataObject) < 1) {
				throw new Exception("Delete bonus bagi hasil temp failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
