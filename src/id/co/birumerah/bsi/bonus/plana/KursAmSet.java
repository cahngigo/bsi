package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class KursAmSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;

	@SuppressWarnings("unchecked")
	public KursAmSet() {
		this.set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(KursAm dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public KursAm get(int index) {
		KursAm result = null;
		
		if ((index >= 0) && (index < length()))
			result = (KursAm) set.get(index);
		
		return result;
	}
}
