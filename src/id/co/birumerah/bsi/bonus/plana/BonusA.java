package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class BonusA {

	private String memberId;
	private Date tanggal;
	private double bonusSponsor;
	private double bonusPasanganSedang;
	private double bonusPasanganKecil;
	private double bonusMatching;
	private int totalPasanganSedang;
	private int totalFlush;
	private double potonganAm;
	private double pkpKumulatif;
	private double tax;
	
	public BonusA() {
		super();
	}

	public BonusA(String memberId, Date tanggal, double bonusSponsor,
			double bonusPasanganSedang, double bonusPasanganKecil,
			double bonusMatching, int totalPasanganSedang, int totalFlush,
			double potonganAm, double pkpKumulatif, double tax) {
		this.memberId = memberId;
		this.tanggal = tanggal;
		this.bonusSponsor = bonusSponsor;
		this.bonusPasanganSedang = bonusPasanganSedang;
		this.bonusPasanganKecil = bonusPasanganKecil;
		this.bonusMatching = bonusMatching;
		this.totalPasanganSedang = totalPasanganSedang;
		this.totalFlush = totalFlush;
		this.potonganAm = potonganAm;
		this.pkpKumulatif = pkpKumulatif;
		this.tax = tax;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public double getBonusSponsor() {
		return bonusSponsor;
	}

	public void setBonusSponsor(double bonusSponsor) {
		this.bonusSponsor = bonusSponsor;
	}

	public double getBonusPasanganSedang() {
		return bonusPasanganSedang;
	}

	public void setBonusPasanganSedang(double bonusPasanganSedang) {
		this.bonusPasanganSedang = bonusPasanganSedang;
	}

	public double getBonusPasanganKecil() {
		return bonusPasanganKecil;
	}

	public void setBonusPasanganKecil(double bonusPasanganKecil) {
		this.bonusPasanganKecil = bonusPasanganKecil;
	}

	public int getTotalFlush() {
		return totalFlush;
	}

	public void setTotalFlush(int totalFlush) {
		this.totalFlush = totalFlush;
	}

	public double getBonusMatching() {
		return bonusMatching;
	}

	public void setBonusMatching(double bonusMatching) {
		this.bonusMatching = bonusMatching;
	}

	public int getTotalPasanganSedang() {
		return totalPasanganSedang;
	}

	public void setTotalPasanganSedang(int totalPasanganSedang) {
		this.totalPasanganSedang = totalPasanganSedang;
	}

	public double getPkpKumulatif() {
		return pkpKumulatif;
	}

	public void setPkpKumulatif(double pkpKumulatif) {
		this.pkpKumulatif = pkpKumulatif;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getPotonganAm() {
		return potonganAm;
	}

	public void setPotonganAm(double potonganAm) {
		this.potonganAm = potonganAm;
	}
}
