package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class LogBonusATempSet implements Serializable {

	@SuppressWarnings("rawtypes")
	ArrayList set = null;
	
	@SuppressWarnings("rawtypes")
	public LogBonusATempSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(LogBonusATemp dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public LogBonusATemp get(int index) {
		LogBonusATemp result = null;
		
		if ((index >= 0) && (index < length()))
			result = (LogBonusATemp) set.get(index);
		
		return result;
	}
}
