package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

public class SettingBagiHasilSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public SettingBagiHasilSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(SettingBagiHasil dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public SettingBagiHasil get(int index) {
		SettingBagiHasil result = null;
		
		if ((index >= 0) && (index < length()))
			result = (SettingBagiHasil) set.get(index);
		
		return result;
	}
}
