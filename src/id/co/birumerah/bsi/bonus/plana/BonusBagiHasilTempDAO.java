package id.co.birumerah.bsi.bonus.plana;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class BonusBagiHasilTempDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusBagiHasilTempDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusBagiHasilTemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_bagi_hasil_temp (");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("litfoot,");
		sb.append("bintang,");
		sb.append("bagian,");
		sb.append("bagi_hasil,");
		sb.append("potensi_micro,");
		sb.append("micro_terjadi,");
		sb.append("bobot_pengali_micro,");
		sb.append("bagian_macro,");
		sb.append("bagi_hasil_macro,");
		sb.append("potensi_macro,");
		sb.append("macro_terjadi,");
		sb.append("bobot_pengali_macro ");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("" + dataObject.getLitfoot() + ",");
		sb.append("'" + dataObject.getBintang() + "',");
		sb.append("" + dataObject.getBagian() + ",");
		sb.append("" + dataObject.getBagiHasil() + ",");
		sb.append("" + dataObject.getPotensiMicro() + ",");
		sb.append("" + dataObject.getMicroTerjadi() + ",");
		sb.append("" + dataObject.getBobotPengaliMicro() + ",");
		sb.append("" + dataObject.getBagianMacro() + ",");
		sb.append("" + dataObject.getBagiHasilMacro() + ",");
		sb.append("" + dataObject.getPotensiMacro() + ",");
		sb.append("" + dataObject.getMacroTerjadi() + ",");
		sb.append("" + dataObject.getBobotPengaliMacro());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusBagiHasilTemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_bagi_hasil_temp SET ");
		sb.append("litfoot = " + dataObject.getLitfoot() + ",");
		sb.append("bintang = '" + dataObject.getBintang() + "',");
		sb.append("bagian = " + dataObject.getBagian() + ",");
		sb.append("bagi_hasil = " + dataObject.getBagiHasil() + ",");
		sb.append("potensi_micro = " + dataObject.getPotensiMicro() + ",");
		sb.append("micro_terjadi = " + dataObject.getMicroTerjadi() + ",");
		sb.append("bobot_pengali_micro = " + dataObject.getBobotPengaliMicro() + ",");
		sb.append("bagian_macro = " + dataObject.getBagianMacro() + ",");
		sb.append("bagi_hasil_macro = " + dataObject.getBagiHasilMacro() + ",");
		sb.append("potensi_macro = " + dataObject.getPotensiMacro() + ",");
		sb.append("macro_terjadi = " + dataObject.getMacroTerjadi() + ",");
		sb.append("bobot_pengali_macro = " + dataObject.getBobotPengaliMacro() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusBagiHasilTemp dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("litfoot,");
		sb.append("bintang,");
		sb.append("bagian,");
		sb.append("bagi_hasil,");
		sb.append("potensi_micro,");
		sb.append("micro_terjadi,");
		sb.append("bobot_pengali_micro,");
		sb.append("bagian_macro,");
		sb.append("bagi_hasil_macro,");
		sb.append("potensi_macro,");
		sb.append("macro_terjadi,");
		sb.append("bobot_pengali_macro ");
		sb.append("FROM bonus_bagi_hasil_temp WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setLitfoot(rs.getInt("litfoot"));
				dataObject.setBintang(rs.getString("bintang"));
				dataObject.setBagian(rs.getInt("bagian"));
				dataObject.setBagiHasil(rs.getDouble("bagi_hasil"));
				dataObject.setPotensiMicro(rs.getInt("potensi_micro"));
				dataObject.setMicroTerjadi(rs.getInt("micro_terjadi"));
				dataObject.setBobotPengaliMicro(rs.getInt("bobot_pengali_micro"));
				dataObject.setBagianMacro(rs.getInt("bagian_macro"));
				dataObject.setBagiHasilMacro(rs.getDouble("bagi_hasil_macro"));
				dataObject.setPotensiMacro(rs.getInt("potensi_macro"));
				dataObject.setMacroTerjadi(rs.getInt("macro_terjadi"));
				dataObject.setBobotPengaliMacro(rs.getInt("bobot_pengali_macro"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BonusBagiHasilTempSet select(String whereClause) throws SQLException {
		BonusBagiHasilTempSet dataObjectSet = new BonusBagiHasilTempSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("litfoot,");
		sb.append("bintang,");
		sb.append("bagian,");
		sb.append("bagi_hasil,");
		sb.append("potensi_micro,");
		sb.append("micro_terjadi,");
		sb.append("bobot_pengali_micro,");
		sb.append("bagian_macro,");
		sb.append("bagi_hasil_macro,");
		sb.append("potensi_macro,");
		sb.append("macro_terjadi,");
		sb.append("bobot_pengali_macro ");
		sb.append("FROM bonus_bagi_hasil_temp WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BonusBagiHasilTemp dataObject = new BonusBagiHasilTemp();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setLitfoot(rs.getInt("litfoot"));
				dataObject.setBintang(rs.getString("bintang"));
				dataObject.setBagian(rs.getInt("bagian"));
				dataObject.setBagiHasil(rs.getDouble("bagi_hasil"));
				dataObject.setPotensiMicro(rs.getInt("potensi_micro"));
				dataObject.setMicroTerjadi(rs.getInt("micro_terjadi"));
				dataObject.setBobotPengaliMicro(rs.getInt("bobot_pengali_micro"));
				dataObject.setBagianMacro(rs.getInt("bagian_macro"));
				dataObject.setBagiHasilMacro(rs.getDouble("bagi_hasil_macro"));
				dataObject.setPotensiMacro(rs.getInt("potensi_macro"));
				dataObject.setMacroTerjadi(rs.getInt("macro_terjadi"));
				dataObject.setBobotPengaliMacro(rs.getInt("bobot_pengali_macro"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int deleteByMember(BonusBagiHasilTemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_bagi_hasil_temp ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
