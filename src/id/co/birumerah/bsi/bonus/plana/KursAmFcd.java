package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class KursAmFcd {

	private KursAm kursAm;
	
	public KursAmFcd() {}

	public KursAmFcd(KursAm kursAm) {
		this.kursAm = kursAm;
	}

	public KursAm getKursAm() {
		return kursAm;
	}

	public void setKursAm(KursAm kursAm) {
		this.kursAm = kursAm;
	}
	
	public KursAmSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		KursAmSet kaSet = null;
		
		try {
			dbConn = new DBConnection();
			KursAmDAO kaDAO = new KursAmDAO(dbConn);
			kaSet = kaDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return kaSet;
	}
	
	public KursAmSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		KursAmSet kaSet = null;
		
		try {
//			dbConn = new DBConnection();
			KursAmDAO kaDAO = new KursAmDAO(dbConn);
			kaSet = kaDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return kaSet;
	}
}
