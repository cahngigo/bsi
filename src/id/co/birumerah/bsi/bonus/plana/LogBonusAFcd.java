package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class LogBonusAFcd {

	private LogBonusA logBonus;
	
	public LogBonusAFcd() {}

	public LogBonusAFcd(LogBonusA logBonus) {
		this.logBonus = logBonus;
	}

	public LogBonusA getLogBonus() {
		return logBonus;
	}

	public void setLogBonus(LogBonusA logBonus) {
		this.logBonus = logBonus;
	}
	
	public void insertLog(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusADAO logDAO = new LogBonusADAO(dbConn);
			logDAO.insert(logBonus);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteLogByDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusADAO logDAO = new LogBonusADAO(dbConn);
			if (logDAO.deleteLogByDate(tanggal) < 1) {
				throw new Exception("Delete log gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteSaldoBonusByDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusADAO logDAO = new LogBonusADAO(dbConn);
			if (logDAO.deleteSaldoBonusByDate(tanggal) < 1) {
				throw new Exception("Delete saldo bonus gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
