package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;
import id.co.birumerah.util.dbaccess.SQLValueFilter;

public class KursAmDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public KursAmDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(KursAm dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO kurs_am (");
		sb.append("point_value,");
		sb.append("business_value,");
		sb.append("start_date,");
		sb.append("status,");
		sb.append("persen_potong_harian,");
		sb.append("max_potong_bulanan");
		sb.append(") VALUES (");
		sb.append("" + dataObject.getPointValue() + ",");
		sb.append("" + dataObject.getBusinessValue() + ",");
		sb.append("'" + shortFormat.format(dataObject.getStartDate()) + "',");
		sb.append("'" + dataObject.getStatus() + "',");
		sb.append("" + dataObject.getPersenPotongHarian() + ",");
		sb.append("" + dataObject.getMaxPotongBulanan());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(KursAm dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE kurs_am ");
		sb.append("SET ");
		sb.append("point_value = " + dataObject.getPointValue() + ",");
		sb.append("business_value = " + dataObject.getBusinessValue() + ",");
		sb.append("start_date = '" + shortFormat.format(dataObject.getStartDate()) + "',");
		sb.append("status = '" + SQLValueFilter.normalizeString(dataObject.getStatus()) + "',");
		sb.append("persen_potong_harian = " + dataObject.getPersenPotongHarian() + ",");
		sb.append("max_potong_bulanan = " + dataObject.getMaxPotongBulanan() + " ");
		sb.append("WHERE kurs_id = " + dataObject.getKursId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(KursAm dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("point_value,");
		sb.append("business_value,");
		sb.append("start_date,");
		sb.append("status,");
		sb.append("persen_potong_harian,");
		sb.append("max_potong_bulanan ");
		sb.append("FROM kurs_am ");
		sb.append("WHERE kurs_id = " + dataObject.getKursId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setPointValue(rs.getFloat("point_value"));
				dataObject.setBusinessValue(rs.getFloat("business_value"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
				dataObject.setPersenPotongHarian(rs.getInt("persen_potong_harian"));
				dataObject.setMaxPotongBulanan(rs.getInt("max_potong_bulanan"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public KursAmSet select(String whereClause) throws SQLException {
		KursAmSet dataObjectSet = new KursAmSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("kurs_id,");
		sb.append("point_value,");
		sb.append("business_value,");
		sb.append("start_date,");
		sb.append("status,");
		sb.append("persen_potong_harian,");
		sb.append("max_potong_bulanan ");
		sb.append("FROM kurs_am ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				KursAm dataObject = new KursAm();
				dataObject.setKursId(rs.getInt("kurs_id"));
				dataObject.setPointValue(rs.getFloat("point_value"));
				dataObject.setBusinessValue(rs.getFloat("business_value"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
				dataObject.setPersenPotongHarian(rs.getInt("persen_potong_harian"));
				dataObject.setMaxPotongBulanan(rs.getInt("max_potong_bulanan"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
