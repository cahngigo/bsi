package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class BonusTransfer {

	private long transferId;
	private String tipeBonus;
	private Date tanggal;
	private String memberId;
	private String noRekening;
	private String namaBank;
	private double amount;
	private double tax;
	private double potonganAm;
	private int statusKirim;
	private String remarks;
	private Date transferDate;
	
	public BonusTransfer() {}

	public long getTransferId() {
		return transferId;
	}

	public void setTransferId(long transferId) {
		this.transferId = transferId;
	}

	public String getTipeBonus() {
		return tipeBonus;
	}

	public void setTipeBonus(String tipeBonus) {
		this.tipeBonus = tipeBonus;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public String getNamaBank() {
		return namaBank;
	}

	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getPotonganAm() {
		return potonganAm;
	}

	public void setPotonganAm(double potonganAm) {
		this.potonganAm = potonganAm;
	}

	public int getStatusKirim() {
		return statusKirim;
	}

	public void setStatusKirim(int statusKirim) {
		this.statusKirim = statusKirim;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}
}
