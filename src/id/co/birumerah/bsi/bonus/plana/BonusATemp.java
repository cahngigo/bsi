package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class BonusATemp {

	private String memberId;
	private Date tanggal;
	private double bonusSponsor;
	private double bonusPasanganSedang;
	private double bonusPasanganKecil;
	private double bonusMatching;
	private int totalPasanganSedang;
	private int totalFlush;
	private double potonganAm;
	private double pkpKumulatif;
	private double tax;
	private int isProcessed;
	
	public BonusATemp() {
		super();
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public double getBonusSponsor() {
		return bonusSponsor;
	}

	public void setBonusSponsor(double bonusSponsor) {
		this.bonusSponsor = bonusSponsor;
	}

	public double getBonusPasanganSedang() {
		return bonusPasanganSedang;
	}

	public void setBonusPasanganSedang(double bonusPasanganSedang) {
		this.bonusPasanganSedang = bonusPasanganSedang;
	}

	public double getBonusPasanganKecil() {
		return bonusPasanganKecil;
	}

	public void setBonusPasanganKecil(double bonusPasanganKecil) {
		this.bonusPasanganKecil = bonusPasanganKecil;
	}

	public double getBonusMatching() {
		return bonusMatching;
	}

	public void setBonusMatching(double bonusMatching) {
		this.bonusMatching = bonusMatching;
	}

	public int getTotalPasanganSedang() {
		return totalPasanganSedang;
	}

	public void setTotalPasanganSedang(int totalPasanganSedang) {
		this.totalPasanganSedang = totalPasanganSedang;
	}

	public int getTotalFlush() {
		return totalFlush;
	}

	public void setTotalFlush(int totalFlush) {
		this.totalFlush = totalFlush;
	}

	public double getPotonganAm() {
		return potonganAm;
	}

	public void setPotonganAm(double potonganAm) {
		this.potonganAm = potonganAm;
	}

	public double getPkpKumulatif() {
		return pkpKumulatif;
	}

	public void setPkpKumulatif(double pkpKumulatif) {
		this.pkpKumulatif = pkpKumulatif;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public int getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}
}
