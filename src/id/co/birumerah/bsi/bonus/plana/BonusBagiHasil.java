package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class BonusBagiHasil {

	private String memberId;
	private Date tanggal;
	private int litfoot;
	private String bintang;
	private int bagian;
	private double bagiHasil;
	private int potensiMicro;
	private int microTerjadi;
	private int bobotPengaliMicro;
	private int bagianMacro;
	private double bagiHasilMacro;
	private int potensiMacro;
	private int macroTerjadi;
	private int bobotPengaliMacro;
	
	public BonusBagiHasil() {}

	public BonusBagiHasil(String memberId, Date tanggal, int litfoot,
			String bintang, int bagian, double bagiHasil, int potensiMicro,
			int microTerjadi, int bobotPengaliMicro, int bagianMacro,
			double bagiHasilMacro, int potensiMacro, int macroTerjadi,
			int bobotPengaliMacro) {
		this.memberId = memberId;
		this.tanggal = tanggal;
		this.litfoot = litfoot;
		this.bintang = bintang;
		this.bagian = bagian;
		this.bagiHasil = bagiHasil;
		this.potensiMicro = potensiMicro;
		this.microTerjadi = microTerjadi;
		this.bobotPengaliMicro = bobotPengaliMicro;
		this.bagianMacro = bagianMacro;
		this.bagiHasilMacro = bagiHasilMacro;
		this.potensiMacro = potensiMacro;
		this.macroTerjadi = macroTerjadi;
		this.bobotPengaliMacro = bobotPengaliMacro;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public int getLitfoot() {
		return litfoot;
	}

	public void setLitfoot(int litfoot) {
		this.litfoot = litfoot;
	}

	public String getBintang() {
		return bintang;
	}

	public void setBintang(String bintang) {
		this.bintang = bintang;
	}

	public int getBagian() {
		return bagian;
	}

	public void setBagian(int bagian) {
		this.bagian = bagian;
	}

	public double getBagiHasil() {
		return bagiHasil;
	}

	public void setBagiHasil(double bagiHasil) {
		this.bagiHasil = bagiHasil;
	}

	public int getPotensiMicro() {
		return potensiMicro;
	}

	public void setPotensiMicro(int potensiMicro) {
		this.potensiMicro = potensiMicro;
	}

	public int getMicroTerjadi() {
		return microTerjadi;
	}

	public void setMicroTerjadi(int microTerjadi) {
		this.microTerjadi = microTerjadi;
	}

	public int getBobotPengaliMicro() {
		return bobotPengaliMicro;
	}

	public void setBobotPengaliMicro(int bobotPengaliMicro) {
		this.bobotPengaliMicro = bobotPengaliMicro;
	}

	public int getBagianMacro() {
		return bagianMacro;
	}

	public void setBagianMacro(int bagianMacro) {
		this.bagianMacro = bagianMacro;
	}

	public double getBagiHasilMacro() {
		return bagiHasilMacro;
	}

	public void setBagiHasilMacro(double bagiHasilMacro) {
		this.bagiHasilMacro = bagiHasilMacro;
	}

	public int getPotensiMacro() {
		return potensiMacro;
	}

	public void setPotensiMacro(int potensiMacro) {
		this.potensiMacro = potensiMacro;
	}

	public int getMacroTerjadi() {
		return macroTerjadi;
	}

	public void setMacroTerjadi(int macroTerjadi) {
		this.macroTerjadi = macroTerjadi;
	}

	public int getBobotPengaliMacro() {
		return bobotPengaliMacro;
	}

	public void setBobotPengaliMacro(int bobotPengaliMacro) {
		this.bobotPengaliMacro = bobotPengaliMacro;
	}
}
