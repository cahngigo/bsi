package id.co.birumerah.bsi.bonus.plana;

import id.co.birumerah.util.dbaccess.DBConnection;

public class SettingBagiHasilFcd {

	private SettingBagiHasil settings;
	
	public SettingBagiHasilFcd() {}

	public SettingBagiHasilFcd(SettingBagiHasil settings) {
		this.settings = settings;
	}

	public SettingBagiHasil getSettings() {
		return settings;
	}

	public void setSettings(SettingBagiHasil settings) {
		this.settings = settings;
	}
	
	public SettingBagiHasilSet search(String whereClause, DBConnection dbConn) throws Exception {
		SettingBagiHasilSet sbhSet = null;
		
		SettingBagiHasilDAO sbhDAO = new SettingBagiHasilDAO(dbConn);
		sbhSet = sbhDAO.select(whereClause);
		
		return sbhSet;
	}
}
