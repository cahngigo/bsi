package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusTransferDAO {

	private DBConnection dbConn;
	private DateFormat shortFormat;
	
	public BonusTransferDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusTransfer dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_transfer (");
		sb.append("tipe_bonus,");
		sb.append("tanggal,");
		sb.append("member_id,");
		sb.append("no_rekening,");
		sb.append("nama_bank,");
		sb.append("amount,");
		sb.append("tax,");
		sb.append("potongan_am,");
		sb.append("status_kirim");
		sb.append(") VALUES (");
		sb.append("'A',");
		sb.append("'" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + dataObject.getNoRekening() + "',");
		sb.append("'" + dataObject.getNamaBank() + "',");
		sb.append("" + dataObject.getAmount() + ",");
		sb.append("0,");
		sb.append("0,");
		sb.append("0");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusTransfer dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_transfer SET ");
//		sb.append("transfer_id = 'transfer_id',");
//		sb.append("tipe_bonus = 'tipe_bonus',");
//		sb.append("tanggal = 'tanggal',");
//		sb.append("member_id = 'member_id',");
//		sb.append("no_rekening = 'no_rekening',");
//		sb.append("nama_bank = 'nama_bank',");
		sb.append("amount = " + dataObject.getAmount() + " ");
//		sb.append("tax = 'tax',");
//		sb.append("potongan_am = 'potongan_am',");
//		sb.append("status_kirim = 'status_kirim',");
//		sb.append("remarks = 'remarks',");
//		sb.append("transfer_date = 'transfer_date' ");
		sb.append("WHERE tipe_bonus = 'A' AND member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusTransfer dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("transfer_id,");
//		sb.append("tipe_bonus,");
//		sb.append("tanggal,");
//		sb.append("member_id,");
		sb.append("no_rekening,");
		sb.append("nama_bank,");
		sb.append("amount,");
		sb.append("tax,");
		sb.append("potongan_am,");
		sb.append("status_kirim,");
		sb.append("remarks,");
		sb.append("transfer_date ");
		sb.append("FROM bonus_transfer ");
		sb.append("WHERE tipe_bonus = 'A' AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "' ");
		sb.append("AND member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			if (rs.next()) {
				result = true;
				dataObject.setTransferId(rs.getLong("transfer_id"));
				dataObject.setNoRekening(rs.getString("no_rekening"));
				dataObject.setNamaBank(rs.getString("nama_bank"));
				dataObject.setAmount(rs.getDouble("amount"));
				dataObject.setTax(rs.getDouble("tax"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setStatusKirim(rs.getInt("status_kirim"));
//				dataObject.setRemarks(rs.getString("remarks"));
//				dataObject.setTransferDate(new Date(rs.getTimestamp("transfer_date").getTime()));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
