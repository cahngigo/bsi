package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class LogBonusATemp {

	private long id;
	private String memberId;
	private String fromMember;
	private Date tanggal;
	private Date tglBonus;
	private double amount;
	private String jenisBonus;
	private int isProcessed;
	
	public LogBonusATemp() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getFromMember() {
		return fromMember;
	}

	public void setFromMember(String fromMember) {
		this.fromMember = fromMember;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public Date getTglBonus() {
		return tglBonus;
	}

	public void setTglBonus(Date tglBonus) {
		this.tglBonus = tglBonus;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getJenisBonus() {
		return jenisBonus;
	}

	public void setJenisBonus(String jenisBonus) {
		this.jenisBonus = jenisBonus;
	}

	public int getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}
}
