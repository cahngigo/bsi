package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BonusBagiHasilSet implements Serializable {

	ArrayList set = null;
	
	public BonusBagiHasilSet() {
		set = new ArrayList();
	}
	
	public void add(BonusBagiHasil dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusBagiHasil get(int index) {
		BonusBagiHasil result = null;
		
		if ((index >= 0) && (index < length()))
			result = (BonusBagiHasil) set.get(index);
		
		return result;
	}
}
