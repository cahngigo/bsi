package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SettingBonusASet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public SettingBonusASet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(SettingBonusA dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public SettingBonusA get(int index) {
		SettingBonusA result = null;
		
		if ((index >= 0) && (index < length()))
			result = (SettingBonusA) set.get(index);
		
		return result;
	}
}
