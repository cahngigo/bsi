package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class LogBonusADAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	DateFormat longFormat;
	
	public LogBonusADAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int insert(LogBonusA dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO log_bonus_a (");
		sb.append("member_id,");
		sb.append("from_member,");
		sb.append("tanggal,");
		sb.append("tgl_bonus,");
		sb.append("amount,");
		sb.append("jenis_bonus");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + dataObject.getFromMember() + "',");
		sb.append("NOW(),");
		sb.append("'" + shortFormat.format(dataObject.getTglBonus()) + "',");
		sb.append("" + dataObject.getAmount() + ",");
		sb.append("'" + dataObject.getJenisBonus() + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteLogByDate(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM log_bonus_a WHERE tgl_bonus = '" + shortFormat.format(tanggal) + "'");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteSaldoBonusByDate(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM saldo_bonus_a WHERE tgl_bonus = '" + shortFormat.format(tanggal) + "'");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
