package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusADAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusADAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusA dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_a (");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pasangan_sedang,");
		sb.append("bonus_pasangan_kecil,");
		sb.append("bonus_matching,");
		sb.append("total_pasangan_sedang,");
		sb.append("total_flush,");
		sb.append("potongan_am,");
		sb.append("pkp_kumulatif,");
		sb.append("tax ");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("" + dataObject.getBonusSponsor() + ",");
		sb.append("" + dataObject.getBonusPasanganSedang() + ",");
		sb.append("" + dataObject.getBonusPasanganKecil() + ",");
		sb.append("" + dataObject.getBonusMatching() + ",");
		sb.append("" + dataObject.getTotalPasanganSedang() + ",");
		sb.append("" + dataObject.getTotalFlush() + ",");
		sb.append("" + dataObject.getPotonganAm() + ",");
		sb.append("" + dataObject.getPkpKumulatif() + ",");
		sb.append("" + dataObject.getTax());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusA dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_a ");
		sb.append("SET ");
		sb.append("bonus_sponsor = " + dataObject.getBonusSponsor() + ",");
		sb.append("bonus_pasangan_sedang = " + dataObject.getBonusPasanganSedang() + ",");
		sb.append("bonus_pasangan_kecil = " + dataObject.getBonusPasanganKecil() + ",");
		sb.append("bonus_matching = " + dataObject.getBonusMatching() + ",");
		sb.append("total_pasangan_sedang = " + dataObject.getTotalPasanganSedang() + ",");
		sb.append("total_flush = " + dataObject.getTotalFlush() + ",");
		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
		sb.append("pkp_kumulatif = " + dataObject.getPkpKumulatif() + ",");
		sb.append("tax = " + dataObject.getTax() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(BonusA dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_a WHERE ");
		sb.append("member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteByDate(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_a WHERE ");
//		sb.append("tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		sb.append("tanggal = '" + shortFormat.format(tanggal) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusA dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pasangan_sedang,");
		sb.append("bonus_pasangan_kecil,");
		sb.append("bonus_matching,");
		sb.append("total_pasangan_sedang,");
		sb.append("total_flush,");
		sb.append("potongan_am,");
		sb.append("pkp_kumulatif,");
		sb.append("tax ");
		sb.append("FROM bonus_a WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("QUERY: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBonusSponsor(rs.getDouble("bonus_sponsor"));
				dataObject.setBonusPasanganSedang(rs.getDouble("bonus_pasangan_sedang"));
				dataObject.setBonusPasanganKecil(rs.getDouble("bonus_pasangan_kecil"));
				dataObject.setBonusMatching(rs.getDouble("bonus_matching"));
				dataObject.setTotalPasanganSedang(rs.getInt("total_pasangan_sedang"));
				dataObject.setTotalFlush(rs.getInt("total_flush"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setPkpKumulatif(rs.getDouble("pkp_kumulatif"));
				dataObject.setTax(rs.getDouble("tax"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BonusASet select(String whereClause) throws SQLException {
		BonusASet dataObjectSet = new BonusASet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pasangan_sedang,");
		sb.append("bonus_pasangan_kecil,");
		sb.append("bonus_matching,");
		sb.append("total_pasangan_sedang,");
		sb.append("total_flush,");
		sb.append("potongan_am,");
		sb.append("pkp_kumulatif,");
		sb.append("tax ");
		sb.append("FROM bonus_a WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BonusA dataObject = new BonusA();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setBonusSponsor(rs.getDouble("bonus_sponsor"));
				dataObject.setBonusPasanganSedang(rs.getDouble("bonus_pasangan_sedang"));
				dataObject.setBonusPasanganKecil(rs.getDouble("bonus_pasangan_kecil"));
				dataObject.setBonusMatching(rs.getDouble("bonus_matching"));
				dataObject.setTotalPasanganSedang(rs.getInt("total_pasangan_sedang"));
				dataObject.setTotalFlush(rs.getInt("total_flush"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setPkpKumulatif(rs.getDouble("pkp_kumulatif"));
				dataObject.setTax(rs.getDouble("tax"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int insertBonusTransfer(String tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("CALL filltransfer_a('" + tanggal + "', '" + tanggal + "')");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public double hitungTotalMicroPair(String whereClause) throws SQLException {
		double result = 0;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT SUM(COALESCE(bonus_pasangan_sedang,0)) total_sedang FROM bonus_a WHERE " + whereClause);
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			if (rs.next()) {
				result = rs.getDouble("total_sedang");
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteTransferByDate(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_transfer WHERE ");
		sb.append("tanggal = '" + shortFormat.format(tanggal) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean getTotalBonusPerDay(BonusA dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("SUM(COALESCE(ba.bonus_sponsor,0)) sponsor, ");
		sb.append("SUM(COALESCE(ba.bonus_pasangan_sedang,0)) sedang, ");
		sb.append("SUM(COALESCE(ba.bonus_pasangan_kecil,0)) kecil, ");
		sb.append("SUM(COALESCE(ba.bonus_matching,0)) matching ");
		sb.append("FROM bonus_a ba ");
		sb.append("WHERE ba.tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBonusSponsor(rs.getDouble("sponsor"));
				dataObject.setBonusPasanganSedang(rs.getDouble("sedang"));
				dataObject.setBonusPasanganKecil(rs.getDouble("kecil"));
				dataObject.setBonusMatching(rs.getDouble("matching"));				
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean anyNullMember(Date tanggal) throws SQLException {
		boolean isNull = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT COUNT(*) jumlah FROM bonus_a ");
		sb.append("WHERE tanggal = '" + shortFormat.format(tanggal) + "' ");
		sb.append("AND (member_id IS NULL OR member_id = '')");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			if (rs.next()) {
				int jumlah = rs.getInt("jumlah");
				if (jumlah > 0) isNull = true;
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return isNull;
	}
}
