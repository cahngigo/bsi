package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BonusATempSet implements Serializable {

	@SuppressWarnings("rawtypes")
	ArrayList set = null;
	
	@SuppressWarnings("rawtypes")
	public BonusATempSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BonusATemp dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusATemp get(int index) {
		BonusATemp result = null;
		
		if ((index >= 0) && (index < length()))
			result = (BonusATemp) set.get(index);
		
		return result;
	}
}
