 package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class SettingBagiHasil {

	private int id;
	private double budgetPerPaket;
	private double budgetMicro;
	private int maxPotensiMicro;
	private int kelipatanPertumbuhanMicro;
	private int bintang1MinGkMicro;
	private int bintang1BagianMicro;
	private int bintang2MinGkMicro;
	private int bintang2BagianMicro;
	private int bintang3MinGkMicro;
	private int bintang3BagianMicro;
	private double budgetMacro;
	private int maxPotensiMacro;
	private int kelipatanPertumbuhanMacro;
	private int bintang1MinGkMacro;
	private int bintang1BagianMacro;
	private int bintang2MinGkMacro;
	private int bintang2BagianMacro;
	private int bintang3MinGkMacro;
	private int bintang3BagianMacro;
	private Date startDate;
	private String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBudgetPerPaket() {
		return budgetPerPaket;
	}
	public void setBudgetPerPaket(double budgetPerPaket) {
		this.budgetPerPaket = budgetPerPaket;
	}
	public double getBudgetMicro() {
		return budgetMicro;
	}
	public void setBudgetMicro(double budgetMicro) {
		this.budgetMicro = budgetMicro;
	}
	public int getMaxPotensiMicro() {
		return maxPotensiMicro;
	}
	public void setMaxPotensiMicro(int maxPotensiMicro) {
		this.maxPotensiMicro = maxPotensiMicro;
	}
	public int getKelipatanPertumbuhanMicro() {
		return kelipatanPertumbuhanMicro;
	}
	public void setKelipatanPertumbuhanMicro(int kelipatanPertumbuhanMicro) {
		this.kelipatanPertumbuhanMicro = kelipatanPertumbuhanMicro;
	}
	public int getBintang1MinGkMicro() {
		return bintang1MinGkMicro;
	}
	public void setBintang1MinGkMicro(int bintang1MinGkMicro) {
		this.bintang1MinGkMicro = bintang1MinGkMicro;
	}
	public int getBintang1BagianMicro() {
		return bintang1BagianMicro;
	}
	public void setBintang1BagianMicro(int bintang1BagianMicro) {
		this.bintang1BagianMicro = bintang1BagianMicro;
	}
	public int getBintang2MinGkMicro() {
		return bintang2MinGkMicro;
	}
	public void setBintang2MinGkMicro(int bintang2MinGkMicro) {
		this.bintang2MinGkMicro = bintang2MinGkMicro;
	}
	public int getBintang2BagianMicro() {
		return bintang2BagianMicro;
	}
	public void setBintang2BagianMicro(int bintang2BagianMicro) {
		this.bintang2BagianMicro = bintang2BagianMicro;
	}
	public int getBintang3MinGkMicro() {
		return bintang3MinGkMicro;
	}
	public void setBintang3MinGkMicro(int bintang3MinGkMicro) {
		this.bintang3MinGkMicro = bintang3MinGkMicro;
	}
	public int getBintang3BagianMicro() {
		return bintang3BagianMicro;
	}
	public void setBintang3BagianMicro(int bintang3BagianMicro) {
		this.bintang3BagianMicro = bintang3BagianMicro;
	}
	public double getBudgetMacro() {
		return budgetMacro;
	}
	public void setBudgetMacro(double budgetMacro) {
		this.budgetMacro = budgetMacro;
	}
	public int getMaxPotensiMacro() {
		return maxPotensiMacro;
	}
	public void setMaxPotensiMacro(int maxPotensiMacro) {
		this.maxPotensiMacro = maxPotensiMacro;
	}
	public int getKelipatanPertumbuhanMacro() {
		return kelipatanPertumbuhanMacro;
	}
	public void setKelipatanPertumbuhanMacro(int kelipatanPertumbuhanMacro) {
		this.kelipatanPertumbuhanMacro = kelipatanPertumbuhanMacro;
	}
	public int getBintang1MinGkMacro() {
		return bintang1MinGkMacro;
	}
	public void setBintang1MinGkMacro(int bintang1MinGkMacro) {
		this.bintang1MinGkMacro = bintang1MinGkMacro;
	}
	public int getBintang1BagianMacro() {
		return bintang1BagianMacro;
	}
	public void setBintang1BagianMacro(int bintang1BagianMacro) {
		this.bintang1BagianMacro = bintang1BagianMacro;
	}
	public int getBintang2MinGkMacro() {
		return bintang2MinGkMacro;
	}
	public void setBintang2MinGkMacro(int bintang2MinGkMacro) {
		this.bintang2MinGkMacro = bintang2MinGkMacro;
	}
	public int getBintang2BagianMacro() {
		return bintang2BagianMacro;
	}
	public void setBintang2BagianMacro(int bintang2BagianMacro) {
		this.bintang2BagianMacro = bintang2BagianMacro;
	}
	public int getBintang3MinGkMacro() {
		return bintang3MinGkMacro;
	}
	public void setBintang3MinGkMacro(int bintang3MinGkMacro) {
		this.bintang3MinGkMacro = bintang3MinGkMacro;
	}
	public int getBintang3BagianMacro() {
		return bintang3BagianMacro;
	}
	public void setBintang3BagianMacro(int bintang3BagianMacro) {
		this.bintang3BagianMacro = bintang3BagianMacro;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
		
}
