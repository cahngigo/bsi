package id.co.birumerah.bsi.bonus.plana;

import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.tree.Tree;

public class CounterInitiator {
	
	private Tree<MemberTree> pohon;
	private BonusCounter bCounter;
	
	public CounterInitiator() {
		super();
		
		bCounter = new BonusCounter(true);
		this.pohon = bCounter.getTree();
	}
	
	private void readMemberTree() {
		traverseTree(pohon);
	}

	private void traverseTree(Tree<MemberTree> traceTree) {
		for (Tree<MemberTree> data : traceTree.getLeafs()) {
			traverseTree(data);
			MemberTree member = data.getHead();
			if (bCounter.getSuccessorsSize(member.getMemberId()) > 0) {
				MemberTree[] directDownline = bCounter.sortDownliners(member);
				MemberTree kakiBesar = directDownline[0];
				System.out.println("Kaki besar dari "+ member.getMemberId() +" adalah "+ kakiBesar.getMemberId());
				member.setBigfoot(kakiBesar.getMemberId());
				member.setAmtBigfoot(bCounter.getSuccessorsSize(kakiBesar.getMemberId()));
				if (directDownline.length > 1) {
					MemberTree kakiSedang = directDownline[1];
					System.out.println("Kaki sedang dari "+ member.getMemberId() +" adalah "+ kakiSedang.getMemberId());
					member.setMediumfoot(kakiSedang.getMemberId());
					member.setAmtMidfoot(bCounter.getSuccessorsSize(kakiSedang.getMemberId()));
					if (directDownline.length > 2) {
						MemberTree kakiKecil = directDownline[2];
						System.out.println("Kaki kecil dari "+ member.getMemberId() +" adalah "+ kakiKecil.getMemberId());
						member.setLitfoot(kakiKecil.getMemberId());
						member.setAmtLitfoot(bCounter.getSuccessorsSize(kakiKecil.getMemberId()));
					}
				}
			}
		}
	}
	
	public void makeTree() {
		readMemberTree();
	}

}
