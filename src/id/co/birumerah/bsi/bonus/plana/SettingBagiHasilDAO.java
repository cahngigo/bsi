package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class SettingBagiHasilDAO {

	private DBConnection dbConn;
	
	public SettingBagiHasilDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public SettingBagiHasilSet select(String whereClause) throws SQLException {
		SettingBagiHasilSet dataObjectSet = new SettingBagiHasilSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("budget_per_paket,");
		sb.append("budget_micro,");
		sb.append("max_potensi_micro,");
		sb.append("kelipatan_pertumbuhan_micro,");
		sb.append("bintang1_min_gk_micro,");
		sb.append("bintang1_bagian_micro,");
		sb.append("bintang2_min_gk_micro,");
		sb.append("bintang2_bagian_micro,");
		sb.append("bintang3_min_gk_micro,");
		sb.append("bintang3_bagian_micro,");
		sb.append("budget_macro,");
		sb.append("max_potensi_macro,");
		sb.append("kelipatan_pertumbuhan_macro,");
		sb.append("bintang1_min_gk_macro,");
		sb.append("bintang1_bagian_macro,");
		sb.append("bintang2_min_gk_macro,");
		sb.append("bintang2_bagian_macro,");
		sb.append("bintang3_min_gk_macro,");
		sb.append("bintang3_bagian_macro,");
		sb.append("start_date,");
		sb.append("status ");
		sb.append("FROM setting_bagi_hasil ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				SettingBagiHasil dataObject = new SettingBagiHasil();
				dataObject.setBudgetPerPaket(rs.getDouble("budget_per_paket"));
				dataObject.setBudgetMicro(rs.getDouble("budget_micro"));
				dataObject.setMaxPotensiMicro(rs.getInt("max_potensi_micro"));
				dataObject.setKelipatanPertumbuhanMicro(rs.getInt("kelipatan_pertumbuhan_micro"));
				dataObject.setBintang1MinGkMicro(rs.getInt("bintang1_min_gk_micro"));
				dataObject.setBintang1BagianMicro(rs.getInt("bintang1_bagian_micro"));
				dataObject.setBintang2MinGkMicro(rs.getInt("bintang2_min_gk_micro"));
				dataObject.setBintang2BagianMicro(rs.getInt("bintang2_bagian_micro"));
				dataObject.setBintang3MinGkMicro(rs.getInt("bintang3_min_gk_micro"));
				dataObject.setBintang3BagianMicro(rs.getInt("bintang3_bagian_micro"));
				dataObject.setBudgetMacro(rs.getDouble("budget_macro"));
				dataObject.setMaxPotensiMacro(rs.getInt("max_potensi_macro"));
				dataObject.setKelipatanPertumbuhanMacro(rs.getInt("kelipatan_pertumbuhan_macro"));
				dataObject.setBintang1MinGkMacro(rs.getInt("bintang1_min_gk_macro"));
				dataObject.setBintang1BagianMacro(rs.getInt("bintang1_bagian_macro"));
				dataObject.setBintang2MinGkMacro(rs.getInt("bintang2_min_gk_macro"));
				dataObject.setBintang2BagianMacro(rs.getInt("bintang2_bagian_macro"));
				dataObject.setBintang3MinGkMacro(rs.getInt("bintang3_min_gk_macro"));
				dataObject.setBintang3BagianMacro(rs.getInt("bintang3_bagian_macro"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
