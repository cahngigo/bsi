package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class SettingBonusADAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public SettingBonusADAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(SettingBonusA dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO setting_bonus_a (");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pas_sedang,");
		sb.append("bonus_pas_kecil,");
		sb.append("bonus_matching_level,");
		sb.append("bonus_matching_pct,");
		sb.append("flush_out_reguler,");
		sb.append("flush_out_gold,");
		sb.append("flush_out_platinum,");
		sb.append("start_date,");
		sb.append("status ");
		sb.append(") VALUES (");
		sb.append("" + dataObject.getBonusSponsor() + ",");
		sb.append("" + dataObject.getBonusPasSedang() + ",");
		sb.append("" + dataObject.getBonusPasKecil() + ",");
		sb.append("" + dataObject.getBonusMatchingLevel() + ",");
		sb.append("" + dataObject.getBonusMatchingPct() + ",");
		sb.append("" + dataObject.getFlushOutReguler() + ",");
		sb.append("" + dataObject.getFlushOutGold() + ",");
		sb.append("" + dataObject.getFlushOutPlatinum() + ",");
		sb.append("'" + shortFormat.format(dataObject.getStartDate()) + "',");
		sb.append("'" + dataObject.getStatus() + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(SettingBonusA dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE setting_bonus_a ");
		sb.append("SET ");
		sb.append("bonus_sponsor = " + dataObject.getBonusSponsor() + ",");
		sb.append("bonus_pas_sedang = " + dataObject.getBonusPasSedang() + ",");
		sb.append("bonus_pas_kecil = " + dataObject.getBonusPasKecil() + ",");
		sb.append("bonus_matching_level = " + dataObject.getBonusMatchingLevel() + ",");
		sb.append("bonus_matching_pct = " + dataObject.getBonusMatchingPct() + ",");
		sb.append("flush_out_reguler = " + dataObject.getFlushOutReguler() + ",");
		sb.append("flush_out_gold = " + dataObject.getFlushOutGold() + ",");
		sb.append("flush_out_platinum = " + dataObject.getFlushOutPlatinum() + ",");
		sb.append("start_date = '" + shortFormat.format(dataObject.getStartDate()) + "',");
		sb.append("status = '" + dataObject.getStatus() + "' ");
		sb.append("WHERE id = " + dataObject.getId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(SettingBonusA dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pas_sedang,");
		sb.append("bonus_pas_kecil,");
		sb.append("bonus_matching_level,");
		sb.append("bonus_matching_pct,");
		sb.append("flush_out_reguler,");
		sb.append("flush_out_gold,");
		sb.append("flush_out_platinum,");
		sb.append("start_date,");
		sb.append("status ");
		sb.append("FROM setting_bonus_a ");
		sb.append("WHERE id = " + dataObject.getId());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBonusSponsor(rs.getDouble("bonus_sponsor"));
				dataObject.setBonusPasSedang(rs.getDouble("bonus_pas_sedang"));
				dataObject.setBonusPasKecil(rs.getDouble("bonus_pas_kecil"));
				dataObject.setBonusMatchingLevel(rs.getInt("bonus_matching_level"));
				dataObject.setBonusMatchingPct(rs.getFloat("bonus_matching_pct"));
				dataObject.setFlushOutReguler(rs.getInt("flush_out_reguler"));
				dataObject.setFlushOutGold(rs.getInt("flush_out_gold"));
				dataObject.setFlushOutPlatinum(rs.getInt("flush_out_platinum"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public SettingBonusASet select(String whereClause) throws SQLException {
		SettingBonusASet dataObjectSet = new SettingBonusASet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pas_sedang,");
		sb.append("bonus_pas_kecil,");
		sb.append("bonus_matching_level,");
		sb.append("bonus_matching_pct,");
		sb.append("flush_out_reguler,");
		sb.append("flush_out_gold,");
		sb.append("flush_out_platinum,");
		sb.append("start_date,");
		sb.append("status ");
		sb.append("FROM setting_bonus_a ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				SettingBonusA dataObject = new SettingBonusA();
				dataObject.setBonusSponsor(rs.getDouble("bonus_sponsor"));
				dataObject.setBonusPasSedang(rs.getDouble("bonus_pas_sedang"));
				dataObject.setBonusPasKecil(rs.getDouble("bonus_pas_kecil"));
				dataObject.setBonusMatchingLevel(rs.getInt("bonus_matching_level"));
				dataObject.setBonusMatchingPct(rs.getFloat("bonus_matching_pct"));
				dataObject.setFlushOutReguler(rs.getInt("flush_out_reguler"));
				dataObject.setFlushOutGold(rs.getInt("flush_out_gold"));
				dataObject.setFlushOutPlatinum(rs.getInt("flush_out_platinum"));
				dataObject.setStartDate(rs.getDate("start_date"));
				dataObject.setStatus(rs.getString("status"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
