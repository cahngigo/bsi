package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusATempDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusATempDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusATemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_a_temp (");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pasangan_sedang,");
		sb.append("bonus_pasangan_kecil,");
		sb.append("bonus_matching,");
		sb.append("total_pasangan_sedang,");
		sb.append("total_flush,");
		sb.append("potongan_am,");
		sb.append("pkp_kumulatif,");
		sb.append("tax ");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("" + dataObject.getBonusSponsor() + ",");
		sb.append("" + dataObject.getBonusPasanganSedang() + ",");
		sb.append("" + dataObject.getBonusPasanganKecil() + ",");
		sb.append("" + dataObject.getBonusMatching() + ",");
		sb.append("" + dataObject.getTotalPasanganSedang() + ",");
		sb.append("" + dataObject.getTotalFlush() + ",");
		sb.append("" + dataObject.getPotonganAm() + ",");
		sb.append("" + dataObject.getPkpKumulatif() + ",");
		sb.append("" + dataObject.getTax());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusATemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_a_temp ");
		sb.append("SET ");
		sb.append("bonus_sponsor = " + dataObject.getBonusSponsor() + ",");
		sb.append("bonus_pasangan_sedang = " + dataObject.getBonusPasanganSedang() + ",");
		sb.append("bonus_pasangan_kecil = " + dataObject.getBonusPasanganKecil() + ",");
		sb.append("bonus_matching = " + dataObject.getBonusMatching() + ",");
		sb.append("total_pasangan_sedang = " + dataObject.getTotalPasanganSedang() + ",");
		sb.append("total_flush = " + dataObject.getTotalFlush() + ",");
		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
		sb.append("pkp_kumulatif = " + dataObject.getPkpKumulatif() + ",");
		sb.append("tax = " + dataObject.getTax() + ",");
		sb.append("is_processed = " + dataObject.getIsProcessed() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(BonusATemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_a_temp WHERE ");
		sb.append("member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteByMember(String memberId) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_a_temp WHERE ");
		sb.append("member_id = '" + memberId + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusATemp dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pasangan_sedang,");
		sb.append("bonus_pasangan_kecil,");
		sb.append("bonus_matching,");
		sb.append("total_pasangan_sedang,");
		sb.append("total_flush,");
		sb.append("potongan_am,");
		sb.append("pkp_kumulatif,");
		sb.append("tax,");
		sb.append("is_processed ");
		sb.append("FROM bonus_a_temp WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("QUERY: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setBonusSponsor(rs.getDouble("bonus_sponsor"));
				dataObject.setBonusPasanganSedang(rs.getDouble("bonus_pasangan_sedang"));
				dataObject.setBonusPasanganKecil(rs.getDouble("bonus_pasangan_kecil"));
				dataObject.setBonusMatching(rs.getDouble("bonus_matching"));
				dataObject.setTotalPasanganSedang(rs.getInt("total_pasangan_sedang"));
				dataObject.setTotalFlush(rs.getInt("total_flush"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setPkpKumulatif(rs.getDouble("pkp_kumulatif"));
				dataObject.setTax(rs.getDouble("tax"));
				dataObject.setIsProcessed(rs.getInt("is_processed"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BonusATempSet select(String whereClause) throws SQLException {
		BonusATempSet dataObjectSet = new BonusATempSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("tanggal,");
		sb.append("bonus_sponsor,");
		sb.append("bonus_pasangan_sedang,");
		sb.append("bonus_pasangan_kecil,");
		sb.append("bonus_matching,");
		sb.append("total_pasangan_sedang,");
		sb.append("total_flush,");
		sb.append("potongan_am,");
		sb.append("pkp_kumulatif,");
		sb.append("tax,");
		sb.append("is_processed ");
		sb.append("FROM bonus_a_temp WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BonusATemp dataObject = new BonusATemp();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setBonusSponsor(rs.getDouble("bonus_sponsor"));
				dataObject.setBonusPasanganSedang(rs.getDouble("bonus_pasangan_sedang"));
				dataObject.setBonusPasanganKecil(rs.getDouble("bonus_pasangan_kecil"));
				dataObject.setBonusMatching(rs.getDouble("bonus_matching"));
				dataObject.setTotalPasanganSedang(rs.getInt("total_pasangan_sedang"));
				dataObject.setTotalFlush(rs.getInt("total_flush"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setPkpKumulatif(rs.getDouble("pkp_kumulatif"));
				dataObject.setTax(rs.getDouble("tax"));
				dataObject.setIsProcessed(rs.getInt("is_processed"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
