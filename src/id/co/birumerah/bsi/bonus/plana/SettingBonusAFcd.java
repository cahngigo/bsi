package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class SettingBonusAFcd {

	private SettingBonusA settings;
	
	public SettingBonusAFcd() {}

	public SettingBonusAFcd(SettingBonusA settings) {
		this.settings = settings;
	}

	public SettingBonusA getSettings() {
		return settings;
	}

	public void setSettings(SettingBonusA settings) {
		this.settings = settings;
	}
	
	public SettingBonusASet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		SettingBonusASet sbSet = null;
		
		try {
			dbConn = new DBConnection();
			SettingBonusADAO sbDAO = new SettingBonusADAO(dbConn);
			sbSet = sbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return sbSet;
	}
	
	public SettingBonusASet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		SettingBonusASet sbSet = null;
		
		try {
//			dbConn = new DBConnection();
			SettingBonusADAO sbDAO = new SettingBonusADAO(dbConn);
			sbSet = sbDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return sbSet;
	}
}
