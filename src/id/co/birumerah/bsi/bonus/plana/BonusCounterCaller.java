package id.co.birumerah.bsi.bonus.plana;

import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.flag.FlagRun;
import id.co.birumerah.bsi.flag.FlagRunFcd;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;
import java.util.Date;

import org.joda.time.MutableDateTime;

public class BonusCounterCaller {

//	private Date tglTrx;
	private MutableDateTime tglTrx;
	
	public BonusCounterCaller() {
		super();
	}
	
//	public BonusCounterCaller(Date trxDate) {
//		this.tglTrx = trxDate;
//	}
	public BonusCounterCaller(MutableDateTime trxDate, boolean isMonthCopy) {
		this.tglTrx = trxDate;
//		copyMemberTreeToHistory(isMonthCopy);
	}
	
	public BonusCounterCaller(MutableDateTime trxDate) {
		this.tglTrx = trxDate;
//		if (isCopyToHistory) copyMemberTreeToHistory();
//		copyMemberTreeToHistory(true);
	}
	
	public void dailyBonusCounter() {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusCounter bc = new BonusCounter(true, tglTrx, dbConn);
//			bc.giveBonusMatchingNew(dbConn);
//			bc.giveBagiHasil(dbConn);
			bc.prepareTransfer(dbConn);
			bc.addBonusTempToTransfer(dbConn);
			MemberTreeFcd mtFcd = new MemberTreeFcd();
			if (tglTrx.getDayOfMonth() == 1) {
				FlagRun flag = FlagRunFcd.getFlag(dbConn);
				while (flag.getIsRun() == 1) {
					System.out.println("Wait for 30 seconds...");
					Thread.sleep(30000);
					flag = null;
					flag = FlagRunFcd.getFlag(dbConn);
				}
				if (flag.getIsRun() == 0) {
					System.out.println("Flag Run dah 0...");
					flag.setIsRun(1);
					FlagRunFcd frFcd = new FlagRunFcd();
					frFcd.updateFlag(flag, dbConn);
					
					bc = null;
					bc = new BonusCounter(false, tglTrx, dbConn);
					bc.updateMemberTree(dbConn);
					flag.setIsRun(0);
					frFcd.updateFlag(flag, dbConn);
				}
				MutableDateTime bulanHitung = new MutableDateTime(tglTrx.getYear(), tglTrx.getMonthOfYear(), tglTrx.getDayOfMonth(), 0, 0, 0, 0);
				bulanHitung.addMonths(-1);
				mtFcd.setTanggal(bulanHitung);
				mtFcd.monthlyBackup(tglTrx);
				
				mtFcd.resetMemberTree(dbConn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
		
	}
	
	public void dailyBonusCounter(DBConnection dbConn) {
		BonusCounter bc = new BonusCounter(tglTrx);
		bc.giveBonusSponsor();
		bc.giveBonusMatching();
	}
	
	public void dailyBonusCounter(boolean isEOD) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusCounter bc = new BonusCounter(isEOD, tglTrx, dbConn);
			if (isEOD) {
//				bc.giveBonusMatchingNew(dbConn);
//				bc.giveBagiHasil(dbConn);
				bc.prepareTransfer(dbConn);
				bc.addBonusTempToTransfer(dbConn);
				MemberTreeFcd mtFcd = new MemberTreeFcd();
				if (tglTrx.getDayOfMonth() == 1) {
					FlagRun flag = FlagRunFcd.getFlag(dbConn);
					while (flag.getIsRun() == 1) {
						System.out.println("Wait for 30 seconds...");
						Thread.sleep(30000);
						
						flag = null;
						flag = FlagRunFcd.getFlag(dbConn);
					}
					if (flag.getIsRun() == 0) {
						flag.setIsRun(1);
						FlagRunFcd frFcd = new FlagRunFcd();
						frFcd.updateFlag(flag, dbConn);
						
						bc = null;
						bc = new BonusCounter(false, tglTrx, dbConn);
						bc.updateMemberTree(dbConn);
						flag.setIsRun(0);
						frFcd.updateFlag(flag, dbConn);
					}
					MutableDateTime bulanHitung = new MutableDateTime(tglTrx.getYear(), tglTrx.getMonthOfYear(), tglTrx.getDayOfMonth(), 0, 0, 0, 0);
					bulanHitung.addMonths(-1);
					mtFcd.setTanggal(bulanHitung);
					mtFcd.monthlyBackup(tglTrx);
					
					mtFcd.resetMemberTree(dbConn);
				}
				
			} else {
				FlagRun flag = FlagRunFcd.getFlag(dbConn);
				if (flag.getIsRun() == 0) {
					flag.setIsRun(1);
					FlagRunFcd frFcd = new FlagRunFcd();
					frFcd.updateFlag(flag, dbConn);
					bc.updateMemberTree(dbConn);
					bc.updatePvGroup(dbConn);
					flag.setIsRun(0);
					frFcd.updateFlag(flag, dbConn);
				}				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}
	
	public void updateNonbasicTree() {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusCounter bc = new BonusCounter(false, tglTrx, dbConn);
			FlagRun flag = FlagRunFcd.getFlag(dbConn);
			while (flag.getIsRun() == 1) {
				System.out.println("Wait for 30 seconds...");
				Thread.sleep(30000);
				
				flag = null;
				flag = FlagRunFcd.getFlag(dbConn);
			}
			if (flag.getIsRun() == 0) {
				flag.setIsRun(1);
				FlagRunFcd frFcd = new FlagRunFcd();
				frFcd.updateFlag(flag, dbConn);
				bc.updateMemberTreeNonbasic(dbConn);
				flag.setIsRun(0);
				frFcd.updateFlag(flag, dbConn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}
	
	public void dailyBonusCounterLog(boolean isEOD) {
//		BonusCounter bc = new BonusCounter(isEOD, tglTrx);
//		bc.giveBonusSponsor();
//		bc.giveBonusMatching();
//		bc.updateMemberTree();
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
//			BonusCounter bc = new BonusCounter(isEOD, tglTrx, dbConn);
			BonusCounter bc = new BonusCounter(true, tglTrx, dbConn);
			
			bc.updateMemberTreeLog(dbConn);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}
	
	public void dailyBonusCounterLogBulanan(boolean isEOD) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
//			BonusCounter bc = new BonusCounter(isEOD, tglTrx, dbConn);
			BonusCounter bc = new BonusCounter(tglTrx, dbConn);
			
			bc.updateMemberTreeLogBulanan(dbConn);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}
	
	private void copyMemberTreeToHistory(boolean isMonthBackup) {
		System.out.println("di copyMemberTreeToHistory()");
		MutableDateTime hariHitung = new MutableDateTime(tglTrx.getYear(), tglTrx.getMonthOfYear(), tglTrx.getDayOfMonth(), 0, 0, 0, 0);
		hariHitung.addDays(-1);
//		String whereClause = "reg_date < '" + tglTrx.toString("yyyy-MM-dd HH:mm:ss") + "'";
//		System.out.println(whereClause);
//		System.out.println("hariHitung = " + hariHitung.toString("yyyy-MM-dd"));
		MemberTreeFcd mtFcd = new MemberTreeFcd();
//		MemberTreeSet mtSet = null;
		try {
//			if (tglTrx.getDayOfMonth() == 1 && isMonthBackup) {
//				MutableDateTime bulanHitung = new MutableDateTime(tglTrx.getYear(), tglTrx.getMonthOfYear(), tglTrx.getDayOfMonth(), 0, 0, 0, 0);
//				bulanHitung.addMonths(-1);
//				mtFcd.setTanggal(bulanHitung);
//				mtFcd.monthlyBackup(tglTrx);
//			}
			mtFcd.setTanggal(hariHitung);
			mtFcd.dailyBackup(tglTrx);
//			if (tglTrx.getDayOfMonth() == 1) mtFcd.resetMemberTree();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
