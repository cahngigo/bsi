package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBagiHasilLogFcd {

	private BonusBagiHasilLog bbhLog;
	
	public BonusBagiHasilLogFcd() {}

	public BonusBagiHasilLogFcd(BonusBagiHasilLog bbhLog) {
		this.bbhLog = bbhLog;
	}

	public BonusBagiHasilLog getBbhLog() {
		return bbhLog;
	}

	public void setBbhLog(BonusBagiHasilLog bbhLog) {
		this.bbhLog = bbhLog;
	}
	
	public static BonusBagiHasilLog getLogByDate(Date tanggal, DBConnection dbConn) throws Exception {
		BonusBagiHasilLog log = new BonusBagiHasilLog();
		log.setTanggal(tanggal);
		
		BonusBagiHasilLogDAO logDAO = new BonusBagiHasilLogDAO(dbConn);
		boolean found = logDAO.select(log);
		
		if (found) {
			return log;
		} else {
			System.err.println("Log bonus bagi hasil hari ini tidak ditemukan.");
		}
		
		return null;
	}
	
	public void insertLogBonus(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilLogDAO logDAO = new BonusBagiHasilLogDAO(dbConn);
			logDAO.insert(bbhLog);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateLogBonus(BonusBagiHasilLog log, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilLogDAO logDAO = new BonusBagiHasilLogDAO(dbConn);
			if (logDAO.update(log) < 1) {
				throw new Exception("Log bonus bagi hasil not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteByDate(BonusBagiHasilLog log, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilLogDAO logDAO = new BonusBagiHasilLogDAO(dbConn);
			if (logDAO.deleteByTanggal(log) < 1) {
				throw new Exception("Delete log bonus bagi hasil failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
