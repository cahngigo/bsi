package id.co.birumerah.bsi.bonus.plana;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogBonusATempDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	DateFormat longFormat;
	
	public LogBonusATempDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int insert(LogBonusATemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO log_bonus_a_temp (");
		sb.append("member_id,");
		sb.append("from_member,");
		sb.append("tanggal,");
		sb.append("tgl_bonus,");
		sb.append("amount,");
		sb.append("jenis_bonus");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + dataObject.getFromMember() + "',");
		sb.append("NOW(),");
		sb.append("'" + shortFormat.format(dataObject.getTglBonus()) + "',");
		sb.append("" + dataObject.getAmount() + ",");
		sb.append("'" + dataObject.getJenisBonus() + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteLogByDate(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE FROM log_bonus_a_temp WHERE tgl_bonus = '" + shortFormat.format(tanggal) + "'");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public LogBonusATempSet select(String whereClause) throws SQLException {
		LogBonusATempSet dataObjectSet = new LogBonusATempSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("member_id,");
		sb.append("from_member,");
		sb.append("tanggal,");
		sb.append("tgl_bonus,");
		sb.append("amount,");
		sb.append("jenis_bonus,");
		sb.append("is_processed ");
		sb.append("FROM log_bonus_a_temp ");
		sb.append("WHERE " + whereClause);
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				LogBonusATemp dataObject = new LogBonusATemp();
				dataObject.setId(rs.getLong("id"));
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setFromMember(rs.getString("from_member"));
				dataObject.setTanggal(new Date(rs.getTimestamp("tanggal").getTime()));
				dataObject.setTglBonus(rs.getDate("tgl_bonus"));
				dataObject.setAmount(rs.getDouble("amount"));
				dataObject.setJenisBonus(rs.getString("jenis_bonus"));
				dataObject.setIsProcessed(rs.getInt("is_processed"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public LogBonusATempSet selectGroupBy(String whereClause) throws SQLException {
		LogBonusATempSet dataObjectSet = new LogBonusATempSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("from_member,");
		sb.append("tgl_bonus,");
		sb.append("SUM(amount) amount,");
		sb.append("jenis_bonus ");
		sb.append("FROM log_bonus_a_temp lbat ");
		sb.append("WHERE " + whereClause + " ");
		sb.append("GROUP BY member_id, from_member, tgl_bonus, jenis_bonus");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				LogBonusATemp dataObject = new LogBonusATemp();
//				dataObject.setId(rs.getLong("id"));
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setFromMember(rs.getString("from_member"));
//				dataObject.setTanggal(new Date(rs.getTimestamp("tanggal").getTime()));
				dataObject.setTglBonus(rs.getDate("tgl_bonus"));
				dataObject.setAmount(rs.getDouble("amount"));
				dataObject.setJenisBonus(rs.getString("jenis_bonus"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int update(LogBonusATemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE log_bonus_a_temp ");
		sb.append("SET ");
		sb.append("is_processed = 1 ");
		sb.append("WHERE id = " + dataObject.getId());
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int updateGrouping(LogBonusATemp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE log_bonus_a_temp ");
		sb.append("SET ");
		sb.append("is_processed = 1 ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND from_member = '" + dataObject.getFromMember() + "' ");
		sb.append("AND tgl_bonus = '" + shortFormat.format(dataObject.getTglBonus()) + "' ");
		sb.append("AND jenis_bonus = '" + dataObject.getJenisBonus() + "'");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
