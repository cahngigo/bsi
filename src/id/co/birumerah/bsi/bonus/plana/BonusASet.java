package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

public class BonusASet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2748755731073096630L;

	@SuppressWarnings("rawtypes")
	ArrayList set = null;
	
	@SuppressWarnings("rawtypes")
	public BonusASet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BonusA dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusA get(int index) {
		BonusA result = null;
		
		if ((index >= 0) && (index < length()))
			result = (BonusA) set.get(index);
		
		return result;
	}
}
