package id.co.birumerah.bsi.bonus.plana;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BonusBagiHasilTempSet implements Serializable {

	@SuppressWarnings("rawtypes")
	ArrayList set = null;
	
	@SuppressWarnings("rawtypes")
	public BonusBagiHasilTempSet() {
		this.set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BonusBagiHasilTemp dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BonusBagiHasilTemp get(int index) {
		BonusBagiHasilTemp result = null;
		
		if((index >= 0) && (index < length()))
			result = (BonusBagiHasilTemp) set.get(index);
		
		return result;
	}
}
