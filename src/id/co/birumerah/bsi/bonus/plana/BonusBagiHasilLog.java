package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class BonusBagiHasilLog {

	private long id;
	private Date tanggal;
	private double budget;
	private double budgetMicro;
	private int totalBagian;
	private double perbagian;
	private double budgetMacro;
	private int totalBagianMacro;
	private double perbagianMacro;
	
	public BonusBagiHasilLog() {}

	public BonusBagiHasilLog(long id, Date tanggal, double budget,
			double budgetMicro, int totalBagian, double perbagian,
			double budgetMacro, int totalBagianMacro, double perbagianMacro) {
		this.id = id;
		this.tanggal = tanggal;
		this.budget = budget;
		this.budgetMicro = budgetMicro;
		this.totalBagian = totalBagian;
		this.perbagian = perbagian;
		this.budgetMacro = budgetMacro;
		this.totalBagianMacro = totalBagianMacro;
		this.perbagianMacro = perbagianMacro;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public int getTotalBagian() {
		return totalBagian;
	}

	public void setTotalBagian(int totalBagian) {
		this.totalBagian = totalBagian;
	}

	public double getPerbagian() {
		return perbagian;
	}

	public void setPerbagian(double perbagian) {
		this.perbagian = perbagian;
	}

	public double getBudgetMicro() {
		return budgetMicro;
	}

	public void setBudgetMicro(double budgetMicro) {
		this.budgetMicro = budgetMicro;
	}

	public double getBudgetMacro() {
		return budgetMacro;
	}

	public void setBudgetMacro(double budgetMacro) {
		this.budgetMacro = budgetMacro;
	}

	public int getTotalBagianMacro() {
		return totalBagianMacro;
	}

	public void setTotalBagianMacro(int totalBagianMacro) {
		this.totalBagianMacro = totalBagianMacro;
	}

	public double getPerbagianMacro() {
		return perbagianMacro;
	}

	public void setPerbagianMacro(double perbagianMacro) {
		this.perbagianMacro = perbagianMacro;
	}
}
