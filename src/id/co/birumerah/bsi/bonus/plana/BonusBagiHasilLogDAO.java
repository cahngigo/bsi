package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BonusBagiHasilLogDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	
	public BonusBagiHasilLogDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.shortFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public int insert(BonusBagiHasilLog dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO bonus_bagi_hasil_log (");
		sb.append("tanggal,");
		sb.append("budget,");
		sb.append("budget_micro,");
		sb.append("total_bagian,");
		sb.append("perbagian,");
		sb.append("budget_macro,");
		sb.append("total_bagian_macro,");
		sb.append("perbagian_macro");
		sb.append(") VALUES (");
		sb.append("'" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("" + dataObject.getBudget() + ",");
		sb.append("" + dataObject.getBudgetMicro() + ",");
		sb.append("" + dataObject.getTotalBagian() + ",");
		sb.append("" + dataObject.getPerbagian() + ",");
		sb.append("" + dataObject.getBudgetMacro() + ",");
		sb.append("" + dataObject.getTotalBagianMacro() + ",");
		sb.append("" + dataObject.getPerbagianMacro());
		sb.append(")");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(BonusBagiHasilLog dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE bonus_bagi_hasil_log SET ");
		sb.append("budget = " + dataObject.getBudget() + ",");
		sb.append("budget_micro = " + dataObject.getBudgetMicro() + ",");
		sb.append("total_bagian = " + dataObject.getTotalBagian() + ",");
		sb.append("perbagian = " + dataObject.getPerbagian() + ",");
		sb.append("budget_macro = " + dataObject.getBudgetMacro() + ",");
		sb.append("total_bagian_macro = " + dataObject.getTotalBagianMacro() + ",");
		sb.append("perbagian_macro = " + dataObject.getPerbagianMacro() + " ");
		sb.append("WHERE tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("Query: " + sb.toString());

		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(BonusBagiHasilLog dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("id,");
		sb.append("budget,");
		sb.append("budget_micro,");
		sb.append("total_bagian,");
		sb.append("perbagian,");
		sb.append("budget_macro,");
		sb.append("total_bagian_macro,");
		sb.append("perbagian_macro ");
		sb.append("FROM bonus_bagi_hasil_log ");
		sb.append("WHERE tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setId(rs.getLong("id"));
				dataObject.setBudget(rs.getDouble("budget"));
				dataObject.setBudgetMicro(rs.getDouble("budget_micro"));
				dataObject.setTotalBagian(rs.getInt("total_bagian"));
				dataObject.setPerbagian(rs.getDouble("perbagian"));
				dataObject.setBudgetMacro(rs.getDouble("budget_macro"));
				dataObject.setTotalBagianMacro(rs.getInt("total_bagian_macro"));
				dataObject.setPerbagianMacro(rs.getDouble("perbagian_macro"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteByTanggal(BonusBagiHasilLog dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM bonus_bagi_hasil_log ");
		sb.append("WHERE tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
