package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class KursAm {

	private int kursId;
	private float pointValue;
	private float businessValue;
	private Date startDate;
	private String status;
	private int persenPotongHarian;
	private int maxPotongBulanan;
	
	public KursAm() {}

	public KursAm(int kursId, float pointValue, float businessValue,
			Date startDate, String status, int persenPotongHarian,
			int maxPotongBulanan) {
		this.kursId = kursId;
		this.pointValue = pointValue;
		this.businessValue = businessValue;
		this.startDate = startDate;
		this.status = status;
		this.persenPotongHarian = persenPotongHarian;
		this.maxPotongBulanan = maxPotongBulanan;
	}

	public int getKursId() {
		return kursId;
	}

	public void setKursId(int kursId) {
		this.kursId = kursId;
	}

	public float getPointValue() {
		return pointValue;
	}

	public void setPointValue(float pointValue) {
		this.pointValue = pointValue;
	}

	public float getBusinessValue() {
		return businessValue;
	}

	public void setBusinessValue(float businessValue) {
		this.businessValue = businessValue;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPersenPotongHarian() {
		return persenPotongHarian;
	}

	public void setPersenPotongHarian(int persenPotongHarian) {
		this.persenPotongHarian = persenPotongHarian;
	}

	public int getMaxPotongBulanan() {
		return maxPotongBulanan;
	}

	public void setMaxPotongBulanan(int maxPotongBulanan) {
		this.maxPotongBulanan = maxPotongBulanan;
	}
}
