package id.co.birumerah.bsi.bonus.plana;

import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;
import java.util.Date;

public class BonusTransferFcd {

	private BonusTransfer bonusTransfer;
	
	public BonusTransferFcd() {}

	public BonusTransferFcd(BonusTransfer bonusTransfer) {
		this.bonusTransfer = bonusTransfer;
	}

	public BonusTransfer getBonusTransfer() {
		return bonusTransfer;
	}

	public void setBonusTransfer(BonusTransfer bonusTransfer) {
		this.bonusTransfer = bonusTransfer;
	}
	
	public BonusTransfer getBonusTransferHarianByMemberDate(String memberId, Date tanggal, DBConnection dbConn) throws Exception {
		try {
			BonusTransfer transfer = new BonusTransfer();
			transfer.setMemberId(memberId);
			transfer.setTanggal(tanggal);
			
			BonusTransferDAO transferDao = new BonusTransferDAO(dbConn);
			boolean found = transferDao.select(transfer);
			
			if (found) {
				return transfer;
			} else {
				System.err.println("Bonus Transfer not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertBonusTransfer(BonusTransfer dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusTransferDAO transferDao = new BonusTransferDAO(dbConn);
			transferDao.insert(dataObject);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateBonusTransfer(BonusTransfer dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusTransferDAO transferDao = new BonusTransferDAO(dbConn);
			if (transferDao.update(dataObject) < 1) {
				throw new Exception("Update Bonus Transfer Harian failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
