package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class LogBonusATempFcd {

	private LogBonusATemp logBonus;
	
	public LogBonusATempFcd() {}

	public LogBonusATempFcd(LogBonusATemp logBonus) {
		this.logBonus = logBonus;
	}

	public LogBonusATemp getLogBonus() {
		return logBonus;
	}

	public void setLogBonus(LogBonusATemp logBonus) {
		this.logBonus = logBonus;
	}
	
	public void insertLog(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusATempDAO logDAO = new LogBonusATempDAO(dbConn);
			logDAO.insert(logBonus);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteLogByDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusATempDAO logDAO = new LogBonusATempDAO(dbConn);
			if (logDAO.deleteLogByDate(tanggal) < 1) {
				throw new Exception("Delete log gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public LogBonusATempSet search(String whereClause, DBConnection dbConn) throws Exception {
		LogBonusATempSet result = null;
		
		try {
			LogBonusATempDAO tempDao = new LogBonusATempDAO(dbConn);
			result = tempDao.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return result;
	}
	
	public LogBonusATempSet searchGrouping(String whereClause, DBConnection dbConn) throws Exception {
		LogBonusATempSet result = null;
		
		try {
			LogBonusATempDAO tempDao = new LogBonusATempDAO(dbConn);
			result = tempDao.selectGroupBy(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return result;
	}
	
	public void updateLogTemp(LogBonusATemp dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusATempDAO logTempDao = new LogBonusATempDAO(dbConn);
			if (logTempDao.update(dataObject) < 1) {
				throw new Exception("Update Log Bonus Temp failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateLogTempGrouping(LogBonusATemp dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			LogBonusATempDAO logTempDao = new LogBonusATempDAO(dbConn);
			if (logTempDao.updateGrouping(dataObject) < 1) {
				throw new Exception("Update Log Bonus Temp failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
