package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusAFcd {

	private BonusA bonusA;
	
	public BonusAFcd() {}

	public BonusAFcd(BonusA bonusA) {
		this.bonusA = bonusA;
	}

	public BonusA getBonusA() {
		return bonusA;
	}

	public void setBonusA(BonusA bonusA) {
		this.bonusA = bonusA;
	}
	
	public BonusASet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		BonusASet baSet = null;
		
		try {
			dbConn = new DBConnection();
			BonusADAO baDAO = new BonusADAO(dbConn);
			baSet = baDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return baSet;
	}
	
	public BonusASet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		BonusASet baSet = null;
		
		try {
//			dbConn = new DBConnection();
			BonusADAO baDAO = new BonusADAO(dbConn);
			baSet = baDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return baSet;
	}
	
	public static BonusA getBonusByMemberAndDate(String memberId, Date tanggal) 
			throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusA bonus = new BonusA();
			bonus.setMemberId(memberId);
			bonus.setTanggal(tanggal);
			
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			boolean found = bonusDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus hari ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static BonusA getBonusByMemberAndDate(String memberId, Date tanggal, DBConnection dbConn) 
			throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			BonusA bonus = new BonusA();
			bonus.setMemberId(memberId);
			bonus.setTanggal(tanggal);
			
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			boolean found = bonusDAO.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus hari ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public void insertBonus() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			bonusDAO.insert(bonusA);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			bonusDAO.insert(bonusA);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void updateBonus(BonusA bonus) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			if (bonusDAO.update(bonus) < 1) {
				throw new Exception("Update bonus member unsuccessful.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateBonus(BonusA bonus, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			if (bonusDAO.update(bonus) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void insertTransfer(String tanggal) throws Exception {
		DBConnection dbConn = new DBConnection();
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			if (bonusDAO.insertBonusTransfer(tanggal) < 1) {
				throw new Exception("Insert ke bonus_transfer gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertTransfer(String tanggal, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = new DBConnection();
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			if (bonusDAO.insertBonusTransfer(tanggal) < 1) {
				throw new Exception("Insert ke bonus_transfer gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public double getTotalMicroPair(String whereClause, DBConnection dbConn) throws Exception {
		double total = 0;
		BonusADAO baDAO = new BonusADAO(dbConn);
		total = baDAO.hitungTotalMicroPair(whereClause);
		
		return total;
	}
	
	public void deleteBonusByDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			if (bonusDAO.deleteByDate(tanggal) < 1) {
				throw new Exception("Delete bonus tanggal tertentu gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteTransferByDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			if (bonusDAO.deleteTransferByDate(tanggal) < 1) {
				throw new Exception("Delete bonus transfer tanggal tertentu gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public static BonusA getTotalBonusByDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			BonusA bonus = new BonusA();
			bonus.setTanggal(tanggal);
			
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			boolean found = bonusDAO.getTotalBonusPerDay(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus hari ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public boolean hasNullMember(Date tanggal, DBConnection dbConn) throws Exception {
		boolean adaNull = false;
		try {
			BonusADAO bonusDAO = new BonusADAO(dbConn);
			adaNull = bonusDAO.anyNullMember(tanggal);
			bonusDAO = null;
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
		return adaNull;
	}
}
