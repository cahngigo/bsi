package id.co.birumerah.bsi.bonus.plana;

import java.util.Date;

public class SettingBonusA {

	private long id;
	private double bonusSponsor;
	private double bonusPasSedang;
	private double bonusPasKecil;
	private int bonusMatchingLevel;
	private float bonusMatchingPct;
	private int flushOutReguler;
	private int flushOutGold;
	private int flushOutPlatinum;
	private Date startDate;
	private String status;
	
	public SettingBonusA() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getBonusSponsor() {
		return bonusSponsor;
	}

	public void setBonusSponsor(double bonusSponsor) {
		this.bonusSponsor = bonusSponsor;
	}

	public double getBonusPasSedang() {
		return bonusPasSedang;
	}

	public void setBonusPasSedang(double bonusPasSedang) {
		this.bonusPasSedang = bonusPasSedang;
	}

	public double getBonusPasKecil() {
		return bonusPasKecil;
	}

	public void setBonusPasKecil(double bonusPasKecil) {
		this.bonusPasKecil = bonusPasKecil;
	}

	public int getBonusMatchingLevel() {
		return bonusMatchingLevel;
	}

	public void setBonusMatchingLevel(int bonusMatchingLevel) {
		this.bonusMatchingLevel = bonusMatchingLevel;
	}

	public float getBonusMatchingPct() {
		return bonusMatchingPct;
	}

	public void setBonusMatchingPct(float bonusMatchingPct) {
		this.bonusMatchingPct = bonusMatchingPct;
	}

	public int getFlushOutReguler() {
		return flushOutReguler;
	}

	public void setFlushOutReguler(int flushOutReguler) {
		this.flushOutReguler = flushOutReguler;
	}

	public int getFlushOutGold() {
		return flushOutGold;
	}

	public void setFlushOutGold(int flushOutGold) {
		this.flushOutGold = flushOutGold;
	}

	public int getFlushOutPlatinum() {
		return flushOutPlatinum;
	}

	public void setFlushOutPlatinum(int flushOutPlatinum) {
		this.flushOutPlatinum = flushOutPlatinum;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
}
