package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBagiHasilFcd {

	private BonusBagiHasil bbHasil;
	
	public BonusBagiHasilFcd() {}

	public BonusBagiHasilFcd(BonusBagiHasil bbHasil) {
		this.bbHasil = bbHasil;
	}

	public BonusBagiHasil getBbHasil() {
		return bbHasil;
	}

	public void setBbHasil(BonusBagiHasil bbHasil) {
		this.bbHasil = bbHasil;
	}
	
	public BonusBagiHasilSet search(String whereClause, DBConnection dbConn) throws Exception {
		BonusBagiHasilSet bbhSet = null;
		
		BonusBagiHasilDAO bbhDAO = new BonusBagiHasilDAO(dbConn);
		bbhSet = bbhDAO.select(whereClause);
		
		return bbhSet;
	}
	
	public static BonusBagiHasil getBonusByMemberAndDate(String memberId, Date tanggal, DBConnection dbConn) 
			throws Exception {
		BonusBagiHasil bonus = new BonusBagiHasil();
		bonus.setMemberId(memberId);
		bonus.setTanggal(tanggal);
		
		BonusBagiHasilDAO bbhDAO = new BonusBagiHasilDAO(dbConn);
		boolean found = bbhDAO.select(bonus);
		
		if (found) {
			return bonus;
		} else {
			System.err.println("Bonus bagi hasil hari ini tidak ditemukan untuk member ybs.");
		}
		
		return null;
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilDAO bbhDAO = new BonusBagiHasilDAO(dbConn);
			bbhDAO.insert(bbHasil);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateBonus(BonusBagiHasil bonus, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilDAO bbhDAO = new BonusBagiHasilDAO(dbConn);
			if (bbhDAO.update(bonus) < 1) {
				throw new Exception("Bonus bagi hasil not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteByDate(BonusBagiHasil dataObject, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusBagiHasilDAO bbhDAO = new BonusBagiHasilDAO(dbConn);
			if (bbhDAO.deleteByTanggal(dataObject) < 1) {
				throw new Exception("Delete bonus bagi hasil failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
