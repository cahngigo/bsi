package id.co.birumerah.bsi.bonus.plana;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.bonus.PoinRecord;
import id.co.birumerah.bsi.bonus.PoinRecordFcd;
import id.co.birumerah.bsi.bonus.SaldoPtkp;
import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.member.BasicUpgrade;
import id.co.birumerah.bsi.member.BasicUpgradeFcd;
import id.co.birumerah.bsi.member.BasicUpgradeSet;
import id.co.birumerah.bsi.member.Member;
import id.co.birumerah.bsi.member.MemberFcd;
import id.co.birumerah.bsi.member.MemberSet;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.bsi.member.MemberTreeSmall;
import id.co.birumerah.bsi.member.MemberTreeSmallHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSmallSet;
import id.co.birumerah.bsi.tree.MemberTreeNode;
import id.co.birumerah.bsi.tree.MemberTreeTree;
import id.co.birumerah.bsi.tree.Node;
import id.co.birumerah.bsi.tree.Tree;
import id.co.birumerah.util.NumberUtil;
import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

/**
 * 
 * @author endi
 *
 */
public class BonusCounter {

//	private MemberTree member;
	private Tree<MemberTree> mTree;
//	private MemberTreeTree mTree;
	private double bonusSponsor;
	private double bonusPasSedang;
	private double bonusPasKecil;
	private int bonusMatchingLevel;
	private float bonusMatchingPct;
	private int flushOutReguler;
	private int flushOutGold;
	private int flushOutPlatinum;
	private Date startDate;
	private String status;
	
	private static final int BONUS_MATCHING_LEVEL = 8;
	private static final double BONUS_SPONSOR_PROMO = 300000;
	
	// tanggal hari berjalan untuk keperluan testing
//	private Date tglTrx;
//	private DateFormat dateFormat;
	private MutableDateTime beginOfDay;
	private MutableDateTime hariHitung;
	
	/**
	 * Constructor
	 * 
	 * @param isEOD apakah dipanggil ketika end of day?
	 */
	public BonusCounter(boolean isEOD) {
		super();
		
//		tglTrx = new Date();
//		dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		hariHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
		hariHitung.addDays(-1);
		try {
			if (isEOD) {
				loadTreeYesterday();
			} else {
				loadTree();
			}
			loadSettingBonusA();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public BonusCounter(boolean isEOD, DBConnection dbConn) {
		super();
		
//		tglTrx = new Date();
//		dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		hariHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
		hariHitung.addDays(-1);
//		System.out.println("*** di constructor BonusCounter");
		try {
			if (isEOD) {
				loadTreeYesterday();
			} else {
				loadTree(dbConn);
			}
			loadSettingBonusA(dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public BonusCounter(Date testDate) {
//		this();
//		this.tglTrx = testDate;
//	}
	public BonusCounter(MutableDateTime testDate) {
		this(false);
		this.beginOfDay = testDate;
		hariHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
		hariHitung.addDays(-1);
	}
	
	public BonusCounter(boolean isEOD, MutableDateTime testDate) {
//		this(isEOD);
		this.beginOfDay = testDate;
		hariHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
		hariHitung.addDays(-1);
		try {
			if (isEOD) {
				loadTreeYesterday();
			} else {
				loadTree();
			}
			loadSettingBonusA();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public BonusCounter(boolean isEOD, MutableDateTime testDate, DBConnection dbConn) {
//		this(isEOD);
		this.beginOfDay = testDate;
		hariHitung = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
		hariHitung.addDays(-1);
		try {
			if (isEOD) {
				loadTreeYesterday(dbConn);
			} else {
				loadTree(dbConn);
			}
			loadSettingBonusA(dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public BonusCounter(MutableDateTime testDate, DBConnection dbConn) {
		this.beginOfDay = testDate;
		try {
			loadTreeLogBulanan(dbConn);
//			loadSettingBonusA();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadTree() throws Exception {
		MemberTreeSet mtSet = MemberTreeFcd.showAllMemberTree();
//		System.out.println("root adalah "+ mtSet.get(0).getMemberId());
		mTree = new Tree<MemberTree>(mtSet.get(0).getMemberId(), mtSet.get(0));
		for (int i=1; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			mTree.addLeaf(member.getUplineId().trim(), MemberTreeFcd.getMemberTreeById(member.getUplineId().trim()), member.getMemberId(), member);
		}
	}
	
	private void loadTree(DBConnection dbConn) throws Exception {
		MemberTreeSet mtSet = MemberTreeFcd.showAllMemberTree(dbConn);
//		System.out.println("root adalah "+ mtSet.get(0).getMemberId());
		mTree = new Tree<MemberTree>(mtSet.get(0).getMemberId(), mtSet.get(0));
		for (int i=1; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			mTree.addLeaf(member.getUplineId().trim(), MemberTreeFcd.getMemberTreeById(member.getUplineId().trim(), dbConn), member.getMemberId(), member);
		}
	}
	
	private void loadTreeLogBulanan(DBConnection dbConn) throws Exception {
		MemberTreeSet mtSet = MemberTreeBulananFcd.showAllMemberTreePerMonth(beginOfDay, dbConn);
		mTree = new Tree<MemberTree>(mtSet.get(0).getMemberId(), mtSet.get(0));
		for (int i=1; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			mTree.addLeaf(member.getUplineId().trim(), MemberTreeBulananFcd.getMemberTreeByIdPerMonth(member.getUplineId().trim(), beginOfDay, dbConn), member.getMemberId(), member);
		}
		System.out.println("Selesai load tree");
	}
	
	private void loadTreeYesterday() throws Exception {
		// TODO kalo production pake yg ini
//		MemberTreeSet mtSet = MemberTreeHarianFcd.showAllMemberTreeYesterday();
		MemberTreeSet mtSet = MemberTreeHarianFcd.showAllMemberTreePerDate(hariHitung);
//		System.out.println("root adalah "+ mtSet.get(0).getMemberId());
		mTree = new Tree<MemberTree>(mtSet.get(0).getMemberId(), mtSet.get(0));
		for (int i=1; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			mTree.addLeaf(member.getUplineId().trim(), MemberTreeFcd.getMemberTreeById(member.getUplineId().trim()), member.getMemberId(), member);
		}
	}
	
	private void loadTreeYesterday(DBConnection dbConn) throws Exception {
		// TODO kalo production pake yg ini
//		MemberTreeSet mtSet = MemberTreeHarianFcd.showAllMemberTreeYesterday();
		MemberTreeSet mtSet = MemberTreeHarianFcd.showAllMemberTreePerDate(hariHitung, dbConn);
//		System.out.println("root adalah "+ mtSet.get(0).getMemberId());
		mTree = new Tree<MemberTree>(mtSet.get(0).getMemberId(), mtSet.get(0));
		for (int i=1; i<mtSet.length(); i++) {
			MemberTree member = mtSet.get(i);
			mTree.addLeaf(member.getUplineId().trim(), MemberTreeFcd.getMemberTreeById(member.getUplineId().trim(), dbConn), member.getMemberId(), member);
		}
	}
	
	private void loadSettingBonusA() throws Exception {
//		String whereClause = "status = 'Berlaku' AND start_date < '" 
//			+ dateFormat.format(tglTrx) + "' ORDER BY start_date DESC";
		String whereClause = "status = 'Berlaku' AND start_date < '" 
			+ beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC";
		SettingBonusAFcd sbFcd = new SettingBonusAFcd();
		SettingBonusASet sbSet = sbFcd.search(whereClause);
		if (sbSet.length() < 1)
			throw new Exception("Belum ada setting bonus yang berlaku.");
		SettingBonusA settings = sbSet.get(0);
		bonusSponsor = settings.getBonusSponsor();
		bonusPasSedang = settings.getBonusPasSedang();
		bonusPasKecil = settings.getBonusPasKecil();
		bonusMatchingLevel = settings.getBonusMatchingLevel();
		bonusMatchingPct = settings.getBonusMatchingPct();
		flushOutReguler = settings.getFlushOutReguler();
		flushOutPlatinum = settings.getFlushOutPlatinum();
		startDate = settings.getStartDate();
		status = settings.getStatus();
	}
	
	private void loadSettingBonusA(DBConnection dbConn) throws Exception {
		String whereClause = "status = 'Berlaku' AND start_date < '" 
			+ beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC";
		SettingBonusAFcd sbFcd = new SettingBonusAFcd();
		SettingBonusASet sbSet = sbFcd.search(whereClause, dbConn);
		if (sbSet.length() < 1)
			throw new Exception("Belum ada setting bonus yang berlaku.");
		SettingBonusA settings = sbSet.get(0);
		bonusSponsor = settings.getBonusSponsor();
		bonusPasSedang = settings.getBonusPasSedang();
		bonusPasKecil = settings.getBonusPasKecil();
		bonusMatchingLevel = settings.getBonusMatchingLevel();
		bonusMatchingPct = settings.getBonusMatchingPct();
		flushOutReguler = settings.getFlushOutReguler();
		flushOutGold = settings.getFlushOutGold();
		flushOutPlatinum = settings.getFlushOutPlatinum();
		startDate = settings.getStartDate();
		status = settings.getStatus();
	}
	
	/**
	 * Fungsi untuk melakukan sorting terhadap downline di bawah langsung
	 * si kepala
	 * 
	 * @param kepala member yang akan diketahui status kaki-kakinya
	 * @return kumpulan downline langsung yang sudah urut dari besar
	 * ke kecil
	 */
	public MemberTree[] sortDownliners(MemberTree kepala) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(kepala.getMemberId());
		int n = keturunan.size();
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		if (n > 0) {
			boolean doMore = true;
			while (doMore) {
				n--;
				doMore = false;
				for (int i=0; i<n; i++) {
					if (getSuccessorsSize(kakikaki[i].getMemberId()) < getSuccessorsSize(kakikaki[i+1].getMemberId())) {
						// exchange elements
						MemberTree temp = kakikaki[i];
						kakikaki[i] = kakikaki[i+1];
						kakikaki[i+1] = temp;
						doMore = true;
					} else if (getSuccessorsSize(kakikaki[i].getMemberId()) == getSuccessorsSize(kakikaki[i+1].getMemberId())) {
						if (kakikaki[i].getSortNumber() > kakikaki[i+1].getSortNumber()) {
							MemberTree temp = kakikaki[i];
							kakikaki[i] = kakikaki[i+1];
							kakikaki[i+1] = temp;
							doMore = true;
						}
					}
				}
			}
		}
		return kakikaki;
	}
	
	public MemberTree[] sortDownlinersNonbasic(MemberTree kepala) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(kepala.getMemberId());
		int n = keturunan.size();
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		if (n > 0) {
			boolean doMore = true;
			while (doMore) {
				n--;
				doMore = false;
				for (int i=0; i<n; i++) {
					if (getSuccessorsSizeNonbasic(kakikaki[i].getMemberId()) < getSuccessorsSizeNonbasic(kakikaki[i+1].getMemberId())) {
						// exchange elements
						MemberTree temp = kakikaki[i];
						kakikaki[i] = kakikaki[i+1];
						kakikaki[i+1] = temp;
						doMore = true;
					} else if (getSuccessorsSizeNonbasic(kakikaki[i].getMemberId()) == getSuccessorsSizeNonbasic(kakikaki[i+1].getMemberId())) {
						if (kakikaki[i].getSortNumber() > kakikaki[i+1].getSortNumber()) {
							MemberTree temp = kakikaki[i];
							kakikaki[i] = kakikaki[i+1];
							kakikaki[i+1] = temp;
							doMore = true;
						}
					}
				}
			}
		}
		return kakikaki;
	}
	
	public MemberTree[] sortDownliners(MemberTree kepala, String memberId, long sortNumber) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(kepala.getMemberId());
		int n = keturunan.size();
//		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray();
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		if (n > 0) {
			boolean doMore = true;
			while (doMore) {
				n--;
				doMore = false;
				for (int i=0; i<n; i++) {
					if (getSuccessorsSize(kakikaki[i].getMemberId()) < getSuccessorsSize(kakikaki[i+1].getMemberId())) {
						// exchange elements
						MemberTree temp = kakikaki[i];
						kakikaki[i] = kakikaki[i+1];
						kakikaki[i+1] = temp;
						doMore = true;
					} else if (getSuccessorsSize(kakikaki[i].getMemberId()) == getSuccessorsSize(kakikaki[i+1].getMemberId())) {
//						if (kakikaki[i].getRegDate().after(kakikaki[i+1].getRegDate())) {
//							MemberTree temp = kakikaki[i];
//							kakikaki[i] = kakikaki[i+1];
//							kakikaki[i+1] = temp;
//							doMore = true;
//						} else 
//						if (kakikaki[i].getSortNumber() > kakikaki[i+1].getSortNumber()) {
						if (memberId.equals(kakikaki[i].getMemberId()) && sortNumber > kakikaki[i+1].getSortNumber()) {
							MemberTree temp = kakikaki[i];
							kakikaki[i] = kakikaki[i+1];
							kakikaki[i+1] = temp;
							doMore = true;
						}
					}
				}
			}
			System.out.print(""+kakikaki[0].getMemberId()+"="+getSuccessorsSize(kakikaki[0].getMemberId())+" kaki;");
			if (n>1) System.out.print(""+kakikaki[1].getMemberId()+"="+getSuccessorsSize(kakikaki[1].getMemberId())+" kaki;");
			if (n>2) System.out.print(""+kakikaki[2].getMemberId()+"="+getSuccessorsSize(kakikaki[2].getMemberId())+" kaki;");
			System.out.println("");
		}
		return kakikaki;
	}
	
	public MemberTree[] sortDownlinersPromo(MemberTree kepala) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(kepala.getMemberId());
		int n = keturunan.size();
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		if (n > 0) {
			boolean doMore = true;
			while (doMore) {
				n--;
				doMore = false;
				for (int i=0; i<n; i++) {
//					if (getSuccessorsSize(kakikaki[i].getMemberId()) < getSuccessorsSize(kakikaki[i+1].getMemberId())) {
					if (getSuccessorsPromoSize(kakikaki[i].getMemberId()) < getSuccessorsPromoSize(kakikaki[i+1].getMemberId())) {
						// exchange elements
						MemberTree temp = kakikaki[i];
						kakikaki[i] = kakikaki[i+1];
						kakikaki[i+1] = temp;
						doMore = true;
					} else if (getSuccessorsPromoSize(kakikaki[i].getMemberId()) == getSuccessorsPromoSize(kakikaki[i+1].getMemberId())) {
//						if (kakikaki[i].getRegDate().after(kakikaki[i+1].getRegDate())) {
//							MemberTree temp = kakikaki[i];
//							kakikaki[i] = kakikaki[i+1];
//							kakikaki[i+1] = temp;
//							doMore = true;
//						} else 
						if (kakikaki[i].getSortNumber() > kakikaki[i+1].getSortNumber()) {
							MemberTree temp = kakikaki[i];
							kakikaki[i] = kakikaki[i+1];
							kakikaki[i+1] = temp;
							doMore = true;
						}
					}
				}
			}
		}
		return kakikaki;
	}
	
	public MemberTree[] sortDownliners(MemberTree kepala, DBConnection dbConn) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(kepala.getMemberId());
		int n = keturunan.size();
//		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray();
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		if (n > 0) {
			boolean doMore = true;
			while (doMore) {
				n--;
				doMore = false;
				for (int i=0; i<n; i++) {
					if (getSuccessorsSize(kakikaki[i].getMemberId()) < getSuccessorsSize(kakikaki[i+1].getMemberId())) {
						// exchange elements
						MemberTree temp = kakikaki[i];
						kakikaki[i] = kakikaki[i+1];
						kakikaki[i+1] = temp;
						doMore = true;
					} else if (getSuccessorsSize(kakikaki[i].getMemberId()) == getSuccessorsSize(kakikaki[i+1].getMemberId())) {
						if (kakikaki[i].getRegDate().after(kakikaki[i+1].getRegDate())) {
							MemberTree temp = kakikaki[i];
							kakikaki[i] = kakikaki[i+1];
							kakikaki[i+1] = temp;
							doMore = true;
						} else if (kakikaki[i].getSortNumber() > kakikaki[i+1].getSortNumber()) {
							MemberTree temp = kakikaki[i];
							kakikaki[i] = kakikaki[i+1];
							kakikaki[i+1] = temp;
							doMore = true;
						}
					}
				}
			}
		}
		return kakikaki;
	}
	
	public int getSuccessorsSize(String key) {
		Collection<MemberTree> successors = mTree.getSuccessors(key);
		int count = successors.size();
		for (MemberTree node : successors) {
			count += getSuccessorsSize(node.getMemberId());
		}
		return count;
	}
	
	public int getSuccessorsSizeNonbasic(String key) {
		Collection<MemberTree> successors = mTree.getSuccessors(key);
		int count = 0;
		for (MemberTree node : successors) {
			if (node.getIsBasicNew() == 0) {
				count++;
			}
		}
		for (MemberTree node : successors) {
			count += getSuccessorsSizeNonbasic(node.getMemberId());
		}
		return count;
	}
	
	public int getSuccessorsPromoSize(String key) {
		Collection<MemberTree> successors = mTree.getSuccessors(key);
//		int count = successors.size();
		int count = 0;
		for(MemberTree node : successors) {
			if (node.getIsPromoxenia()==1) {
				count++;
			}
		}
		for (MemberTree node : successors) {
			count += getSuccessorsPromoSize(node.getMemberId());
		}
		return count;
	}
	
	private float countPvGrup(MemberTree memberTree, float pvGrup, DBConnection dbConn) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(memberTree.getMemberId());
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		for (int i=0; i<keturunan.size(); i++) {
			MemberTree currFoot = kakikaki[i];
			pvGrup = currFoot.getPvpribadi() + countPvGrup(currFoot, pvGrup, dbConn);
		}
		return pvGrup;
	}
	
	private int countDdrGrup(MemberTree memberTree, int ddrGrup, DBConnection dbConn) {
		Collection<MemberTree> keturunan = mTree.getSuccessors(memberTree.getMemberId());
		MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
		for (int i=0; i<keturunan.size(); i++) {
			MemberTree currFoot = kakikaki[i];
			ddrGrup = currFoot.getSaldoDdr() + countDdrGrup(currFoot, ddrGrup, dbConn);
		}
		return ddrGrup;
	}
	
	public Tree<MemberTree> getTree() {
		return mTree;
	}
	
	public String[] addMember(String uplineId, MemberTree upline, String memberId, MemberTree regMember) {
		String[] hasil = mTree.addLeaf(uplineId, upline, memberId, regMember);
//		if (hasil[0].equals("true")) {
//			giveBonusPasangan(regMember, uplineId);
//		}
		return hasil;
	}
	
	public String[] addMember(String uplineId, MemberTree upline, String memberId, MemberTree regMember, DBConnection dbConn) {
		String[] hasil = mTree.addLeaf(uplineId, upline, memberId, regMember);
		try {
			MemberTreeSet mtSet = MemberTreeFcd.getMemberTreeByUplineId(memberId, dbConn);
			if (mtSet != null && mtSet.length()>0) {
				MemberTree bawahan = mtSet.get(0);
				System.out.println("member selipan, punya downline " + bawahan.getMemberId());
				mTree.addLeaf(memberId, regMember, bawahan.getMemberId(), bawahan);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	/**
	 * Fungsi yang akan dipanggil dari aplikasi eksternal (web page) untuk
	 * melakukan perhitungan bonus pasangan kepada member-member yang berhak
	 * karena adanya member baru yang mendaftar
	 * 
	 * @param newMemberId id member yang baru mendaftar
	 * @param uplineId id member yang menjadi upline member baru dalam pohon
	 */
	public void giveBonusPasangan(String newMemberId, String uplineId) {
//		System.out.println("*** di giveBonusPasangan()");
		try {
			MemberTree newMember = MemberTreeFcd.getMemberTreeById(newMemberId);
			if (newMember == null)
				System.out.println("newMember dengan id "+newMemberId+" = NULL");
//			giveBonusPasangan(newMember, uplineId);
			giveBonusPasangan(newMember, uplineId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBonusPasangan(String newMemberId, String uplineId, DBConnection dbConn) {
//		System.out.println("*** di giveBonusPasangan()");
		try {
			MemberTree newMember = MemberTreeFcd.getMemberTreeById(newMemberId, dbConn);
			if (newMember == null)
				System.out.println("newMember dengan id "+newMemberId+" = NULL");
//			giveBonusPasangan(newMember, uplineId);
//			giveBonusPasangan(newMember, uplineId, dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fungsi recursive untuk menghitung bonus pasangan dari member yang baru
	 * didaftarkan. Fungsi ini akan terus melacak ke jaringan di atas member
	 * baru tersebut dan menambahkan bonus yang berhak didapat oleh jaringan
	 * di atasnya (para upline). Fungsi ini akan berhenti jika menemui kaki
	 * kecil atau pucuk pohon member, yaitu perusahaan.
	 * 
	 * @param newMember posisi pengecekan dimulai dari member baru ke atas
	 * @param uplineId id member dari upline
	 */
	public void giveBonusPasangan(MemberTree newMember, String uplineId) {
		try {
			System.out.println("Cari member dengan id "+uplineId);
			MemberTree currMember = MemberTreeFcd.getMemberTreeById(uplineId);
//			MemberTree currMember = MemberTreeFcd.getMemberTreeById(uplineId, dbConn);
			if (currMember == null)
				System.out.println("currMember NULL");
//			MemberTree[] downlines = sortDownliners(currMember);
//			System.out.println("banyaknya downline dari "+currMember.getMemberId()+" = "+downlines.length);
//			if (newMember.getMemberId().equals(downlines[0].getMemberId())) {
//				if (currMember.getUplineId() != null)
//					giveBonusPasangan(currMember, currMember.getUplineId());
////					giveBonusPasangan(currMember, currMember.getUplineId(), dbConn);
//			} else {
				BonusA bonus = BonusAFcd.getBonusByMemberAndDate(uplineId, beginOfDay.toDate());
//				BonusA bonus = BonusAFcd.getBonusByMemberAndDate(uplineId, beginOfDay.toDate(), dbConn);
//				if (newMember.getMemberId().equals(downlines[1].getMemberId())) {
//					if (bonus == null) {
//						bonus = new BonusA();
//						bonus.setMemberId(uplineId);
//						bonus.setTanggal(beginOfDay.toDate());
//						bonus.setBonusPasanganSedang(bonusPasSedang);
//						bonus.setTotalPasanganSedang(1);
//						BonusAFcd bonusFcd = new BonusAFcd(bonus);
//						bonusFcd.insertBonus();
////						bonusFcd.insertBonus(dbConn);
//					} else {
//						int totalSedang = bonus.getTotalPasanganSedang();
//						Member cekMember = MemberFcd.getMemberById(uplineId);
////						Member cekMember = MemberFcd.getMemberById(uplineId, dbConn);
//						int maxFlushOut;
//						if (cekMember.getJenisMember().equalsIgnoreCase("Platinum")) {
//							maxFlushOut = flushOutPlatinum;
//						} else {
//							maxFlushOut = flushOutReguler;
//						}
//						if (totalSedang < maxFlushOut) {
//							double bonusPas = bonus.getBonusPasanganSedang();
//							bonusPas += bonusPasSedang;
//							totalSedang++;
//							bonus.setBonusPasanganSedang(bonusPas);
//							bonus.setTotalPasanganSedang(totalSedang);
//							BonusAFcd bonusFcd = new BonusAFcd();
//							bonusFcd.updateBonus(bonus);
////							bonusFcd.updateBonus(bonus, dbConn);
//						} else {
//							int totalFlush = bonus.getTotalFlush();
//							totalFlush++;
//							bonus.setTotalFlush(totalFlush);
//							BonusAFcd bonusFcd = new BonusAFcd();
//							bonusFcd.updateBonus(bonus);
////							bonusFcd.updateBonus(bonus, dbConn);
//						}
//					}
//					if (currMember.getUplineId() != null)
//						giveBonusPasangan(currMember, currMember.getUplineId());
////						giveBonusPasangan(currMember, currMember.getUplineId(), dbConn);
//				} else {
//					if (bonus == null) {
//						bonus = new BonusA();
//						bonus.setMemberId(uplineId);
//						bonus.setTanggal(beginOfDay.toDate());
//						bonus.setBonusPasanganKecil(bonusPasKecil);
//						BonusAFcd bonusFcd = new BonusAFcd(bonus);
//						bonusFcd.insertBonus();
////						bonusFcd.insertBonus(dbConn);
//					} else {
//						double bonusPas = bonus.getBonusPasanganKecil();
//						bonusPas += bonusPasKecil;
//						bonus.setBonusPasanganKecil(bonusPas);
//						BonusAFcd bonusFcd = new BonusAFcd();
//						bonusFcd.updateBonus(bonus);
////						bonusFcd.updateBonus(bonus, dbConn);
//					}
//				}
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void giveBonusPasangan(MemberTree newMember, String uplineId, DBConnection dbConn, long sortNumber, Date tglDaftar, boolean isBasicNewMember) {
		try {
			System.out.println("Cari member dengan id "+uplineId);
			MemberTree currMember = MemberTreeFcd.getMemberTreeById(uplineId, dbConn);
			if (currMember == null)
				System.out.println("currMember NULL");
			String newMemberId = newMember.getMemberId();
			MemberTree[] downlines = sortDownliners(currMember, newMemberId, sortNumber);
			if (downlines.length > 0) System.out.print("KB="+downlines[0].getMemberId());
			if (downlines.length > 1) System.out.print("; KS="+downlines[1].getMemberId());
			if (downlines.length > 2) System.out.print("; KK="+downlines[2].getMemberId());
			System.out.println();
			if (newMember.getMemberId().equals(downlines[0].getMemberId())) {
				if (currMember.getUplineId() != null && !currMember.getUplineId().equals(""))
					giveBonusPasangan(currMember, currMember.getUplineId(), dbConn, sortNumber, tglDaftar, isBasicNewMember);
			} else {
				if (currMember.getIsBasic() == 0 && currMember.getIsBasicNew() == 0 && !isBasicNewMember) {
					BonusA bonus = BonusAFcd.getBonusByMemberAndDate(uplineId, tglDaftar, dbConn);
					if (newMember.getMemberId().equals(downlines[1].getMemberId())) {
						if (bonus == null) {
							bonus = new BonusA();
							bonus.setMemberId(uplineId);
							bonus.setTanggal(tglDaftar);
							bonus.setBonusPasanganSedang(bonusPasSedang);
							bonus.setTotalPasanganSedang(1);
							BonusAFcd bonusFcd = new BonusAFcd(bonus);
							bonusFcd.insertBonus(dbConn);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(uplineId);
							MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
							logBonus.setFromMember(sumber.getMemberId());
							logBonus.setTglBonus(tglDaftar);
							logBonus.setAmount(bonusPasSedang);
							logBonus.setJenisBonus("pas_sedang");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						} else {
							int totalSedang = bonus.getTotalPasanganSedang();
	//						Member cekMember = MemberFcd.getMemberById(uplineId);
							Member cekMember = MemberFcd.getMemberById(uplineId, dbConn);
							int maxFlushOut;
							if (cekMember.getJenisMember().equalsIgnoreCase("Platinum")) {
								maxFlushOut = flushOutPlatinum;
							} else if (cekMember.getJenisMember().equalsIgnoreCase("Gold")) {
								maxFlushOut = flushOutGold;
							} else {
								maxFlushOut = flushOutReguler;
							}
							if (totalSedang < maxFlushOut) {
								double bonusPas = bonus.getBonusPasanganSedang();
								bonusPas += bonusPasSedang;
								totalSedang++;
								bonus.setBonusPasanganSedang(bonusPas);
								bonus.setTotalPasanganSedang(totalSedang);
								BonusAFcd bonusFcd = new BonusAFcd();
								bonusFcd.updateBonus(bonus, dbConn);
								LogBonusA logBonus = new LogBonusA();
								logBonus.setMemberId(uplineId);
								MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
								logBonus.setFromMember(sumber.getMemberId());
								logBonus.setTglBonus(tglDaftar);
								logBonus.setAmount(bonusPasSedang);
								logBonus.setJenisBonus("pas_sedang");
								LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
								lbaFcd.insertLog(dbConn);
							} else {
								int totalFlush = bonus.getTotalFlush();
								totalFlush++;
								bonus.setTotalFlush(totalFlush);
								BonusAFcd bonusFcd = new BonusAFcd();
								bonusFcd.updateBonus(bonus, dbConn);
							}
						}
						if (currMember.getUplineId() != null && !currMember.getUplineId().equals(""))
							giveBonusPasangan(currMember, currMember.getUplineId(), dbConn, sortNumber, tglDaftar, isBasicNewMember);
					} else {
						if (bonus == null) {
							bonus = new BonusA();
							bonus.setMemberId(uplineId);
							bonus.setTanggal(tglDaftar);
							bonus.setBonusPasanganKecil(bonusPasKecil);
							BonusAFcd bonusFcd = new BonusAFcd(bonus);
							bonusFcd.insertBonus(dbConn);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(uplineId);
							MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
							logBonus.setFromMember(sumber.getMemberId());
							logBonus.setTglBonus(tglDaftar);
							logBonus.setAmount(bonusPasKecil);
							logBonus.setJenisBonus("pas_kecil");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						} else {
							double bonusPas = bonus.getBonusPasanganKecil();
							bonusPas += bonusPasKecil;
							bonus.setBonusPasanganKecil(bonusPas);
							BonusAFcd bonusFcd = new BonusAFcd();
	//						bonusFcd.updateBonus(bonus);
							bonusFcd.updateBonus(bonus, dbConn);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(uplineId);
							MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
							logBonus.setFromMember(sumber.getMemberId());
	//						logBonus.setTglBonus(beginOfDay.toDate());
							logBonus.setTglBonus(tglDaftar);
							logBonus.setAmount(bonusPasKecil);
							logBonus.setJenisBonus("pas_kecil");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						}
					}
				} else {
					BonusATemp bonus = BonusATempFcd.getBonusByMemberAndDate(uplineId, tglDaftar, dbConn);
					if (newMember.getMemberId().equals(downlines[1].getMemberId())) {
						if (currMember.getIsBasic() > -1 || currMember.getIsBasicNew() > -1) {
							if (bonus == null) {
								bonus = new BonusATemp();
								bonus.setMemberId(uplineId);
								bonus.setTanggal(tglDaftar);
								bonus.setBonusPasanganSedang(bonusPasSedang);
								bonus.setTotalPasanganSedang(1);
								BonusATempFcd bonusFcd = new BonusATempFcd(bonus);
								bonusFcd.insertBonus(dbConn);
								LogBonusATemp logBonus = new LogBonusATemp();
								logBonus.setMemberId(uplineId);
								MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
								logBonus.setFromMember(sumber.getMemberId());
								logBonus.setTglBonus(tglDaftar);
								logBonus.setAmount(bonusPasSedang);
								logBonus.setJenisBonus("pas_sedang");
								LogBonusATempFcd lbaFcd = new LogBonusATempFcd(logBonus);
								lbaFcd.insertLog(dbConn);
							} else {
								int totalSedang = bonus.getTotalPasanganSedang();
								Member cekMember = MemberFcd.getMemberById(uplineId, dbConn);
								int maxFlushOut;
								if (cekMember.getJenisMember().equalsIgnoreCase("Platinum")) {
									maxFlushOut = flushOutPlatinum;
								} else if (cekMember.getJenisMember().equalsIgnoreCase("Gold")) {
									maxFlushOut = flushOutGold;
								} else {
									maxFlushOut = flushOutReguler;
								}
								if (totalSedang < maxFlushOut) {
									double bonusPas = bonus.getBonusPasanganSedang();
									bonusPas += bonusPasSedang;
									totalSedang++;
									bonus.setBonusPasanganSedang(bonusPas);
									bonus.setTotalPasanganSedang(totalSedang);
									BonusATempFcd bonusFcd = new BonusATempFcd();
									bonusFcd.updateBonus(bonus, dbConn);
									LogBonusATemp logBonus = new LogBonusATemp();
									logBonus.setMemberId(uplineId);
									MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
									logBonus.setFromMember(sumber.getMemberId());
									logBonus.setTglBonus(tglDaftar);
									logBonus.setAmount(bonusPasSedang);
									logBonus.setJenisBonus("pas_sedang");
									LogBonusATempFcd lbaFcd = new LogBonusATempFcd(logBonus);
									lbaFcd.insertLog(dbConn);
								} else {
									int totalFlush = bonus.getTotalFlush();
									totalFlush++;
									bonus.setTotalFlush(totalFlush);
									BonusATempFcd bonusFcd = new BonusATempFcd();
									bonusFcd.updateBonus(bonus, dbConn);
								}
							}
						}
						if (currMember.getUplineId() != null && !currMember.getUplineId().equals(""))
							giveBonusPasangan(currMember, currMember.getUplineId(), dbConn, sortNumber, tglDaftar, isBasicNewMember);
					} else {
						if (currMember.getIsBasic() > -1 || currMember.getIsBasicNew() > -1) {
							if (bonus == null) {
								bonus = new BonusATemp();
								bonus.setMemberId(uplineId);
								bonus.setTanggal(tglDaftar);
								bonus.setBonusPasanganKecil(bonusPasKecil);
								BonusATempFcd bonusFcd = new BonusATempFcd(bonus);
								bonusFcd.insertBonus(dbConn);
								LogBonusATemp logBonus = new LogBonusATemp();
								logBonus.setMemberId(uplineId);
								MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
								logBonus.setFromMember(sumber.getMemberId());
								logBonus.setTglBonus(tglDaftar);
								logBonus.setAmount(bonusPasKecil);
								logBonus.setJenisBonus("pas_kecil");
								LogBonusATempFcd lbaFcd = new LogBonusATempFcd(logBonus);
								lbaFcd.insertLog(dbConn);
							} else {
								double bonusPas = bonus.getBonusPasanganKecil();
								bonusPas += bonusPasKecil;
								bonus.setBonusPasanganKecil(bonusPas);
								BonusATempFcd bonusFcd = new BonusATempFcd();
								bonusFcd.updateBonus(bonus, dbConn);
								LogBonusATemp logBonus = new LogBonusATemp();
								logBonus.setMemberId(uplineId);
								MemberTree sumber = MemberTreeFcd.getMemberTreeBySortNumber(sortNumber, dbConn);
								logBonus.setFromMember(sumber.getMemberId());
								logBonus.setTglBonus(tglDaftar);
								logBonus.setAmount(bonusPasKecil);
								logBonus.setJenisBonus("pas_kecil");
								LogBonusATempFcd lbaFcd = new LogBonusATempFcd(logBonus);
								lbaFcd.insertLog(dbConn);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Fungsi untuk menghitung bonus sponsor yang berhak didapat oleh
	 * setiap member yang mensponsori member baru setiap harinya
	 */
	public void giveBonusSponsor() {
		DBConnection dbConn = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT sponsor, COUNT(sponsor) jumlah ");
		sb.append("FROM member WHERE sponsor IS NOT NULL ");
		sb.append("AND reg_date LIKE '" + hariHitung.toString("yyyy-MM-dd") + "%' ");
		sb.append("GROUP BY sponsor");
		
		SQLAccess sqlAccess = null;
		try {
			dbConn = new DBConnection();
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			while (rs.next()) {
				String sponsor = rs.getString("sponsor");
				int jumlah = rs.getInt("jumlah");
				BonusA bonus = BonusAFcd.getBonusByMemberAndDate(sponsor, hariHitung.toDate());
				if (bonus == null) {
					bonus = new BonusA();
					bonus.setMemberId(sponsor);
					bonus.setTanggal(hariHitung.toDate());
					bonus.setBonusSponsor(jumlah * bonusSponsor);
					BonusAFcd bonusFcd = new BonusAFcd(bonus);
					bonusFcd.insertBonus();
				} else {
					bonus.setBonusSponsor(jumlah * bonusSponsor);
					BonusAFcd bonusFcd = new BonusAFcd();
					bonusFcd.updateBonus(bonus);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
	}
	
	public void giveBonusSponsor(DBConnection dbConn) {
//		DBConnection dbConn = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT sponsor, COUNT(sponsor) jumlah ");
		sb.append("FROM member WHERE sponsor IS NOT NULL ");
		sb.append("AND reg_date LIKE '" + hariHitung.toString("yyyy-MM-dd") + "%' ");
		sb.append("GROUP BY sponsor");
		
		SQLAccess sqlAccess = null;
		try {
//			dbConn = new DBConnection();
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			while (rs.next()) {
				String sponsor = rs.getString("sponsor");
				int jumlah = rs.getInt("jumlah");
//				BonusA bonus = BonusAFcd.getBonusByMemberAndDate(sponsor, hariHitung.toDate());
				BonusA bonus = BonusAFcd.getBonusByMemberAndDate(sponsor, hariHitung.toDate(), dbConn);
				if (bonus == null) {
					bonus = new BonusA();
					bonus.setMemberId(sponsor);
					bonus.setTanggal(hariHitung.toDate());
					bonus.setBonusSponsor(jumlah * bonusSponsor);
					BonusAFcd bonusFcd = new BonusAFcd(bonus);
//					bonusFcd.insertBonus();
					bonusFcd.insertBonus(dbConn);
				} else {
					bonus.setBonusSponsor(jumlah * bonusSponsor);
					BonusAFcd bonusFcd = new BonusAFcd();
//					bonusFcd.updateBonus(bonus);
					bonusFcd.updateBonus(bonus, dbConn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
	}
	
	/**
	 * Fungsi recursive yang menghitung bonus matching yang berhak didapatkan
	 * oleh satu member
	 * 
	 * @param headId member id yang ingin dihitung bonus matchingnya
	 * @param memberTree pohon member yang ingin dihitung bonus matchingnya 
	 * @param level level bonus matching
	 */
	private void hitungBonusMatching(String headId, MemberTree memberTree, int level) {
		System.out.println("level = " + level);
		if (level == 1) {
			String whereClause = "sponsor = '" + memberTree.getMemberId() + "' ORDER BY reg_date";
			System.out.println("whereClause= " + whereClause);
			MemberFcd mFcd = new MemberFcd();
			MemberSet mSet = null;
			try {
				mSet = mFcd.search(whereClause);
				System.out.println("Banyaknya member yang disponsori oleh " + memberTree.getMemberId() + " = " + mSet.length());
				for (int j=0; j<mSet.length(); j++) {
					Member member = mSet.get(j);
					System.out.println("member_id="+ member.getMemberId() + "; tanggal="+ hariHitung.toString("yyyy-MM-dd"));
					MemberTree currFoot = MemberTreeHarianFcd.getMemberTreeByIdAndDate(member.getMemberId(), hariHitung.toDate());
					BonusA childBonus = BonusAFcd.getBonusByMemberAndDate(member.getMemberId(), hariHitung.toDate());
					double childMatchBonus = 0;
					if ((childBonus != null) && ((childBonus.getBonusPasanganSedang() > 0) || (childBonus.getBonusPasanganKecil() > 0))) {
//						childMatchBonus = (childBonus.getBonusPasanganSedang() + childBonus.getBonusPasanganKecil()) * bonusMatchingPct / 100;
						childMatchBonus = childBonus.getBonusPasanganSedang() * bonusMatchingLevel / 100;
						childMatchBonus = NumberUtil.roundToDecimals(childMatchBonus, 0);
						BonusA headBonus = BonusAFcd.getBonusByMemberAndDate(headId, hariHitung.toDate());
						if (headBonus == null) {
							headBonus = new BonusA();
							headBonus.setMemberId(headId);
							headBonus.setTanggal(hariHitung.toDate());
							headBonus.setBonusMatching(childMatchBonus);
							BonusAFcd bonusFcd = new BonusAFcd(headBonus);
							bonusFcd.insertBonus();
						} else {
							double headBonusMatch = headBonus.getBonusMatching();
							headBonusMatch += childMatchBonus;
							headBonus.setBonusMatching(headBonusMatch);
							BonusAFcd bonusFcd = new BonusAFcd();
							bonusFcd.updateBonus(headBonus);
						}
					}
					hitungBonusMatching(headId, currFoot, level + 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Collection<MemberTree> keturunan = mTree.getSuccessors(memberTree.getMemberId());
			MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
			for (int i=0; i<keturunan.size(); i++) {
				MemberTree currFoot = kakikaki[i];
				if (level <= BONUS_MATCHING_LEVEL ) {
					try {
						BonusA childBonus = BonusAFcd.getBonusByMemberAndDate(currFoot.getMemberId(), hariHitung.toDate());
						double childMatchBonus = 0;
						if ((childBonus != null) && ((childBonus.getBonusPasanganSedang() > 0) || (childBonus.getBonusPasanganKecil() > 0))) {
//							childMatchBonus = (childBonus.getBonusPasanganSedang() + childBonus.getBonusPasanganKecil()) * bonusMatchingPct / 100;
							childMatchBonus = childBonus.getBonusPasanganSedang() * bonusMatchingPct / 100;
							childMatchBonus = NumberUtil.roundToDecimals(childMatchBonus, 0);
							BonusA headBonus = BonusAFcd.getBonusByMemberAndDate(headId, hariHitung.toDate());
							if (headBonus == null) {
								headBonus = new BonusA();
								headBonus.setMemberId(headId);
								headBonus.setTanggal(hariHitung.toDate());
								headBonus.setBonusMatching(childMatchBonus);
								BonusAFcd bonusFcd = new BonusAFcd(headBonus);
								bonusFcd.insertBonus();
							} else {
								double headBonusMatch = headBonus.getBonusMatching();
								headBonusMatch += childMatchBonus;
								headBonus.setBonusMatching(headBonusMatch);
								BonusAFcd bonusFcd = new BonusAFcd();
								bonusFcd.updateBonus(headBonus);
							}
						}					
					} catch (Exception e) {
						e.printStackTrace();
					}
					hitungBonusMatching(headId, currFoot, level + 1);
				}
			}
		}
	}
	
	private void hitungBonusMatching(String headId, MemberTree memberTree, int level, DBConnection dbConn) {
		System.out.println("level = " + level);
		if (level == 1) {
//			String whereClause = "sponsor = '" + headId + "' ORDER BY reg_date";
			String whereClause = "sponsor = '" + memberTree.getMemberId() + "' ORDER BY reg_date";
			System.out.println("whereClause= " + whereClause);
			MemberFcd mFcd = new MemberFcd();
			MemberSet mSet = null;
			try {
//				mSet = mFcd.search(whereClause);
				mSet = mFcd.search(whereClause, dbConn);
				System.out.println("Banyaknya member yang disponsori oleh " + memberTree.getMemberId() + " = " + mSet.length());
				for (int j=0; j<mSet.length(); j++) {
					Member member = mSet.get(j);
					System.out.println("member_id="+ member.getMemberId() + "; tanggal="+ hariHitung.toString("yyyy-MM-dd"));
					MemberTree currFoot = MemberTreeHarianFcd.getMemberTreeByIdAndDate(member.getMemberId(), hariHitung.toDate(), dbConn);
					BonusA childBonus = BonusAFcd.getBonusByMemberAndDate(member.getMemberId(), hariHitung.toDate(), dbConn);
					double childMatchBonus = 0;
//					if ((childBonus != null) && ((childBonus.getBonusPasanganSedang() > 0) || (childBonus.getBonusPasanganKecil() > 0))) {
					if ((childBonus != null) && (childBonus.getBonusPasanganSedang() > 0)) {
//						childMatchBonus = (childBonus.getBonusPasanganSedang() + childBonus.getBonusPasanganKecil()) * bonusMatchingPct / 100;
						System.out.println("Dapat bonus matching dari member " + member.getMemberId());
						childMatchBonus = childBonus.getBonusPasanganSedang() * bonusMatchingLevel / 100;
						childMatchBonus = NumberUtil.roundToDecimals(childMatchBonus, 0);
						BonusA headBonus = BonusAFcd.getBonusByMemberAndDate(headId, hariHitung.toDate(), dbConn);
						if (headBonus == null) {
							headBonus = new BonusA();
							headBonus.setMemberId(headId);
							headBonus.setTanggal(hariHitung.toDate());
							headBonus.setBonusMatching(childMatchBonus);
							BonusAFcd bonusFcd = new BonusAFcd(headBonus);
							bonusFcd.insertBonus(dbConn);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(headId);
							logBonus.setFromMember(member.getMemberId());
							logBonus.setTglBonus(hariHitung.toDate());
							logBonus.setAmount(childMatchBonus);
							logBonus.setJenisBonus("matching");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						} else {
							double headBonusMatch = headBonus.getBonusMatching();
							double awal = headBonusMatch;
							headBonusMatch += childMatchBonus;
							headBonus.setBonusMatching(headBonusMatch);
							BonusAFcd bonusFcd = new BonusAFcd();
							bonusFcd.updateBonus(headBonus, dbConn);
							double selisih = headBonusMatch - awal;
							System.out.println("headBonusMatch="+headBonusMatch+"; awal="+awal);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(headId);
							logBonus.setFromMember(member.getMemberId());
							logBonus.setTglBonus(hariHitung.toDate());
							logBonus.setAmount(selisih);
							logBonus.setJenisBonus("matching");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						}
					}
					if (currFoot != null) hitungBonusMatching(headId, currFoot, level + 1, dbConn);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("mau cek keturunan");
			System.out.println("***Cek keturunan dari "+memberTree.getMemberId());
			Collection<MemberTree> keturunan = mTree.getSuccessors(memberTree.getMemberId());
			MemberTree[] kakikaki = (MemberTree[]) keturunan.toArray(new MemberTree[0]);
			for (int i=0; i<keturunan.size(); i++) {
				MemberTree currFoot = kakikaki[i];
				if (level <= BONUS_MATCHING_LEVEL ) {
					try {
						BonusA childBonus = BonusAFcd.getBonusByMemberAndDate(currFoot.getMemberId(), hariHitung.toDate(), dbConn);
						double childMatchBonus = 0;
//						if ((childBonus != null) && ((childBonus.getBonusPasanganSedang() > 0) || (childBonus.getBonusPasanganKecil() > 0))) {
						if ((childBonus != null) && (childBonus.getBonusPasanganSedang() > 0)) {
//							childMatchBonus = (childBonus.getBonusPasanganSedang() + childBonus.getBonusPasanganKecil()) * bonusMatchingPct / 100;
							System.out.println("Dapat bonus matching dari member " + currFoot.getMemberId());
							childMatchBonus = childBonus.getBonusPasanganSedang() * bonusMatchingPct / 100;
							childMatchBonus = NumberUtil.roundToDecimals(childMatchBonus, 0);
							BonusA headBonus = BonusAFcd.getBonusByMemberAndDate(headId, hariHitung.toDate(), dbConn);
							if (headBonus == null) {
								headBonus = new BonusA();
								headBonus.setMemberId(headId);
								headBonus.setTanggal(hariHitung.toDate());
								headBonus.setBonusMatching(childMatchBonus);
								BonusAFcd bonusFcd = new BonusAFcd(headBonus);
								bonusFcd.insertBonus(dbConn);
								LogBonusA logBonus = new LogBonusA();
								logBonus.setMemberId(headId);
								logBonus.setFromMember(currFoot.getMemberId());
								logBonus.setTglBonus(hariHitung.toDate());
								logBonus.setAmount(childMatchBonus);
								logBonus.setJenisBonus("matching");
								LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
								lbaFcd.insertLog(dbConn);
							} else {
								double headBonusMatch = headBonus.getBonusMatching();
								double awal = headBonusMatch;
								headBonusMatch += childMatchBonus;
								headBonus.setBonusMatching(headBonusMatch);
								BonusAFcd bonusFcd = new BonusAFcd();
								bonusFcd.updateBonus(headBonus, dbConn);
								double selisih = headBonusMatch - awal;
								LogBonusA logBonus = new LogBonusA();
								logBonus.setMemberId(headId);
								logBonus.setFromMember(currFoot.getMemberId());
								logBonus.setTglBonus(hariHitung.toDate());
								logBonus.setAmount(selisih);
								logBonus.setJenisBonus("matching");
								LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
								lbaFcd.insertLog(dbConn);
							}
						}					
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (currFoot != null) hitungBonusMatching(headId, currFoot, level + 1, dbConn);
				}
			}
		}
	}
	
	public void giveBonusMatching() {
		MemberTree kepala = mTree.getHead();
		hitungBonusMatching(kepala.getMemberId(), kepala, 1);
		traverseTree(mTree);
	}
	
	public void giveBonusMatching(DBConnection dbConn) {
		MemberTree kepala = mTree.getHead();
		hitungBonusMatching(kepala.getMemberId(), kepala, 1, dbConn);
		traverseTree(mTree, dbConn);
	}
	
	public void giveBonusMatchingNew(DBConnection dbConn) {
		BonusAFcd baFcd = new BonusAFcd();
		BonusATempFcd batFcd = new BonusATempFcd();
		String whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "' AND coalesce(bonus_pasangan_sedang,0) > 0";
		try {
			BonusASet baSet = baFcd.search(whereClause, dbConn);
			for (int i=0; i<baSet.length(); i++) {
				BonusA bonus = baSet.get(i);
				hitungBonusMatchingNew(bonus.getMemberId(), MemberTreeFcd.getMemberTreeById(bonus.getMemberId(), dbConn), BONUS_MATCHING_LEVEL, bonus.getBonusPasanganSedang(), bonus.getMemberId(), dbConn);
			}
			
			BonusATempSet batSet = batFcd.search(whereClause, dbConn);
			for (int i=0; i<batSet.length(); i++) {
				BonusATemp bonus = batSet.get(i);
				hitungBonusMatchingNew(bonus.getMemberId(), MemberTreeFcd.getMemberTreeById(bonus.getMemberId(), dbConn), BONUS_MATCHING_LEVEL, bonus.getBonusPasanganSedang(), bonus.getMemberId(), dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void hitungBonusMatchingNew(String memberId, MemberTree memberTree, int level, double bonusMicro, String fromMember, DBConnection dbConn) throws Exception {
		if (level >= 1) {
			Member member = MemberFcd.getMemberById(memberId, dbConn);
			String sponsor = member.getSponsor();
			MemberTree mtSponsor = MemberTreeFcd.getMemberTreeById(sponsor, dbConn);
			MemberTree mtFrom = MemberTreeFcd.getMemberTreeById(fromMember, dbConn);
			if (sponsor != null) {
				if (mtSponsor.getIsBasic() == 0 && mtFrom.getIsBasic() == 0) {
					BonusA bonus = BonusAFcd.getBonusByMemberAndDate(sponsor, hariHitung.toDate(), dbConn);
					double bonusMatch;
					if (level > 1) {
						bonusMatch = NumberUtil.roundToDecimals((bonusMicro * bonusMatchingPct / 100), 0);
					} else {
						bonusMatch = NumberUtil.roundToDecimals((bonusMicro * bonusMatchingLevel / 100), 0);
					}
//					if (sponsor != null) {
						if (bonus != null) {
							bonus.setBonusMatching(bonus.getBonusMatching() + bonusMatch);
							BonusAFcd baFcd = new BonusAFcd();
							baFcd.updateBonus(bonus, dbConn);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(sponsor);
							logBonus.setFromMember(fromMember);
							logBonus.setTglBonus(hariHitung.toDate());
							logBonus.setAmount(bonusMatch);
							logBonus.setJenisBonus("matching");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						} else {
							bonus = new BonusA();
							bonus.setMemberId(sponsor);
							bonus.setBonusMatching(bonusMatch);
							bonus.setTanggal(hariHitung.toDate());
							BonusAFcd baFcd = new BonusAFcd(bonus);
							baFcd.insertBonus(dbConn);
							LogBonusA logBonus = new LogBonusA();
							logBonus.setMemberId(member.getMemberId());
							logBonus.setFromMember(fromMember);
							logBonus.setTglBonus(hariHitung.toDate());
							logBonus.setAmount(bonusMatch);
							logBonus.setJenisBonus("matching");
							LogBonusAFcd lbaFcd = new LogBonusAFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						}
//					}
					bonus = null;
					bonusMatch = 0;
				} else {
					BonusATemp bonus = BonusATempFcd.getBonusByMemberAndDate(sponsor, hariHitung.toDate(), dbConn);
					double bonusMatch;
					if (level > 1) {
						bonusMatch = bonusMicro * bonusMatchingPct / 100;
					} else {
						bonusMatch = bonusMicro * bonusMatchingLevel / 100;
					}
//					if (sponsor != null && mtSponsor.getIsBasic() > -1) {
					if (mtSponsor.getIsBasic() > -1) {
						if (bonus != null) {
							bonus.setBonusMatching(bonus.getBonusMatching() + bonusMatch);
							BonusATempFcd batFcd = new BonusATempFcd();
							batFcd.updateBonus(bonus, dbConn);
							LogBonusATemp logBonus = new LogBonusATemp();
							logBonus.setMemberId(sponsor);
							logBonus.setFromMember(fromMember);
							logBonus.setTglBonus(hariHitung.toDate());
							logBonus.setAmount(bonusMatch);
							logBonus.setJenisBonus("matching");
							LogBonusATempFcd lbaFcd = new LogBonusATempFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						} else {
							bonus = new BonusATemp();
							bonus.setMemberId(sponsor);
							bonus.setBonusMatching(bonusMatch);
							bonus.setTanggal(hariHitung.toDate());
							BonusATempFcd baFcd = new BonusATempFcd(bonus);
							baFcd.insertBonus(dbConn);
							LogBonusATemp logBonus = new LogBonusATemp();
							logBonus.setMemberId(member.getMemberId());
							logBonus.setFromMember(fromMember);
							logBonus.setTglBonus(hariHitung.toDate());
							logBonus.setAmount(bonusMatch);
							logBonus.setJenisBonus("matching");
							LogBonusATempFcd lbaFcd = new LogBonusATempFcd(logBonus);
							lbaFcd.insertLog(dbConn);
						}
					}
					bonus = null;
					bonusMatch = 0;
				}
			}
			
			if (level > 1 && !memberTree.getUplineId().equals("")) {
				MemberTree memTree = MemberTreeFcd.getMemberTreeById(memberTree.getUplineId(), dbConn);
				hitungBonusMatchingNew(memTree.getMemberId(), memTree, level-1, bonusMicro, fromMember, dbConn);
			}
		}
	}

	private void traverseTree(Tree<MemberTree> pohon) {
//		MemberTree kepala = pohon.getHead();
//		hitungBonusMatching(kepala.getMemberId(), kepala, 1);
		for (Tree<MemberTree> data : pohon.getLeafs()) {
			traverseTree(data);
			MemberTree kepala = data.getHead();
			hitungBonusMatching(kepala.getMemberId(), kepala, 1);
		}
	}
	
	private void traverseTree(Tree<MemberTree> pohon, DBConnection dbConn) {
		for (Tree<MemberTree> data : pohon.getLeafs()) {
			if (data != null) {
				traverseTree(data, dbConn);
				MemberTree kepala = data.getHead();
				if (kepala != null) hitungBonusMatching(kepala.getMemberId(), kepala, 1, dbConn);
			}
		}
	}
	
	public void potongAm() {
		String whereClause = "start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
		KursAmFcd kaFcd = new KursAmFcd();
		KursAmSet kaSet = null;
		try {
			kaSet = kaFcd.search(whereClause);
			if (kaSet.length() < 1)
				throw new Exception("Belum ada setting Automaintenance yang berlaku.");
			KursAm kAm = kaSet.get(0);
			double potongBulanan = kAm.getMaxPotongBulanan();
			int pctPotong = kAm.getPersenPotongHarian();
			whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "'";
			BonusAFcd baFcd = new BonusAFcd();
			BonusASet baSet = baFcd.search(whereClause);
			for (int i=0; i<baSet.length(); i++) {
				BonusA bonus = baSet.get(i);
				MemberTree member = MemberTreeFcd.getMemberTreeById(bonus.getMemberId());
				if (member.getPotonganAm() < potongBulanan) {
//					double totalBonus = bonus.getBonusSponsor() + bonus.getBonusPasanganSedang() + bonus.getBonusPasanganKecil() + bonus.getBonusMatching();
					double totalBonus = bonus.getBonusPasanganSedang() + bonus.getBonusPasanganKecil() + bonus.getBonusMatching();
					double potong = (double)pctPotong / 100 * totalBonus;
					double saldoAm = member.getPotonganAm();
					if ((potong + saldoAm) > potongBulanan) {
						potong = potongBulanan - saldoAm;
//						saldoAm += potong;
//					} else {
//						saldoAm = potong + saldoAm;
					}
					saldoAm += potong;
					bonus.setPotonganAm(potong);
					baFcd.updateBonus(bonus);
					member.setPotonganAm(saldoAm);
					member.setSaldoAm(member.getSaldoAm() + potong);
					float pValue = kAm.getPointValue();
					float bValue = kAm.getBusinessValue();
					float curPv = (float) NumberUtil.roundToDecimals(potong / pValue, 1);
					float curBv = (float) NumberUtil.roundToDecimals((bValue * potong / 100), 0);
					float pvpribadi = member.getPvpribadi() + curPv;
					double saldoBv = member.getSaldoBv() + curBv;
					member.setPvpribadi(pvpribadi);
					member.setSaldoBv(saldoBv);
					MemberTreeFcd mtFcd = new MemberTreeFcd();
					mtFcd.updateMemberTree(member);
					PoinRecord poin = new PoinRecord();
					poin.setMemberId(member.getMemberId());
					poin.setPoin(curPv);
					poin.setTrxId("AM" + hariHitung.toString("yyyyMMdd"));
					PoinRecordFcd prFcd = new PoinRecordFcd(poin);
					prFcd.insertPoin();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public void potongAm(DBConnection dbConn) {
		String whereClause = "start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
		KursAmFcd kaFcd = new KursAmFcd();
		KursAmSet kaSet = null;
		try {
			kaSet = kaFcd.search(whereClause, dbConn);
			if (kaSet.length() < 1)
				throw new Exception("Belum ada setting Automaintenance yang berlaku.");
			KursAm kAm = kaSet.get(0);
			double potongBulanan = kAm.getMaxPotongBulanan();
			int pctPotong = kAm.getPersenPotongHarian();
			whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "'";
			BonusAFcd baFcd = new BonusAFcd();
			BonusASet baSet = baFcd.search(whereClause, dbConn);
			for (int i=0; i<baSet.length(); i++) {
				BonusA bonus = baSet.get(i);
				MemberTree member = MemberTreeFcd.getMemberTreeById(bonus.getMemberId(), dbConn);
				if (member.getPotonganAm() < potongBulanan) {
//					double totalBonus = bonus.getBonusSponsor() + bonus.getBonusPasanganSedang() + bonus.getBonusPasanganKecil() + bonus.getBonusMatching() - bonus.getTax();
					BonusBagiHasilFcd bbhFcd = new BonusBagiHasilFcd();
					whereClause = whereClause + " AND member_id = '" + member.getMemberId() + "'";
//					BonusBagiHasilSet bbhSet = bbhFcd.search(whereClause, dbConn);
//					double totalBonus = bonus.getBonusPasanganSedang() + bonus.getBonusPasanganKecil() + bonus.getBonusMatching();
					double totalBonus = bonus.getBonusSponsor() + bonus.getBonusPasanganSedang() + bonus.getBonusPasanganKecil() + bonus.getBonusMatching();
					BonusBagiHasil bbHasil = bbhFcd.getBonusByMemberAndDate(member.getMemberId(), hariHitung.toDate(), dbConn);  
//					if (bbhSet != null && bbhSet.length()>0) {
					if (bbHasil != null) {
//						BonusBagiHasil bbHasil = bbhSet.get(0);
//						totalBonus += bbHasil.getBagiHasil();
						totalBonus += bbHasil.getBagiHasil() + bbHasil.getBagiHasilMacro();
					}
					if (totalBonus > 0) {
						double potong = (double)pctPotong / 100 * totalBonus;
						double saldoAm = member.getPotonganAm();
						if ((potong + saldoAm) > potongBulanan) {
							potong = potongBulanan - saldoAm;
//							saldoAm += potong;
//						} else {
//							saldoAm = potong + saldoAm;
						}
						saldoAm += potong;
						bonus.setPotonganAm(potong);
						baFcd.updateBonus(bonus, dbConn);
						member.setPotonganAm(saldoAm);
						member.setSaldoAm(member.getSaldoAm() + potong);
						float pValue = kAm.getPointValue();
						float bValue = kAm.getBusinessValue();
//						float curPv = (float) NumberUtil.roundToDecimals(potong / pValue, 1);
						float curPv = (float) (potong / pValue);
						float curBv = (float) NumberUtil.roundToDecimals((bValue * potong / 100), 0);
						float pvpribadi = member.getPvpribadi() + curPv;
						double saldoBv = member.getSaldoBv() + curBv;
						member.setPvpribadi(pvpribadi);
						member.setSaldoBv(saldoBv);
						MemberTreeFcd mtFcd = new MemberTreeFcd();
						mtFcd.updateMemberTree(member, dbConn);
						System.out.println("hariHitung = " + hariHitung.toString("yyyy-MM-dd"));
						PoinRecord poin = new PoinRecord();
						poin.setMemberId(member.getMemberId());
						poin.setPoin(curPv);
						poin.setTrxId("AM" + hariHitung.toString("yyyyMMdd"));
						poin.setTanggal(hariHitung.toDate());
						poin.setAm(potong);
						poin.setBv(curBv);
//						System.out.println("nilai tanggal di poin = " + poin.getTanggal().toString());
						PoinRecordFcd prFcd = new PoinRecordFcd(poin);
						prFcd.insertPoin(dbConn);
					}
				}
			}
			whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "' AND member_id NOT IN " +
							"(SELECT member_id FROM bonus_a WHERE tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "')";
			BonusBagiHasilFcd bbhFcd = new BonusBagiHasilFcd();
			BonusBagiHasilSet bbhSet = bbhFcd.search(whereClause, dbConn);
			for (int i=0; i<bbhSet.length(); i++) {
				double totalBonus = 0;
				BonusBagiHasil bbHasil = bbhSet.get(i);
				totalBonus += bbHasil.getBagiHasil() + bbHasil.getBagiHasilMacro();
				MemberTree member = MemberTreeFcd.getMemberTreeById(bbHasil.getMemberId(), dbConn);
				
				if (totalBonus > 0) {
					double potong = (double)pctPotong / 100 * totalBonus;
					double saldoAm = member.getPotonganAm();
					if ((potong + saldoAm) > potongBulanan) {
						potong = potongBulanan - saldoAm;
//						saldoAm += potong;
//					} else {
//						saldoAm = potong + saldoAm;
					}
					saldoAm += potong;
					BonusA bonus = new BonusA();
					bonus.setPotonganAm(potong);
					bonus.setMemberId(bbHasil.getMemberId());
					bonus.setTanggal(hariHitung.toDate());
					BonusAFcd bonusFcd = new BonusAFcd(bonus);
					bonusFcd.insertBonus(dbConn);
					member.setPotonganAm(saldoAm);
					member.setSaldoAm(member.getSaldoAm() + potong);
					float pValue = kAm.getPointValue();
					float bValue = kAm.getBusinessValue();
//					float curPv = (float) NumberUtil.roundToDecimals(potong / pValue, 1);
					float curPv = (float) (potong / pValue);
					float curBv = (float) NumberUtil.roundToDecimals((bValue * potong / 100), 0);
					float pvpribadi = member.getPvpribadi() + curPv;
					double saldoBv = member.getSaldoBv() + curBv;
					member.setPvpribadi(pvpribadi);
					member.setSaldoBv(saldoBv);
					MemberTreeFcd mtFcd = new MemberTreeFcd();
					mtFcd.updateMemberTree(member, dbConn);
					System.out.println("hariHitung = " + hariHitung.toString("yyyy-MM-dd"));
					PoinRecord poin = new PoinRecord();
					poin.setMemberId(member.getMemberId());
					poin.setPoin(curPv);
					poin.setTrxId("AM" + hariHitung.toString("yyyyMMdd"));
					poin.setTanggal(hariHitung.toDate());
					poin.setAm(potong);
					poin.setBv(curBv);
//					System.out.println("nilai tanggal di poin = " + poin.getTanggal().toString());
					PoinRecordFcd prFcd = new PoinRecordFcd(poin);
					prFcd.insertPoin(dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateMemberTree(DBConnection dbConn) {
		MemberTree kepala = mTree.getHead();
		if (kepala != null) {
			MemberTree[] feet = sortDownliners(kepala);
			MemberTree[] feetPromo = sortDownlinersPromo(kepala);
			float pvGb, pvGs, pvGk = 0;
			String bigfoot, medfoot, litfoot, bigfootPromo, medfootPromo, litfootPromo = "";
			int amtBigfoot, amtMedfoot, amtLitfoot, amtBigfootPromo, amtMedfootPromo, amtLitfootPromo = 0;
			if (feet.length > 0) {
				pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
				kepala.setPvBigfoot(pvGb);
				bigfoot = feet[0].getMemberId();
				bigfootPromo = feetPromo[0].getMemberId();
				kepala.setBigfoot(bigfoot);
				amtBigfoot = 1 + getSuccessorsSize(bigfoot);
				amtBigfootPromo = getSuccessorsPromoSize(bigfootPromo);
				if (feetPromo[0].getIsPromoxenia()==1) amtBigfootPromo++;
				kepala.setAmtBigfoot(amtBigfoot);
				kepala.setBigfootPromo(amtBigfootPromo);
				if (feet.length > 1) {
					pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
					kepala.setPvMidfoot(pvGs);
					medfoot = feet[1].getMemberId();
					medfootPromo = feetPromo[1].getMemberId();
					kepala.setMediumfoot(medfoot);
					amtMedfoot = 1 + getSuccessorsSize(medfoot);
					amtMedfootPromo = getSuccessorsPromoSize(medfootPromo);
					if (feetPromo[1].getIsPromoxenia()==1) amtMedfootPromo++;
					kepala.setAmtMidfoot(amtMedfoot);
					kepala.setMidfootPromo(amtMedfootPromo);
					if (feet.length > 2) {
						pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
						kepala.setPvLitfoot(pvGk);
						litfoot = feet[2].getMemberId();
						litfootPromo = feetPromo[2].getMemberId();
						kepala.setLitfoot(litfoot);
						amtLitfoot = 1 + getSuccessorsSize(litfoot);
						amtLitfootPromo = getSuccessorsPromoSize(litfootPromo);
						if (feetPromo[2].getIsPromoxenia() == 1) amtLitfootPromo++;
						kepala.setAmtLitfoot(amtLitfoot);
						kepala.setLitfootPromo(amtLitfootPromo);
					}
				}
			}
			MemberTreeFcd mtFcd = new MemberTreeFcd();
			try {
				mtFcd.updateMemberTree(kepala, dbConn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		lacakPohon(mTree, dbConn);
		
	}
	
	public void updateMemberTreeNonbasic(DBConnection dbConn) throws Exception {
		MemberTree kepala = mTree.getHead();
		if (kepala != null) {
			MemberTree[] feet = sortDownlinersNonbasic(kepala);
			float pvGb, pvGs, pvGk = 0;
			int ddrGb, ddrGs, ddrGk = 0;
			String bigfoot, medfoot, litfoot = "";
			int amtBigfoot, amtMedfoot, amtLitfoot = 0;
			if (feet.length > 0) {
				pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
				kepala.setPvBigfoot(pvGb);
				bigfoot = feet[0].getMemberId();
				kepala.setBigfootNonbasic(bigfoot);
				amtBigfoot = checkBasicOrNot(bigfoot, dbConn) + getSuccessorsSizeNonbasic(bigfoot);
//				if (kepala.getDdrBigfoot() == 0) {
//					kepala.setDdrBigfoot(amtBigfoot);
//				}
//				else {
					ddrGb = amtBigfoot + feet[0].getSaldoDdr() + countDdrGrup(feet[0], 0, dbConn);
					kepala.setDdrBigfoot(ddrGb);
//				}
				kepala.setAmtBigfootNonbasic(amtBigfoot);
				if (feet.length > 1) {
					pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
					kepala.setPvMidfoot(pvGs);
					medfoot = feet[1].getMemberId();
					kepala.setMediumfootNonbasic(medfoot);
					amtMedfoot = checkBasicOrNot(medfoot, dbConn) + getSuccessorsSizeNonbasic(medfoot);
					ddrGs = amtMedfoot + feet[1].getSaldoDdr() + countDdrGrup(feet[1], 0, dbConn);
					kepala.setDdrMidfoot(ddrGs);
					kepala.setAmtMidfootNonbasic(amtMedfoot);
					if (feet.length > 2) {
						pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
						kepala.setPvLitfoot(pvGk);
						litfoot = feet[2].getMemberId();
						kepala.setLitfootNonbasic(litfoot);
						amtLitfoot = checkBasicOrNot(litfoot, dbConn) + getSuccessorsSizeNonbasic(litfoot);
						ddrGk = amtLitfoot + feet[2].getSaldoDdr() + countDdrGrup(feet[2], 0, dbConn);
						kepala.setAmtLitfootNonbasic(amtLitfoot);
					}
				}
			}
			MemberTreeFcd mtFcd = new MemberTreeFcd();
			try {
				mtFcd.updateMemberTree(kepala, dbConn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		lacakPohonNonbasic(mTree, dbConn);
		
	}
	
	public int checkBasicOrNot(String memberId, DBConnection dbConn) throws Exception {
		MemberTree anggota = MemberTreeFcd.getMemberTreeById(memberId, dbConn);
		if (anggota.getIsBasicNew() == 0)
			return 1;
		else
			return 0;
	}
	
	public void updatePvGroup(DBConnection dbConn) {
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		MemberTreeSmallSet mtSet = null;
		try {
			mtSet = MemberTreeFcd.showAllPvGroupMemberTree();
			for (int i=0; i<mtSet.length(); i++) {
				MemberTreeSmall member = mtSet.get(i);
				float[] urutPv = sortPvMember(member);
				member.setPvBesar(urutPv[0]);
				member.setPvSedang(urutPv[1]);
				member.setPvKecil(urutPv[2]);
				mtFcd.updatePvGroup(member, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private float[] sortPvMember(MemberTreeSmall member) {
		float[] urutPv = new float[3];
		urutPv[0] = member.getPvBigfoot();
		urutPv[1] = member.getPvMidfoot();
		urutPv[2] = member.getPvLitfoot();
		
		int n = 3;
		boolean doMore = true;
		while (doMore) {
			n--;
			doMore = false;
			for (int i=0; i<n; i++) {
				if (urutPv[i]<urutPv[i+1]) {
					float temp = urutPv[i];
					urutPv[i] = urutPv[i+1];
					urutPv[i+1] = temp;
					doMore = true;
				}
			}
		}
		return urutPv;
	}
	
	public void updateMemberTreeLog(DBConnection dbConn) {
		MemberTree kepala = mTree.getHead();
		if (kepala != null) {
			MemberTree[] feet = sortDownliners(kepala);
			MemberTree[] feetPromo = sortDownlinersPromo(kepala);
			float pvGb, pvGs, pvGk = 0;
			String bigfoot, medfoot, litfoot, bigfootPromo, medfootPromo, litfootPromo = "";
			int amtBigfoot, amtMedfoot, amtLitfoot, amtBigfootPromo, amtMedfootPromo, amtLitfootPromo = 0;
			if (feet.length > 0) {
				pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
				kepala.setPvBigfoot(pvGb);
				bigfoot = feet[0].getMemberId();
				bigfootPromo = feetPromo[0].getMemberId();
				kepala.setBigfoot(bigfoot);
				amtBigfoot = 1 + getSuccessorsSize(bigfoot);
				amtBigfootPromo = getSuccessorsPromoSize(bigfootPromo);
				if (feetPromo[0].getIsPromoxenia()==1) amtBigfootPromo++;
				kepala.setAmtBigfoot(amtBigfoot);
				kepala.setBigfootPromo(amtBigfootPromo);
				if (feet.length > 1) {
					pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
					kepala.setPvMidfoot(pvGs);
					medfoot = feet[1].getMemberId();
					medfootPromo = feetPromo[1].getMemberId();
					kepala.setMediumfoot(medfoot);
					amtMedfoot = 1 + getSuccessorsSize(medfoot);
					amtMedfootPromo = getSuccessorsPromoSize(medfootPromo);
					if (feetPromo[1].getIsPromoxenia()==1) amtMedfootPromo++;
					kepala.setAmtMidfoot(amtMedfoot);
					kepala.setMidfootPromo(amtMedfootPromo);
					if (feet.length > 2) {
						pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
						kepala.setPvLitfoot(pvGk);
						litfoot = feet[2].getMemberId();
						litfootPromo = feetPromo[2].getMemberId();
						kepala.setLitfoot(litfoot);
						amtLitfoot = 1 + getSuccessorsSize(litfoot);
						amtLitfootPromo = getSuccessorsPromoSize(litfootPromo);
						if (feetPromo[2].getIsPromoxenia() == 1) amtLitfootPromo++;
						kepala.setAmtLitfoot(amtLitfoot);
						kepala.setLitfootPromo(amtLitfootPromo);
					}
				}
			}
//			MemberTreeFcd mtFcd = new MemberTreeFcd();
			if (kepala.getBigfootPromo()>0 || kepala.getMidfootPromo()>0 || kepala.getLitfootPromo()>0) {
				MemberTreeHarianFcd mtFcd = new MemberTreeHarianFcd();
				try {
					mtFcd.updateMemberTree(kepala, dbConn);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		lacakPohonLog(mTree, dbConn);
		
	}
	
	public void updateMemberTreeLogBulanan(DBConnection dbConn) {
		System.out.println("mulai update member tree");
		MemberTree kepala = mTree.getHead();
		if (kepala != null) {
			System.out.println("kepala tidak null");
			MemberTree[] feet = sortDownliners(kepala);
			MemberTree[] feetPromo = sortDownlinersPromo(kepala);
			float pvGb, pvGs, pvGk = 0;
			String bigfoot, medfoot, litfoot, bigfootPromo, medfootPromo, litfootPromo = "";
			int amtBigfoot, amtMedfoot, amtLitfoot, amtBigfootPromo, amtMedfootPromo, amtLitfootPromo = 0;
			if (feet.length > 0) {
				pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
				kepala.setPvBigfoot(pvGb);
				bigfoot = feet[0].getMemberId();
				bigfootPromo = feetPromo[0].getMemberId();
				kepala.setBigfoot(bigfoot);
				amtBigfoot = 1 + getSuccessorsSize(bigfoot);
				amtBigfootPromo = getSuccessorsPromoSize(bigfootPromo);
				if (feetPromo[0].getIsPromoxenia()==1) amtBigfootPromo++;
				kepala.setAmtBigfoot(amtBigfoot);
				kepala.setBigfootPromo(amtBigfootPromo);
				if (feet.length > 1) {
					pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
					kepala.setPvMidfoot(pvGs);
					medfoot = feet[1].getMemberId();
					medfootPromo = feetPromo[1].getMemberId();
					kepala.setMediumfoot(medfoot);
					amtMedfoot = 1 + getSuccessorsSize(medfoot);
					amtMedfootPromo = getSuccessorsPromoSize(medfootPromo);
					if (feetPromo[1].getIsPromoxenia()==1) amtMedfootPromo++;
					kepala.setAmtMidfoot(amtMedfoot);
					kepala.setMidfootPromo(amtMedfootPromo);
					if (feet.length > 2) {
						pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
						kepala.setPvLitfoot(pvGk);
						litfoot = feet[2].getMemberId();
						litfootPromo = feetPromo[2].getMemberId();
						kepala.setLitfoot(litfoot);
						amtLitfoot = 1 + getSuccessorsSize(litfoot);
						amtLitfootPromo = getSuccessorsPromoSize(litfootPromo);
						if (feetPromo[2].getIsPromoxenia() == 1) amtLitfootPromo++;
						kepala.setAmtLitfoot(amtLitfoot);
						kepala.setLitfootPromo(amtLitfootPromo);
					}
				}
			}
//			if (kepala.getBigfootPromo()>0 || kepala.getMidfootPromo()>0 || kepala.getLitfootPromo()>0) {
//				MemberTreeHarianFcd mtFcd = new MemberTreeHarianFcd();
				MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
				try {
					mtFcd.updateMemberTree(kepala, dbConn);
				} catch (Exception e) {
					e.printStackTrace();
				}
//			}
		}
		lacakPohonLogBulanan(mTree, dbConn);
		
	}
	
	private void lacakPohon(Tree<MemberTree> pohon, DBConnection dbConn) {
		for (Tree<MemberTree> data : pohon.getLeafs()) {
			if (data != null) {
				lacakPohon(data, dbConn);
				MemberTree kepala = data.getHead();
				if (kepala != null) {
					MemberTree[] feet = sortDownliners(kepala);
					MemberTree[] feetPromo = sortDownlinersPromo(kepala);
					float pvGb, pvGs, pvGk = 0;
					String bigfoot, medfoot, litfoot, bigfootPromo, medfootPromo, litfootPromo = "";
					int amtBigfoot, amtMedfoot, amtLitfoot, amtBigfootPromo, amtMedfootPromo, amtLitfootPromo = 0;
					if (feet.length > 0) {
						pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
						kepala.setPvBigfoot(pvGb);
						bigfoot = feet[0].getMemberId();
						bigfootPromo = feetPromo[0].getMemberId();
						kepala.setBigfoot(bigfoot);
						amtBigfoot = 1 + getSuccessorsSize(bigfoot);
						amtBigfootPromo = getSuccessorsPromoSize(bigfootPromo);
						if (feetPromo[0].getIsPromoxenia() == 1) amtBigfootPromo++;
						kepala.setAmtBigfoot(amtBigfoot);
						kepala.setBigfootPromo(amtBigfootPromo);
						if (feet.length > 1) {
							pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
							kepala.setPvMidfoot(pvGs);
							medfoot = feet[1].getMemberId();
							medfootPromo = feetPromo[1].getMemberId();
							kepala.setMediumfoot(medfoot);
							amtMedfoot = 1 + getSuccessorsSize(medfoot);
							amtMedfootPromo = getSuccessorsPromoSize(medfootPromo);
							if (feetPromo[1].getIsPromoxenia() == 1) amtMedfootPromo++;
							kepala.setAmtMidfoot(amtMedfoot);
							kepala.setMidfootPromo(amtMedfootPromo);
							if (feet.length > 2) {
								pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
								kepala.setPvLitfoot(pvGk);
								litfoot = feet[2].getMemberId();
								litfootPromo = feetPromo[2].getMemberId();
								kepala.setLitfoot(litfoot);
								amtLitfoot = 1 + getSuccessorsSize(litfoot);
								amtLitfootPromo = getSuccessorsPromoSize(litfootPromo);
								if (feetPromo[2].getIsPromoxenia() == 1) amtLitfootPromo++;
								kepala.setAmtLitfoot(amtLitfoot);
								kepala.setLitfootPromo(amtLitfootPromo);
							}
						}
					}
					MemberTreeFcd mtFcd = new MemberTreeFcd();
					try {
						mtFcd.updateMemberTree(kepala, dbConn);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private void lacakPohonNonbasic(Tree<MemberTree> pohon, DBConnection dbConn) throws Exception {
		for (Tree<MemberTree> data : pohon.getLeafs()) {
			if (data != null) {
				lacakPohonNonbasic(data, dbConn);
				MemberTree kepala = data.getHead();
				if (kepala != null) {
					MemberTree[] feet = sortDownlinersNonbasic(kepala);
					float pvGb, pvGs, pvGk = 0;
					int ddrGb, ddrGs, ddrGk = 0;
					String bigfoot, medfoot, litfoot = "";
					int amtBigfoot, amtMedfoot, amtLitfoot = 0;
					if (feet.length > 0) {
						pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
						kepala.setPvBigfoot(pvGb);
						bigfoot = feet[0].getMemberId();
						kepala.setBigfootNonbasic(bigfoot);
						amtBigfoot = checkBasicOrNot(bigfoot, dbConn) + getSuccessorsSizeNonbasic(bigfoot);
//						if (kepala.getDdrBigfoot() == 0) {
//						kepala.setDdrBigfoot(amtBigfoot);
//					}
//					else {
						ddrGb = amtBigfoot + feet[0].getSaldoDdr() + countDdrGrup(feet[0], 0, dbConn);
						kepala.setDdrBigfoot(ddrGb);
//					}
						kepala.setAmtBigfootNonbasic(amtBigfoot);
						if (feet.length > 1) {
							pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
							kepala.setPvMidfoot(pvGs);
							medfoot = feet[1].getMemberId();
							kepala.setMediumfootNonbasic(medfoot);
							amtMedfoot = checkBasicOrNot(medfoot, dbConn) + getSuccessorsSizeNonbasic(medfoot);
							ddrGs = amtMedfoot + feet[1].getSaldoDdr() + countDdrGrup(feet[1], 0, dbConn);
							kepala.setDdrMidfoot(ddrGs);
							kepala.setAmtMidfootNonbasic(amtMedfoot);
							if (feet.length > 2) {
								pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
								kepala.setPvLitfoot(pvGk);
								litfoot = feet[2].getMemberId();
								kepala.setLitfootNonbasic(litfoot);
								amtLitfoot = checkBasicOrNot(litfoot, dbConn) + getSuccessorsSizeNonbasic(litfoot);
								ddrGk = amtLitfoot + feet[2].getSaldoDdr() + countDdrGrup(feet[2], 0, dbConn);
								kepala.setDdrLitfoot(ddrGk);
								kepala.setAmtLitfootNonbasic(amtLitfoot);
							}
						}
					}
					MemberTreeFcd mtFcd = new MemberTreeFcd();
					try {
						mtFcd.updateMemberTree(kepala, dbConn);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private void lacakPohonLog(Tree<MemberTree> pohon, DBConnection dbConn) {
		for (Tree<MemberTree> data : pohon.getLeafs()) {
			if (data != null) {
				lacakPohonLog(data, dbConn);
				MemberTree kepala = data.getHead();
				if (kepala != null) {
					MemberTree[] feet = sortDownliners(kepala);
					MemberTree[] feetPromo = sortDownlinersPromo(kepala);
					float pvGb, pvGs, pvGk = 0;
					String bigfoot, medfoot, litfoot, bigfootPromo, medfootPromo, litfootPromo = "";
					int amtBigfoot, amtMedfoot, amtLitfoot, amtBigfootPromo, amtMedfootPromo, amtLitfootPromo = 0;
					if (feet.length > 0) {
						pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
						kepala.setPvBigfoot(pvGb);
						bigfoot = feet[0].getMemberId();
						bigfootPromo = feetPromo[0].getMemberId();
						kepala.setBigfoot(bigfoot);
						amtBigfoot = 1 + getSuccessorsSize(bigfoot);
						amtBigfootPromo = getSuccessorsPromoSize(bigfootPromo);
						if (feetPromo[0].getIsPromoxenia() == 1) amtBigfootPromo++;
						kepala.setAmtBigfoot(amtBigfoot);
						kepala.setBigfootPromo(amtBigfootPromo);
						if (feet.length > 1) {
							pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
							kepala.setPvMidfoot(pvGs);
							medfoot = feet[1].getMemberId();
							medfootPromo = feetPromo[1].getMemberId();
							kepala.setMediumfoot(medfoot);
							amtMedfoot = 1 + getSuccessorsSize(medfoot);
							amtMedfootPromo = getSuccessorsPromoSize(medfootPromo);
							if (feetPromo[1].getIsPromoxenia() == 1) amtMedfootPromo++;
							kepala.setAmtMidfoot(amtMedfoot);
							kepala.setMidfootPromo(amtMedfootPromo);
							if (feet.length > 2) {
								pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
								kepala.setPvLitfoot(pvGk);
								litfoot = feet[2].getMemberId();
								litfootPromo = feetPromo[2].getMemberId();
								kepala.setLitfoot(litfoot);
								amtLitfoot = 1 + getSuccessorsSize(litfoot);
								amtLitfootPromo = getSuccessorsPromoSize(litfootPromo);
								if (feetPromo[2].getIsPromoxenia() == 1) amtLitfootPromo++;
								kepala.setAmtLitfoot(amtLitfoot);
								kepala.setLitfootPromo(amtLitfootPromo);
							}
						}
					}
//					MemberTreeFcd mtFcd = new MemberTreeFcd();
					if (kepala.getBigfootPromo()>0 || kepala.getMidfootPromo()>0 || kepala.getLitfootPromo()>0) {
						MemberTreeHarianFcd mtFcd = new MemberTreeHarianFcd();
						try {
							mtFcd.updateMemberTree(kepala, dbConn);
							mtFcd = null;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	private void lacakPohonLogBulanan(Tree<MemberTree> pohon, DBConnection dbConn) {
		for (Tree<MemberTree> data : pohon.getLeafs()) {
			if (data != null) {
				lacakPohonLogBulanan(data, dbConn);
				MemberTree kepala = data.getHead();
				if (kepala != null) {
					MemberTree[] feet = sortDownliners(kepala);
					MemberTree[] feetPromo = sortDownlinersPromo(kepala);
					float pvGb, pvGs, pvGk = 0;
					String bigfoot, medfoot, litfoot, bigfootPromo, medfootPromo, litfootPromo = "";
					int amtBigfoot, amtMedfoot, amtLitfoot, amtBigfootPromo, amtMedfootPromo, amtLitfootPromo = 0;
					if (feet.length > 0) {
						pvGb = feet[0].getPvpribadi() + countPvGrup(feet[0], 0, dbConn);
						kepala.setPvBigfoot(pvGb);
						bigfoot = feet[0].getMemberId();
						bigfootPromo = feetPromo[0].getMemberId();
						kepala.setBigfoot(bigfoot);
						amtBigfoot = 1 + getSuccessorsSize(bigfoot);
						amtBigfootPromo = getSuccessorsPromoSize(bigfootPromo);
						if (feetPromo[0].getIsPromoxenia() == 1) amtBigfootPromo++;
						kepala.setAmtBigfoot(amtBigfoot);
						kepala.setBigfootPromo(amtBigfootPromo);
						if (feet.length > 1) {
							pvGs = feet[1].getPvpribadi() + countPvGrup(feet[1], 0, dbConn);
							kepala.setPvMidfoot(pvGs);
							medfoot = feet[1].getMemberId();
							medfootPromo = feetPromo[1].getMemberId();
							kepala.setMediumfoot(medfoot);
							amtMedfoot = 1 + getSuccessorsSize(medfoot);
							amtMedfootPromo = getSuccessorsPromoSize(medfootPromo);
							if (feetPromo[1].getIsPromoxenia() == 1) amtMedfootPromo++;
							kepala.setAmtMidfoot(amtMedfoot);
							kepala.setMidfootPromo(amtMedfootPromo);
							if (feet.length > 2) {
								pvGk = feet[2].getPvpribadi() + countPvGrup(feet[2], 0, dbConn);
								kepala.setPvLitfoot(pvGk);
								litfoot = feet[2].getMemberId();
								litfootPromo = feetPromo[2].getMemberId();
								kepala.setLitfoot(litfoot);
								amtLitfoot = 1 + getSuccessorsSize(litfoot);
								amtLitfootPromo = getSuccessorsPromoSize(litfootPromo);
								if (feetPromo[2].getIsPromoxenia() == 1) amtLitfootPromo++;
								kepala.setAmtLitfoot(amtLitfoot);
								kepala.setLitfootPromo(amtLitfootPromo);
							}
						}
					}
//					if (kepala.getBigfootPromo()>0 || kepala.getMidfootPromo()>0 || kepala.getLitfootPromo()>0) {
//						MemberTreeHarianFcd mtFcd = new MemberTreeHarianFcd();
						MemberTreeBulananFcd mtFcd = new MemberTreeBulananFcd();
						try {
							mtFcd.updateMemberTree(kepala, dbConn);
							mtFcd = null;
						} catch (Exception e) {
							e.printStackTrace();
						}
//					}
				}
			} else {
				System.out.println("data di lacakPohonLogBulanan NULL");
			}
		}
	}
	
	public void potongPajak(DBConnection dbConn) {
		String whereClause = "start_date < '" + hariHitung.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
		TaxRateFcd trFcd = new TaxRateFcd();
		TaxRateSet trSet = null;
		try {
			trSet = trFcd.search(whereClause);
			if (trSet.length() < 1)
				throw new Exception("Belum ada setting Rate Pajak yang berlaku.");
			TaxRate tax = trSet.get(0);
			whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "'";
			BonusAFcd baFcd = new BonusAFcd();
			BonusASet baSet = baFcd.search(whereClause, dbConn);
			MutableDateTime kemarin = hariHitung.copy();
			kemarin.addDays(-1);
			System.out.println("hariHitung = "+hariHitung.toString("yyyy-MM-dd")+"; kemarin="+kemarin.toString("yyyy-MM-dd"));
			for (int i=0; i<baSet.length(); i++) {
				BonusA bonus = baSet.get(i);
				SaldoPtkp saldoPtkp = SaldoPtkpFcd.getSaldoPtkpByMemberId(bonus.getMemberId(), dbConn);
				int sisaPtkp = saldoPtkp.getSisaPtkp();
				int sisaPtkpLalu = sisaPtkp;
				whereClause = "member_id = '" + bonus.getMemberId() + "' AND tanggal < '" + hariHitung.toString("yyyy-MM-dd") + "' ORDER BY tanggal DESC LIMIT 1";
				BonusASet bakSet = baFcd.search(whereClause, dbConn);
//				BonusA bonusKemarin = BonusAFcd.getBonusByMemberAndDate(bonus.getMemberId(), kemarin.toDate(), dbConn);
				BonusA bonusKemarin = null;
				if (bakSet != null) bonusKemarin = bakSet.get(0);
				double totalBonus = bonus.getBonusSponsor() + bonus.getBonusPasanganSedang() + bonus.getBonusPasanganKecil() + bonus.getBonusMatching() - bonus.getPotonganAm();
				if (totalBonus >= sisaPtkp) {
					sisaPtkp = 0;
				} else {
					sisaPtkp = (int) (sisaPtkp - totalBonus);
				}
				boolean isNpwp = false;
				Member member = MemberFcd.getMemberById(bonus.getMemberId(), dbConn);
				if (member.getNpwp() != null && !member.getNpwp().equals("")) {
					isNpwp = true;
				}
//				if (!isNpwp) {
//					sisaPtkp = 0;
//				}
				if (sisaPtkp == 0) {
					double pkp = 0;
//					if (isNpwp) {
						pkp = totalBonus - sisaPtkpLalu;
//					} else {
//						pkp = totalBonus;
//					}
					double pkpKumKemarin = 0;
					if (bonusKemarin != null) {
						pkpKumKemarin = bonusKemarin.getPkpKumulatif();
					}
					double pkpKum = pkpKumKemarin + pkp;
					double pphTerutang = 0;
					int levelNow, levelKemarin = 0;
					long batasPkp = 0;
					float pctNpwpKemarin, pctNpwpNow = 0;
					if (pkpKumKemarin > tax.getPkpFromLevel4()) {
						levelKemarin = 4;
						pctNpwpKemarin = tax.getPctNpwpLevel4();
					} else if (pkpKumKemarin > tax.getPkpFromLevel3()) {
						levelKemarin = 3;
						pctNpwpKemarin = tax.getPctNpwpLevel3();
					} else if (pkpKumKemarin > tax.getPkpFromLevel2()) {
						levelKemarin = 2;
						pctNpwpKemarin = tax.getPctNpwpLevel2();
					} else {
						levelKemarin = 1;
						pctNpwpKemarin = tax.getPctNpwpLevel1();
					}
					if (pkpKum > tax.getPkpFromLevel4()) {
						levelNow = 4;
						pctNpwpNow = tax.getPctNpwpLevel4();
						batasPkp = tax.getPkpFromLevel4();
					} else if (pkpKum > tax.getPkpFromLevel3()) {
						levelNow = 3;
						pctNpwpNow = tax.getPctNpwpLevel3();
						batasPkp = tax.getPkpFromLevel3();
					} else if (pkpKum > tax.getPkpFromLevel2()) {
						levelNow = 2;
						pctNpwpNow = tax.getPctNpwpLevel2();
						batasPkp = tax.getPkpFromLevel2();
					} else {
						levelNow = 1;
						pctNpwpNow = tax.getPctNpwpLevel1();
					}
					if (isNpwp) {
						if (levelNow == levelKemarin) {
							pphTerutang = pctNpwpNow * pkp / 100;
						} else {
							if ((levelNow - levelKemarin) == 1) {
								double pkpAtas = pkpKum - batasPkp;
								double pkpBawah = pkp - pkpAtas;
								pphTerutang = (pctNpwpKemarin * pkpBawah / 100) + (pctNpwpNow * pkpAtas / 100);
							} else if ((levelNow - levelKemarin) == 2) {
								long batasPkp2 = 0;
								float pctNpwpTengah = 0;
								if (levelKemarin == 1) {
									batasPkp2 = tax.getPkpFromLevel2();
									pctNpwpTengah = tax.getPctNpwpLevel2();
								} else {
									batasPkp2 = tax.getPkpFromLevel3();
									pctNpwpTengah = tax.getPctNpwpLevel3();
								}
								double pkpAtas = pkpKum - batasPkp;
								double pkpTengah = batasPkp - batasPkp2;
								double pkpBawah = pkp - (pkpAtas + pkpTengah);
								pphTerutang = (pctNpwpKemarin * pkpBawah / 100) + (pctNpwpTengah * pkpTengah / 100) + (pctNpwpNow * pkpAtas / 100);
							}
						}
					} else {
						if (levelNow == levelKemarin) {
							pphTerutang = pctNpwpNow * tax.getPctPenalti() * pkp / 10000; 
						} else {
							if ((levelNow - levelKemarin) == 1) {
								double pkpAtas = pkpKum - batasPkp;
								double pkpBawah = pkp - pkpAtas;
								pphTerutang = (pctNpwpKemarin * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
							} else if ((levelNow - levelKemarin) == 2) {
								long batasPkp2 = 0;
								float pctNpwpTengah = 0;
								if (levelKemarin == 1) {
									batasPkp2 = tax.getPkpFromLevel2();
									pctNpwpTengah = tax.getPctNpwpLevel2();
								} else {
									batasPkp2 = tax.getPkpFromLevel3();
									pctNpwpTengah = tax.getPctNpwpLevel3();
								}
								double pkpAtas = pkpKum - batasPkp;
								double pkpTengah = batasPkp - batasPkp2;
								double pkpBawah = pkp - (pkpAtas + pkpTengah);
								pphTerutang = (pctNpwpKemarin * tax.getPctPenalti() * pkpBawah / 10000) + (pctNpwpTengah * tax.getPctPenalti() * pkpTengah / 10000) + (pctNpwpNow * tax.getPctPenalti() * pkpAtas / 10000);
							}
						}
					}
					bonus.setTax(pphTerutang);
					bonus.setPkpKumulatif(pkpKum);
					baFcd.updateBonus(bonus, dbConn);
					if (sisaPtkpLalu > 0) {
						saldoPtkp.setSisaPtkp(sisaPtkp);
						SaldoPtkpFcd spFcd = new SaldoPtkpFcd();
						spFcd.updateSaldo(saldoPtkp, dbConn);
					}
				} else {
					saldoPtkp.setSisaPtkp(sisaPtkp);
					SaldoPtkpFcd spFcd = new SaldoPtkpFcd();
					spFcd.updateSaldo(saldoPtkp, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void prepareTransfer(DBConnection dbConn) {
		BonusAFcd baFcd = new BonusAFcd();
		try {
			baFcd.insertTransfer(hariHitung.toString("yyyy-MM-dd"), dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void giveBagiHasil(DBConnection dbConn) {
		try {
			SettingBagiHasil settings = loadSettingBagiHasil(dbConn);
			MemberFcd mFcd = new MemberFcd();
			String whereClause = "DATE(reg_date)='" + hariHitung.toString("yyyy-MM-dd") + "' AND jenis_member <> 'Basic'";
			int jmlBaru = mFcd.getTotal(whereClause, dbConn);
			BasicUpgradeFcd upgradeFcd = new BasicUpgradeFcd();
			jmlBaru += upgradeFcd.getTotalUpgradedMember(hariHitung.toDate(), dbConn);
			System.out.println("Member baru: " + jmlBaru);
			double budgetBagiHasil = jmlBaru * settings.getBudgetPerPaket();
			double budgetRbhMicro = budgetBagiHasil>0 ? (settings.getBudgetMicro() * budgetBagiHasil / 100) : 0;
			double budgetRbhMacro = budgetBagiHasil>0 ? (settings.getBudgetMacro() * budgetBagiHasil / 100) : 0;
			System.out.println("Budget RBH Micro="+budgetRbhMicro+"; Macro="+budgetRbhMacro);
			
			whereClause = "tanggal='" + hariHitung.toString("yyyy-MM-dd") + "' AND amt_litfoot >= " + settings.getBintang1MinGkMicro();
			MemberTreeHarianFcd mthFcd = new MemberTreeHarianFcd();
			MemberTreeSet mtSet = mthFcd.search(whereClause, dbConn);
			
			double totalBagianMicro = getTotalBagianBagiHasilMicro(settings, mtSet, dbConn);
			double perbagianMicro = totalBagianMicro>0 ? budgetRbhMicro / totalBagianMicro : 0;
			System.out.println("Total bagian bagi hasil micro ="+totalBagianMicro+"; perbagian Micro = "+perbagianMicro);
			
			double totalBagianMacro = getTotalBagianBagiHasilMacro(settings, mtSet, dbConn);
			double perbagianMacro = totalBagianMacro>0 ? budgetRbhMacro / totalBagianMacro : 0;
			System.out.println("Total bagian bagi hasil macro ="+totalBagianMacro+"; perbagian Macro = "+perbagianMacro);
			
			BonusBagiHasilLog log = BonusBagiHasilLogFcd.getLogByDate(hariHitung.toDate(), dbConn);
			if (log == null) {
				log = new BonusBagiHasilLog();
				log.setTanggal(hariHitung.toDate());
				log.setBudget(budgetBagiHasil);
				log.setBudgetMicro(budgetRbhMicro);
				log.setTotalBagian((int)totalBagianMicro);
				log.setPerbagian(perbagianMicro);
				log.setBudgetMacro(budgetRbhMacro);
				log.setTotalBagianMacro((int)totalBagianMacro);
				log.setPerbagianMacro(perbagianMacro);
				BonusBagiHasilLogFcd logFcd = new BonusBagiHasilLogFcd(log);
				logFcd.insertLogBonus(dbConn);
			} else {
				log.setBudget(budgetBagiHasil);
				log.setBudgetMicro(budgetRbhMicro);
				log.setTotalBagian((int)totalBagianMicro);
				log.setPerbagian(perbagianMicro);
				log.setBudgetMacro(budgetRbhMacro);
				log.setTotalBagianMacro((int)totalBagianMacro);
				log.setPerbagianMacro(perbagianMacro);
				BonusBagiHasilLogFcd logFcd = new BonusBagiHasilLogFcd();
				logFcd.updateLogBonus(log, dbConn);
			}
			
			if (perbagianMicro == 0 && perbagianMacro == 0) return;
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree memberTree = mtSet.get(i);
//				BonusA bonus = BonusAFcd.getBonusByMemberAndDate(memberTree.getMemberId(), hariHitung.toDate(), dbConn);
				int bagianMicro, bagianMacro;
				int bintang;
				
				if (memberTree.getAmtLitfoot() >= settings.getBintang3MinGkMicro()) {
					bintang = 3;
				} else if (memberTree.getAmtLitfoot() >= settings.getBintang2MinGkMicro()) {
					bintang = 2;
				} else if (memberTree.getAmtLitfoot() >= settings.getBintang1MinGkMicro()) {
					bintang = 1;
				} else {
					bintang = 0;
				}
				if (bintang > 0) {
					if (memberTree.getIsBasic() == 0) {
						BonusBagiHasil bagiHasil = new BonusBagiHasil();
						bagiHasil.setMemberId(memberTree.getMemberId());
						bagiHasil.setTanggal(hariHitung.toDate());
						bagiHasil.setLitfoot(memberTree.getAmtLitfoot());
						bagiHasil.setBintang(""+bintang);
						bagianMicro = getBagianBagiHasilMicro(settings, memberTree, bagiHasil, dbConn);
						if (bagianMicro > 0) {
							double bonusBagiHasil = NumberUtil.roundToDecimals(bagianMicro * perbagianMicro, 0);
							bagiHasil.setBagiHasil(bonusBagiHasil);
						}
						
						bagianMacro = getBagianBagiHasilMacro(settings, memberTree, bagiHasil, dbConn);
						if (bagianMacro > 0) {
							double bonusBagiHasilMacro = NumberUtil.roundToDecimals(bagianMacro * perbagianMacro, 0);
							bagiHasil.setBagiHasilMacro(bonusBagiHasilMacro);
						}
						if ((bagianMacro>0) || (bagianMicro>0)) {
							BonusBagiHasilFcd bbhFcd = new BonusBagiHasilFcd(bagiHasil);
							bbhFcd.insertBonus(dbConn);
						}
						bagiHasil = null;
//					} else if (memberTree.getIsBasic() == 1) {
//						BonusBagiHasilTemp bagiHasil = new BonusBagiHasilTemp();
//						bagiHasil.setMemberId(memberTree.getMemberId());
//						bagiHasil.setTanggal(hariHitung.toDate());
//						bagiHasil.setLitfoot(memberTree.getAmtLitfoot());
//						bagiHasil.setBintang(""+bintang);
//						bagianMicro = getBagianBagiHasilTempMicro(settings, memberTree, bagiHasil, dbConn);
//						if (bagianMicro > 0) {
//							double bonusBagiHasil = NumberUtil.roundToDecimals(bagianMicro * perbagianMicro, 0);
//							bagiHasil.setBagiHasil(bonusBagiHasil);
//						}
//						
//						bagianMacro = getBagianBagiHasilTempMacro(settings, memberTree, bagiHasil, dbConn);
//						if (bagianMacro > 0) {
//							double bonusBagiHasilMacro = NumberUtil.roundToDecimals(bagianMacro * perbagianMacro, 0);
//							bagiHasil.setBagiHasilMacro(bonusBagiHasilMacro);
//						}
//						if ((bagianMacro>0) || (bagianMicro>0)) {
//							BonusBagiHasilTempFcd bbhFcd = new BonusBagiHasilTempFcd(bagiHasil);
//							bbhFcd.insertBonus(dbConn);
//						}
//						bagiHasil = null;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private SettingBagiHasil loadSettingBagiHasil(DBConnection dbConn) throws Exception {
		String whereClause = "status='Berlaku' AND start_date < '" + beginOfDay.toString("yyyy-MM-dd") + "' ORDER BY start_date DESC LIMIT 1";
		SettingBagiHasilFcd sbhFcd = new SettingBagiHasilFcd();
		SettingBagiHasilSet sbhSet = sbhFcd.search(whereClause, dbConn);
		if (sbhSet.length() < 1)
			throw new Exception("Belum ada setting bagi hasil yang berlaku.");
		SettingBagiHasil settings = sbhSet.get(0);
		return settings;
	}
	
	private double getTotalBagianBagiHasilMicro(SettingBagiHasil settings, MemberTreeSet mtSet, DBConnection dbConn) throws Exception {
		double totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTree mt = mtSet.get(i);
			int bagian = 0;
			if (mt.getIsBasic() == 0) {
				bagian = getBagianBagiHasilMicro(settings, mt, null, dbConn);
//			} else if (mt.getIsBasic() == 1) {
//				bagian = getBagianBagiHasilTempMicro(settings, mt, null, dbConn);
			}
			System.out.println("Cek Member "+mt.getMemberId()+" dengan kaki kecil sebanyak "+mt.getAmtLitfoot()+" memiliki "+bagian+" bagian micro");
			if (bagian > 0) {
				totalBagian += bagian;
			}
		}
		return totalBagian;
	}
	
	private double getTotalBagianBagiHasilMacro(SettingBagiHasil settings, MemberTreeSet mtSet, DBConnection dbConn) throws Exception {
		double totalBagian = 0;
		for (int i=0; i<mtSet.length(); i++) {
			MemberTree mt = mtSet.get(i);
			int bagian = 0;
			if (mt.getIsBasic() == 0) {
				bagian = getBagianBagiHasilMacro(settings, mt, null, dbConn);
//			} else if (mt.getIsBasic() == 1) {
//				bagian = getBagianBagiHasilTempMacro(settings, mt, null, dbConn);
			}
			System.out.println("Cek Member "+mt.getMemberId()+" dengan kaki kecil sebanyak "+mt.getAmtLitfoot()+" memiliki "+bagian+" bagian macro");
			if (bagian > 0) {
				totalBagian += bagian;
			}
		}
		return totalBagian;
	}
	
	private int getBagianBagiHasilMicro(SettingBagiHasil settings, MemberTree memberTree, BonusBagiHasil bbHasil, DBConnection dbConn) throws Exception {
		int bagian = 0;
		BonusA bonus = BonusAFcd.getBonusByMemberAndDate(memberTree.getMemberId(), hariHitung.toDate(), dbConn);
		MutableDateTime kemarin = hariHitung.copy();
		kemarin.addDays(-1);
		MemberTree mtKemarin = MemberTreeHarianFcd.getMemberTreeByIdAndDate(memberTree.getMemberId(), kemarin.toDate(), dbConn);
		int kakikecil = memberTree.getAmtLitfoot();
		int kakisedang = memberTree.getAmtMidfoot();
		int gsKemarin = mtKemarin != null ? mtKemarin.getAmtMidfoot() : 0;
		int potensiMicro = kakisedang - gsKemarin;
		int microTerjadi = bonus != null ? (int) (bonus.getBonusPasanganSedang() / bonusPasSedang) : 0;
		int deltaMicro = potensiMicro - microTerjadi;
		if (deltaMicro > 0) {
			if (deltaMicro > settings.getMaxPotensiMicro()) {
				deltaMicro = settings.getMaxPotensiMicro();
			}
			if (kakikecil >= settings.getBintang3MinGkMicro()) {
				bagian = settings.getBintang3BagianMicro();
			} else if (kakikecil >= settings.getBintang2MinGkMicro()) {
				bagian = settings.getBintang2BagianMicro();
			} else if (kakikecil >= settings.getBintang1MinGkMicro()) {
				bagian = settings.getBintang1BagianMicro();
			}
			if (bagian > 0) {
				bagian = (deltaMicro / settings.getKelipatanPertumbuhanMicro()) * bagian;
				if (bbHasil != null) {
					bbHasil.setBagian(bagian);
					bbHasil.setPotensiMicro(potensiMicro);
					bbHasil.setMicroTerjadi(microTerjadi);
					bbHasil.setBobotPengaliMicro(deltaMicro / settings.getKelipatanPertumbuhanMicro());
				}
			}
		}
		return bagian;
	}
	
	private int getBagianBagiHasilTempMicro(SettingBagiHasil settings, MemberTree memberTree, BonusBagiHasilTemp bbHasil, DBConnection dbConn) throws Exception {
		int bagian = 0;
		BonusATemp bonus = BonusATempFcd.getBonusByMemberAndDate(memberTree.getMemberId(), hariHitung.toDate(), dbConn);
		MutableDateTime kemarin = hariHitung.copy();
		kemarin.addDays(-1);
		MemberTree mtKemarin = MemberTreeHarianFcd.getMemberTreeByIdAndDate(memberTree.getMemberId(), kemarin.toDate(), dbConn);
		int kakikecil = memberTree.getAmtLitfoot();
		int kakisedang = memberTree.getAmtMidfoot();
		int gsKemarin = mtKemarin != null ? mtKemarin.getAmtMidfoot() : 0;
		int potensiMicro = kakisedang - gsKemarin;
		int microTerjadi = bonus != null ? (int) (bonus.getBonusPasanganSedang() / bonusPasSedang) : 0;
		int deltaMicro = potensiMicro - microTerjadi;
		if (deltaMicro > 0) {
			if (deltaMicro > settings.getMaxPotensiMicro()) {
				deltaMicro = settings.getMaxPotensiMicro();
			}
			if (kakikecil >= settings.getBintang3MinGkMicro()) {
				bagian = settings.getBintang3BagianMicro();
			} else if (kakikecil >= settings.getBintang2MinGkMicro()) {
				bagian = settings.getBintang2BagianMicro();
			} else if (kakikecil >= settings.getBintang1MinGkMicro()) {
				bagian = settings.getBintang1BagianMicro();
			}
			if (bagian > 0) {
				bagian = (deltaMicro / settings.getKelipatanPertumbuhanMicro()) * bagian;
				if (bbHasil != null) {
					bbHasil.setBagian(bagian);
					bbHasil.setPotensiMicro(potensiMicro);
					bbHasil.setMicroTerjadi(microTerjadi);
					bbHasil.setBobotPengaliMicro(deltaMicro / settings.getKelipatanPertumbuhanMicro());
				}
			}
		}
		return bagian;
	}
	
	private int getBagianBagiHasilMacro(SettingBagiHasil settings, MemberTree memberTree, BonusBagiHasil bbHasil, DBConnection dbConn) throws Exception {
		int bagian = 0;
		BonusA bonus = BonusAFcd.getBonusByMemberAndDate(memberTree.getMemberId(), hariHitung.toDate(), dbConn);
		MutableDateTime kemarin = hariHitung.copy();
		kemarin.addDays(-1);
		MemberTree mtKemarin = MemberTreeHarianFcd.getMemberTreeByIdAndDate(memberTree.getMemberId(), kemarin.toDate(), dbConn);
		int kakikecil = memberTree.getAmtLitfoot();
		int gkKemarin = mtKemarin != null ? mtKemarin.getAmtLitfoot() : 0;
		int potensiMacro = kakikecil - gkKemarin;
		int macroTerjadi = bonus != null ? (int) (bonus.getBonusPasanganKecil() / bonusPasKecil) : 0;
		int deltaMacro = potensiMacro - macroTerjadi;
		if (deltaMacro > 0) {
			if (deltaMacro > settings.getMaxPotensiMacro()) {
				deltaMacro = settings.getMaxPotensiMacro();
			}
			if (kakikecil >= settings.getBintang3MinGkMacro()) {
				bagian = settings.getBintang3BagianMacro();
			} else if (kakikecil >= settings.getBintang2MinGkMacro()) {
				bagian = settings.getBintang2BagianMacro();
			} else if (kakikecil >= settings.getBintang1MinGkMacro()) {
				bagian = settings.getBintang1BagianMacro();
			}
			if (bagian > 0) {
				bagian = (deltaMacro / settings.getKelipatanPertumbuhanMacro()) * bagian;
				if (bbHasil != null) {
					bbHasil.setBagianMacro(bagian);
					bbHasil.setPotensiMacro(potensiMacro);
					bbHasil.setMacroTerjadi(macroTerjadi);
					bbHasil.setBobotPengaliMacro(deltaMacro / settings.getKelipatanPertumbuhanMacro());
				}
			}
		}
		return bagian;
	}
	
	private int getBagianBagiHasilTempMacro(SettingBagiHasil settings, MemberTree memberTree, BonusBagiHasilTemp bbHasil, DBConnection dbConn) throws Exception {
		int bagian = 0;
		BonusATemp bonus = BonusATempFcd.getBonusByMemberAndDate(memberTree.getMemberId(), hariHitung.toDate(), dbConn);
		MutableDateTime kemarin = hariHitung.copy();
		kemarin.addDays(-1);
		MemberTree mtKemarin = MemberTreeHarianFcd.getMemberTreeByIdAndDate(memberTree.getMemberId(), kemarin.toDate(), dbConn);
		int kakikecil = memberTree.getAmtLitfoot();
		int gkKemarin = mtKemarin != null ? mtKemarin.getAmtLitfoot() : 0;
		int potensiMacro = kakikecil - gkKemarin;
		int macroTerjadi = bonus != null ? (int) (bonus.getBonusPasanganKecil() / bonusPasKecil) : 0;
		int deltaMacro = potensiMacro - macroTerjadi;
		if (deltaMacro > 0) {
			if (deltaMacro > settings.getMaxPotensiMacro()) {
				deltaMacro = settings.getMaxPotensiMacro();
			}
			if (kakikecil >= settings.getBintang3MinGkMacro()) {
				bagian = settings.getBintang3BagianMacro();
			} else if (kakikecil >= settings.getBintang2MinGkMacro()) {
				bagian = settings.getBintang2BagianMacro();
			} else if (kakikecil >= settings.getBintang1MinGkMacro()) {
				bagian = settings.getBintang1BagianMacro();
			}
			if (bagian > 0) {
				bagian = (deltaMacro / settings.getKelipatanPertumbuhanMacro()) * bagian;
				if (bbHasil != null) {
					bbHasil.setBagianMacro(bagian);
					bbHasil.setPotensiMacro(potensiMacro);
					bbHasil.setMacroTerjadi(macroTerjadi);
					bbHasil.setBobotPengaliMacro(deltaMacro / settings.getKelipatanPertumbuhanMacro());
				}
			}
		}
		return bagian;
	}
	
	/*private double getTotalBagianBagiHasil(SettingBagiHasil settings, BonusASet baSet, DBConnection dbConn) throws Exception {
		double totalBagian = 0;
		for (int i=0; i<baSet.length(); i++) {
			BonusA bonus = baSet.get(i);
			MemberTree memberTree = MemberTreeHarianFcd.getMemberTreeByIdAndDate(bonus.getMemberId(), hariHitung.toDate(), dbConn);
			int bagian;
			if (memberTree.getAmtLitfoot() >= settings.getBintang3MinGk()) {
				bagian = settings.getBintang3Bagian();
			} else if (memberTree.getAmtLitfoot() >= settings.getBintang2MinGk()) {
				bagian = settings.getBintang2Bagian();
			} else if (memberTree.getAmtLitfoot() >= settings.getBintang1MinGk()) {
				bagian = settings.getBintang1Bagian();
			} else {
				bagian = 0;
			}
			System.out.println("Jumlah kaki kecil "+memberTree.getMemberId()+"="+memberTree.getAmtLitfoot()+"; bagian="+bagian);
			if (bagian > 0)
				totalBagian += bagian * bonus.getBonusPasanganSedang() / bonusPasSedang;
		}
		return totalBagian;
	}*/
	
	private double getBatasToleransi(DBConnection dbConn) {
//		String whereClause = "jenis=1 AND DATE(reg_date) = '" + hariHitung.toString("yyyy-MM-dd") + "'";
		String whereClause = "DATE(reg_date) = '" + hariHitung.toString("yyyy-MM-dd") + "'";
		MemberFcd mFcd = new MemberFcd();
		double toleransi = 0;
		try {
			int jmlNonPromo = mFcd.getTotal(whereClause, dbConn);
//			whereClause = "jenis=0 AND DATE(reg_date) = '" + hariHitung.toString("yyyy-MM-dd") + "'";
//			int jmlPromo = mFcd.getTotal(whereClause, dbConn);
//			int jmlAll = jmlNonPromo + jmlPromo;
			int jmlAll = jmlNonPromo;
			//toleransi dari bonus sponsor
//			toleransi += (jmlNonPromo * bonusSponsor) + (jmlPromo * BONUS_SPONSOR_PROMO);
			toleransi += (jmlNonPromo * BONUS_SPONSOR_PROMO);
			
			//toleransi dari bonus pasangan sedang (micro pair)
			toleransi += (jmlAll * bonusPasSedang);
			
			//toleransi dari bonus pasangan kecil (macro pair)
			toleransi += (jmlAll * bonusPasKecil);
			
			//toleransi dari bonus matching
			toleransi += (jmlAll * bonusPasSedang * 0.55);
			return toleransi;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	private double getBudgetBagiHasil(DBConnection dbConn) {
		try {
			BonusA totalBonus = BonusAFcd.getTotalBonusByDate(hariHitung.toDate(), dbConn);
			double totalDibagi = totalBonus.getBonusSponsor() + totalBonus.getBonusPasanganSedang() + totalBonus.getBonusPasanganKecil() + totalBonus.getBonusMatching();
//			System.out.println("Total Bonus="+totalBonus+"; Total dibagi="+totalDibagi);
			System.out.println("Total dibagi="+totalDibagi);
			double toleransi = getBatasToleransi(dbConn);
			double budget = toleransi - totalDibagi;
			System.out.println("Batas Toleransi="+toleransi+"; Budget="+budget);
			return budget;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public void addBonusTempToTransfer(DBConnection dbConn) {
		BasicUpgradeFcd upgradeFcd = new BasicUpgradeFcd();
		String whereClause = "upgrade_date LIKE '" + hariHitung.toString("yyyy-MM-dd") + "%'";
		try {
			BasicUpgradeSet upgradeSet = upgradeFcd.search(whereClause, dbConn);
			for (int i=0; i<upgradeSet.length(); i++) {
				BasicUpgrade upgrade = upgradeSet.get(i);
				String memberId = upgrade.getMemberId();
				Member memberUpgraded = MemberFcd.getMemberById(memberId, dbConn);
				BonusTransferFcd transferFcd = new BonusTransferFcd();
				BonusTransfer transfer;
//				BonusTransfer transfer = transferFcd.getBonusTransferHarianByMemberDate(memberUpgraded.getSponsor(), hariHitung.toDate(), dbConn);
//				if (transfer != null) {
//					System.out.println("Member "+transfer.getMemberId()+" sebelumnya ada "+transfer.getAmount()+" dapat pending sponsor "+bonusSponsor);
//					transfer.setAmount(transfer.getAmount() + bonusSponsor);
//					transferFcd.updateBonusTransfer(transfer, dbConn);
//				} else {
//					Member sponsor = MemberFcd.getMemberById(memberUpgraded.getSponsor(), dbConn);
//					System.out.println("Member "+sponsor.getMemberId()+" dapat pendingan sponsor "+bonusSponsor);
//					transfer = new BonusTransfer();
//					transfer.setTanggal(hariHitung.toDate());
//					transfer.setMemberId(sponsor.getMemberId());
//					transfer.setNoRekening(sponsor.getNoRekening());
//					String namabank = "";
//					if ("0".equals(sponsor.getBank())) {
//						namabank = "";
//					} else if ("1".equals(sponsor.getBank())) {
//						namabank = "Mandiri";
//					} else if ("2".equals(sponsor.getBank())) {
//						namabank = "BCA";
//					} else if ("3".equals(sponsor.getBank())) {
//						namabank = "BNI";
//					} else if ("4".equals(sponsor.getBank())) {
//						namabank = "BRI";
//					} else if ("5".equals(sponsor.getBank())) {
//						namabank = "CIMB Niaga";
//					} else {
//						namabank = "Lainnya";
//					}
//					transfer.setNamaBank(namabank);
//					transfer.setAmount(bonusSponsor);
//					transferFcd.insertBonusTransfer(transfer, dbConn);
//				}
				transfer = null;
				double totalBonus = 0;
				LogBonusATempFcd logTempFcd = new LogBonusATempFcd();
				whereClause = "member_id = '" + memberId + "' AND is_processed = 0";
				LogBonusATempSet logTempSet = logTempFcd.search(whereClause, dbConn);
				for (int j=0; j<logTempSet.length(); j++) {
					LogBonusATemp logTemp = logTempSet.get(j);
					MemberTree mtFromMember = MemberTreeHarianFcd.getMemberTreeByIdAndDate(logTemp.getFromMember(), hariHitung.toDate(), dbConn);
					if (mtFromMember.getIsBasic() == 0) {
						System.out.println("Total Bonus " + logTemp.getMemberId() + " bertambah " + logTemp.getAmount());
						totalBonus += logTemp.getAmount();
						logTemp.setIsProcessed(1);
						logTempFcd.updateLogTemp(logTemp, dbConn);
					}
				}
				logTempSet = null;
				
//				whereClause = "member_id = '" + memberId + "'";
//				BonusBagiHasilTempFcd bbhtFcd = new BonusBagiHasilTempFcd();
//				BonusBagiHasilTempSet bbhtSet = bbhtFcd.search(whereClause, dbConn);
//				for (int j=0; j<bbhtSet.length(); j++) {
//					BonusBagiHasilTemp bbhTemp = bbhtSet.get(j);
//					totalBonus += bbhTemp.getBagiHasil() + bbhTemp.getBagiHasilMacro();
//				}
				
				transfer = transferFcd.getBonusTransferHarianByMemberDate(memberId, hariHitung.toDate(), dbConn);
				if (totalBonus > 0) {
					if (transfer != null) {
						System.out.println("Member "+memberId+" sebelumnya di bonus transfer ada "+transfer.getAmount()+" ditambah "+totalBonus);
						transfer.setAmount(transfer.getAmount() + totalBonus);
						transferFcd.updateBonusTransfer(transfer, dbConn);
					} else {
						System.out.println("Member "+memberId+" jadi ada bonus transfer "+totalBonus);
						Member member = MemberFcd.getMemberById(memberId, dbConn);
						transfer = new BonusTransfer();
						transfer.setTanggal(hariHitung.toDate());
						transfer.setMemberId(memberId);
						transfer.setNoRekening(member.getNoRekening());
						String namabank = "";
						if ("0".equals(member.getBank())) {
							namabank = "";
						} else if ("1".equals(member.getBank())) {
							namabank = "Mandiri";
						} else if ("2".equals(member.getBank())) {
							namabank = "BCA";
						} else if ("3".equals(member.getBank())) {
							namabank = "BNI";
						} else if ("4".equals(member.getBank())) {
							namabank = "BRI";
						} else if ("5".equals(member.getBank())) {
							namabank = "CIMB Niaga";
						} else {
							namabank = "Lainnya";
						}
						transfer.setNamaBank(namabank);
						transfer.setAmount(totalBonus);
						transferFcd.insertBonusTransfer(transfer, dbConn);
					}
				}
				transfer = null;
				
				whereClause = "from_member = '" + memberId + "' AND is_processed = 0";
				logTempSet = logTempFcd.searchGrouping(whereClause, dbConn);
				for (int j=0; j<logTempSet.length(); j++) {
					LogBonusATemp logTemp = logTempSet.get(j);
					MemberTree memberBonus = MemberTreeHarianFcd.getMemberTreeByIdAndDate(logTemp.getMemberId(), hariHitung.toDate(), dbConn);
					if (memberBonus.getIsBasic() == 0) {
						transfer = transferFcd.getBonusTransferHarianByMemberDate(memberBonus.getMemberId(), hariHitung.toDate(), dbConn);
						logTemp.setIsProcessed(1);
						logTempFcd.updateLogTempGrouping(logTemp, dbConn);
						if (transfer != null) {
							System.out.println("Member "+memberBonus.getMemberId()+" sebelumnya punya "+transfer.getAmount()+" ditambah "+logTemp.getAmount());
							transfer.setAmount(transfer.getAmount() + logTemp.getAmount());
							transferFcd.updateBonusTransfer(transfer, dbConn);
						} else {
							transfer = new BonusTransfer();
							System.out.println("Member "+memberBonus.getMemberId()+" dapat bonus dari pending "+logTemp.getAmount());
							Member member = MemberFcd.getMemberById(memberBonus.getMemberId(), dbConn);
							transfer.setTanggal(hariHitung.toDate());
							transfer.setMemberId(member.getMemberId());
							transfer.setNoRekening(member.getNoRekening());
							String namabank = "";
							if ("0".equals(member.getBank())) {
								namabank = "";
							} else if ("1".equals(member.getBank())) {
								namabank = "Mandiri";
							} else if ("2".equals(member.getBank())) {
								namabank = "BCA";
							} else if ("3".equals(member.getBank())) {
								namabank = "BNI";
							} else if ("4".equals(member.getBank())) {
								namabank = "BRI";
							} else if ("5".equals(member.getBank())) {
								namabank = "CIMB Niaga";
							} else {
								namabank = "Lainnya";
							}
							transfer.setNamaBank(namabank);
							transfer.setAmount(logTemp.getAmount());
							transferFcd.insertBonusTransfer(transfer, dbConn);
						}
						transfer = null;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
