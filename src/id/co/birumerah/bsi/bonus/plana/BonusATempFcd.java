package id.co.birumerah.bsi.bonus.plana;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusATempFcd {

	private BonusATemp bonusATemp;
	
	public BonusATempFcd() {}

	public BonusATempFcd(BonusATemp bonusATemp) {
		this.bonusATemp = bonusATemp;
	}

	public BonusATemp getBonusATemp() {
		return bonusATemp;
	}

	public void setBonusATemp(BonusATemp bonusATemp) {
		this.bonusATemp = bonusATemp;
	}
	
	public BonusATempSet search(String whereClause, DBConnection dbConn) throws Exception {
		BonusATempSet batSet = null;
		
		try {
			BonusATempDAO batDao = new BonusATempDAO(dbConn);
			batSet = batDao.select(whereClause);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return batSet;
	}
	
	public static BonusATemp getBonusByMemberAndDate(String memberId, Date tanggal, DBConnection dbConn) throws Exception {
		try {
			BonusATemp bonus = new BonusATemp();
			bonus.setMemberId(memberId);
			bonus.setTanggal(tanggal);
			
			BonusATempDAO bonusDao = new BonusATempDAO(dbConn);
			boolean found = bonusDao.select(bonus);
			
			if (found) {
				return bonus;
			} else {
				System.err.println("Bonus hari ini tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertBonus(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusATempDAO bonusDao = new BonusATempDAO(dbConn);
			bonusDao.insert(bonusATemp);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateBonus(BonusATemp bonus, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusATempDAO bonusDao = new BonusATempDAO(dbConn);
			if (bonusDao.update(bonus) < 1) {
				throw new Exception("Update BonusATemp failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteBonusByMember(String memberId, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			BonusATempDAO bonusDao = new BonusATempDAO(dbConn);
			if (bonusDao.deleteByMember(memberId) < 1) {
				throw new Exception("Delete bonus member " + memberId + " gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
