package id.co.birumerah.bsi.bonus;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class TaxRateFcd {

	private TaxRate taxRate;
	
	public TaxRateFcd() {}

	public TaxRateFcd(TaxRate taxRate) {
		this.taxRate = taxRate;
	}

	public TaxRate getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(TaxRate taxRate) {
		this.taxRate = taxRate;
	}
	
	public TaxRateSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		TaxRateSet trSet = null;
		
		try {
			dbConn = new DBConnection();
			TaxRateDAO trDAO = new TaxRateDAO(dbConn);
			trSet = trDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return trSet;
	}
	
	public TaxRateSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		TaxRateSet trSet = null;
		
		try {
//			dbConn = new DBConnection();
			TaxRateDAO trDAO = new TaxRateDAO(dbConn);
			trSet = trDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return trSet;
	}
}
