package id.co.birumerah.bsi.bonus;

import java.util.Date;

public class TaxRate {

	private int rateId;
	private int ptkp;
	private long pkpFromLevel2;
	private long pkpFromLevel3;
	private long pkpFromLevel4;
	private float pctNpwpLevel1;
	private float pctNpwpLevel2;
	private float pctNpwpLevel3;
	private float pctNpwpLevel4;
	private float pctPenalti;
	private float pctNonnpwp;
	private Date startDate;
	private String status;
	
	public TaxRate() {}

	public TaxRate(int rateId, int ptkp, long pkpFromLevel2,
			long pkpFromLevel3, long pkpFromLevel4, float pctNpwpLevel1,
			float pctNpwpLevel2, float pctNpwpLevel3, float pctNpwpLevel4,
			float pctPenalti, float pctNonnpwp, Date startDate, String status) {
		this.rateId = rateId;
		this.ptkp = ptkp;
		this.pkpFromLevel2 = pkpFromLevel2;
		this.pkpFromLevel3 = pkpFromLevel3;
		this.pkpFromLevel4 = pkpFromLevel4;
		this.pctNpwpLevel1 = pctNpwpLevel1;
		this.pctNpwpLevel2 = pctNpwpLevel2;
		this.pctNpwpLevel3 = pctNpwpLevel3;
		this.pctNpwpLevel4 = pctNpwpLevel4;
		this.pctPenalti = pctPenalti;
		this.pctNonnpwp = pctNonnpwp;
		this.startDate = startDate;
		this.status = status;
	}

	public int getRateId() {
		return rateId;
	}

	public void setRateId(int rateId) {
		this.rateId = rateId;
	}

	public int getPtkp() {
		return ptkp;
	}

	public void setPtkp(int ptkp) {
		this.ptkp = ptkp;
	}

	public long getPkpFromLevel2() {
		return pkpFromLevel2;
	}

	public void setPkpFromLevel2(long pkpFromLevel2) {
		this.pkpFromLevel2 = pkpFromLevel2;
	}

	public long getPkpFromLevel3() {
		return pkpFromLevel3;
	}

	public void setPkpFromLevel3(long pkpFromLevel3) {
		this.pkpFromLevel3 = pkpFromLevel3;
	}

	public long getPkpFromLevel4() {
		return pkpFromLevel4;
	}

	public void setPkpFromLevel4(long pkpFromLevel4) {
		this.pkpFromLevel4 = pkpFromLevel4;
	}

	public float getPctNpwpLevel1() {
		return pctNpwpLevel1;
	}

	public void setPctNpwpLevel1(float pctNpwpLevel1) {
		this.pctNpwpLevel1 = pctNpwpLevel1;
	}

	public float getPctNpwpLevel2() {
		return pctNpwpLevel2;
	}

	public void setPctNpwpLevel2(float pctNpwpLevel2) {
		this.pctNpwpLevel2 = pctNpwpLevel2;
	}

	public float getPctNpwpLevel3() {
		return pctNpwpLevel3;
	}

	public void setPctNpwpLevel3(float pctNpwpLevel3) {
		this.pctNpwpLevel3 = pctNpwpLevel3;
	}

	public float getPctNpwpLevel4() {
		return pctNpwpLevel4;
	}

	public void setPctNpwpLevel4(float pctNpwpLevel4) {
		this.pctNpwpLevel4 = pctNpwpLevel4;
	}

	public float getPctPenalti() {
		return pctPenalti;
	}

	public void setPctPenalti(float pctPenalti) {
		this.pctPenalti = pctPenalti;
	}

	public float getPctNonnpwp() {
		return pctNonnpwp;
	}

	public void setPctNonnpwp(float pctNonnpwp) {
		this.pctNonnpwp = pctNonnpwp;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
