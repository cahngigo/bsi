package id.co.birumerah.bsi.bonus;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class SaldoPtkpFcd {

	private SaldoPtkp saldoPtkp;
	
	public SaldoPtkpFcd() {}

	public SaldoPtkpFcd(SaldoPtkp saldoPtkp) {
		this.saldoPtkp = saldoPtkp;
	}

	public SaldoPtkp getSaldoPtkp() {
		return saldoPtkp;
	}

	public void setSaldoPtkp(SaldoPtkp saldoPtkp) {
		this.saldoPtkp = saldoPtkp;
	}
	
	public static SaldoPtkp getSaldoPtkpByMemberId(String memberId) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			SaldoPtkp saldo = new SaldoPtkp();
			saldo.setMemberId(memberId);
			
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			boolean found = saldoDAO.select(saldo);
			
			if (found) {
				return saldo;
			} else {
				System.err.println("Saldo PTKP tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static SaldoPtkp getSaldoPtkpByMemberId(String memberId, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			SaldoPtkp saldo = new SaldoPtkp();
			saldo.setMemberId(memberId);
			
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			boolean found = saldoDAO.select(saldo);
			
			if (found) {
				return saldo;
			} else {
				System.err.println("Saldo PTKP tidak ditemukan untuk member ybs.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public void insertSaldo() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			saldoDAO.insert(saldoPtkp);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertSaldo(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			saldoDAO.insert(saldoPtkp);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void updateSaldo(SaldoPtkp saldo) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			if (saldoDAO.update(saldo) < 1) {
				throw new Exception("Saldo not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateSaldo(SaldoPtkp saldo, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			if (saldoDAO.update(saldo) < 1) {
				throw new Exception("Saldo not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void resetSisaPtkp(int ptkp) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			if (saldoDAO.resetSisaPtkp(ptkp) < 0) {
				throw new Exception("Reset saldo ptkp gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void resetSisaPtkp(int ptkp, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			if (saldoDAO.resetSisaPtkp(ptkp) < 0) {
				throw new Exception("Reset saldo ptkp gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void deleteByRegDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			SaldoPtkpDAO saldoDAO = new SaldoPtkpDAO(dbConn);
			if (saldoDAO.deleteByRegDate(tanggal) < 0) {
				throw new Exception("Delete saldo ptkp gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
