package id.co.birumerah.bsi.bonus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class SaldoPtkpDAO {

	DBConnection dbConn;
	
	public SaldoPtkpDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}
	
	public int insert(SaldoPtkp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO saldo_ptkp (");
		sb.append("member_id,");
		sb.append("sisa_ptkp");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("" + dataObject.getSisaPtkp());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(SaldoPtkp dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE saldo_ptkp ");
		sb.append("SET ");
		sb.append("sisa_ptkp = " + dataObject.getSisaPtkp() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(SaldoPtkp dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("sisa_ptkp ");
		sb.append("FROM saldo_ptkp ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setSisaPtkp(rs.getInt("sisa_ptkp"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int resetSisaPtkp(int ptkp) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE saldo_ptkp SET sisa_ptkp = " + ptkp);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int deleteByRegDate(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		DateFormat shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		sb.append("DELETE FROM saldo_ptkp WHERE member_id in (");
		sb.append("SELECT member_id FROM member WHERE reg_date LIKE '" + shortFormat.format(tanggal) + "%')");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
