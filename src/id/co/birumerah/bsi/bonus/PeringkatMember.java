package id.co.birumerah.bsi.bonus;

public class PeringkatMember {

	private int tingkat;
	private String peringkat;
	private String singkatan;
	private int minPvgk;
	
	public PeringkatMember() {}

	public PeringkatMember(int tingkat, String peringkat, String singkatan,
			int minPvgk) {
		this.tingkat = tingkat;
		this.peringkat = peringkat;
		this.singkatan = singkatan;
		this.minPvgk = minPvgk;
	}

	public int getTingkat() {
		return tingkat;
	}

	public void setTingkat(int tingkat) {
		this.tingkat = tingkat;
	}

	public String getPeringkat() {
		return peringkat;
	}

	public void setPeringkat(String peringkat) {
		this.peringkat = peringkat;
	}

	public String getSingkatan() {
		return singkatan;
	}

	public void setSingkatan(String singkatan) {
		this.singkatan = singkatan;
	}

	public int getMinPvgk() {
		return minPvgk;
	}

	public void setMinPvgk(int minPvgk) {
		this.minPvgk = minPvgk;
	}
}
