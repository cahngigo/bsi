package id.co.birumerah.bsi.bonus;

public class PeringkatCounter {

	private String memberId;
	private int lastTingkat;
	private int tingkatRepetitif;
	private int jmlRepetitif;
	private int tingkatAchieved;
	
	public PeringkatCounter() {}

	public PeringkatCounter(String memberId, int lastTingkat,
			int tingkatRepetitif, int jmlRepetitif, int tingkatAchieved) {
		this.memberId = memberId;
		this.lastTingkat = lastTingkat;
		this.tingkatRepetitif = tingkatRepetitif;
		this.jmlRepetitif = jmlRepetitif;
		this.tingkatAchieved = tingkatAchieved;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public int getLastTingkat() {
		return lastTingkat;
	}

	public void setLastTingkat(int lastTingkat) {
		this.lastTingkat = lastTingkat;
	}

	public int getTingkatRepetitif() {
		return tingkatRepetitif;
	}

	public void setTingkatRepetitif(int tingkatRepetitif) {
		this.tingkatRepetitif = tingkatRepetitif;
	}

	public int getJmlRepetitif() {
		return jmlRepetitif;
	}

	public void setJmlRepetitif(int jmlRepetitif) {
		this.jmlRepetitif = jmlRepetitif;
	}

	public int getTingkatAchieved() {
		return tingkatAchieved;
	}

	public void setTingkatAchieved(int tingkatAchieved) {
		this.tingkatAchieved = tingkatAchieved;
	}
}
