package id.co.birumerah.bsi.bonus;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class PeringkatCounterFcd {

	private PeringkatCounter peringkatCounter;
	
	public PeringkatCounterFcd() {}

	public PeringkatCounterFcd(PeringkatCounter peringkatCounter) {
		this.peringkatCounter = peringkatCounter;
	}

	public PeringkatCounter getPeringkatCounter() {
		return peringkatCounter;
	}

	public void setPeringkatCounter(PeringkatCounter peringkatCounter) {
		this.peringkatCounter = peringkatCounter;
	}
	
	public static PeringkatCounter getCounterByMemberId(String memberId) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			PeringkatCounter pCounter = new PeringkatCounter();
			pCounter.setMemberId(memberId);
			
			PeringkatCounterDAO pcDAO = new PeringkatCounterDAO(dbConn);
			boolean found = pcDAO.select(pCounter);
			
			if (found) {
				return pCounter;
			} else {
				System.err.println("Peringkat Counter not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static PeringkatCounter getCounterByMemberId(String memberId, DBConnection dbConn) throws Exception {
		try {
			PeringkatCounter pCounter = new PeringkatCounter();
			pCounter.setMemberId(memberId);
			
			PeringkatCounterDAO pcDAO = new PeringkatCounterDAO(dbConn);
			boolean found = pcDAO.select(pCounter);
			
			if (found) {
				return pCounter;
			} else {
				System.err.println("Peringkat Counter not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void insertPeringkatCounter() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			PeringkatCounterDAO pcDAO = new PeringkatCounterDAO(dbConn);
			pcDAO.insert(peringkatCounter);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void insertPeringkatCounter(DBConnection dbConn) throws Exception {
		 try {
			dbConn.beginTransaction();
			PeringkatCounterDAO pcDAO = new PeringkatCounterDAO(dbConn);
			pcDAO.insert(peringkatCounter);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updatePeringkatCounter(PeringkatCounter peringkatCounter) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			PeringkatCounterDAO pcDAO = new PeringkatCounterDAO(dbConn);
			if (pcDAO.update(peringkatCounter) < 1) {
				throw new Exception("Peringkat Counter not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updatePeringkatCounter(PeringkatCounter peringkatCounter, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			PeringkatCounterDAO pcDAO = new PeringkatCounterDAO(dbConn);
			if (pcDAO.update(peringkatCounter) < 1) {
				throw new Exception("Peringkat Counter not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
