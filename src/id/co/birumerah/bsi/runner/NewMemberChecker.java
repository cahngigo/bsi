package id.co.birumerah.bsi.runner;

import java.sql.SQLException;
import java.util.Date;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.bonus.SaldoPtkp;
import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.bonus.plana.BonusCounter;
import id.co.birumerah.bsi.flag.FlagRun;
import id.co.birumerah.bsi.flag.FlagRunFcd;
import id.co.birumerah.bsi.member.Member;
import id.co.birumerah.bsi.member.MemberFcd;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.mail.SendMail;

public class NewMemberChecker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		try {
			dbConn = new DBConnection();
			FlagRun flag = FlagRunFcd.getFlag(dbConn);
			boolean hasNull = mtFcd.hasNullMember(dbConn);
			if (hasNull) {
				System.err.println("Ada member yang null");
				String isiMail = "Ada member yang null ketika menjalankan NewMemberChecker.";
				SendMail sendMail = new SendMail(isiMail);
				sendMail.send("confirmation");
			} else {
				if (flag.getIsRun() == 0) {
					flag.setIsRun(1);
					FlagRunFcd frFcd = new FlagRunFcd();
					frFcd.updateFlag(flag, dbConn);

					//				String whereClause = "status=0 ORDER BY sort_number limit 1";
					String whereClause = "status=0 ORDER BY sort_number";
					MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
					for (int i=0; i<mtSet.length(); i++) {
						MemberTree member = mtSet.get(i);
						//					MemberTree member = mtSet.get(0);
						System.out.println("===== Penambahan Member "+member.getMemberId()+"=====");
						member.setStatus(1);
						mtFcd.updateMemberTree(member, dbConn);
						BonusCounter bc = new BonusCounter(false, dbConn);
						MemberTree upline = mtFcd.getMemberTreeById(member.getUplineId(), dbConn);
						Member anggota = MemberFcd.getMemberById(member.getMemberId(), dbConn);
						Date regDate = anggota.getRegDate();
						long sortNumber = member.getSortNumber();
						boolean isBasic = (member.getIsBasic() == 1 || member.getIsBasicNew() == 1);
						bc.giveBonusPasangan(member, member.getUplineId(), dbConn, sortNumber, regDate, isBasic);
					}

					flag.setIsRun(0);
					frFcd.updateFlag(flag, dbConn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
