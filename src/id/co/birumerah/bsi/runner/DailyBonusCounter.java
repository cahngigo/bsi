package id.co.birumerah.bsi.runner;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import id.co.birumerah.bsi.bonus.plana.BonusAFcd;
import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;
import id.co.birumerah.bsi.flag.FlagBonusHarian;
import id.co.birumerah.bsi.flag.FlagBonusHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.mail.SendMail;

import org.joda.time.MutableDateTime;

public class DailyBonusCounter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MutableDateTime bod = new MutableDateTime();
		bod.setHourOfDay(0);
		bod.setMinuteOfHour(0);
		bod.setSecondOfMinute(0);
		MutableDateTime hariHitung = bod.copy();
		hariHitung.addDays(-1);
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			FlagBonusHarian flag = FlagBonusHarianFcd.getFlag(dbConn);
			DateFormat shortFormat = new SimpleDateFormat("yyyy-MM-dd");
			MemberTreeHarianFcd mthFcd = new MemberTreeHarianFcd();
			boolean memberHasNull = mthFcd.hasNullMember(hariHitung.toDate(), dbConn);
			BonusAFcd bonusFcd = new BonusAFcd();
			boolean bonusHasNull = bonusFcd.hasNullMember(hariHitung.toDate(), dbConn);
			int total = mthFcd.countTotal(dbConn);
			if (memberHasNull || bonusHasNull) {
				System.err.println("Ada member yang null.");
				String isiMail = "Ada member yang null ketika menjalankan DailyBonusCounter.";
				SendMail sendMail = new SendMail(isiMail);
				sendMail.send("confirmation");
			}
			else if (total > 0) {
				System.err.println("Ada member yang memiliki status 0 (belum dihitung bonus pasangannya).");
			}
			else {
				if (shortFormat.format(flag.getLastDate()).equalsIgnoreCase(hariHitung.toString("yyyy-MM-dd"))) {
					System.err.println("Bonus tanggal " + hariHitung.toString("dd-MM-yyyy") + " sudah dihitung.");
				} else {
					String whereClause = "status=0 AND member_id IN (SELECT member_id FROM member WHERE reg_date LIKE '" + hariHitung.toString("yyyy-MM-dd") + "%')";
					MemberTreeFcd mtFcd = new MemberTreeFcd();
					MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
					if (mtSet.length() < 1) {
						flag.setLastDate(hariHitung.toDate());
						flag.setHasDone(0);
						FlagBonusHarianFcd flagFcd = new FlagBonusHarianFcd();
						flagFcd.updateFlag(flag, dbConn);
	
						BonusCounterCaller bcc = new BonusCounterCaller(bod, true);
						bcc.dailyBonusCounter(true);
	
						flag.setLastDate(hariHitung.toDate());
						flag.setHasDone(1);
						flagFcd.updateFlag(flag, dbConn);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
