package id.co.birumerah.bsi.runner;

import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;

import org.joda.time.MutableDateTime;

public class MemberTreeUpdateNonbasic {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MutableDateTime tglTrx = new MutableDateTime();
		BonusCounterCaller bcc = new BonusCounterCaller(tglTrx);
		bcc.updateNonbasicTree();
	}

}
