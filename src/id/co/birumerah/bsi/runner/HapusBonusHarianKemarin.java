package id.co.birumerah.bsi.runner;

import id.co.birumerah.bsi.bonus.PoinRecord;
import id.co.birumerah.bsi.bonus.PoinRecordFcd;
import id.co.birumerah.bsi.bonus.PoinRecordSet;
import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.plana.BonusAFcd;
import id.co.birumerah.bsi.bonus.plana.BonusBagiHasil;
import id.co.birumerah.bsi.bonus.plana.BonusBagiHasilFcd;
import id.co.birumerah.bsi.bonus.plana.BonusBagiHasilLog;
import id.co.birumerah.bsi.bonus.plana.BonusBagiHasilLogFcd;
import id.co.birumerah.bsi.bonus.plana.LogBonusAFcd;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.util.dbaccess.DBConnection;

import org.joda.time.MutableDateTime;

public class HapusBonusHarianKemarin {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MutableDateTime callDate = new MutableDateTime();
		callDate.setHourOfDay(0);
		callDate.setMinuteOfHour(0);
		callDate.setSecondOfMinute(0);
		MutableDateTime hariHitung = callDate.copy();
		hariHitung.addDays(-1);
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			
			BonusAFcd baFcd = new BonusAFcd();
			baFcd.deleteBonusByDate(hariHitung.toDate(), dbConn);
			
			MemberTreeHarianFcd mthFcd = new MemberTreeHarianFcd();
			mthFcd.deleteByLogDate(hariHitung.toDate(), dbConn);
			
			SaldoPtkpFcd saldoFcd = new SaldoPtkpFcd();
			saldoFcd.deleteByRegDate(hariHitung.toDate(), dbConn);
			
			MemberTreeFcd mtFcd = new MemberTreeFcd();
			mtFcd.updateStatus(hariHitung.toDate(), dbConn);
			
			baFcd.deleteTransferByDate(hariHitung.toDate(), dbConn);
			
			LogBonusAFcd logFcd = new LogBonusAFcd();
			logFcd.deleteLogByDate(hariHitung.toDate(), dbConn);
			logFcd.deleteSaldoBonusByDate(hariHitung.toDate(), dbConn);
			
			String whereClause = "tanggal = '" + hariHitung.toString("yyyy-MM-dd") + "'";
			PoinRecordFcd prFcd = new PoinRecordFcd();
			PoinRecordSet prSet = prFcd.search(whereClause, dbConn);
			for (int i=0; i<prSet.length(); i++) {
				PoinRecord pRecord = prSet.get(i);
				MemberTree mt = MemberTreeFcd.getMemberTreeById(pRecord.getMemberId(), dbConn);
				mt.setPvpribadi(mt.getPvpribadi() - pRecord.getPoin());
				mt.setPotonganAm(mt.getPotonganAm() - pRecord.getAm());
				mt.setSaldoAm(mt.getSaldoAm() - pRecord.getAm());
				mt.setSaldoBv(mt.getSaldoBv() - pRecord.getBv());
				
				mtFcd.updateAm(mt, dbConn);
			}
			
			BonusBagiHasilFcd bbhFcd = new BonusBagiHasilFcd();
			BonusBagiHasil bbHasil = new BonusBagiHasil();
			bbHasil.setTanggal(hariHitung.toDate());
			bbhFcd.deleteByDate(bbHasil, dbConn);
			
			BonusBagiHasilLogFcd bbhlFcd = new BonusBagiHasilLogFcd();
			BonusBagiHasilLog bbhLog = new BonusBagiHasilLog();
			bbhLog.setTanggal(hariHitung.toDate());
			bbhlFcd.deleteByDate(bbhLog, dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
