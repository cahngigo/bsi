package id.co.birumerah.bsi.runner;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.bonus.planb.BonusBCounter;
import id.co.birumerah.bsi.flag.FlagBonusBulanan;
import id.co.birumerah.bsi.flag.FlagBonusBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeSmallBulananFcd;
import id.co.birumerah.util.dbaccess.DBConnection;

public class MonthlyBonusCounter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MutableDateTime bulanHitung = new MutableDateTime();
			bulanHitung.setDayOfMonth(1);
			bulanHitung.addMonths(-1);
			FlagBonusBulanan flag = FlagBonusBulananFcd.getFlag(dbConn);
			DateFormat shortFormat = new SimpleDateFormat("yyyy-MM-dd");
			if (shortFormat.format(flag.getLastMonth()).equalsIgnoreCase(bulanHitung.toString("yyyy-MM-dd"))) {
				System.err.println("Bonus tanggal " + bulanHitung.toString("dd-MM-yyyy") + " sudah dihitung.");
			} else {
				MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
				int jumlah = mtFcd.countTotal(bulanHitung.toDate(), dbConn);
				if (jumlah > 0) {
					flag.setLastMonth(bulanHitung.toDate());
					flag.setHasDone(0);
					FlagBonusBulananFcd flagFcd = new FlagBonusBulananFcd();
					flagFcd.updateFlag(flag, dbConn);
					
					BonusBCounter bonus = new BonusBCounter(dbConn);
					bonus.giveBonusRO(dbConn);
					bonus.giveBonusIG(dbConn);
					bonus.giveBonusUL(dbConn);
					bonus.giveBonusEB(dbConn);
//					bonus.giveBonusOR(dbConn);
					bonus.giveBonusRC(dbConn);
					bonus.giveBonusWT(dbConn);
					bonus.giveLdp(dbConn);
					bonus.countPbr(dbConn);
					bonus.potongPajak(dbConn);
					System.out.println("=== Pengecekan peringkat member ===");
					bonus.getPeringkatMember(dbConn);
					bonus.prepareTransfer(dbConn);
					
					flag.setLastMonth(bulanHitung.toDate());
					flag.setHasDone(1);
					flagFcd.updateFlag(flag, dbConn);
				}
			}
			
			
			// untuk me-reset PTKP per bulan
//			MutableDateTime tglTrx = new MutableDateTime();
//			if (tglTrx.getDayOfMonth() == 1) {
//				String whereClause = "start_date < '" + tglTrx.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
//				TaxRateFcd trFcd = new TaxRateFcd();
//				TaxRateSet trSet = trFcd.search(whereClause);
//				if (trSet.length() < 1)
//					throw new Exception("Belum ada setting Rate Pajak yang berlaku.");
//				TaxRate tax = trSet.get(0);
//				SaldoPtkpFcd spFcd = new SaldoPtkpFcd();
//				spFcd.resetSisaPtkp(tax.getPtkp(), dbConn);
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
