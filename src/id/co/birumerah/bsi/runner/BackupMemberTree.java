package id.co.birumerah.bsi.runner;

import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.util.dbaccess.DBConnection;

import org.joda.time.MutableDateTime;

public class BackupMemberTree {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MutableDateTime bod = new MutableDateTime();
		bod.setHourOfDay(0);
		bod.setMinuteOfHour(0);
		bod.setSecondOfMinute(0);
		
//		BonusCounterCaller bcc = new BonusCounterCaller(bod, true);
		MutableDateTime hariHitung = bod.copy();
		hariHitung.addDays(-1);
		MemberTreeFcd mFcd = new MemberTreeFcd();
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			mFcd.setTanggal(hariHitung, dbConn);
			mFcd.dailyBackup(bod, dbConn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
