package id.co.birumerah.bsi.runner;

import java.sql.SQLException;

import id.co.birumerah.bsi.bonus.stockist.BonusCounterSc;
import id.co.birumerah.util.dbaccess.DBConnection;

public class ScBonusCounter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusCounterSc bcs = new BonusCounterSc(dbConn);
			bcs.giveBonusKso(dbConn);
			bcs.giveBonusKpo(dbConn);
			bcs.giveBonusKsl(dbConn);
			bcs.giveBonusLpku(dbConn);
			bcs.giveBonusTop5(dbConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
		
	}

}
