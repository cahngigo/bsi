package id.co.birumerah.bsi.runner;

import java.sql.SQLException;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.member.PoinCounterMonthly;
import id.co.birumerah.util.dbaccess.DBConnection;

public class MonthlyHitungAkumulasiPoinTour {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MutableDateTime bulanHitung = new MutableDateTime();
			bulanHitung.setDayOfMonth(1);
			bulanHitung.addMonths(-1);
			PoinCounterMonthly pcm = new PoinCounterMonthly(bulanHitung);
			pcm.getMemberTutupPoin(dbConn);
			pcm.processAkumulasiPoinTour(dbConn);
			pcm.countAkumulasiPoin(dbConn);
			pcm.processAkumulasiPoinTourEropa(dbConn);
			pcm.countAkumulasiPoinEropa(dbConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
