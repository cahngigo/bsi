package id.co.birumerah.bsi.runner;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.bsi.bonus.SaldoPtkp;
import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.bonus.plana.BonusAFcd;
import id.co.birumerah.bsi.bonus.plana.BonusCounter;
import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;
import id.co.birumerah.bsi.bonus.plana.LogBonusADAO;
import id.co.birumerah.bsi.bonus.plana.LogBonusAFcd;
import id.co.birumerah.bsi.flag.FlagBonusHarian;
import id.co.birumerah.bsi.flag.FlagBonusHarianFcd;
import id.co.birumerah.bsi.flag.FlagRun;
import id.co.birumerah.bsi.flag.FlagRunFcd;
import id.co.birumerah.bsi.member.Member;
import id.co.birumerah.bsi.member.MemberFcd;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.dbaccess.DBConnection;

import org.joda.time.MutableDateTime;

public class HitungUlangBonusHarian {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MutableDateTime callDate = new MutableDateTime();
		callDate.setHourOfDay(0);
		callDate.setMinuteOfHour(0);
		callDate.setSecondOfMinute(0);
		MutableDateTime hariHitung = callDate.copy();
		hariHitung.addDays(-1);
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			
//			BonusAFcd baFcd = new BonusAFcd();
//			baFcd.deleteBonusByDate(hariHitung.toDate(), dbConn);
//			
//			MemberTreeHarianFcd mthFcd = new MemberTreeHarianFcd();
//			mthFcd.deleteByLogDate(hariHitung.toDate(), dbConn);
//			
//			SaldoPtkpFcd saldoFcd = new SaldoPtkpFcd();
//			saldoFcd.deleteByRegDate(hariHitung.toDate(), dbConn);
//			
			MemberTreeFcd mtFcd = new MemberTreeFcd();
//			mtFcd.updateStatus(hariHitung.toDate(), dbConn);
//			
//			baFcd.deleteTransferByDate(hariHitung.toDate(), dbConn);
//			
//			LogBonusAFcd logFcd = new LogBonusAFcd();
//			logFcd.deleteLogByDate(hariHitung.toDate(), dbConn);
//			logFcd.deleteSaldoBonusByDate(hariHitung.toDate(), dbConn);
			
			//mulai perhitungan ulang untuk bonus pasangan
			FlagRun flag = FlagRunFcd.getFlag(dbConn);
			if (flag.getIsRun() == 0) {
				flag.setIsRun(1);
				FlagRunFcd frFcd = new FlagRunFcd();
				frFcd.updateFlag(flag, dbConn);
				
				String whereClause = "status=0 ORDER BY sort_number";
				MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
				for (int i=0; i<mtSet.length(); i++) {
					MemberTree member = mtSet.get(i);
					System.out.println("===== Penambahan Member "+member.getMemberId()+"=====");
					member.setStatus(1);
					mtFcd.updateMemberTree(member, dbConn);
					BonusCounter bc = new BonusCounter(false, dbConn);
					MemberTree upline = mtFcd.getMemberTreeById(member.getUplineId(), dbConn);
					long sortNumber = member.getSortNumber();
					Member anggota = MemberFcd.getMemberById(member.getMemberId(), dbConn);
					Date regDate = anggota.getRegDate();
					boolean isBasic = (member.getIsBasic() == 1);
//					bc.giveBonusPasangan(member, member.getUplineId(), dbConn, sortNumber, regDate);
					bc.giveBonusPasangan(member, member.getUplineId(), dbConn, sortNumber, regDate, isBasic);
					
				}
				
				flag.setIsRun(0);
				frFcd.updateFlag(flag, dbConn);
			}
			
			//perhitungan untuk bonus harian (matching, potongan am, & pajak)
			FlagBonusHarian flagHarian = FlagBonusHarianFcd.getFlag(dbConn);
			flagHarian.setHasDone(0);
			FlagBonusHarianFcd flagFcd = new FlagBonusHarianFcd();
			flagFcd.updateFlag(flagHarian, dbConn);
			
			BonusCounterCaller bcc = new BonusCounterCaller(callDate, false);
			bcc.dailyBonusCounter(true);
			
			flagHarian.setLastDate(hariHitung.toDate());
			flagHarian.setHasDone(1);
			flagFcd.updateFlag(flagHarian, dbConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
