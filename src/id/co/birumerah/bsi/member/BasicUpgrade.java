package id.co.birumerah.bsi.member;

import java.util.Date;

public class BasicUpgrade {

	private String memberId;
	private Date upgradeDate;
	
	public BasicUpgrade() {}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getUpgradeDate() {
		return upgradeDate;
	}

	public void setUpgradeDate(Date upgradeDate) {
		this.upgradeDate = upgradeDate;
	}
}
