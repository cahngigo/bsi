package id.co.birumerah.bsi.member;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BasicUpgradeSet implements Serializable {

	@SuppressWarnings("rawtypes")
	ArrayList set = null;
	
	@SuppressWarnings("rawtypes")
	public BasicUpgradeSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(BasicUpgrade dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public BasicUpgrade get(int index) {
		BasicUpgrade result = null;
		
		if ((index >= 0) && (index < length()))
			result = (BasicUpgrade) set.get(index);
		
		return result;
	}
}
