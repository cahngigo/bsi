package id.co.birumerah.bsi.member;

import java.util.Date;

public class Member {

	private String memberId;
	private String jenisMember;
	private String nama;
	private String jenisIdentitas;
	private String noIdentitas;
	private String alamat;
	private String zip;
	private String propinsi;
	private String kota;
	private String email;
	private String kelamin;
	private String birthplace;
	private Date birthdate;
	private String phone;
	private String mobilePhone;
	private String npwp;
	private String bank;
	private String noRekening;
	private String namaRek;
	private String cabangBank;
	private String webReplika;
	private String password;
	private String pin;
	private String sponsor;
	private String uplineId;
	private Date regDate;
	private int statusAnggota;
	private String alasanPerubahan;
	private int readySms;
	
	public Member() {
		super();
	}

	public Member(String memberId, String jenisMember, String nama,
			String jenisIdentitas, String noIdentitas, String alamat,
			String zip, String propinsi, String kota, String email,
			String kelamin, String birthplace, Date birthdate, String phone,
			String mobilePhone, String npwp, String bank, String noRekening,
			String namaRek, String cabangBank, String webReplika,
			String password, String pin, String sponsor, String uplineId,
			Date regDate, int statusAnggota, String alasanPerubahan,
			int readySms) {
		this.memberId = memberId;
		this.jenisMember = jenisMember;
		this.nama = nama;
		this.jenisIdentitas = jenisIdentitas;
		this.noIdentitas = noIdentitas;
		this.alamat = alamat;
		this.zip = zip;
		this.propinsi = propinsi;
		this.kota = kota;
		this.email = email;
		this.kelamin = kelamin;
		this.birthplace = birthplace;
		this.birthdate = birthdate;
		this.phone = phone;
		this.mobilePhone = mobilePhone;
		this.npwp = npwp;
		this.bank = bank;
		this.noRekening = noRekening;
		this.namaRek = namaRek;
		this.cabangBank = cabangBank;
		this.webReplika = webReplika;
		this.password = password;
		this.pin = pin;
		this.sponsor = sponsor;
		this.uplineId = uplineId;
		this.regDate = regDate;
		this.statusAnggota = statusAnggota;
		this.alasanPerubahan = alasanPerubahan;
		this.readySms = readySms;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getJenisMember() {
		return jenisMember;
	}

	public void setJenisMember(String jenisMember) {
		this.jenisMember = jenisMember;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getJenisIdentitas() {
		return jenisIdentitas;
	}

	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}

	public String getNoIdentitas() {
		return noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPropinsi() {
		return propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKelamin() {
		return kelamin;
	}

	public void setKelamin(String kelamin) {
		this.kelamin = kelamin;
	}

	public String getBirthplace() {
		return birthplace;
	}

	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public String getNamaRek() {
		return namaRek;
	}

	public void setNamaRek(String namaRek) {
		this.namaRek = namaRek;
	}

	public String getCabangBank() {
		return cabangBank;
	}

	public void setCabangBank(String cabangBank) {
		this.cabangBank = cabangBank;
	}

	public String getWebReplika() {
		return webReplika;
	}

	public void setWebReplika(String webReplika) {
		this.webReplika = webReplika;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getUplineId() {
		return uplineId;
	}

	public void setUplineId(String uplineId) {
		this.uplineId = uplineId;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public int getStatusAnggota() {
		return statusAnggota;
	}

	public void setStatusAnggota(int statusAnggota) {
		this.statusAnggota = statusAnggota;
	}

	public String getAlasanPerubahan() {
		return alasanPerubahan;
	}

	public void setAlasanPerubahan(String alasanPerubahan) {
		this.alasanPerubahan = alasanPerubahan;
	}

	public int getReadySms() {
		return readySms;
	}

	public void setReadySms(int readySms) {
		this.readySms = readySms;
	}
	
}
