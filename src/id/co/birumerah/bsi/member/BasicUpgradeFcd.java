package id.co.birumerah.bsi.member;

import java.sql.SQLException;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;

public class BasicUpgradeFcd {

	public BasicUpgrade upgrade;
	
	public BasicUpgradeFcd() {}

	public BasicUpgradeFcd(BasicUpgrade upgrade) {
		this.upgrade = upgrade;
	}

	public BasicUpgrade getUpgrade() {
		return upgrade;
	}

	public void setUpgrade(BasicUpgrade upgrade) {
		this.upgrade = upgrade;
	}
	
	public BasicUpgradeSet search(String whereClause, DBConnection dbConn) throws Exception {
		BasicUpgradeSet upgradeSet = null;
		
		try {
			BasicUpgradeDAO upgradeDao = new BasicUpgradeDAO(dbConn);
			upgradeSet = upgradeDao.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return upgradeSet;
	}
	
	public int getTotalUpgradedMember(Date tanggal, DBConnection dbConn) throws Exception {
		int result = 0;
		try {
			BasicUpgradeDAO upgradeDao = new BasicUpgradeDAO(dbConn);
			result = upgradeDao.getTotalUpgradedMemberByDate(tanggal);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return result;
	}
}
