package id.co.birumerah.bsi.member;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;
import id.co.birumerah.util.dbaccess.SQLValueFilter;

public class MemberDAO {

	DBConnection dbConn;
	DateFormat dateFormat;
	DateFormat longFormat;
	
	public MemberDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		this.longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int insert(Member dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO member (");
		sb.append("member_id,");
		sb.append("jenis_member,");
		sb.append("nama,");
		sb.append("jenis_identitas,");
		sb.append("no_identitas,");
		sb.append("alamat,");
		sb.append("zip,");
		sb.append("propinsi,");
		sb.append("kota,");
		sb.append("email,");
		sb.append("kelamin,");
		sb.append("birthplace,");
		sb.append("birthdate,");
		sb.append("phone,");
		sb.append("mobile_phone,");
		sb.append("npwp,");
		sb.append("bank,");
		sb.append("no_rekening,");
		sb.append("nama_rek,");
		sb.append("cabang_bank,");
		sb.append("web_replika,");
		sb.append("password,");
		sb.append("pin,");
		sb.append("sponsor,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("status_anggota,");
		sb.append("alasan_perubahan ");
		sb.append(") VALUES (");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getMemberId()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getJenisMember()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getNama()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getJenisIdentitas()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getNoIdentitas()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getAlamat()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getZip()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getPropinsi()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getKota()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getEmail()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getKelamin()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getBirthplace()) + "',");
		sb.append("'" + dateFormat.format(dataObject.getBirthdate()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getPhone()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getMobilePhone()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getNpwp()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getBank()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getNoRekening()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getNamaRek()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getCabangBank()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getWebReplika()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getPassword()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getPin()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getSponsor()) + "',");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getUplineId()) + "',");
		sb.append("'" + longFormat.format(dataObject.getRegDate()) + "',");
		sb.append("" + dataObject.getStatusAnggota() + ",");
		sb.append("'" + SQLValueFilter.normalizeString(dataObject.getAlasanPerubahan()) + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(Member dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE member ");
		sb.append("SET ");
//		sb.append("jenis_member = '" + SQLValueFilter.normalizeString(dataObject.getJenisMember()) + "',");
//		sb.append("nama = '" + SQLValueFilter.normalizeString(dataObject.getNama()) + "',");
//		sb.append("jenis_identitas = '" + SQLValueFilter.normalizeString(dataObject.getJenisIdentitas()) + "',");
//		sb.append("no_identitas = '" + SQLValueFilter.normalizeString(dataObject.getNoIdentitas()) + "',");
//		sb.append("alamat = '" + SQLValueFilter.normalizeString(dataObject.getAlamat()) + "',");
//		sb.append("zip = '" + SQLValueFilter.normalizeString(dataObject.getZip()) + "',");
//		sb.append("propinsi = '" + SQLValueFilter.normalizeString(dataObject.getPropinsi()) + "',");
//		sb.append("kota = '" + SQLValueFilter.normalizeString(dataObject.getKota()) + "',");
//		sb.append("email = '" + SQLValueFilter.normalizeString(dataObject.getEmail()) + "',");
//		sb.append("kelamin = '" + SQLValueFilter.normalizeString(dataObject.getKelamin()) + "',");
//		sb.append("birthplace = '" + SQLValueFilter.normalizeString(dataObject.getBirthplace()) + "',");
//		sb.append("birthdate = '" + dateFormat.format(dataObject.getBirthdate()) + "',");
//		sb.append("phone = '" + SQLValueFilter.normalizeString(dataObject.getPhone()) + "',");
//		sb.append("mobile_phone = '" + SQLValueFilter.normalizeString(dataObject.getMobilePhone()) + "',");
//		sb.append("npwp = '" + SQLValueFilter.normalizeString(dataObject.getNpwp()) + "',");
//		sb.append("bank = '" + SQLValueFilter.normalizeString(dataObject.getBank()) + "',");
//		sb.append("no_rekening = '" + SQLValueFilter.normalizeString(dataObject.getNoRekening()) + "',");
//		sb.append("nama_rek = '" + SQLValueFilter.normalizeString(dataObject.getNamaRek()) + "',");
//		sb.append("cabang_bank = '" + SQLValueFilter.normalizeString(dataObject.getCabangBank()) + "',");
//		sb.append("web_replika = '" + SQLValueFilter.normalizeString(dataObject.getWebReplika()) + "',");
//		sb.append("password = '" + SQLValueFilter.normalizeString(dataObject.getPassword()) + "',");
//		sb.append("pin = '" + SQLValueFilter.normalizeString(dataObject.getPin()) + "',");
//		sb.append("sponsor = '" + SQLValueFilter.normalizeString(dataObject.getSponsor()) + "',");
//		sb.append("upline_id = '" + SQLValueFilter.normalizeString(dataObject.getUplineId()) + "',");
//		sb.append("reg_date = '" + longFormat.format(dataObject.getRegDate()) + "',");
//		sb.append("status_anggota = '" + dataObject.getStatusAnggota() + "',");
//		sb.append("alasan_perubahan = '" + SQLValueFilter.normalizeString(dataObject.getAlasanPerubahan()) + "' ");
		sb.append("ready_sms = " + dataObject.getReadySms() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		System.out.println("Query update member: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(Member dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM member ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(Member dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("jenis_member,");
		sb.append("nama,");
		sb.append("jenis_identitas,");
		sb.append("no_identitas,");
		sb.append("alamat,");
		sb.append("zip,");
		sb.append("propinsi,");
		sb.append("kota,");
		sb.append("email,");
		sb.append("kelamin,");
		sb.append("birthplace,");
		sb.append("birthdate,");
		sb.append("phone,");
		sb.append("mobile_phone,");
		sb.append("npwp,");
		sb.append("bank,");
		sb.append("no_rekening,");
		sb.append("nama_rek,");
		sb.append("cabang_bank,");
		sb.append("web_replika,");
		sb.append("password,");
		sb.append("pin,");
		sb.append("sponsor,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("status_anggota,");
		sb.append("alasan_perubahan ");
		sb.append("FROM member ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setJenisMember(rs.getString("jenis_member"));
				dataObject.setNama(rs.getString("nama"));
				dataObject.setJenisIdentitas(rs.getString("jenis_identitas"));
				dataObject.setNoIdentitas(rs.getString("no_identitas"));
				dataObject.setAlamat(rs.getString("alamat"));
				dataObject.setZip(rs.getString("zip"));
				dataObject.setPropinsi(rs.getString("propinsi"));
				dataObject.setKota(rs.getString("kota"));
				dataObject.setEmail(rs.getString("email"));
				dataObject.setKelamin(rs.getString("kelamin"));
				dataObject.setBirthplace(rs.getString("birthplace"));
				dataObject.setBirthdate(rs.getDate("birthdate"));
				dataObject.setPhone(rs.getString("phone"));
				dataObject.setMobilePhone(rs.getString("mobile_phone"));
				dataObject.setNpwp(rs.getString("npwp"));
				dataObject.setBank(rs.getString("bank"));
				dataObject.setNoRekening(rs.getString("no_rekening"));
				dataObject.setNamaRek(rs.getString("nama_rek"));
				dataObject.setCabangBank(rs.getString("cabang_bank"));
				dataObject.setWebReplika(rs.getString("web_replika"));
				dataObject.setPassword(rs.getString("password"));
				dataObject.setPin(rs.getString("pin"));
				dataObject.setSponsor(rs.getString("sponsor"));
				dataObject.setUplineId(rs.getString("upline_id"));
				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setStatusAnggota(rs.getInt("status_anggota"));
				dataObject.setAlasanPerubahan(rs.getString("alasan_perubahan"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public MemberSet select(String whereClause) throws SQLException {
		MemberSet dataObjectSet = new MemberSet();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("jenis_member,");
		sb.append("nama,");
		sb.append("jenis_identitas,");
		sb.append("no_identitas,");
		sb.append("alamat,");
		sb.append("zip,");
		sb.append("propinsi,");
		sb.append("kota,");
		sb.append("email,");
		sb.append("kelamin,");
		sb.append("birthplace,");
		sb.append("birthdate,");
		sb.append("phone,");
		sb.append("mobile_phone,");
		sb.append("npwp,");
		sb.append("bank,");
		sb.append("no_rekening,");
		sb.append("nama_rek,");
		sb.append("cabang_bank,");
		sb.append("web_replika,");
		sb.append("password,");
		sb.append("pin,");
		sb.append("sponsor,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("status_anggota,");
		sb.append("alasan_perubahan ");
//		sb.append("ready_sms ");
		sb.append("FROM member ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			while (rs.next()) {
				Member dataObject = new Member();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setJenisMember(rs.getString("jenis_member"));
				dataObject.setNama(rs.getString("nama"));
				dataObject.setJenisIdentitas(rs.getString("jenis_identitas"));
				dataObject.setNoIdentitas(rs.getString("no_identitas"));
				dataObject.setAlamat(rs.getString("alamat"));
				dataObject.setZip(rs.getString("zip"));
				dataObject.setPropinsi(rs.getString("propinsi"));
				dataObject.setKota(rs.getString("kota"));
				dataObject.setEmail(rs.getString("email"));
				dataObject.setKelamin(rs.getString("kelamin"));
				dataObject.setBirthplace(rs.getString("birthplace"));
				dataObject.setBirthdate(rs.getDate("birthdate"));
				dataObject.setPhone(rs.getString("phone"));
				dataObject.setMobilePhone(rs.getString("mobile_phone"));
				dataObject.setNpwp(rs.getString("npwp"));
				dataObject.setBank(rs.getString("bank"));
				dataObject.setNoRekening(rs.getString("no_rekening"));
				dataObject.setNamaRek(rs.getString("nama_rek"));
				dataObject.setCabangBank(rs.getString("cabang_bank"));
				dataObject.setWebReplika(rs.getString("web_replika"));
				dataObject.setPassword(rs.getString("password"));
				dataObject.setPin(rs.getString("pin"));
				dataObject.setSponsor(rs.getString("sponsor"));
				dataObject.setUplineId(rs.getString("upline_id"));
//				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setStatusAnggota(rs.getInt("status_anggota"));
				dataObject.setAlasanPerubahan(rs.getString("alasan_perubahan"));
//				dataObject.setReadySms(rs.getInt("ready_sms"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int getTotal(String whereClause) throws SQLException {
		int total = 0;
		String query = "SELECT COUNT(*) total FROM member WHERE " + whereClause;
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(query);
			if (rs.next()) {
				total = rs.getInt("total");
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return total;
	}
}
