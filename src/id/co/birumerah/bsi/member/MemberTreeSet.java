package id.co.birumerah.bsi.member;

import java.io.Serializable;
import java.util.ArrayList;

public class MemberTreeSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3046157419241748674L;
	
	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public MemberTreeSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(MemberTree dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public MemberTree get(int index) {
		MemberTree result = null;
		
		if ((index >= 0) && (index < length())) 
			result = (MemberTree) set.get(index);
		
		return result;
	}

}
