package id.co.birumerah.bsi.member;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class MemberFcd {

	private Member member;
	
	public MemberFcd() {}

	public MemberFcd(Member member) {
		this.member = member;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	public static MemberSet showAllMember() throws Exception {
		DBConnection dbConn = null;
		MemberSet mSet = null;
		
		String whereClause = "1=1 ORDER BY reg_date";
		try {
			dbConn = new DBConnection();
			MemberDAO memberDAO = new MemberDAO(dbConn);
			mSet = memberDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mSet;
	}
	
	public MemberSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		MemberSet mSet = null;
		
		try {
			dbConn = new DBConnection();
			MemberDAO mDAO = new MemberDAO(dbConn);
			mSet = mDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mSet;
	}
	
	public MemberSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberSet mSet = null;
		
		try {
//			dbConn = new DBConnection();
			MemberDAO mDAO = new MemberDAO(dbConn);
			mSet = mDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mSet;
	}
	
	public static Member getMemberById(String memberId) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			Member member = new Member();
			member.setMemberId(memberId);
			
			MemberDAO mDAO = new MemberDAO(dbConn);
			boolean found = mDAO.select(member);
			
			if (found) {
				return member;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static Member getMemberById(String memberId, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			Member member = new Member();
			member.setMemberId(memberId);
			
			MemberDAO mDAO = new MemberDAO(dbConn);
			boolean found = mDAO.select(member);
			
			if (found) {
				return member;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public void insertMember() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberDAO mDAO = new MemberDAO(dbConn);
			mDAO.insert(member);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}		
	}
	
	public void updateMember(Member member) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberDAO mDAO = new MemberDAO(dbConn);
			if (mDAO.update(member) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateMember(Member member, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberDAO mDAO = new MemberDAO(dbConn);
			if (mDAO.update(member) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public int getTotal(String whereClause, DBConnection dbConn) throws Exception {
		int total = 0;
		MemberDAO mDAO = new MemberDAO(dbConn);
		total = mDAO.getTotal(whereClause);
		
		return total;
	}
}
