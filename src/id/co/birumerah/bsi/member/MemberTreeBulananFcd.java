package id.co.birumerah.bsi.member;

import java.sql.SQLException;
import java.util.List;

import org.joda.time.MutableDateTime;

import id.co.birumerah.util.dbaccess.DBConnection;

public class MemberTreeBulananFcd {

	private MemberTree mTree;
	
	public MemberTreeBulananFcd() {}

	public MemberTreeBulananFcd(MemberTree mTree) {
		this.mTree = mTree;
	}

	public MemberTree getMemberTree() {
		return mTree;
	}

	public void setMemberTree(MemberTree mTree) {
		this.mTree = mTree;
	}
	
	public static MemberTreeSet showAllMemberTree() throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		String whereClause = "1=1 ORDER BY reg_date";
		try {
			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreeBeforeToday() throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
//		DateTime beginOfDay = new DateTime();
		MutableDateTime beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") 
			+ "' AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "' ORDER BY reg_date";
		try {
			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreeBeforeToday(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
//		DateTime beginOfDay = new DateTime();
		MutableDateTime beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		lastMonth.addMonths(-1);
		
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") 
			+ "' AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "' ORDER BY reg_date";
		try {
//			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreeBeforeToday(MutableDateTime beginOfDay, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
//		DateTime beginOfDay = new DateTime();
//		MutableDateTime beginOfDay = new MutableDateTime();
//		beginOfDay.setHourOfDay(0);
//		beginOfDay.setMinuteOfHour(0);
//		beginOfDay.setSecondOfMinute(0);
//		MutableDateTime lastMonth = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), 1, 0, 0, 0, 0);
		MutableDateTime lastMonth = beginOfDay.copy();
		lastMonth.addMonths(-1);
		
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") 
			+ "' AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "' ORDER BY reg_date";
		try {
//			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreePerMonth(MutableDateTime tanggal, DBConnection dbConn) throws Exception {
		MemberTreeSet mtSet = null;
		
		String whereClause = "tanggal = '" + tanggal.toString("yyyy-MM-dd") + "' ORDER BY reg_date, sort_number";
		System.out.println("whereClause= " + whereClause);
		try {
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return mtSet;
	}
	
	public static MemberTree getMemberTreeByIdPerMonth(String memberId, MutableDateTime tanggal, DBConnection dbConn) throws Exception {
		MemberTreeSet mtSet = null;
		MemberTree mTree = null;
		
		String whereClause = "tanggal = '" + tanggal.toString("yyyy-MM-dd") + "' AND member_id = '" + memberId + "' ORDER BY reg_date, sort_number";
		System.out.println("whereClause= " + whereClause);
		try {
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
			if (mtSet.length() > 0) {
				mTree = mtSet.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return mTree;
	}
	
	public MemberTreeSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		try {
			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public MemberTreeSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		try {
//			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTree getMemberTreeById(String memberTreeId) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MemberTree memTree = new MemberTree();
			memTree.setMemberId(memberTreeId);
			
			MemberTreeBulananDAO memDAO = new MemberTreeBulananDAO(dbConn);
			boolean found = memDAO.select(memTree);
			
			if (found) {
				return memTree;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static List<MemberTree> getMemberTreeByUplineId(String uplineId) throws Exception {
		DBConnection dbConn = null;
		List<MemberTree> members = null;
		
		try {
			dbConn = new DBConnection();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			String whereClause = "upline_id = '" + uplineId + "'";
			members = mtDAO.selectMembersByUplineId(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return members;
	}
	
	public void insertMemberTree() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			mtDAO.insert(mTree);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		
	}
	
	public void updateMemberTree(MemberTree memberTree) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateMemberTree(MemberTree memberTree, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeBulananDAO mtDAO = new MemberTreeBulananDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
}
