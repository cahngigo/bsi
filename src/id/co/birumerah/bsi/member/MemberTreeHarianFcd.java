package id.co.birumerah.bsi.member;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.joda.time.MutableDateTime;

import id.co.birumerah.util.dbaccess.DBConnection;

public class MemberTreeHarianFcd {

	private MemberTree mTree;
	
	public MemberTreeHarianFcd() {}

	public MemberTreeHarianFcd(MemberTree mTree) {
		this.mTree = mTree;
	}

	public MemberTree getMemberTree() {
		return mTree;
	}

	public void setMemberTree(MemberTree mTree) {
		this.mTree = mTree;
	}
	
	public static MemberTreeSet showAllMemberTreePerDate(MutableDateTime tanggal) throws Exception {
		System.out.println("di showAllMemberTreePerDate");
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		String whereClause = "tanggal='" + tanggal.toString("yyyy-MM-dd") + "' ORDER BY reg_date, sort_number";
		System.out.println("whereClause= " + whereClause);
		try {
			dbConn = new DBConnection();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreePerDate(MutableDateTime tanggal, DBConnection dbConn) throws Exception {
		System.out.println("di showAllMemberTreePerDate");
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		String whereClause = "tanggal='" + tanggal.toString("yyyy-MM-dd") + "' ORDER BY reg_date, sort_number";
//		String whereClause = "tanggal='" + tanggal.toString("yyyy-MM-dd") + "' ORDER BY sort_number";
		System.out.println("whereClause= " + whereClause);
		try {
//			dbConn = new DBConnection();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreeYesterday() throws Exception {
		System.out.println("di showAllMemberTreeYesterday()");
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
//		DateTime beginOfDay = new DateTime();
		MutableDateTime beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		MutableDateTime yesterday = new MutableDateTime(beginOfDay.getYear(), beginOfDay.getMonthOfYear(), beginOfDay.getDayOfMonth(), 0, 0, 0, 0);
		yesterday.addDays(-1);
		
		String whereClause = "tanggal = '" + yesterday.toString("yyyy-MM-dd") + "' ORDER BY reg_date";
		System.out.println("whereClause= " + whereClause);
		try {
			dbConn = new DBConnection();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public MemberTreeSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		try {
			dbConn = new DBConnection();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public MemberTreeSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		try {
//			dbConn = new DBConnection();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTree getMemberTreeByIdAndDate(String memberTreeId, Date tanggal) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MemberTree memTree = new MemberTree();
			memTree.setMemberId(memberTreeId);
			memTree.setTanggal(tanggal);
			
			MemberTreeHarianDAO memDAO = new MemberTreeHarianDAO(dbConn);
			boolean found = memDAO.select(memTree);
			
			if (found) {
				return memTree;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static MemberTree getMemberTreeByIdAndDate(String memberTreeId, Date tanggal, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			MemberTree memTree = new MemberTree();
			memTree.setMemberId(memberTreeId);
			memTree.setTanggal(tanggal);
			
			MemberTreeHarianDAO memDAO = new MemberTreeHarianDAO(dbConn);
			boolean found = memDAO.select(memTree);
			
			if (found) {
				return memTree;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static List<MemberTree> getMemberTreeByUplineId(String uplineId) throws Exception {
		DBConnection dbConn = null;
		List<MemberTree> members = null;
		
		try {
			dbConn = new DBConnection();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			String whereClause = "upline_id = '" + uplineId + "'";
			members = mtDAO.selectMembersByUplineId(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return members;
	}
	
	public void insertMemberTree() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			mtDAO.insert(mTree);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateMemberTree(MemberTree memberTree) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateMemberTree(MemberTree memberTree, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Update Member Tree failed.");
			}
			dbConn.commitTransaction();
			mtDAO = null;
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void deleteByLogDate(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			if (mtDAO.deleteByLogDate(tanggal) < 1) {
				throw new Exception("Delete log failed.");
			}
			dbConn.commitTransaction();
			mtDAO = null;
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public boolean hasNullMember(Date tanggal, DBConnection dbConn) throws Exception {
		boolean adaNull = false;
		try {
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			adaNull = mtDAO.anyNullMember(tanggal);
			mtDAO = null;
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
		return adaNull;
	}
	
	public int countTotal(DBConnection dbConn) throws Exception {
		int jumlah = 0;
		try {
			MemberTreeHarianDAO mtDAO = new MemberTreeHarianDAO(dbConn);
			jumlah = mtDAO.countTotal();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return jumlah;
	}
}
