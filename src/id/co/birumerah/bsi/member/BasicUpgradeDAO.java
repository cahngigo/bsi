package id.co.birumerah.bsi.member;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

public class BasicUpgradeDAO {

	private DBConnection dbConn;
	private DateFormat shortFormat;
	private DateFormat longFormat;
	
	public BasicUpgradeDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int insert(BasicUpgrade dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO basic_upgrade (");
		sb.append("member_id,");
		sb.append("upgrade_date");
		sb.append(") VALUES (");
		sb.append("'" + dataObject.getMemberId() + "',");
		sb.append("'" + longFormat.format(dataObject.getUpgradeDate()) + "'");
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public BasicUpgradeSet select(String whereClause) throws SQLException {
		BasicUpgradeSet dataObjectSet = new BasicUpgradeSet();
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("upgrade_date ");
		sb.append("FROM basic_upgrade ");
		sb.append("WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				BasicUpgrade dataObject = new BasicUpgrade();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setUpgradeDate(new Date(rs.getTimestamp("upgrade_date").getTime()));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public int getTotalUpgradedMemberByDate(Date tanggal) throws SQLException {
		int result = 0;
		String query = "SELECT COUNT(member_id) total FROM basic_upgrade " +
				"WHERE DATE(upgrade_date) = '" + shortFormat.format(tanggal) + "'";
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(query);
			if (rs.next()) {
				result = rs.getInt("total");
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
}
