package id.co.birumerah.bsi.member;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class MemberTreeSmallHarianDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	DateFormat longFormat;
	
	public MemberTreeSmallHarianDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int update(MemberTreeSmall dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE lg_member_tree_harian ");
		sb.append("SET ");
////		sb.append("upline_id = '" + SQLValueFilter.normalizeString(dataObject.getUplineId()) + "',");
////		sb.append("reg_date = '" + longFormat.format(dataObject.getRegDate()) + "',");
//		sb.append("pvpribadi = " + dataObject.getPvpribadi() + ",");
//		sb.append("bigfoot = '" + SQLValueFilter.normalizeString(dataObject.getBigfoot()) + "',");
//		sb.append("mediumfoot = '" + SQLValueFilter.normalizeString(dataObject.getMediumfoot()) + "',");
//		sb.append("litfoot = '" + SQLValueFilter.normalizeString(dataObject.getLitfoot()) + "',");
////		sb.append("left_foot = '" + SQLValueFilter.normalizeString(dataObject.getLeftFoot()) + "',");
////		sb.append("middle_foot = '" + SQLValueFilter.normalizeString(dataObject.getMiddleFoot()) + "',");
////		sb.append("right_foot = '" + SQLValueFilter.normalizeString(dataObject.getRightFoot()) + "',");
//		sb.append("amt_bigfoot = " + dataObject.getAmtBigfoot() + ",");
//		sb.append("amt_midfoot = " + dataObject.getAmtMidfoot() + ",");
//		sb.append("amt_litfoot = " + dataObject.getAmtLitfoot() + ",");
//		sb.append("peringkat = '" + SQLValueFilter.normalizeString(dataObject.getPeringkat()) + "',");
//		sb.append("kualifikasi = '" + SQLValueFilter.normalizeString(dataObject.getKualifikasi()) + "',");
//		sb.append("tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "',");
//		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
//		sb.append("saldo_am = " + dataObject.getSaldoAm() + ",");
//		sb.append("saldo_poin = " + dataObject.getSaldoPoin() + ",");
//		sb.append("saldo_bv = " + dataObject.getSaldoBv() + ",");
////		sb.append("saldo_ewallet = " + dataObject.getSaldoEwallet() + ",");
		sb.append("pv_bigfoot = " + dataObject.getPvBigfoot() + ",");
		sb.append("pv_midfoot = " + dataObject.getPvMidfoot() + ",");
		sb.append("pv_litfoot = " + dataObject.getPvLitfoot() + ",");
////		sb.append("is_promoxenia = " + dataObject.getIsPromoxenia() + ",");
//		sb.append("bigfoot_promo = " + dataObject.getBigfootPromo() + ",");
//		sb.append("midfoot_promo = " + dataObject.getMidfootPromo() + ",");
//		sb.append("litfoot_promo = " + dataObject.getLitfootPromo() + " ");
		sb.append("pv_besar = " + dataObject.getPvBesar() + ",");
		sb.append("pv_sedang = " + dataObject.getPvSedang() + ",");
		sb.append("pv_kecil = " + dataObject.getPvKecil() + " ");
////		sb.append("sort_number = " + dataObject.getSortNumber() + ",");
//		sb.append("status = " + dataObject.getStatus() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "' ");
		sb.append("AND tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "'");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public MemberTreeSmallSet select(String whereClause) throws SQLException {
		MemberTreeSmallSet dataObjectSet = new MemberTreeSmallSet();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
//		sb.append("upline_id,");
//		sb.append("reg_date,");
		sb.append("pvpribadi,");
//		sb.append("bigfoot,");
//		sb.append("mediumfoot,");
//		sb.append("litfoot,");
//		sb.append("left_foot,");
//		sb.append("middle_foot,");
//		sb.append("right_foot,");
//		sb.append("amt_bigfoot,");
//		sb.append("amt_midfoot,");
//		sb.append("amt_litfoot,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
//		sb.append("potongan_am,");
//		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
//		sb.append("saldo_ewallet,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("pv_besar,");
		sb.append("pv_sedang,");
		sb.append("pv_kecil,");
		sb.append("is_basic,");
		sb.append("is_basic_new ");
//		sb.append("pv_litfoot,");
//		sb.append("sort_number,");
//		sb.append("status ");
		sb.append("FROM lg_member_tree_harian WHERE " + whereClause);
		System.out.println("Query: "+sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				MemberTreeSmall dataObject = new MemberTreeSmall();
				dataObject.setMemberId(rs.getString("member_id"));
//				dataObject.setUplineId(rs.getString("upline_id"));
//				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
//				dataObject.setBigfoot(rs.getString("bigfoot"));
//				dataObject.setMediumfoot(rs.getString("mediumfoot"));
//				dataObject.setLitfoot(rs.getString("litfoot"));
//				dataObject.setLeftFoot(rs.getString("left_foot"));
//				dataObject.setMiddleFoot(rs.getString("middle_foot"));
//				dataObject.setRightFoot(rs.getString("right_foot"));
//				dataObject.setAmtBigfoot(rs.getInt("amt_bigfoot"));
//				dataObject.setAmtMidfoot(rs.getInt("amt_midfoot"));
//				dataObject.setAmtLitfoot(rs.getInt("amt_litfoot"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setKualifikasi(rs.getString("kualifikasi"));
				dataObject.setTanggal(rs.getDate("tanggal"));
//				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
//				dataObject.setSaldoAm(rs.getDouble("saldo_am"));
				dataObject.setSaldoPoin(rs.getFloat("saldo_poin"));
				dataObject.setSaldoBv(rs.getFloat("saldo_bv"));
//				dataObject.setSaldoEwallet(rs.getDouble("saldo_ewallet"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				dataObject.setPvBesar(rs.getFloat("pv_besar"));
				dataObject.setPvSedang(rs.getFloat("pv_sedang"));
				dataObject.setPvKecil(rs.getFloat("pv_kecil"));
//				dataObject.setSortNumber(rs.getLong("sort_number"));
//				dataObject.setStatus(rs.getInt("status"));
				dataObject.setIsBasic(rs.getInt("is_basic"));
				dataObject.setIsBasicNew(rs.getInt("is_basic_new"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
}
