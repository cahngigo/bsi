package id.co.birumerah.bsi.member;

import java.sql.SQLException;
import java.util.Date;

import org.joda.time.MutableDateTime;

import id.co.birumerah.util.dbaccess.DBConnection;

public class MemberTreeSmallBulananFcd {

	private MemberTreeSmall mTree;
	
	public MemberTreeSmallBulananFcd() {}

	public MemberTreeSmallBulananFcd(MemberTreeSmall mTree) {
		this.mTree = mTree;
	}

	public MemberTreeSmall getmTree() {
		return mTree;
	}

	public void setmTree(MemberTreeSmall mTree) {
		this.mTree = mTree;
	}
	
	public static MemberTreeSmallSet showAllMemberTreeBeforeToday(MutableDateTime beginOfDay, DBConnection dbConn) throws Exception {
		MemberTreeSmallSet mtSet = null;
		MutableDateTime lastMonth = beginOfDay.copy();
		lastMonth.addMonths(-1);
		
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") 
			+ "' AND tanggal = '" + lastMonth.toString("yyyy-MM-dd") + "' ORDER BY reg_date";
		try {
//			dbConn = new DBConnection();
			MemberTreeSmallBulananDAO mtDAO = new MemberTreeSmallBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public MemberTreeSmallSet search(String whereClause, DBConnection dbConn) throws Exception {
		MemberTreeSmallSet mtSet = null;
		
		try {
//			dbConn = new DBConnection();
			MemberTreeSmallBulananDAO mtDAO = new MemberTreeSmallBulananDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public int countTotal(Date tanggal, DBConnection dbConn) throws Exception {
		int jumlah = 0;
		try {
			MemberTreeSmallBulananDAO mtDAO = new MemberTreeSmallBulananDAO(dbConn);
			jumlah = mtDAO.countTotal(tanggal);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return jumlah; 
	}
	
	public void updateMemberTree(MemberTreeSmall memberTree, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeSmallBulananDAO mtDAO = new MemberTreeSmallBulananDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public MemberTreeSmall getMemberTreeByIdAndBulan(String memberId, Date bulan, DBConnection dbConn) throws Exception {
		MemberTreeSmall dataObject = null;
		try {
			MemberTreeSmallBulananDAO mtsbDao = new MemberTreeSmallBulananDAO(dbConn);
			dataObject = mtsbDao.getByMemberIdAndBulan(memberId, bulan);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return dataObject;
	}
}
