package id.co.birumerah.bsi.member;

import java.util.HashMap;
import java.util.Map;

import id.co.birumerah.bsi.bonus.planb.AkumulasiPoinTour;
import id.co.birumerah.bsi.bonus.planb.AkumulasiPoinTourEropa;
import id.co.birumerah.bsi.bonus.planb.AkumulasiPoinTourEropaFcd;
import id.co.birumerah.bsi.bonus.planb.AkumulasiPoinTourEropaSet;
import id.co.birumerah.bsi.bonus.planb.AkumulasiPoinTourFcd;
import id.co.birumerah.bsi.bonus.planb.AkumulasiPoinTourSet;
import id.co.birumerah.util.dbaccess.DBConnection;

import org.joda.time.MutableDateTime;

public class PoinCounterMonthly {

	private MutableDateTime bulan;
	
	private static final float TUTUP_POIN = 50;
	private static final float AKUMULASI_POIN_TOUR = 15000;
	private static final float AKUMULASI_POIN_TOUR_EROPA = 200000;
	
	public PoinCounterMonthly(MutableDateTime bulan) {
		this.bulan = bulan;
	}
	
	public void getMemberTutupPoin(DBConnection dbConn) {
//		String whereClause = "pvpribadi >= " + TUTUP_POIN + " AND tanggal = '" + bulan.toString("yyyy-MM-dd") + "'";
		String whereClause = "tanggal = '" + bulan.toString("yyyy-MM-dd") + "' AND NOT (pv_bigfoot=0 AND pv_midfoot=0 AND pv_litfoot=0)";
		try {
			MemberTreeBulananFcd mtsbFcd = new MemberTreeBulananFcd();
			MemberTreeSet mtsSet = mtsbFcd.search(whereClause, dbConn);
			for (int i=0; i<mtsSet.length(); i++) {
				MemberTree mtSmall = mtsSet.get(i);
				AkumulasiPoinTour akumPoin = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(mtSmall.getMemberId(), bulan.toDate(), dbConn);
				AkumulasiPoinTourEropa akumPoinEropa = AkumulasiPoinTourEropaFcd.getAkumulasiByMemberAndMonth(mtSmall.getMemberId(), bulan.toDate(), dbConn);
				if (akumPoin == null) {
					akumPoin = new AkumulasiPoinTour();
					akumPoin.setMemberId(mtSmall.getMemberId());
					akumPoin.setBulan(bulan.toDate());
					akumPoin.setRegDate(mtSmall.getRegDate());
					akumPoin.setPvGroup(mtSmall.getPvBesar() + mtSmall.getPvSedang() + mtSmall.getPvKecil());
					akumPoin.setAkumKiri(getPvFoot(mtSmall, mtSmall.getLeftFoot()));
					akumPoin.setAkumTengah(getPvFoot(mtSmall, mtSmall.getMiddleFoot()));
					akumPoin.setAkumKanan(getPvFoot(mtSmall, mtSmall.getRightFoot()));
					akumPoin.setAkumPv(0);
					akumPoin.setBrokenByKiri("");
					akumPoin.setBrokenByTengah("");
					akumPoin.setBrokenByKanan("");
					AkumulasiPoinTourFcd aptFcd = new AkumulasiPoinTourFcd(akumPoin);
					aptFcd.insertAkumulasi(dbConn);
					
					akumPoinEropa = new AkumulasiPoinTourEropa();
					akumPoinEropa.setMemberId(mtSmall.getMemberId());
					akumPoinEropa.setBulan(bulan.toDate());
					akumPoinEropa.setRegDate(mtSmall.getRegDate());
					akumPoinEropa.setPvGroup(mtSmall.getPvBesar() + mtSmall.getPvSedang() + mtSmall.getPvKecil());
					akumPoinEropa.setAkumKiri(getPvFoot(mtSmall, mtSmall.getLeftFoot()));
					akumPoinEropa.setAkumTengah(getPvFoot(mtSmall, mtSmall.getMiddleFoot()));
					akumPoinEropa.setAkumKanan(getPvFoot(mtSmall, mtSmall.getRightFoot()));
					akumPoinEropa.setAkumPv(0);
					akumPoinEropa.setBrokenByKiri("");
					akumPoinEropa.setBrokenByTengah("");
					akumPoinEropa.setBrokenByKanan("");
					AkumulasiPoinTourEropaFcd aptEropaFcd = new AkumulasiPoinTourEropaFcd(akumPoinEropa);
					aptEropaFcd.insertAkumulasi(dbConn);
				} else {
					akumPoin.setRegDate(mtSmall.getRegDate());
					akumPoin.setPvGroup(mtSmall.getPvBesar() + mtSmall.getPvSedang() + mtSmall.getPvKecil());
					akumPoin.setAkumKiri(getPvFoot(mtSmall, mtSmall.getLeftFoot()));
					akumPoin.setAkumTengah(getPvFoot(mtSmall, mtSmall.getMiddleFoot()));
					akumPoin.setAkumKanan(getPvFoot(mtSmall, mtSmall.getRightFoot()));
					akumPoin.setAkumPv(0);
					AkumulasiPoinTourFcd aptFcd = new AkumulasiPoinTourFcd();
					aptFcd.updateAkumulasi(akumPoin, dbConn);
					
					akumPoinEropa.setRegDate(mtSmall.getRegDate());
					akumPoinEropa.setPvGroup(mtSmall.getPvBesar() + mtSmall.getPvSedang() + mtSmall.getPvKecil());
					akumPoinEropa.setAkumKiri(getPvFoot(mtSmall, mtSmall.getLeftFoot()));
					akumPoinEropa.setAkumTengah(getPvFoot(mtSmall, mtSmall.getMiddleFoot()));
					akumPoinEropa.setAkumKanan(getPvFoot(mtSmall, mtSmall.getRightFoot()));
					akumPoinEropa.setAkumPv(0);
					AkumulasiPoinTourEropaFcd aptEropaFcd = new AkumulasiPoinTourEropaFcd();
					aptEropaFcd.updateAkumulasi(akumPoinEropa, dbConn);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void processAkumulasiPoinTour(DBConnection dbConn) {
		try {
			AkumulasiPoinTourFcd aptFcd = new AkumulasiPoinTourFcd();
			AkumulasiPoinTourSet aptSet = aptFcd.getMemberPass(bulan.toDate(), dbConn);
			Map<String, String> passMember = new HashMap<String, String>();
			for (int i=0; i<aptSet.length(); i++) {
				AkumulasiPoinTour akumulasi = aptSet.get(i);
				passMember.put(akumulasi.getMemberId(), akumulasi.getMemberId());
			}
			
			//trace ke jaringan di atasnya & kurangi PV Group jaringan di atasnya dengan perolehan PV Group member ybs.
			for (int i=0; i<aptSet.length(); i++) {
				AkumulasiPoinTour akumulasi = aptSet.get(i);
				System.out.println("****** Pengecekan member " + akumulasi.getMemberId() + " ******");
//				akumulasi = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(akumulasi.getMemberId(), akumulasi.getBulan(), dbConn);
//				AkumulasiPoinTour apt = aptFcd.getMemberPassByMember(akumulasi.getMemberId(), akumulasi.getBulan(), dbConn);
				AkumulasiPoinTour apt = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(akumulasi.getMemberId(), akumulasi.getBulan(), dbConn);
//				float akumPv = akumulasi.getPvGroup() - akumulasi.getPoinBroken();
				float akumPv = akumulasi.getPvGroup() - apt.getPoinBroken();
				if (akumPv > 0) apt.setAkumPv(akumPv);
				//apt.setPvGroup(akumulasi.getPvGroup());
				System.out.println("apt.getPvGroup() == " +apt.getPvGroup() + "; akumulasi.getPvGroup() == " + akumulasi.getPvGroup());
//				aptFcd.updateAkumulasi(akumulasi, dbConn);
				aptFcd.updateAkumulasi(apt, dbConn);
				System.out.println("***start traceUpline***");
				traceUpline(akumulasi.getMemberId(), akumulasi.getMemberId(), passMember, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void processAkumulasiPoinTourEropa(DBConnection dbConn) {
		try {
			AkumulasiPoinTourEropaFcd aptFcd = new AkumulasiPoinTourEropaFcd();
			AkumulasiPoinTourEropaSet aptSet = aptFcd.getMemberPass(bulan.toDate(), dbConn);
			Map<String, String> passMember = new HashMap<String, String>();
			for (int i=0; i<aptSet.length(); i++) {
				AkumulasiPoinTourEropa akumulasi = aptSet.get(i);
				passMember.put(akumulasi.getMemberId(), akumulasi.getMemberId());
			}
			
			//trace ke jaringan di atasnya & kurangi PV Group jaringan di atasnya dengan perolehan PV Group member ybs.
			for (int i=0; i<aptSet.length(); i++) {
				AkumulasiPoinTourEropa akumulasi = aptSet.get(i);
				System.out.println("****** Pengecekan member " + akumulasi.getMemberId() + " ******");
//				akumulasi = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(akumulasi.getMemberId(), akumulasi.getBulan(), dbConn);
//				AkumulasiPoinTour apt = aptFcd.getMemberPassByMember(akumulasi.getMemberId(), akumulasi.getBulan(), dbConn);
				AkumulasiPoinTourEropa apt = AkumulasiPoinTourEropaFcd.getAkumulasiByMemberAndMonth(akumulasi.getMemberId(), akumulasi.getBulan(), dbConn);
//				float akumPv = akumulasi.getPvGroup() - akumulasi.getPoinBroken();
				float akumPv = akumulasi.getPvGroup() - apt.getPoinBroken();
				if (akumPv > 0) apt.setAkumPv(akumPv);
				//apt.setPvGroup(akumulasi.getPvGroup());
				System.out.println("apt.getPvGroup() == " +apt.getPvGroup() + "; akumulasi.getPvGroup() == " + akumulasi.getPvGroup());
//				aptFcd.updateAkumulasi(akumulasi, dbConn);
				aptFcd.updateAkumulasi(apt, dbConn);
				traceUplineEropa(akumulasi.getMemberId(), akumulasi.getMemberId(), passMember, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void traceUpline(String checkMember, String memberId, Map<String, String> passMember, DBConnection dbConn) throws Exception {
		try {
			MemberTree memTree = MemberTreeFcd.getMemberTreeById((MemberTreeFcd.getMemberTreeById(memberId, dbConn)).getUplineId(), dbConn);
			//memTree = MemberTreeFcd.getMemberTreeById(memTree.getUplineId(), dbConn);
			if (memTree == null) {
				return;
			}
			System.out.println("Member saat ini === " + memTree.getMemberId());
			String findMember = (String) passMember.get(memTree.getMemberId());
			if (findMember != null) {
				System.out.println("Member ada di list");
				String downlineChecker = checkMember;
				String checker = MemberTreeFcd.getMemberTreeById((MemberTreeFcd.getMemberTreeById(checkMember, dbConn)).getUplineId(), dbConn).getMemberId();
				while (!checker.equals(findMember)) {
					MemberTree mtChecker = MemberTreeFcd.getMemberTreeById((MemberTreeFcd.getMemberTreeById(checker, dbConn)).getUplineId(), dbConn);
					downlineChecker = checker;
					checker = mtChecker.getMemberId();
				}
				
				AkumulasiPoinTourFcd aptFcd = new AkumulasiPoinTourFcd();
				AkumulasiPoinTour aptCheck = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(checkMember, bulan.toDate(), dbConn);
				AkumulasiPoinTour aptCheckTotal = aptFcd.getMemberPassByMember(checkMember, bulan.toDate(), dbConn);
				AkumulasiPoinTour akumPoin = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(memTree.getMemberId(), bulan.toDate(), dbConn);
				AkumulasiPoinTour akumPoinTotal = aptFcd.getMemberPassByMember(memTree.getMemberId(), bulan.toDate(), dbConn);
				akumPoin.setAkumPv(akumPoinTotal.getPvGroup()-akumPoin.getPoinBroken());
				System.out.println("aptCheckTotal.getPvGroup()=="+aptCheckTotal.getPvGroup()+"; akumPoin.getAkumPv()=="+akumPoin.getAkumPv());
//				if (aptCheckTotal.getPvGroup() >= 2*AKUMULASI_POIN_TOUR && (akumPoin.getAkumPv() >= 2*AKUMULASI_POIN_TOUR)) {
				if (aptCheck.getAkumPv() >= 2*AKUMULASI_POIN_TOUR && (akumPoin.getAkumPv() >= 2*AKUMULASI_POIN_TOUR)) {
					akumPoin.setPoinBroken(akumPoin.getPoinBroken() + (2*AKUMULASI_POIN_TOUR));
//					akumPoin.setAkumPv(akumPoin.getPvGroup() - akumPoin.getPoinBroken());
					akumPoin.setAkumPv(akumPoinTotal.getPvGroup() - akumPoin.getPoinBroken());
					if (downlineChecker.equals(memTree.getLeftFoot())) {
						akumPoin.setAkumKiri(akumPoin.getAkumKiri() - 2*AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByKiri())) {
							akumPoin.setBrokenByKiri(checkMember);
						} else {
							akumPoin.setBrokenByKiri(akumPoin.getBrokenByKiri() + ";" + checkMember);
						}
					} else if (downlineChecker.equals(memTree.getMiddleFoot())) {
						akumPoin.setAkumTengah(akumPoin.getAkumTengah() - 2*AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByTengah())) {
							akumPoin.setBrokenByTengah(checkMember);
						} else {
							akumPoin.setBrokenByTengah(akumPoin.getBrokenByTengah() + ";" + checkMember);
						}
					} else {
						akumPoin.setAkumKanan(akumPoin.getAkumKanan() - 2*AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByKanan())) {
							akumPoin.setBrokenByKanan(checkMember);
						} else {
							akumPoin.setBrokenByKanan(akumPoin.getBrokenByKanan() + ";" + checkMember);
						}
					}
					
				} else if (aptCheck.getAkumPv() >= AKUMULASI_POIN_TOUR && (akumPoin.getAkumPv() >= AKUMULASI_POIN_TOUR)) {
					akumPoin.setPoinBroken(akumPoin.getPoinBroken() + AKUMULASI_POIN_TOUR);
					akumPoin.setAkumPv(akumPoinTotal.getPvGroup() - akumPoin.getPoinBroken());
					if (downlineChecker.equals(memTree.getLeftFoot())) {
						akumPoin.setAkumKiri(akumPoin.getAkumKiri() - AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByKiri())) {
							akumPoin.setBrokenByKiri(checkMember);
						} else {
							akumPoin.setBrokenByKiri(akumPoin.getBrokenByKiri() + ";" + checkMember);
						}
					} else if (downlineChecker.equals(memTree.getMiddleFoot())) {
						akumPoin.setAkumTengah(akumPoin.getAkumTengah() - AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByTengah())) {
							akumPoin.setBrokenByTengah(checkMember);
						} else {
							akumPoin.setBrokenByTengah(akumPoin.getBrokenByTengah() + ";" + checkMember);
						}
					} else {
						akumPoin.setAkumKanan(akumPoin.getAkumKanan() - AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByKanan())) {
							akumPoin.setBrokenByKanan(checkMember);
						} else {
							akumPoin.setBrokenByKanan(akumPoin.getBrokenByKanan() + ";" + checkMember);
						}
					}
					
				}
				System.out.println("aptCheck.getAkumPv()=="+aptCheck.getAkumPv()+"; akumPoin.getPoinBroken()=="+akumPoin.getPoinBroken());
				aptFcd.updateAkumulasi(akumPoin, dbConn);
				//}
			}
			traceUpline(checkMember, memTree.getMemberId(), passMember, dbConn);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void traceUplineEropa(String checkMember, String memberId, Map<String, String> passMember, DBConnection dbConn) throws Exception {
		try {
			MemberTree memTree = MemberTreeFcd.getMemberTreeById((MemberTreeFcd.getMemberTreeById(memberId, dbConn)).getUplineId(), dbConn);
			//memTree = MemberTreeFcd.getMemberTreeById(memTree.getUplineId(), dbConn);
			if (memTree == null) {
				return;
			}
			System.out.println("Member saat ini === " + memTree.getMemberId());
			String findMember = (String) passMember.get(memTree.getMemberId());
			if (findMember != null) {
				System.out.println("Member ada di list");
				String downlineChecker = checkMember;
				String checker = MemberTreeFcd.getMemberTreeById((MemberTreeFcd.getMemberTreeById(checkMember, dbConn)).getUplineId(), dbConn).getMemberId();
				while (!checker.equals(findMember)) {
					MemberTree mtChecker = MemberTreeFcd.getMemberTreeById((MemberTreeFcd.getMemberTreeById(checker, dbConn)).getUplineId(), dbConn);
					downlineChecker = checker;
					checker = mtChecker.getMemberId();
				}
				
				AkumulasiPoinTourEropaFcd aptFcd = new AkumulasiPoinTourEropaFcd();
				AkumulasiPoinTourEropa aptCheck = AkumulasiPoinTourEropaFcd.getAkumulasiByMemberAndMonth(checkMember, bulan.toDate(), dbConn);
				AkumulasiPoinTourEropa aptCheckTotal = aptFcd.getMemberPassByMember(checkMember, bulan.toDate(), dbConn);
				AkumulasiPoinTourEropa akumPoin = AkumulasiPoinTourEropaFcd.getAkumulasiByMemberAndMonth(memTree.getMemberId(), bulan.toDate(), dbConn);
				AkumulasiPoinTourEropa akumPoinTotal = aptFcd.getMemberPassByMember(memTree.getMemberId(), bulan.toDate(), dbConn);
				akumPoin.setAkumPv(akumPoinTotal.getPvGroup()-akumPoin.getPoinBroken());
				System.out.println("aptCheckTotal.getPvGroup()=="+aptCheckTotal.getPvGroup()+"; akumPoin.getAkumPv()=="+akumPoin.getAkumPv());
//				if (aptCheckTotal.getPvGroup() >= 2*AKUMULASI_POIN_TOUR && (akumPoin.getAkumPv() >= 2*AKUMULASI_POIN_TOUR)) {
				/*
				if (aptCheck.getAkumPv() >= 2*AKUMULASI_POIN_TOUR_EROPA && (akumPoin.getAkumPv() >= 2*AKUMULASI_POIN_TOUR_EROPA)) {
					akumPoin.setPoinBroken(akumPoin.getPoinBroken() + (2*AKUMULASI_POIN_TOUR));
//					akumPoin.setAkumPv(akumPoin.getPvGroup() - akumPoin.getPoinBroken());
					akumPoin.setAkumPv(akumPoinTotal.getPvGroup() - akumPoin.getPoinBroken());
					if (downlineChecker.equals(memTree.getLeftFoot())) {
						akumPoin.setAkumKiri(akumPoin.getAkumKiri() - 2*AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByKiri())) {
							akumPoin.setBrokenByKiri(checkMember);
						} else {
							akumPoin.setBrokenByKiri(akumPoin.getBrokenByKiri() + ";" + checkMember);
						}
					} else if (downlineChecker.equals(memTree.getMiddleFoot())) {
						akumPoin.setAkumTengah(akumPoin.getAkumTengah() - 2*AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByTengah())) {
							akumPoin.setBrokenByTengah(checkMember);
						} else {
							akumPoin.setBrokenByTengah(akumPoin.getBrokenByTengah() + ";" + checkMember);
						}
					} else {
						akumPoin.setAkumKanan(akumPoin.getAkumKanan() - 2*AKUMULASI_POIN_TOUR);
						if ("".equals(akumPoin.getBrokenByKanan())) {
							akumPoin.setBrokenByKanan(checkMember);
						} else {
							akumPoin.setBrokenByKanan(akumPoin.getBrokenByKanan() + ";" + checkMember);
						}
					}
					
				} else*/
				if (aptCheck.getAkumPv() >= AKUMULASI_POIN_TOUR_EROPA && (akumPoin.getAkumPv() >= AKUMULASI_POIN_TOUR_EROPA)) {
					akumPoin.setPoinBroken(akumPoin.getPoinBroken() + AKUMULASI_POIN_TOUR_EROPA);
					akumPoin.setAkumPv(akumPoinTotal.getPvGroup() - akumPoin.getPoinBroken());
					if (downlineChecker.equals(memTree.getLeftFoot())) {
						akumPoin.setAkumKiri(akumPoin.getAkumKiri() - AKUMULASI_POIN_TOUR_EROPA);
						if ("".equals(akumPoin.getBrokenByKiri())) {
							akumPoin.setBrokenByKiri(checkMember);
						} else {
							akumPoin.setBrokenByKiri(akumPoin.getBrokenByKiri() + ";" + checkMember);
						}
					} else if (downlineChecker.equals(memTree.getMiddleFoot())) {
						akumPoin.setAkumTengah(akumPoin.getAkumTengah() - AKUMULASI_POIN_TOUR_EROPA);
						if ("".equals(akumPoin.getBrokenByTengah())) {
							akumPoin.setBrokenByTengah(checkMember);
						} else {
							akumPoin.setBrokenByTengah(akumPoin.getBrokenByTengah() + ";" + checkMember);
						}
					} else {
						akumPoin.setAkumKanan(akumPoin.getAkumKanan() - AKUMULASI_POIN_TOUR_EROPA);
						if ("".equals(akumPoin.getBrokenByKanan())) {
							akumPoin.setBrokenByKanan(checkMember);
						} else {
							akumPoin.setBrokenByKanan(akumPoin.getBrokenByKanan() + ";" + checkMember);
						}
					}
					
				}
				System.out.println("aptCheck.getAkumPv()=="+aptCheck.getAkumPv()+"; akumPoin.getPoinBroken()=="+akumPoin.getPoinBroken());
				aptFcd.updateAkumulasi(akumPoin, dbConn);
				//}
			}
			traceUplineEropa(checkMember, memTree.getMemberId(), passMember, dbConn);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void countAkumulasiPoin(DBConnection dbConn) {
		try {
			AkumulasiPoinTourFcd aptFcd = new AkumulasiPoinTourFcd();
			AkumulasiPoinTourSet aptSet = aptFcd.getWrongAkumulasiPv(bulan.toDate(), dbConn);
			for (int i=0; i<aptSet.length(); i++) {
				AkumulasiPoinTour apt = aptSet.get(i);
				AkumulasiPoinTour akum = AkumulasiPoinTourFcd.getAkumulasiByMemberAndMonth(apt.getMemberId(), bulan.toDate(), dbConn);
				akum.setAkumPv(apt.getPvGroup() - apt.getPoinBroken());
				aptFcd.updateAkumulasi(akum, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void countAkumulasiPoinEropa(DBConnection dbConn) {
		try {
			AkumulasiPoinTourEropaFcd aptFcd = new AkumulasiPoinTourEropaFcd();
			AkumulasiPoinTourEropaSet aptSet = aptFcd.getWrongAkumulasiPv(bulan.toDate(), dbConn);
			for (int i=0; i<aptSet.length(); i++) {
				AkumulasiPoinTourEropa apt = aptSet.get(i);
				AkumulasiPoinTourEropa akum = AkumulasiPoinTourEropaFcd.getAkumulasiByMemberAndMonth(apt.getMemberId(), bulan.toDate(), dbConn);
				akum.setAkumPv(apt.getPvGroup() - apt.getPoinBroken());
				aptFcd.updateAkumulasi(akum, dbConn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private float getPvFoot(MemberTree mtree, String memberid) {
		float pvGroup;
		if (memberid.equals(mtree.getBigfoot())) {
			pvGroup = mtree.getPvBigfoot();
		} else if (memberid.equals(mtree.getMediumfoot())) {
			pvGroup = mtree.getPvMidfoot();
		} else {
			pvGroup = mtree.getPvLitfoot();
		}
		return pvGroup;
	}
}
