package id.co.birumerah.bsi.member;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.joda.time.MutableDateTime;

import id.co.birumerah.util.dbaccess.DBConnection;

public class MemberTreeFcd {

	private MemberTree mTree;
	
	public MemberTreeFcd() {}
	
	public MemberTreeFcd(MemberTree mTree) {
		this.mTree = mTree;
	}

	public MemberTree getMemberTree() {
		return mTree;
	}

	public void setMemberTree(MemberTree mTree) {
		this.mTree = mTree;
	}
	
	public static MemberTreeSet showAllMemberTree() throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		String whereClause = "status=1 ORDER BY reg_date, sort_number";
		try {
			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSmallSet showAllPvGroupMemberTree() throws Exception {
		DBConnection dbConn = null;
		MemberTreeSmallSet mtSet = null;
		
		String whereClause = "status=1 ORDER BY reg_date, sort_number";
		try {
			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtSet = mtDAO.selectPvGroup(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTree(DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		String whereClause = "status=1 ORDER BY reg_date, sort_number";
//		String whereClause = "status=1 ORDER BY sort_number";
		try {
//			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTreeSet showAllMemberTreeBeforeToday() throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
//		DateTime beginOfDay = new DateTime();
		MutableDateTime beginOfDay = new MutableDateTime();
		beginOfDay.setHourOfDay(0);
		beginOfDay.setMinuteOfHour(0);
		beginOfDay.setSecondOfMinute(0);
		
		String whereClause = "reg_date < '" + beginOfDay.toString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY reg_date";
		try {
			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public MemberTreeSet search(String whereClause) throws Exception {
		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		try {
			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public MemberTreeSet search(String whereClause, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		MemberTreeSet mtSet = null;
		
		try {
//			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return mtSet;
	}
	
	public static MemberTree getMemberTreeById(String memberTreeId) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MemberTree memTree = new MemberTree();
			memTree.setMemberId(memberTreeId);
			
			MemberTreeDAO memDAO = new MemberTreeDAO(dbConn);
			boolean found = memDAO.select(memTree);
			
			if (found) {
				return memTree;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static MemberTree getMemberTreeById(String memberTreeId, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			if (dbConn == null || dbConn.isClosed())
				System.out.println("koneksinya null atau ketutup");
			MemberTree memTree = new MemberTree();
			memTree.setMemberId(memberTreeId);
			
			MemberTreeDAO memDAO = new MemberTreeDAO(dbConn);
			boolean found = memDAO.select(memTree);
			
			if (found) {
				return memTree;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public static List<MemberTree> getMemberTreeByUplineId(String uplineId) throws Exception {
		DBConnection dbConn = null;
		List<MemberTree> members = null;
		
		try {
			dbConn = new DBConnection();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			String whereClause = "upline_id = '" + uplineId + "'";
			members = mtDAO.selectMembersByUplineId(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			if (dbConn != null) dbConn.close();
		}
		return members;
	}
	
	public static MemberTreeSet getMemberTreeByUplineId(String uplineId, DBConnection dbConn) throws Exception {
		MemberTreeSet mtSet = null;
		
		try {
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			String whereClause = "upline_id = '" + uplineId + "' AND status=1";
			mtSet = mtDAO.selectMembersByUplineId2(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return mtSet;
	}
	
	public static MemberTree getMemberTreeBySortNumber(long sortNumber, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			if (dbConn == null || dbConn.isClosed())
				System.out.println("koneksinya null atau ketutup");
			MemberTree memTree = new MemberTree();
			memTree.setSortNumber(sortNumber);
			
			MemberTreeDAO memDAO = new MemberTreeDAO(dbConn);
			boolean found = memDAO.selectMemberBySortNumber(memTree);
			
			if (found) {
				return memTree;
			} else {
				System.err.println("Member not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
//		} finally {
//			if (dbConn != null) dbConn.close();
		}
		return null;
	}
	
	public void insertMemberTree() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			mtDAO.insert(mTree);
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		
	}
	
	public void updateMemberTree(MemberTree memberTree) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Update Member Tree failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void updateMemberTree(MemberTree memberTree, DBConnection dbConn) throws Exception {
//		DBConnection dbConn = null;
		try {
//			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
//		} finally {
//			if (dbConn != null) {
//				dbConn.close();
//			}
		}
	}
	
	public void updateAm(MemberTree memberTree, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updatePvGroup(MemberTreeSmall memberTree, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.updatePvGrup(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void updateStatus(Date tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.updateStatus(tanggal) < 1) {
				throw new Exception("Update status failed.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public void setTanggal(MutableDateTime tanggal) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.setTanggalMemberTree(tanggal) < 1) {
				throw new Exception("Update tanggal gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void setTanggal(MutableDateTime tanggal, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.setTanggalMemberTree(tanggal) < 1) {
				throw new Exception("Update tanggal gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			dbConn.rollbackTransaction();
			throw new Exception(e);
		}
	}
	
	public void dailyBackup(MutableDateTime tglBatas) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.backupHarianMemberTree(tglBatas) < 1) {
				throw new Exception("Backup harian gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void dailyBackup(MutableDateTime tglBatas, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.backupHarianMemberTree(tglBatas) < 1) {
				throw new Exception("Backup harian gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			dbConn.rollbackTransaction();
			throw new Exception(e);
		}
	}
	
	public void monthlyBackup(MutableDateTime tglBatas) throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.backupBulananMemberTree(tglBatas) < 1) {
				throw new Exception("Backup bulanan gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void resetMemberTree() throws Exception {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.resetMemberTree() < 1) {
				throw new Exception("Reset member tree gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
	}
	
	public void resetMemberTree(DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			if (mtDAO.resetMemberTree() < 1) {
				throw new Exception("Reset member tree gagal.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
	
	public boolean hasNullMember(DBConnection dbConn) throws Exception {
		boolean adaNull = false;
		try {
			MemberTreeDAO mtDAO = new MemberTreeDAO(dbConn);
			adaNull = mtDAO.anyNullMember();
			mtDAO = null;
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
		return adaNull;
	}
}
