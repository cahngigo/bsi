package id.co.birumerah.bsi.member;

import java.io.Serializable;
import java.util.ArrayList;

public class MemberSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5941711567700426766L;
	
	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public MemberSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(Member dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public Member get(int index) {
		Member result = null;
		
		if ((index >= 0) && (index < length()))
			result = (Member) set.get(index);
		
		return result;
	}

}
