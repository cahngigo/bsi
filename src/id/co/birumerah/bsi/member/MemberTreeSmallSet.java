package id.co.birumerah.bsi.member;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class MemberTreeSmallSet implements Serializable {

	@SuppressWarnings("unchecked")
	ArrayList set = null;
	
	@SuppressWarnings("unchecked")
	public MemberTreeSmallSet() {
		set = new ArrayList();
	}
	
	@SuppressWarnings("unchecked")
	public void add(MemberTreeSmall dataObject) {
		set.add(dataObject);
	}
	
	public int length() {
		return set.size();
	}
	
	public MemberTreeSmall get(int index) {
		MemberTreeSmall result = null;
		
		if ((index >= 0) && (index < length())) 
			result = (MemberTreeSmall) set.get(index);
		
		return result;
	}
}
