package id.co.birumerah.bsi.member;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class MemberTreeSmallHarianFcd {

	public MemberTreeSmallHarianFcd() {}
	
	public MemberTreeSmallSet search(String whereClause, DBConnection dbConn) throws Exception {
		MemberTreeSmallSet mtSet = null;
		
		try {
			MemberTreeSmallHarianDAO mtDAO = new MemberTreeSmallHarianDAO(dbConn);
			mtSet = mtDAO.select(whereClause);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return mtSet;
	}
	
	public void updateMemberTree(MemberTreeSmall memberTree, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			MemberTreeSmallHarianDAO mtDAO = new MemberTreeSmallHarianDAO(dbConn);
			if (mtDAO.update(memberTree) < 1) {
				throw new Exception("Member not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
