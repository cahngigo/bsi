package id.co.birumerah.bsi.member;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.util.dbaccess.SQLAccess;
import id.co.birumerah.util.dbaccess.SQLValueFilter;

public class MemberTreeDAO {

	DBConnection dbConn;
	DateFormat shortFormat;
	DateFormat longFormat;
	
	public MemberTreeDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
		shortFormat = new SimpleDateFormat("yyyy-MM-dd");
		longFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public int insert(MemberTree dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO member_tree (");
		sb.append("member_id,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("pvpribadi,");
		sb.append("bigfoot,");
		sb.append("mediumfoot,");
		sb.append("litfoot,");
		sb.append("bigfoot_nonbasic,");
		sb.append("mediumfoot_nonbasic,");
		sb.append("litfoot_nonbasic,");
		sb.append("left_foot,");
		sb.append("middle_foot,");
		sb.append("right_foot,");
		sb.append("amt_bigfoot,");
		sb.append("amt_midfoot,");
		sb.append("amt_litfoot,");
		sb.append("amt_bigfoot_nonbasic,");
		sb.append("amt_midfoot_nonbasic,");
		sb.append("amt_litfoot_nonbasic,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
		sb.append("potongan_am,");
		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
		sb.append("saldo_ewallet,");
		sb.append("saldo_ddr,");
		sb.append("ddr_bigfoot,");
		sb.append("ddr_midfoot,");
		sb.append("ddr_litfoot,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("is_basic ");
		
		sb.append(") VALUES (");
		
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getMemberId()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getUplineId()) +"',");
		sb.append("'"+ longFormat.format(dataObject.getRegDate()) +"',");
		sb.append(""+ dataObject.getPvpribadi() +",");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getBigfoot()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getMediumfoot()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getLitfoot()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getBigfootNonbasic()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getMediumfootNonbasic()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getLitfootNonbasic()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getLeftFoot()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getMiddleFoot()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getRightFoot()) +"',");
		sb.append(""+ dataObject.getAmtBigfoot() +",");
		sb.append(""+ dataObject.getAmtMidfoot() +",");
		sb.append(""+ dataObject.getAmtLitfoot() +",");
		sb.append(""+ dataObject.getAmtBigfootNonbasic() +",");
		sb.append(""+ dataObject.getAmtMidfootNonbasic() +",");
		sb.append(""+ dataObject.getAmtLitfootNonbasic() +",");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getPeringkat()) +"',");
		sb.append("'"+ SQLValueFilter.normalizeString(dataObject.getKualifikasi()) +"',");
		sb.append("'"+ shortFormat.format(dataObject.getTanggal()) +"',");
		sb.append(""+ dataObject.getPotonganAm() +",");
		sb.append(""+ dataObject.getSaldoAm() +",");
		sb.append(""+ dataObject.getSaldoPoin() +",");
		sb.append(""+ dataObject.getSaldoBv() +",");
		sb.append(""+ dataObject.getSaldoEwallet() +",");
		sb.append(""+ dataObject.getSaldoDdr() +",");
		sb.append(""+ dataObject.getDdrBigfoot() +",");
		sb.append(""+ dataObject.getDdrMidfoot() +",");
		sb.append(""+ dataObject.getDdrLitfoot() +",");
		sb.append(""+ dataObject.getPvBigfoot() +",");
		sb.append(""+ dataObject.getPvMidfoot() +",");
		sb.append(""+ dataObject.getPvLitfoot() +",");
		sb.append(""+ dataObject.getIsBasic());
		sb.append(")");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int update(MemberTree dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE member_tree ");
		sb.append("SET ");
//		sb.append("upline_id = '" + SQLValueFilter.normalizeString(dataObject.getUplineId()) + "',");
//		sb.append("reg_date = '" + longFormat.format(dataObject.getRegDate()) + "',");
//		sb.append("pvpribadi = " + dataObject.getPvpribadi() + ",");
		sb.append("bigfoot = '" + SQLValueFilter.normalizeString(dataObject.getBigfoot()) + "',");
		sb.append("mediumfoot = '" + SQLValueFilter.normalizeString(dataObject.getMediumfoot()) + "',");
		sb.append("litfoot = '" + SQLValueFilter.normalizeString(dataObject.getLitfoot()) + "',");
		sb.append("bigfoot_nonbasic = '" + SQLValueFilter.normalizeString(dataObject.getBigfootNonbasic()) + "',");
		sb.append("mediumfoot_nonbasic = '" + SQLValueFilter.normalizeString(dataObject.getMediumfootNonbasic()) + "',");
		sb.append("litfoot_nonbasic = '" + SQLValueFilter.normalizeString(dataObject.getLitfootNonbasic()) + "',");
//		sb.append("left_foot = '" + SQLValueFilter.normalizeString(dataObject.getLeftFoot()) + "',");
//		sb.append("middle_foot = '" + SQLValueFilter.normalizeString(dataObject.getMiddleFoot()) + "',");
//		sb.append("right_foot = '" + SQLValueFilter.normalizeString(dataObject.getRightFoot()) + "',");
		sb.append("amt_bigfoot = " + dataObject.getAmtBigfoot() + ",");
		sb.append("amt_midfoot = " + dataObject.getAmtMidfoot() + ",");
		sb.append("amt_litfoot = " + dataObject.getAmtLitfoot() + ",");
		sb.append("amt_bigfoot_nonbasic = " + dataObject.getAmtBigfootNonbasic() + ",");
		sb.append("amt_midfoot_nonbasic = " + dataObject.getAmtMidfootNonbasic() + ",");
		sb.append("amt_litfoot_nonbasic = " + dataObject.getAmtLitfootNonbasic() + ",");
		sb.append("peringkat = '" + SQLValueFilter.normalizeString(dataObject.getPeringkat()) + "',");
		sb.append("kualifikasi = '" + SQLValueFilter.normalizeString(dataObject.getKualifikasi()) + "',");
		if (dataObject.getTanggal() != null) sb.append("tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
		sb.append("saldo_am = " + dataObject.getSaldoAm() + ",");
//		sb.append("saldo_poin = " + dataObject.getSaldoPoin() + ",");
//		sb.append("saldo_bv = " + dataObject.getSaldoBv() + ",");
//		sb.append("saldo_ewallet = " + dataObject.getSaldoEwallet() + ",");
		sb.append("saldo_ddr = " + dataObject.getSaldoDdr() + ",");
		sb.append("ddr_bigfoot = " + dataObject.getDdrBigfoot() + ",");
		sb.append("ddr_midfoot = " + dataObject.getDdrMidfoot() + ",");
		sb.append("ddr_litfoot = " + dataObject.getDdrLitfoot() + ",");
		sb.append("pv_bigfoot = " + dataObject.getPvBigfoot() + ",");
		sb.append("pv_midfoot = " + dataObject.getPvMidfoot() + ",");
		sb.append("pv_litfoot = " + dataObject.getPvLitfoot() + ",");
//		sb.append("is_promoxenia = " + dataObject.getIsPromoxenia() + ",");
		sb.append("bigfoot_promo = " + dataObject.getBigfootPromo() + ",");
		sb.append("midfoot_promo = " + dataObject.getMidfootPromo() + ",");
		sb.append("litfoot_promo = " + dataObject.getLitfootPromo() + ",");
//		sb.append("is_basic = " + dataObject.getIsBasic() + ",");
//		sb.append("sort_number = " + dataObject.getSortNumber() + ",");
		sb.append("status = " + dataObject.getStatus() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int updateAm(MemberTree dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE member_tree ");
		sb.append("SET ");
//		sb.append("upline_id = '" + SQLValueFilter.normalizeString(dataObject.getUplineId()) + "',");
//		sb.append("reg_date = '" + longFormat.format(dataObject.getRegDate()) + "',");
		sb.append("pvpribadi = " + dataObject.getPvpribadi() + ",");
//		sb.append("bigfoot = '" + SQLValueFilter.normalizeString(dataObject.getBigfoot()) + "',");
//		sb.append("mediumfoot = '" + SQLValueFilter.normalizeString(dataObject.getMediumfoot()) + "',");
//		sb.append("litfoot = '" + SQLValueFilter.normalizeString(dataObject.getLitfoot()) + "',");
//		sb.append("left_foot = '" + SQLValueFilter.normalizeString(dataObject.getLeftFoot()) + "',");
//		sb.append("middle_foot = '" + SQLValueFilter.normalizeString(dataObject.getMiddleFoot()) + "',");
//		sb.append("right_foot = '" + SQLValueFilter.normalizeString(dataObject.getRightFoot()) + "',");
//		sb.append("amt_bigfoot = " + dataObject.getAmtBigfoot() + ",");
//		sb.append("amt_midfoot = " + dataObject.getAmtMidfoot() + ",");
//		sb.append("amt_litfoot = " + dataObject.getAmtLitfoot() + ",");
//		sb.append("peringkat = '" + SQLValueFilter.normalizeString(dataObject.getPeringkat()) + "',");
//		sb.append("kualifikasi = '" + SQLValueFilter.normalizeString(dataObject.getKualifikasi()) + "',");
//		if (dataObject.getTanggal() != null) sb.append("tanggal = '" + shortFormat.format(dataObject.getTanggal()) + "',");
		sb.append("potongan_am = " + dataObject.getPotonganAm() + ",");
		sb.append("saldo_am = " + dataObject.getSaldoAm() + ",");
//		sb.append("saldo_poin = " + dataObject.getSaldoPoin() + ",");
		sb.append("saldo_bv = " + dataObject.getSaldoBv() + " ");
//		sb.append("saldo_ewallet = " + dataObject.getSaldoEwallet() + ",");
//		sb.append("pv_bigfoot = " + dataObject.getPvBigfoot() + ",");
//		sb.append("pv_midfoot = " + dataObject.getPvMidfoot() + ",");
//		sb.append("pv_litfoot = " + dataObject.getPvLitfoot() + ",");
//		sb.append("is_promoxenia = " + dataObject.getIsPromoxenia() + ",");
//		sb.append("bigfoot_promo = " + dataObject.getBigfootPromo() + ",");
//		sb.append("midfoot_promo = " + dataObject.getMidfootPromo() + ",");
//		sb.append("litfoot_promo = " + dataObject.getLitfootPromo() + ",");
//		sb.append("sort_number = " + dataObject.getSortNumber() + ",");
//		sb.append("status = " + dataObject.getStatus() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int updatePvGrup(MemberTreeSmall dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE member_tree ");
		sb.append("SET ");
//		sb.append("pvpribadi = " + dataObject.getPvpribadi() + ",");
		sb.append("pv_besar = " + dataObject.getPvBesar() + ",");
		sb.append("pv_sedang = " + dataObject.getPvSedang() + ",");
		sb.append("pv_kecil = " + dataObject.getPvKecil() + " ");
		sb.append("WHERE member_id = '" + dataObject.getMemberId() + "'");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int updateStatus(Date tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE member_tree SET status=0 WHERE member_id in (");
		sb.append("SELECT member_id FROM member WHERE DATE(reg_date) >= '" + shortFormat.format(tanggal) + "')");
		System.out.println("Query: " + sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int setTanggalMemberTree(MutableDateTime tanggal) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE member_tree SET tanggal = '" + tanggal.toString("yyyy-MM-dd") + "'");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int delete(MemberTree dataObject) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM member_tree WHERE ");
		sb.append("member_id = '" + dataObject.getMemberId() + "' ");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean select(MemberTree dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("pvpribadi,");
		sb.append("bigfoot,");
		sb.append("mediumfoot,");
		sb.append("litfoot,");
		sb.append("bigfoot_nonbasic,");
		sb.append("mediumfoot_nonbasic,");
		sb.append("litfoot_nonbasic,");
		sb.append("left_foot,");
		sb.append("middle_foot,");
		sb.append("right_foot,");
		sb.append("amt_bigfoot,");
		sb.append("amt_midfoot,");
		sb.append("amt_litfoot,");
		sb.append("amt_bigfoot_nonbasic,");
		sb.append("amt_midfoot_nonbasic,");
		sb.append("amt_litfoot_nonbasic,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
		sb.append("potongan_am,");
		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
		sb.append("saldo_ewallet,");
		sb.append("saldo_ddr,");
		sb.append("ddr_bigfoot,");
		sb.append("ddr_midfoot,");
		sb.append("ddr_litfoot,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("is_promoxenia,");
		sb.append("bigfoot_promo,");
		sb.append("midfoot_promo,");
		sb.append("litfoot_promo,");
		sb.append("is_basic,");
		sb.append("is_basic_new,");
		sb.append("sort_number,");
		sb.append("status ");
		sb.append("FROM member_tree WHERE member_id = '" + dataObject.getMemberId() + "'");
//		System.out.println("Query ("+dataObject.getMemberId()+") : " +sb.toString());
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
//			MutableDateTime sekarang = new MutableDateTime();
//			System.out.println("Sebelum execute select "+sekarang.toString("yyyy-MM-dd HH:mm:ss.SSS"));
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
//			sekarang = new MutableDateTime();
//			System.out.println("Selesai execute select "+sekarang.toString("yyyy-MM-dd HH:mm:ss.SSS"));
//			System.out.println("rs member id = "+rs.getString("member_id"));
			if (rs.next()) {
//				System.out.println("member id dlm if: "+dataObject.getMemberId());
				result = true;
				dataObject.setUplineId(rs.getString("upline_id"));
//				System.out.println("upline id = "+ dataObject.getUplineId());
//				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
//				System.out.println("reg_date = "+dataObject.getRegDate());
//				System.out.println("reg_date = "+longFormat.format(dataObject.getRegDate()));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
				dataObject.setBigfoot(rs.getString("bigfoot"));
				dataObject.setMediumfoot(rs.getString("mediumfoot"));
				dataObject.setLitfoot(rs.getString("litfoot"));
				dataObject.setBigfootNonbasic(rs.getString("bigfoot_nonbasic"));
				dataObject.setMediumfootNonbasic(rs.getString("mediumfoot_nonbasic"));
				dataObject.setLitfootNonbasic(rs.getString("litfoot_nonbasic"));
				dataObject.setLeftFoot(rs.getString("left_foot"));
				dataObject.setMiddleFoot(rs.getString("middle_foot"));
				dataObject.setRightFoot(rs.getString("right_foot"));
				dataObject.setAmtBigfoot(rs.getInt("amt_bigfoot"));
				dataObject.setAmtMidfoot(rs.getInt("amt_midfoot"));
				dataObject.setAmtLitfoot(rs.getInt("amt_litfoot"));
				dataObject.setAmtBigfootNonbasic(rs.getInt("amt_bigfoot_nonbasic"));
				dataObject.setAmtMidfootNonbasic(rs.getInt("amt_midfoot_nonbasic"));
				dataObject.setAmtLitfootNonbasic(rs.getInt("amt_litfoot_nonbasic"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setKualifikasi(rs.getString("kualifikasi"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setSaldoAm(rs.getDouble("saldo_am"));
				dataObject.setSaldoPoin(rs.getFloat("saldo_poin"));
				dataObject.setSaldoBv(rs.getDouble("saldo_bv"));
				dataObject.setSaldoEwallet(rs.getDouble("saldo_ewallet"));
				dataObject.setSaldoDdr(rs.getInt("saldo_ddr"));
				dataObject.setDdrBigfoot(rs.getInt("ddr_bigfoot"));
				dataObject.setDdrMidfoot(rs.getInt("ddr_midfoot"));
				dataObject.setDdrLitfoot(rs.getInt("ddr_litfoot"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				dataObject.setIsPromoxenia(rs.getInt("is_promoxenia"));
				dataObject.setBigfootPromo(rs.getInt("bigfoot_promo"));
				dataObject.setMidfootPromo(rs.getInt("midfoot_promo"));
				dataObject.setLitfootPromo(rs.getInt("litfoot_promo"));
				dataObject.setIsBasic(rs.getInt("is_basic"));
				dataObject.setIsBasicNew(rs.getInt("is_basic_new"));
				dataObject.setSortNumber(rs.getLong("sort_number"));
				dataObject.setStatus(rs.getInt("status"));
			} 
			else{
				System.out.println("member id: "+dataObject.getMemberId());
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public MemberTreeSet select(String whereClause) throws SQLException {
		MemberTreeSet dataObjectSet = new MemberTreeSet();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("pvpribadi,");
		sb.append("bigfoot,");
		sb.append("mediumfoot,");
		sb.append("litfoot,");
		sb.append("bigfoot_nonbasic,");
		sb.append("mediumfoot_nonbasic,");
		sb.append("litfoot_nonbasic,");
		sb.append("left_foot,");
		sb.append("middle_foot,");
		sb.append("right_foot,");
		sb.append("amt_bigfoot,");
		sb.append("amt_midfoot,");
		sb.append("amt_litfoot,");
		sb.append("amt_bigfoot_nonbasic,");
		sb.append("amt_midfoot_nonbasic,");
		sb.append("amt_litfoot_nonbasic,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
		sb.append("potongan_am,");
		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
		sb.append("saldo_ewallet,");
		sb.append("saldo_ddr,");
		sb.append("ddr_bigfoot,");
		sb.append("ddr_midfoot,");
		sb.append("ddr_litfoot,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("is_promoxenia,");
		sb.append("bigfoot_promo,");
		sb.append("midfoot_promo,");
		sb.append("litfoot_promo,");
		sb.append("is_basic,");
		sb.append("is_basic_new,");
		sb.append("sort_number,");
		sb.append("status ");
		sb.append("FROM member_tree WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				MemberTree dataObject = new MemberTree();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setUplineId(rs.getString("upline_id"));
//				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
				dataObject.setBigfoot(rs.getString("bigfoot"));
				dataObject.setMediumfoot(rs.getString("mediumfoot"));
				dataObject.setLitfoot(rs.getString("litfoot"));
				dataObject.setBigfootNonbasic(rs.getString("bigfoot_nonbasic"));
				dataObject.setMediumfootNonbasic(rs.getString("mediumfoot_nonbasic"));
				dataObject.setLitfootNonbasic(rs.getString("litfoot_nonbasic"));
				dataObject.setLeftFoot(rs.getString("left_foot"));
				dataObject.setMiddleFoot(rs.getString("middle_foot"));
				dataObject.setRightFoot(rs.getString("right_foot"));
				dataObject.setAmtBigfoot(rs.getInt("amt_bigfoot"));
				dataObject.setAmtMidfoot(rs.getInt("amt_midfoot"));
				dataObject.setAmtLitfoot(rs.getInt("amt_litfoot"));
				dataObject.setAmtBigfootNonbasic(rs.getInt("amt_bigfoot_nonbasic"));
				dataObject.setAmtMidfootNonbasic(rs.getInt("amt_midfoot_nonbasic"));
				dataObject.setAmtLitfootNonbasic(rs.getInt("amt_litfoot_nonbasic"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setKualifikasi(rs.getString("kualifikasi"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setSaldoAm(rs.getDouble("saldo_am"));
				dataObject.setSaldoPoin(rs.getFloat("saldo_poin"));
				dataObject.setSaldoBv(rs.getFloat("saldo_bv"));
				dataObject.setSaldoEwallet(rs.getDouble("saldo_ewallet"));
				dataObject.setSaldoDdr(rs.getInt("saldo_ddr"));
				dataObject.setDdrBigfoot(rs.getInt("ddr_bigfoot"));
				dataObject.setDdrMidfoot(rs.getInt("ddr_midfoot"));
				dataObject.setDdrLitfoot(rs.getInt("ddr_litfoot"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				dataObject.setIsPromoxenia(rs.getInt("is_promoxenia"));
				dataObject.setBigfootPromo(rs.getInt("bigfoot_promo"));
				dataObject.setMidfootPromo(rs.getInt("midfoot_promo"));
				dataObject.setLitfootPromo(rs.getInt("litfoot_promo"));
				dataObject.setIsBasic(rs.getInt("is_basic"));
				dataObject.setIsBasicNew(rs.getInt("is_basic_new"));
				dataObject.setSortNumber(rs.getLong("sort_number"));
				dataObject.setStatus(rs.getInt("status"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public MemberTreeSmallSet selectPvGroup(String whereClause) throws SQLException {
		MemberTreeSmallSet dataObjectSet = new MemberTreeSmallSet();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("pvpribadi,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("pv_besar,");
		sb.append("pv_sedang,");
		sb.append("pv_kecil ");
		sb.append("FROM member_tree WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				MemberTreeSmall dataObject = new MemberTreeSmall();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
				dataObject.setPvBesar(rs.getFloat("pv_besar"));
				dataObject.setPvSedang(rs.getFloat("pv_sedang"));
				dataObject.setPvKecil(rs.getFloat("pv_kecil"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public List<MemberTree> selectMembersByUplineId(String whereClause) throws Exception {
		List<MemberTree> lstMember = new ArrayList<MemberTree>();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("pvpribadi,");
		sb.append("bigfoot,");
		sb.append("mediumfoot,");
		sb.append("litfoot,");
		sb.append("bigfoot_nonbasic,");
		sb.append("mediumfoot_nonbasic,");
		sb.append("litfoot_nonbasic,");
		sb.append("left_foot,");
		sb.append("middle_foot,");
		sb.append("right_foot,");
		sb.append("amt_bigfoot,");
		sb.append("amt_midfoot,");
		sb.append("amt_litfoot,");
		sb.append("amt_bigfoot_nonbasic,");
		sb.append("amt_midfoot_nonbasic,");
		sb.append("amt_litfoot_nonbasic,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
		sb.append("potongan_am,");
		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
		sb.append("saldo_ewallet,");
		sb.append("saldo_ddr,");
		sb.append("ddr_bigfoot,");
		sb.append("ddr_midfoot,");
		sb.append("ddr_litfoot,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("is_basic,");
		sb.append("is_basic_new,");
		sb.append("sort_number,");
		sb.append("status ");
		sb.append("FROM member_tree WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				MemberTree dataObject = new MemberTree();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setUplineId(rs.getString("upline_id"));
//				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
				dataObject.setBigfoot(rs.getString("bigfoot"));
				dataObject.setMediumfoot(rs.getString("mediumfoot"));
				dataObject.setLitfoot(rs.getString("litfoot"));
				dataObject.setBigfootNonbasic(rs.getString("bigfoot_nonbasic"));
				dataObject.setMediumfootNonbasic(rs.getString("mediumfoot_nonbasic"));
				dataObject.setLitfootNonbasic(rs.getString("litfoot_nonbasic"));
				dataObject.setLeftFoot(rs.getString("left_foot"));
				dataObject.setMiddleFoot(rs.getString("middle_foot"));
				dataObject.setRightFoot(rs.getString("right_foot"));
				dataObject.setAmtBigfoot(rs.getInt("amt_bigfoot"));
				dataObject.setAmtMidfoot(rs.getInt("amt_midfoot"));
				dataObject.setAmtLitfoot(rs.getInt("amt_litfoot"));
				dataObject.setAmtBigfootNonbasic(rs.getInt("amt_bigfoot_nonbasic"));
				dataObject.setAmtMidfootNonbasic(rs.getInt("amt_midfoot_nonbasic"));
				dataObject.setAmtLitfootNonbasic(rs.getInt("amt_litfoot_nonbasic"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setKualifikasi(rs.getString("kualifikasi"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setSaldoAm(rs.getDouble("saldo_am"));
				dataObject.setSaldoPoin(rs.getFloat("saldo_poin"));
				dataObject.setSaldoBv(rs.getFloat("saldo_bv"));
				dataObject.setSaldoEwallet(rs.getDouble("saldo_ewallet"));
				dataObject.setSaldoDdr(rs.getInt("saldo_ddr"));
				dataObject.setDdrBigfoot(rs.getInt("ddr_bigfoot"));
				dataObject.setDdrMidfoot(rs.getInt("ddr_midfoot"));
				dataObject.setDdrLitfoot(rs.getInt("ddr_litfoot"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				dataObject.setIsBasic(rs.getInt("is_basic"));
				dataObject.setIsBasicNew(rs.getInt("is_basic_new"));
				dataObject.setSortNumber(rs.getLong("sort_number"));
				dataObject.setStatus(rs.getInt("status"));
				
				lstMember.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return lstMember;
	}
	
	public MemberTreeSet selectMembersByUplineId2(String whereClause) throws SQLException {
		MemberTreeSet dataObjectSet = new MemberTreeSet();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("pvpribadi,");
		sb.append("bigfoot,");
		sb.append("mediumfoot,");
		sb.append("litfoot,");
		sb.append("bigfoot_nonbasic,");
		sb.append("mediumfoot_nonbasic,");
		sb.append("litfoot_nonbasic,");
		sb.append("left_foot,");
		sb.append("middle_foot,");
		sb.append("right_foot,");
		sb.append("amt_bigfoot,");
		sb.append("amt_midfoot,");
		sb.append("amt_litfoot,");
		sb.append("amt_bigfoot_nonbasic,");
		sb.append("amt_midfoot_nonbasic,");
		sb.append("amt_litfoot_nonbasic,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
		sb.append("potongan_am,");
		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
		sb.append("saldo_ewallet,");
		sb.append("saldo_ddr,");
		sb.append("ddr_bigfoot,");
		sb.append("ddr_midfoot,");
		sb.append("ddr_litfoot,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("is_basic,");
		sb.append("is_basic_new,");
		sb.append("sort_number,");
		sb.append("status ");
		sb.append("FROM member_tree WHERE " + whereClause);
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			while (rs.next()) {
				MemberTree dataObject = new MemberTree();
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setUplineId(rs.getString("upline_id"));
//				dataObject.setRegDate(rs.getDate("reg_date"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
				dataObject.setBigfoot(rs.getString("bigfoot"));
				dataObject.setMediumfoot(rs.getString("mediumfoot"));
				dataObject.setLitfoot(rs.getString("litfoot"));
				dataObject.setBigfootNonbasic(rs.getString("bigfoot_nonbasic"));
				dataObject.setMediumfootNonbasic(rs.getString("mediumfoot_nonbasic"));
				dataObject.setLitfootNonbasic(rs.getString("litfoot_nonbasic"));
				dataObject.setLeftFoot(rs.getString("left_foot"));
				dataObject.setMiddleFoot(rs.getString("middle_foot"));
				dataObject.setRightFoot(rs.getString("right_foot"));
				dataObject.setAmtBigfoot(rs.getInt("amt_bigfoot"));
				dataObject.setAmtMidfoot(rs.getInt("amt_midfoot"));
				dataObject.setAmtLitfoot(rs.getInt("amt_litfoot"));
				dataObject.setAmtBigfootNonbasic(rs.getInt("amt_bigfoot_nonbasic"));
				dataObject.setAmtMidfootNonbasic(rs.getInt("amt_midfoot_nonbasic"));
				dataObject.setAmtLitfootNonbasic(rs.getInt("amt_litfoot_nonbasic"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setKualifikasi(rs.getString("kualifikasi"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setSaldoAm(rs.getDouble("saldo_am"));
				dataObject.setSaldoPoin(rs.getFloat("saldo_poin"));
				dataObject.setSaldoBv(rs.getFloat("saldo_bv"));
				dataObject.setSaldoEwallet(rs.getDouble("saldo_ewallet"));
				dataObject.setSaldoDdr(rs.getInt("saldo_ddr"));
				dataObject.setDdrBigfoot(rs.getInt("ddr_bigfoot"));
				dataObject.setDdrMidfoot(rs.getInt("ddr_midfoot"));
				dataObject.setDdrLitfoot(rs.getInt("ddr_litfoot"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				dataObject.setIsBasic(rs.getInt("is_basic"));
				dataObject.setIsBasicNew(rs.getInt("is_basic_new"));
				dataObject.setSortNumber(rs.getLong("sort_number"));
				dataObject.setStatus(rs.getInt("status"));
				
				dataObjectSet.add(dataObject);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return dataObjectSet;
	}
	
	public boolean selectMemberBySortNumber(MemberTree dataObject) throws SQLException {
		boolean result = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		sb.append("member_id,");
		sb.append("upline_id,");
		sb.append("reg_date,");
		sb.append("pvpribadi,");
		sb.append("bigfoot,");
		sb.append("mediumfoot,");
		sb.append("litfoot,");
		sb.append("bigfoot_nonbasic,");
		sb.append("mediumfoot_nonbasic,");
		sb.append("litfoot_nonbasic,");
		sb.append("left_foot,");
		sb.append("middle_foot,");
		sb.append("right_foot,");
		sb.append("amt_bigfoot,");
		sb.append("amt_midfoot,");
		sb.append("amt_litfoot,");
		sb.append("amt_bigfoot_nonbasic,");
		sb.append("amt_midfoot_nonbasic,");
		sb.append("amt_litfoot_nonbasic,");
		sb.append("peringkat,");
		sb.append("kualifikasi,");
		sb.append("tanggal,");
		sb.append("potongan_am,");
		sb.append("saldo_am,");
		sb.append("saldo_poin,");
		sb.append("saldo_bv,");
		sb.append("saldo_ewallet,");
		sb.append("saldo_ddr,");
		sb.append("ddr_bigfoot,");
		sb.append("ddr_midfoot,");
		sb.append("ddr_litfoot,");
		sb.append("pv_bigfoot,");
		sb.append("pv_midfoot,");
		sb.append("pv_litfoot,");
		sb.append("is_basic,");
		sb.append("is_basic_new,");
		sb.append("sort_number,");
		sb.append("status ");
		sb.append("FROM member_tree WHERE sort_number = " + dataObject.getSortNumber());
//		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString()); 
			if (rs.next()) {
				result = true;
				dataObject.setMemberId(rs.getString("member_id"));
				dataObject.setUplineId(rs.getString("upline_id"));
				dataObject.setRegDate(new Date(rs.getTimestamp("reg_date").getTime()));
				dataObject.setPvpribadi(rs.getFloat("pvpribadi"));
				dataObject.setBigfoot(rs.getString("bigfoot"));
				dataObject.setMediumfoot(rs.getString("mediumfoot"));
				dataObject.setLitfoot(rs.getString("litfoot"));
				dataObject.setBigfootNonbasic(rs.getString("bigfoot_nonbasic"));
				dataObject.setMediumfootNonbasic(rs.getString("mediumfoot_nonbasic"));
				dataObject.setLitfootNonbasic(rs.getString("litfoot_nonbasic"));
				dataObject.setLeftFoot(rs.getString("left_foot"));
				dataObject.setMiddleFoot(rs.getString("middle_foot"));
				dataObject.setRightFoot(rs.getString("right_foot"));
				dataObject.setAmtBigfoot(rs.getInt("amt_bigfoot"));
				dataObject.setAmtMidfoot(rs.getInt("amt_midfoot"));
				dataObject.setAmtLitfoot(rs.getInt("amt_litfoot"));
				dataObject.setAmtBigfootNonbasic(rs.getInt("amt_bigfoot_nonbasic"));
				dataObject.setAmtMidfootNonbasic(rs.getInt("amt_midfoot_nonbasic"));
				dataObject.setAmtLitfootNonbasic(rs.getInt("amt_litfoot_nonbasic"));
				dataObject.setPeringkat(rs.getString("peringkat"));
				dataObject.setKualifikasi(rs.getString("kualifikasi"));
				dataObject.setTanggal(rs.getDate("tanggal"));
				dataObject.setPotonganAm(rs.getDouble("potongan_am"));
				dataObject.setSaldoAm(rs.getDouble("saldo_am"));
				dataObject.setSaldoPoin(rs.getFloat("saldo_poin"));
				dataObject.setSaldoBv(rs.getDouble("saldo_bv"));
				dataObject.setSaldoEwallet(rs.getDouble("saldo_ewallet"));
				dataObject.setSaldoDdr(rs.getInt("saldo_ddr"));
				dataObject.setDdrBigfoot(rs.getInt("ddr_bigfoot"));
				dataObject.setDdrMidfoot(rs.getInt("ddr_midfoot"));
				dataObject.setDdrLitfoot(rs.getInt("ddr_litfoot"));
				dataObject.setPvBigfoot(rs.getFloat("pv_bigfoot"));
				dataObject.setPvMidfoot(rs.getFloat("pv_midfoot"));
				dataObject.setPvLitfoot(rs.getFloat("pv_litfoot"));
				dataObject.setIsBasic(rs.getInt("is_basic"));
				dataObject.setIsBasicNew(rs.getInt("is_basic_new"));
//				dataObject.setSortNumber(rs.getLong("sort_number"));
				dataObject.setStatus(rs.getInt("status"));
			} 
			else{
				System.out.println("member id: "+dataObject.getMemberId());
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int backupHarianMemberTree(MutableDateTime tglBatas) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO lg_member_tree_harian ");
//		sb.append("SELECT * FROM member_tree WHERE reg_date < '" + tglBatas.toString("yyyy-MM-dd HH:mm:ss") + "'");
		sb.append("SELECT * FROM member_tree WHERE member_id IN (SELECT member_id FROM member WHERE reg_date < '" + tglBatas.toString("yyyy-MM-dd HH:mm:ss") + "')");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int backupBulananMemberTree(MutableDateTime tglBatas) throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO lg_member_tree_bulanan ");
//		sb.append("SELECT * FROM member_tree WHERE reg_date < '" + tglBatas.toString("yyyy-MM-dd HH:mm:ss") + "'");
		sb.append("SELECT * FROM member_tree WHERE member_id in (SELECT member_id FROM member WHERE reg_date < '" + tglBatas.toString("yyyy-MM-dd HH:mm:ss") + "')");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public int resetMemberTree() throws SQLException {
		int result;
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE member_tree SET pvpribadi=0, potongan_am=0, saldo_poin=0, saldo_bv=0, pv_bigfoot=0, pv_midfoot=0, pv_litfoot=0");
		
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			result = sqlAccess.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return result;
	}
	
	public boolean anyNullMember() throws SQLException {
		boolean isNull = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT COUNT(*) jumlah FROM member_tree ");
		sb.append("WHERE status = 0 ");
		sb.append("AND (member_id IS NULL OR upline_id IS NULL)");
		SQLAccess sqlAccess = null;
		try {
			sqlAccess = new SQLAccess(dbConn);
			ResultSet rs = sqlAccess.executeQuery(sb.toString());
			
			if (rs.next()) {
				int jumlah = rs.getInt("jumlah");
				if (jumlah > 0) isNull = true;
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (sqlAccess != null) sqlAccess.close();
		}
		return isNull;
	}
}
