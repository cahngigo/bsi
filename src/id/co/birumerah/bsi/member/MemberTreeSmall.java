package id.co.birumerah.bsi.member;

import java.util.Date;

public class MemberTreeSmall {

	private String memberId;
	private float pvpribadi;
	private String peringkat;
	private String kualifikasi;
	private Date tanggal;
	private float saldoPoin;
	private double saldoBv;
	private float pvBigfoot;
	private float pvMidfoot;
	private float pvLitfoot;
	private float pvBesar;
	private float pvSedang;
	private float pvKecil;
	private float pvBesar2Jalur;
	private float pvKecil2Jalur;
	private int isBasic;
	private int isBasicNew;
	
	public MemberTreeSmall() {}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public float getPvpribadi() {
		return pvpribadi;
	}

	public void setPvpribadi(float pvpribadi) {
		this.pvpribadi = pvpribadi;
	}

	public String getPeringkat() {
		return peringkat;
	}

	public void setPeringkat(String peringkat) {
		this.peringkat = peringkat;
	}

	public String getKualifikasi() {
		return kualifikasi;
	}

	public void setKualifikasi(String kualifikasi) {
		this.kualifikasi = kualifikasi;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public float getSaldoPoin() {
		return saldoPoin;
	}

	public void setSaldoPoin(float saldoPoin) {
		this.saldoPoin = saldoPoin;
	}

	public double getSaldoBv() {
		return saldoBv;
	}

	public void setSaldoBv(double saldoBv) {
		this.saldoBv = saldoBv;
	}

	public float getPvBigfoot() {
		return pvBigfoot;
	}

	public void setPvBigfoot(float pvBigfoot) {
		this.pvBigfoot = pvBigfoot;
	}

	public float getPvMidfoot() {
		return pvMidfoot;
	}

	public void setPvMidfoot(float pvMidfoot) {
		this.pvMidfoot = pvMidfoot;
	}

	public float getPvLitfoot() {
		return pvLitfoot;
	}

	public void setPvLitfoot(float pvLitfoot) {
		this.pvLitfoot = pvLitfoot;
	}

	public float getPvBesar() {
		return pvBesar;
	}

	public void setPvBesar(float pvBesar) {
		this.pvBesar = pvBesar;
	}

	public float getPvSedang() {
		return pvSedang;
	}

	public void setPvSedang(float pvSedang) {
		this.pvSedang = pvSedang;
	}

	public float getPvKecil() {
		return pvKecil;
	}

	public void setPvKecil(float pvKecil) {
		this.pvKecil = pvKecil;
	}

	public float getPvBesar2Jalur() {
		return pvBesar2Jalur;
	}

	public void setPvBesar2Jalur(float pvBesar2Jalur) {
		this.pvBesar2Jalur = pvBesar2Jalur;
	}

	public float getPvKecil2Jalur() {
		return pvKecil2Jalur;
	}

	public void setPvKecil2Jalur(float pvKecil2Jalur) {
		this.pvKecil2Jalur = pvKecil2Jalur;
	}

	public int getIsBasic() {
		return isBasic;
	}

	public void setIsBasic(int isBasic) {
		this.isBasic = isBasic;
	}

	public int getIsBasicNew() {
		return isBasicNew;
	}

	public void setIsBasicNew(int isBasicNew) {
		this.isBasicNew = isBasicNew;
	}
}
