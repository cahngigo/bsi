package id.co.birumerah.bsi.member;

import java.util.Date;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.bonus.plana.BonusCounter;

public class AddMember {

//	private Date tglTrx;
	private MutableDateTime tglTrx;
	
	public AddMember() {
		super();
	}
	
	public AddMember(MutableDateTime testDate) {
		this();
		this.tglTrx = testDate;
	}
	
	public String[] registerMember(Member newMember) {
		BonusCounter bc = new BonusCounter(tglTrx);
		String[] hasil = new String[2];
		try {
			MemberTree upline = MemberTreeFcd.getMemberTreeById(newMember.getUplineId());
			MemberTree newMemberTree = new MemberTree();
			newMemberTree.setMemberId(newMember.getMemberId());
			newMemberTree.setUplineId(newMember.getUplineId());
			newMemberTree.setRegDate(newMember.getRegDate());
			newMemberTree.setTanggal(tglTrx.toDate());
			String[] hasilReg = bc.addMember(newMember.getUplineId(), upline, newMember.getMemberId(), newMemberTree);
			if (hasilReg[0].equals("true")) {
				MemberFcd memFcd = new MemberFcd(newMember);
				memFcd.insertMember();
				MemberTreeFcd memTreeFcd = new MemberTreeFcd(newMemberTree);
				memTreeFcd.insertMemberTree();
//				bc.giveBonusPasangan(newMemberTree, newMember.getUplineId());
				hasil[0] = "true";
				hasil[1] = "Registrasi member berhasil.";
			} else {
				hasil[0] = hasilReg[0];
				hasil[1] = hasilReg[1];
			}
		} catch (Exception e) {
			hasil[0] = "false";
			hasil[1] = "Registrasi member tidak berhasil.";
			e.printStackTrace();
		}
		return hasil;
	}
}
