package id.co.birumerah.bsi.member;

import java.util.Date;

public class MemberTree {

	private String memberId;
	private String uplineId;
	private Date regDate;
	private float pvpribadi;
	private String bigfoot;
	private String mediumfoot;
	private String litfoot;
	private String bigfootNonbasic;
	private String mediumfootNonbasic;
	private String litfootNonbasic;
	private String leftFoot;
	private String middleFoot;
	private String rightFoot;
	private int amtBigfoot;
	private int amtMidfoot;
	private int amtLitfoot;
	private int amtBigfootNonbasic;
	private int amtMidfootNonbasic;
	private int amtLitfootNonbasic;
	private String peringkat;
	private String kualifikasi;
	private Date tanggal;
	private double potonganAm;
	private double saldoAm;
	private float saldoPoin;
	private double saldoBv;
	private double saldoEwallet;
	private int saldoDdr;
	private int ddrBigfoot;
	private int ddrMidfoot;
	private int ddrLitfoot;
	private float pvBigfoot;
	private float pvMidfoot;
	private float pvLitfoot;
	private int isPromoxenia;
	private int bigfootPromo;
	private int midfootPromo;
	private int litfootPromo;
	private float pvBesar;
	private float pvSedang;
	private float pvKecil;
	private float pvBesar2Jalur;
	private float pvKecil2Jalur;
	private int isBasic;
	private int isBasicNew;
	private long sortNumber;
	private int status;
	
	public MemberTree() {}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getUplineId() {
		return uplineId;
	}

	public void setUplineId(String uplineId) {
		this.uplineId = uplineId;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public float getPvpribadi() {
		return pvpribadi;
	}

	public void setPvpribadi(float pvpribadi) {
		this.pvpribadi = pvpribadi;
	}

	public String getBigfoot() {
		return bigfoot;
	}

	public void setBigfoot(String bigfoot) {
		this.bigfoot = bigfoot;
	}

	public String getMediumfoot() {
		return mediumfoot;
	}

	public void setMediumfoot(String mediumfoot) {
		this.mediumfoot = mediumfoot;
	}

	public String getLitfoot() {
		return litfoot;
	}

	public void setLitfoot(String litfoot) {
		this.litfoot = litfoot;
	}

	public String getBigfootNonbasic() {
		return bigfootNonbasic;
	}

	public void setBigfootNonbasic(String bigfootNonbasic) {
		this.bigfootNonbasic = bigfootNonbasic;
	}

	public String getMediumfootNonbasic() {
		return mediumfootNonbasic;
	}

	public void setMediumfootNonbasic(String mediumfootNonbasic) {
		this.mediumfootNonbasic = mediumfootNonbasic;
	}

	public String getLitfootNonbasic() {
		return litfootNonbasic;
	}

	public void setLitfootNonbasic(String litfootNonbasic) {
		this.litfootNonbasic = litfootNonbasic;
	}

	public String getLeftFoot() {
		return leftFoot;
	}

	public void setLeftFoot(String leftFoot) {
		this.leftFoot = leftFoot;
	}

	public String getMiddleFoot() {
		return middleFoot;
	}

	public void setMiddleFoot(String middleFoot) {
		this.middleFoot = middleFoot;
	}

	public String getRightFoot() {
		return rightFoot;
	}

	public void setRightFoot(String rightFoot) {
		this.rightFoot = rightFoot;
	}

	public int getAmtBigfoot() {
		return amtBigfoot;
	}

	public void setAmtBigfoot(int amtBigfoot) {
		this.amtBigfoot = amtBigfoot;
	}

	public int getAmtMidfoot() {
		return amtMidfoot;
	}

	public void setAmtMidfoot(int amtMidfoot) {
		this.amtMidfoot = amtMidfoot;
	}

	public int getAmtLitfoot() {
		return amtLitfoot;
	}

	public void setAmtLitfoot(int amtLitfoot) {
		this.amtLitfoot = amtLitfoot;
	}

	public int getAmtBigfootNonbasic() {
		return amtBigfootNonbasic;
	}

	public void setAmtBigfootNonbasic(int amtBigfootNonbasic) {
		this.amtBigfootNonbasic = amtBigfootNonbasic;
	}

	public int getAmtMidfootNonbasic() {
		return amtMidfootNonbasic;
	}

	public void setAmtMidfootNonbasic(int amtMidfootNonbasic) {
		this.amtMidfootNonbasic = amtMidfootNonbasic;
	}

	public int getAmtLitfootNonbasic() {
		return amtLitfootNonbasic;
	}

	public void setAmtLitfootNonbasic(int amtLitfootNonbasic) {
		this.amtLitfootNonbasic = amtLitfootNonbasic;
	}

	public String getPeringkat() {
		return peringkat;
	}

	public void setPeringkat(String peringkat) {
		this.peringkat = peringkat;
	}

	public String getKualifikasi() {
		return kualifikasi;
	}

	public void setKualifikasi(String kualifikasi) {
		this.kualifikasi = kualifikasi;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public double getPotonganAm() {
		return potonganAm;
	}

	public void setPotonganAm(double potonganAm) {
		this.potonganAm = potonganAm;
	}

	public double getSaldoAm() {
		return saldoAm;
	}

	public void setSaldoAm(double saldoAm) {
		this.saldoAm = saldoAm;
	}

	public float getSaldoPoin() {
		return saldoPoin;
	}

	public void setSaldoPoin(float saldoPoin) {
		this.saldoPoin = saldoPoin;
	}

	public double getSaldoBv() {
		return saldoBv;
	}

	public void setSaldoBv(double saldoBv) {
		this.saldoBv = saldoBv;
	}

	public double getSaldoEwallet() {
		return saldoEwallet;
	}

	public void setSaldoEwallet(double saldoEwallet) {
		this.saldoEwallet = saldoEwallet;
	}

	public int getSaldoDdr() {
		return saldoDdr;
	}

	public void setSaldoDdr(int saldoDdr) {
		this.saldoDdr = saldoDdr;
	}

	public int getDdrBigfoot() {
		return ddrBigfoot;
	}

	public void setDdrBigfoot(int ddrBigfoot) {
		this.ddrBigfoot = ddrBigfoot;
	}

	public int getDdrMidfoot() {
		return ddrMidfoot;
	}

	public void setDdrMidfoot(int ddrMidfoot) {
		this.ddrMidfoot = ddrMidfoot;
	}

	public int getDdrLitfoot() {
		return ddrLitfoot;
	}

	public void setDdrLitfoot(int ddrLitfoot) {
		this.ddrLitfoot = ddrLitfoot;
	}

	public float getPvLitfoot() {
		return pvLitfoot;
	}

	public void setPvLitfoot(float pvLitfoot) {
		this.pvLitfoot = pvLitfoot;
	}

	public float getPvBigfoot() {
		return pvBigfoot;
	}

	public void setPvBigfoot(float pvBigfoot) {
		this.pvBigfoot = pvBigfoot;
	}

	public float getPvMidfoot() {
		return pvMidfoot;
	}

	public void setPvMidfoot(float pvMidfoot) {
		this.pvMidfoot = pvMidfoot;
	}

	public long getSortNumber() {
		return sortNumber;
	}

	public int getIsPromoxenia() {
		return isPromoxenia;
	}

	public void setIsPromoxenia(int isPromoxenia) {
		this.isPromoxenia = isPromoxenia;
	}

	public int getBigfootPromo() {
		return bigfootPromo;
	}

	public void setBigfootPromo(int bigfootPromo) {
		this.bigfootPromo = bigfootPromo;
	}

	public int getMidfootPromo() {
		return midfootPromo;
	}

	public void setMidfootPromo(int midfootPromo) {
		this.midfootPromo = midfootPromo;
	}

	public int getLitfootPromo() {
		return litfootPromo;
	}

	public void setLitfootPromo(int litfootPromo) {
		this.litfootPromo = litfootPromo;
	}

	public void setSortNumber(long sortNumber) {
		this.sortNumber = sortNumber;
	}

	public float getPvBesar() {
		return pvBesar;
	}

	public void setPvBesar(float pvBesar) {
		this.pvBesar = pvBesar;
	}

	public float getPvSedang() {
		return pvSedang;
	}

	public void setPvSedang(float pvSedang) {
		this.pvSedang = pvSedang;
	}

	public float getPvKecil() {
		return pvKecil;
	}

	public void setPvKecil(float pvKecil) {
		this.pvKecil = pvKecil;
	}

	public float getPvBesar2Jalur() {
		return pvBesar2Jalur;
	}

	public void setPvBesar2Jalur(float pvBesar2Jalur) {
		this.pvBesar2Jalur = pvBesar2Jalur;
	}

	public float getPvKecil2Jalur() {
		return pvKecil2Jalur;
	}

	public void setPvKecil2Jalur(float pvKecil2Jalur) {
		this.pvKecil2Jalur = pvKecil2Jalur;
	}

	public int getIsBasic() {
		return isBasic;
	}

	public void setIsBasic(int isBasic) {
		this.isBasic = isBasic;
	}

	public int getIsBasicNew() {
		return isBasicNew;
	}

	public void setIsBasicNew(int isBasicNew) {
		this.isBasicNew = isBasicNew;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
