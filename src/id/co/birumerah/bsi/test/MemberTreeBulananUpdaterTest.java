package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.joda.time.MutableDateTime;

public class MemberTreeBulananUpdaterTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String dataLine = "";
		String[] tanggal = null;
		try {
			File inFile = new File("memberTreeBulananUpdater.config");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inFile)));
			while ((dataLine = br.readLine()) != null) {
				StringTokenizer token = new StringTokenizer(dataLine, "=");
				String tipe = token.nextToken();
				String isi = token.nextToken();
				if (tipe.equals("tanggal.awal.bulan")) {
					tanggal = (isi != null ? isi.split("-") : null);
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		MutableDateTime testDate = new MutableDateTime(Integer.parseInt(tanggal[0]), Integer.parseInt(tanggal[1]), Integer.parseInt(tanggal[2]), 0, 0, 0, 0);
		System.out.println("tanggal test = " + testDate.toString("yyyy-MM-dd"));
		BonusCounterCaller bcc = new BonusCounterCaller(testDate);
		bcc.dailyBonusCounterLogBulanan(false);
	}

}
