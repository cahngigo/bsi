package id.co.birumerah.bsi.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.StringTokenizer;

import org.joda.time.MutableDateTime;

import id.co.birumerah.bsi.bonus.plana.BonusCounter;
import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;
import id.co.birumerah.bsi.bonus.plana.CounterInitiator;
import id.co.birumerah.bsi.tree.Tree;
import id.co.birumerah.util.dbaccess.DBConnection;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeHarianFcd;

public class Test {
	
	static id.co.birumerah.bsi.tree.Tree<MemberTree> pohon;
	public static final String JAVABRIDGE_PORT = "8087";
	static final php.java.bridge.JavaBridgeRunner runner = 
	   php.java.bridge.JavaBridgeRunner.getInstance(JAVABRIDGE_PORT);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		BonusCounter bc = new BonusCounter();
//		pohon = bc.getTree();
//		pohon.toString();
//		try {
//			System.out.println("b punya keturunan sebanyak: " + getSuccessorsSize("00000002B"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
//		CounterInitiator ci = new CounterInitiator();
//		ci.makeTree();
		
//		Date testDate = new Date(2011-1900, 9, 2);
		
		//--- untuk tes perhitungan simulasi
		String dataLine = "";
		String[] tanggal = null;
		try {
			File inFile = new File("testing.config");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inFile)));
			while ((dataLine = br.readLine()) != null) {
				StringTokenizer token = new StringTokenizer(dataLine, "=");
				String tipe = token.nextToken();
				String isi = token.nextToken();
				if (tipe.equals("tanggal.bonus.harian")) {
					tanggal = (isi != null ? isi.split("-") : null);
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		MutableDateTime testDate = new MutableDateTime(2013, 1, 7, 0, 0, 0, 0);
		MutableDateTime testDate = new MutableDateTime(Integer.parseInt(tanggal[0]), Integer.parseInt(tanggal[1]), Integer.parseInt(tanggal[2]), 0, 0, 0, 0);
		testDate.addDays(1);
		System.out.println("tanggal test = " + testDate.toString("yyyy-MM-dd"));
//		BonusCounterCaller bcc = new BonusCounterCaller(testDate, true);
		
		MemberTreeHarianFcd mthFcd = new MemberTreeHarianFcd();
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			int total = mthFcd.countTotal(dbConn);
			if (total == 0) {
				BonusCounterCaller bcc = new BonusCounterCaller(testDate);
				bcc.dailyBonusCounter();
			} else {
				System.err.println("Ada member yang memiliki status 0 (belum dihitung bonus pasangannya).");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
		
		//--- untuk tes update table member_tree periodik
//		MutableDateTime testDate = new MutableDateTime();
//		BonusCounterCaller bcc = new BonusCounterCaller(testDate, false);
//		bcc.dailyBonusCounter();
	}
	
	private static int getSuccessorsSize(String key) {
//		Collection<T> successors = getSuccessors(root);
		Collection<MemberTree> successors = pohon.getSuccessors(key);
		int count = successors.size();
		for (MemberTree node : successors) {
//			count += getSuccessorsSize(node);
			count += getSuccessorsSize(node.getMemberId());
		}
		return count;
	}

}
