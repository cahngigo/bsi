package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.bonus.plana.BonusCounter;
import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Statement;


import org.joda.time.MutableDateTime;

public class MatrixRandom {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	        Connection con = null;
	        PreparedStatement pst = null;
			Statement st = null;
	        
	        
	        DBConnection dbConn = null;
	        
	        try {
	        	dbConn = new DBConnection();

	        	
//	            con = DriverManager.getConnection(cs, user, password);
//	            con.setAutoCommit(true);
				con = dbConn.getConnection();
				
				String tingkat;
				int awal =0;
				int tempAwal = 0;

				for (int i = 1; i < 100; i++)
				{
					
					if (i==1)
					{
						tingkat = "TK. Satu";
						System.out.println(tingkat);
						awal = 2;
						tempAwal = 2;
//						
					}				
					else 
					{
							System.out.println("Temp Awal:" + tempAwal);
							awal = awal  + 3;						
							
							System.out.println("Awal TK " + i + " Nilai: " + awal);
							int c = 3;
							int counter = 0;
							for (int a = 0; a < c;a++)
							{
								int akhir = awal + a;
								if(counter==3)
								{
									tempAwal++;
									counter=0;
								}
								counter++;
								
							
								// insert new member
								//insert into member(member_id,upline_id,reg_date) values('2','1',now());
								st = con.createStatement();

	           
								MutableDateTime tanggal = new MutableDateTime();
								String query = "insert into member(member_id,jenis_member,upline_id,reg_date) VALUES("+akhir + ",'Reguler',"+tempAwal+",'"+tanggal.toString("yyyy-MM-dd HH:mm:ss.SSS")+"')";
								st.executeUpdate(query);
								System.out.println("INSERT: " + query);
								
							
								//insert new member tree
								//insert into member_tree(member_id,upline_id) values('2','1');
								query = "insert into member_tree(member_id,upline_id,reg_date) VALUES("+akhir + ","+tempAwal+",'"+tanggal.toString("yyyy-MM-dd HH:mm:ss.SSS")+"')";
								st.executeUpdate(query); 
								System.out.println("INSERT Tree: " + query);
								//System.out.print(" "+akhir +"-" + tempAwal);
								tanggal = new MutableDateTime();
								//System.out.println("tanggal.toString() = "+tanggal.toString("yyyy-MM-dd HH:mm:ss.SSS"));

						   
								BonusCounter bc = new BonusCounter(false, dbConn);
								System.out.print("Awal dan akhir bonus "+akhir +"-" + tempAwal);
//								bc.giveBonusPasangan(""+akhir, ""+tempAwal);
								
//								if(akhir > 9)
//								{
									
							
//								try {
//								Thread.sleep(50000);
//							} catch (InterruptedException e) {
//								e.printStackTrace();
//							}
//								}
								
								bc.giveBonusPasangan(""+akhir, ""+tempAwal, dbConn);
								
								
							}
							System.out.println("");
							System.out.println("----");
							
							//random tempAwal
							int randomNumber = (int)(Math.random()*3);							
							//System.out.println("random: " + randomNumber);
							tempAwal = awal + randomNumber;
							
					}
					
				}

	        } catch (SQLException ex) {
	            Logger lgr = Logger.getLogger(MatrixSempurnaSim.class.getName());
	            lgr.log(Level.SEVERE, ex.getMessage(), ex);

	        } finally {

	        	if (dbConn != null) dbConn.close();
	            try {
	                if (pst != null) {
	                    pst.close();
	                }
	                if (con != null) {
	                    con.close();
	                }
	            } catch (SQLException ex) {
	                Logger lgr = Logger.getLogger(MatrixSempurnaSim.class.getName());
	                lgr.log(Level.SEVERE, ex.getMessage(), ex);
	            }
	        }
	    }
		
		public static int HitungPangkat(int x, int y)
		{
			if(y==1)
			{
				return x;
			}	
			else if (y==0)
			{
				return 1;
			}
			else
			{
				return x * HitungPangkat(x,y-1);
			}
		}
}
