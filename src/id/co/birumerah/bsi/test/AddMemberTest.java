package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.bonus.plana.BonusCounterCaller;
import id.co.birumerah.bsi.member.AddMember;
import id.co.birumerah.bsi.member.Member;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.StringTokenizer;

import org.joda.time.MutableDateTime;

public class AddMemberTest {

	/**
	 * @param args
	 */
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		// prompt untuk memasukkan tanggal simulasi test
		System.out.print("Masukkan tanggal testing (dd-MM-yyyy): ");
		
		// baca input dari user
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String tanggal = null;
		try {
			tanggal = br.readLine();
			StringTokenizer token = new StringTokenizer(tanggal, "-");
			if (token.countTokens() < 3) {
				System.out.println("Salah format tanggal.");
				System.exit(1);
			}
			int dd = Integer.parseInt(token.nextToken());
			int MM = Integer.parseInt(token.nextToken());
			int yyyy = Integer.parseInt(token.nextToken());
//			Date tglTrx = new Date(yyyy-1900, MM-1, dd);
			MutableDateTime tglTrx = new MutableDateTime(yyyy, MM, dd, 0, 0, 0, 0);
//			AddMember newMember = new AddMember(tglTrx);
			AddMember newMember = null;
			boolean run = true;
			while(run) {
				newMember = new AddMember(tglTrx);
				System.out.println("\n\n===========================");
				System.out.print("ID Member Baru: ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String memberId = br.readLine();
				System.out.print("Jenis Member (Reguler/Platinum): ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String jenis = br.readLine();
				System.out.print("Nama: ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String nama = br.readLine();
				System.out.print("Sponsor ID: ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String sponsor = br.readLine();
				System.out.print("Upline ID: ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String uplineId = br.readLine();
				Date tglLahir = new Date(80, 1, 1);
//				System.out.print("Tanggal Registrasi(dd-MM-yyyy): ");
//				br = new BufferedReader(new InputStreamReader(System.in));
//				String dateReg = br.readLine();
//				token = new StringTokenizer(dateReg, "-");
//				if (token.countTokens() < 3) {
//					System.out.println("Salah format tanggal.");
//					System.exit(1);
//				}
//				dd = Integer.parseInt(token.nextToken());
//				MM = Integer.parseInt(token.nextToken());
//				yyyy = Integer.parseInt(token.nextToken());
				dd = tglTrx.getDayOfMonth();
				MM = tglTrx.getMonthOfYear();
				yyyy = tglTrx.getYear();
				System.out.print("Waktu Registrasi(HH:mm): ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String timeReg = br.readLine();
				token = new StringTokenizer(timeReg, ":");
				int HH = Integer.parseInt(token.nextToken());
				int mm = Integer.parseInt(token.nextToken());
				Date regDate = new Date(yyyy-1900, MM-1, dd, HH, mm);
				Member member = new Member();
				member.setMemberId(memberId);
				member.setJenisMember(jenis);
				member.setNama(nama);
				member.setSponsor(sponsor);
				member.setUplineId(uplineId);
				member.setBirthdate(tglLahir);
				member.setRegDate(regDate);
				String[] hasil = newMember.registerMember(member);
				System.out.println("\n" + hasil[1]);
				System.out.print("Ingin mendaftarkan member lagi? (Y/N): ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String lagi = br.readLine();
				if ("N".equalsIgnoreCase(lagi)) {
					run = false;
				}
				System.out.print("Ganti hari? (Y/N): ");
				br = new BufferedReader(new InputStreamReader(System.in));
				String ganti = br.readLine();
				if ("Y".equalsIgnoreCase(ganti)) {
					tglTrx.addDays(1);
					BonusCounterCaller bcc = new BonusCounterCaller(tglTrx, false);
					bcc.dailyBonusCounter(true);
				}
			}
		} catch (IOException e) {
			System.out.println("IO error pas baca tanggal!");
	        System.exit(1);
//			e.printStackTrace();
		}

	}

}
