package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.member.PoinCounterMonthly;
import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;

import org.joda.time.MutableDateTime;

public class MonthlyHitungAkumulasiPoinTourTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MutableDateTime bulanHitung = new MutableDateTime(2015, 6, 1, 0, 0, 0, 0);
//			bulanHitung.setDayOfMonth(1);
//			bulanHitung.addMonths(-1);
			PoinCounterMonthly pcm = new PoinCounterMonthly(bulanHitung);
			pcm.getMemberTutupPoin(dbConn);
			pcm.processAkumulasiPoinTour(dbConn);
			pcm.countAkumulasiPoin(dbConn);
			pcm.processAkumulasiPoinTourEropa(dbConn);
			pcm.countAkumulasiPoinEropa(dbConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
