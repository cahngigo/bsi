package id.co.birumerah.bsi.test;

import java.sql.SQLException;

import id.co.birumerah.bsi.bonus.stockist.BonusCounterSc;
import id.co.birumerah.util.dbaccess.DBConnection;

import org.joda.time.MutableDateTime;

public class HitungBonusScTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MutableDateTime testDate = new MutableDateTime(2012, 3, 1, 0, 0, 0, 0);
		BonusCounterSc bcs = new BonusCounterSc(testDate);
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
//			bcs.giveBonusKso();
//			bcs.giveBonusKpo();
//			bcs.giveBonusKsl();
//			bcs.giveBonusLpku();
//			bcs.giveBonusTop5();
			bcs.giveBonusKso(dbConn);
			bcs.giveBonusKpo(dbConn);
			bcs.giveBonusKsl(dbConn);
			bcs.giveBonusLpku(dbConn);
			bcs.giveBonusTop5(dbConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
