package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.bonus.SaldoPtkp;
import id.co.birumerah.bsi.bonus.SaldoPtkpFcd;
import id.co.birumerah.bsi.bonus.TaxRate;
import id.co.birumerah.bsi.bonus.TaxRateFcd;
import id.co.birumerah.bsi.bonus.TaxRateSet;
import id.co.birumerah.bsi.bonus.plana.BonusCounter;
import id.co.birumerah.bsi.member.Member;
import id.co.birumerah.bsi.member.MemberFcd;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;
import java.util.Date;

import org.joda.time.MutableDateTime;

public class NewMemberCheckerTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		MemberTreeFcd mtFcd = new MemberTreeFcd();
		try {
			dbConn = new DBConnection();
			String whereClause = "status=0 AND member_id IN (SELECT member_id FROM member WHERE reg_date LIKE '2014-04-18%') ORDER BY sort_number";
			MemberTreeSet mtSet = mtFcd.search(whereClause, dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree member = mtSet.get(i);
				MutableDateTime testDate = new MutableDateTime(2014, 4, 18, 0, 0, 0, 0);
				System.out.println("===== Penambahan Member "+member.getMemberId()+"=====");
				member.setStatus(1);
				mtFcd.updateMemberTree(member, dbConn);
				BonusCounter bc = new BonusCounter(false, testDate, dbConn);
				MemberTree upline = mtFcd.getMemberTreeById(member.getUplineId(), dbConn);
				Member anggota = MemberFcd.getMemberById(member.getMemberId(), dbConn);
				Date regDate = anggota.getRegDate();
				long sortNumber = member.getSortNumber();
				boolean isBasic = (member.getIsBasic() == 1);
//				bc.giveBonusPasangan(member, member.getUplineId(), dbConn, sortNumber, regDate);
				bc.giveBonusPasangan(member, member.getUplineId(), dbConn, sortNumber, regDate, isBasic);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
