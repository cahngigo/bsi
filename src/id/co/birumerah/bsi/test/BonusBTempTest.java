package id.co.birumerah.bsi.test;

import java.sql.SQLException;

import id.co.birumerah.bsi.bonus.planb.BonusBTempCounter;
import id.co.birumerah.util.dbaccess.DBConnection;

public class BonusBTempTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			BonusBTempCounter bbtc = new BonusBTempCounter(dbConn);
			bbtc.cleanTemporary(dbConn);
			bbtc.giveBonusRO(dbConn);
			bbtc.giveBonusIG(dbConn);
			bbtc.giveBonusUL(dbConn);
			bbtc.giveBonusEB(dbConn);
			bbtc.giveBonusOR(dbConn);
			bbtc.giveBonusRC(dbConn);
			bbtc.giveBonusWT(dbConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
