package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.bonus.KualifikasiMemberChecker;
import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeSet;
import id.co.birumerah.bsi.member.MemberTreeSmall;
import id.co.birumerah.bsi.member.MemberTreeSmallBulananFcd;
import id.co.birumerah.bsi.member.MemberTreeSmallSet;
import id.co.birumerah.util.dbaccess.DBConnection;

public class PeringkatMember {
	
	private static final float MINPV_SRM = 1000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			dbConn = new DBConnection();
			MemberTreeSet mtSet = MemberTreeFcd.showAllMemberTree(dbConn);
			for (int i=0; i<mtSet.length(); i++) {
				MemberTree mTree = mtSet.get(i);
				MemberTreeSmallBulananFcd mtsbFcd = new MemberTreeSmallBulananFcd();
				String whereClause = "member_id = '"+ mTree.getMemberId() +"'";
				MemberTreeSmallSet mtsSet = mtsbFcd.search(whereClause, dbConn);
				float highestPvgk = 0;
				for (int j=0; j<mtsSet.length(); j++) {
					MemberTreeSmall mtsCek = mtsSet.get(j);
					if (highestPvgk < mtsCek.getPvKecil()) {
						highestPvgk = mtsCek.getPvKecil();
					}
				}
				if (highestPvgk >= MINPV_SRM) {
					KualifikasiMemberChecker checker = new KualifikasiMemberChecker();
					String peringkat = checker.getPeringkatMember(mTree.getMemberId(), highestPvgk, dbConn);
					System.out.println("Member "+mTree.getMemberId()+" dg PV Terbesar "+highestPvgk+" berperingkat "+peringkat);
					mTree.setPeringkat(peringkat);
					MemberTreeFcd mtFcd = new MemberTreeFcd();
					mtFcd.updateMemberTree(mTree, dbConn);
				}
			}
		} catch (Exception e) {
			System.out.println("ERROR: "+e.toString());
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
