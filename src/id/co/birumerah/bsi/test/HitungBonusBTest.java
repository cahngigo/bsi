package id.co.birumerah.bsi.test;

import id.co.birumerah.bsi.bonus.planb.BonusBCounter;
import id.co.birumerah.bsi.member.MemberTreeFcd;
import id.co.birumerah.bsi.member.MemberTreeSmallBulananFcd;
import id.co.birumerah.util.dbaccess.DBConnection;

import java.sql.SQLException;

import org.joda.time.MutableDateTime;

public class HitungBonusBTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBConnection dbConn = null;
		try {
			MutableDateTime tglTrx = new MutableDateTime(2016, 8, 1, 0, 0, 0, 0);
			dbConn = new DBConnection();
			BonusBCounter bonus = new BonusBCounter(tglTrx, dbConn);
			MemberTreeSmallBulananFcd mtFcd = new MemberTreeSmallBulananFcd();
			int jumlah = mtFcd.countTotal(tglTrx.toDate(), dbConn);
			System.out.println("***jumlah = " + jumlah);
			
			if (jumlah > 0) {
				bonus.giveBonusRO(dbConn);
				bonus.giveBonusIG(dbConn);
				bonus.giveBonusUL(dbConn);
				bonus.giveBonusEB(dbConn);
//				bonus.giveBonusOR(dbConn);
				bonus.giveBonusRC(dbConn);
				bonus.giveBonusWT(dbConn);
				bonus.giveLdp(dbConn);
				bonus.countPbr(dbConn);
				bonus.potongPajak(dbConn);
				System.out.println("=== Pengecekan peringkat member ===");
				bonus.getPeringkatMember(dbConn);
				bonus.prepareTransfer(dbConn);
			}
			
//			String whereClause = "start_date < '" + tglTrx.toString("yyyy-MM-dd") + "' AND status = 'Berlaku' ORDER BY start_date DESC";
//			TaxRateFcd trFcd = new TaxRateFcd();
//			TaxRateSet trSet = trFcd.search(whereClause);
//			if (trSet.length() < 1)
//				throw new Exception("Belum ada setting Rate Pajak yang berlaku.");
//			TaxRate tax = trSet.get(0);
//			SaldoPtkpFcd spFcd = new SaldoPtkpFcd();
//			spFcd.resetSisaPtkp(tax.getPtkp(), dbConn);
			
//		} catch (IOException e) {
//			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbConn != null) dbConn.close();
		}
	}

}
