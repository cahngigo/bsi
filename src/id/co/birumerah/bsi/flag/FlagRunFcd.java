package id.co.birumerah.bsi.flag;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class FlagRunFcd {

	private FlagRun flagRun;
	
	public FlagRunFcd() {}

	public FlagRunFcd(FlagRun flagRun) {
		this.flagRun = flagRun;
	}

	public FlagRun getFlagRun() {
		return flagRun;
	}

	public void setFlagRun(FlagRun flagRun) {
		this.flagRun = flagRun;
	}
	
	public static FlagRun getFlag(DBConnection dbConn) throws Exception {
		try {
			FlagRun flag = new FlagRun();
			
			FlagRunDAO flagDAO = new FlagRunDAO(dbConn);
			boolean found = flagDAO.select(flag);
			
			if (found) {
				return flag;
			} else {
				System.err.println("Flag not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void updateFlag(FlagRun flag, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			FlagRunDAO flagDAO = new FlagRunDAO(dbConn);
			if (flagDAO.update(flag) < 1) {
				throw new Exception("Flag not found.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
