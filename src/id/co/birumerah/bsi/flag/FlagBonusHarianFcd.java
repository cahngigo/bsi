package id.co.birumerah.bsi.flag;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class FlagBonusHarianFcd {

	private FlagBonusHarian flagHarian;
	
	public FlagBonusHarianFcd() {}

	public FlagBonusHarianFcd(FlagBonusHarian flagHarian) {
		this.flagHarian = flagHarian;
	}

	public FlagBonusHarian getFlagHarian() {
		return flagHarian;
	}

	public void setFlagHarian(FlagBonusHarian flagHarian) {
		this.flagHarian = flagHarian;
	}
	
	public static FlagBonusHarian getFlag(DBConnection dbConn) throws Exception {
		try {
			FlagBonusHarian flag = new FlagBonusHarian();
			
			FlagBonusHarianDAO flagDAO = new FlagBonusHarianDAO(dbConn);
			boolean found = flagDAO.select(flag);
			
			if (found) {
				return flag;
			} else {
				System.err.println("Flag not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void updateFlag(FlagBonusHarian flag, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			FlagBonusHarianDAO flagDAO = new FlagBonusHarianDAO(dbConn);
			if (flagDAO.update(flag) < 1) {
				throw new Exception("Failed on updating flag.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
