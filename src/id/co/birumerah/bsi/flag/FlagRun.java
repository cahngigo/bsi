package id.co.birumerah.bsi.flag;

public class FlagRun {

	private int isRun;
	
	public FlagRun() {}

	public FlagRun(int isRun) {
		this.isRun = isRun;
	}

	public int getIsRun() {
		return isRun;
	}

	public void setIsRun(int isRun) {
		this.isRun = isRun;
	}
}
