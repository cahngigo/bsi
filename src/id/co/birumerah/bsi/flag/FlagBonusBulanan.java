package id.co.birumerah.bsi.flag;

import java.util.Date;

public class FlagBonusBulanan {

	private Date lastMonth;
	private int hasDone;
	
	public FlagBonusBulanan() {}

	public FlagBonusBulanan(Date lastMonth, int hasDone) {
		this.lastMonth = lastMonth;
		this.hasDone = hasDone;
	}

	public Date getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(Date lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getHasDone() {
		return hasDone;
	}

	public void setHasDone(int hasDone) {
		this.hasDone = hasDone;
	}
}
