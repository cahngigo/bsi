package id.co.birumerah.bsi.flag;

import java.sql.SQLException;

import id.co.birumerah.util.dbaccess.DBConnection;

public class FlagBonusBulananFcd {

	private FlagBonusBulanan flagBulanan;
	
	public FlagBonusBulananFcd() {}

	public FlagBonusBulananFcd(FlagBonusBulanan flagBulanan) {
		this.flagBulanan = flagBulanan;
	}

	public FlagBonusBulanan getFlagBulanan() {
		return flagBulanan;
	}

	public void setFlagBulanan(FlagBonusBulanan flagBulanan) {
		this.flagBulanan = flagBulanan;
	}
	
	public static FlagBonusBulanan getFlag(DBConnection dbConn) throws Exception {
		try {
			FlagBonusBulanan flag = new FlagBonusBulanan();
			
			FlagBonusBulananDAO flagDAO = new FlagBonusBulananDAO(dbConn);
			boolean found = flagDAO.select(flag);
			
			if (found) {
				return flag;
			} else {
				System.err.println("Flag not found.");
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	public void updateFlag(FlagBonusBulanan flag, DBConnection dbConn) throws Exception {
		try {
			dbConn.beginTransaction();
			FlagBonusBulananDAO flagDAO = new FlagBonusBulananDAO(dbConn);
			if (flagDAO.update(flag) < 1) {
				throw new Exception("Failed on updating flag.");
			}
			dbConn.commitTransaction();
		} catch (SQLException e) {
			if (dbConn != null) {
				dbConn.rollbackTransaction();
			}
			throw new Exception(e);
		}
	}
}
