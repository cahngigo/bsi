package id.co.birumerah.bsi.flag;

import java.util.Date;

public class FlagBonusHarian {

	private Date lastDate;
	private int hasDone;
	
	public FlagBonusHarian() {}

	public FlagBonusHarian(Date lastDate, int hasDone) {
		this.lastDate = lastDate;
		this.hasDone = hasDone;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public int getHasDone() {
		return hasDone;
	}

	public void setHasDone(int hasDone) {
		this.hasDone = hasDone;
	}
}
