package id.co.birumerah.bsi.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


//@SuppressWarnings("unchecked")
public class Tree<T> {

	private T head;
	private ArrayList<Tree<T>> leafs = new ArrayList<Tree<T>>();
	private Tree<T> parent = null;
//	private HashMap<T, Tree<T>> locate = new HashMap<T, Tree<T>>();
	private HashMap<String, Tree<T>> locate = new HashMap<String, Tree<T>>();
	
//	public Tree(T head) {
//		this.head = head;
//		locate.put(head, this);
//	}
	public Tree(String key, T head) {
		this.head = head;
		locate.put(key, this);
	}
	
//	public void addLeaf(T root, T leaf) {
//		System.out.println("di dalam addLeaf(T root, T leaf)");
//		if (locate.containsKey(root)) {
//			if (getSuccessors(root).size() < 3) {
//				System.out.println("di if");
//				locate.get(root).addLeaf(leaf);
//			} else {
//				System.out.println("dah punya 3 anak, gak bisa nambah");
//			}
//		} else {
//			System.out.println("di else");
//			addLeaf(root).addLeaf(leaf);
//		}
//		System.out.println("di akhir addLeaf(T root, T leaf)");
//	}
//	public void addLeaf(String parentKey, T root, String key, T leaf) {
	public String[] addLeaf(String parentKey, T root, String key, T leaf) {
//		System.out.println("di dalam addLeaf(T root, T leaf)");
		String[] hasil = new String[2];
		if (locate.containsKey(parentKey)) {
//			if (getSuccessors(root).size() < 3) {
//			if (getSuccessors(key).size() < 3) {
			if (getSuccessors(parentKey).size() < 3) {
//				System.out.println("di if");
				locate.get(parentKey).addLeaf(key, leaf);
				hasil[0] = "true";
				hasil[1] = "";
			} else {
				System.out.println("dah punya 3 anak, gak bisa nambah");
				hasil[0] = "false";
				hasil[1] = "Member " + parentKey + " sudah mempunyai 3 direct downline.";
			}
		} else {
			System.out.println("di else");
			System.out.println("mencoba menambahkan "+key+" di bawah "+parentKey);
			addLeaf(parentKey, root).addLeaf(key, leaf);
			hasil[0] = "false";
			hasil[1] = "Upline dengan ID Member " + parentKey + " tidak terdaftar.";
		}
//		System.out.println("di akhir addLeaf(T root, T leaf)");
		return hasil;
	}
	
	public Tree<T> addLeaf(String key, T leaf) {
//		System.out.println("Tree<T> addLeaf(T leaf)");
		Tree<T> t = new Tree<T>(key, leaf);
		leafs.add(t);
		t.parent = this;
		t.locate = this.locate;
		locate.put(key, t);
		return t;
	}
	
//	public Tree<T> setAsParent(T parentRoot) {
//		Tree<T> t = new Tree<T>(parentRoot);
//		t.leafs.add(this);
//		this.parent = t;
//		t.locate = this.locate;
//		t.locate.put(head, this);
//		t.locate.put(parentRoot, t);
//		return t;
//	}
	
	public T getHead() {
		return head;
	}
	
	/**
	 * 
	 * @param element
	 * @return tree yang merupakan semua keturunan dari parameter element 
	 */
//	public Tree<T> getTree(T element) {
	public Tree<T> getTree(String key) {
		return locate.get(key);
	}
	
	public Tree<T> getParent() {
		return parent;
	}
	
//	public Collection<T> getSuccessors(T root) {
	public Collection<T> getSuccessors(String key) {
		Collection<T> successors = new ArrayList<T>();
//		Tree<T> tree = getTree(root);
		Tree<T> tree = getTree(key);
		if (null != tree) {
			for(Tree<T> leaf : tree.leafs) {
				successors.add(leaf.head);
			}
		}
		return successors;
	}
	
	public Collection<Tree<T>> getSubTrees() {
		return leafs;
	}
	
//	public static <T> Collection<T> getSuccessors(T of, Collection<Tree<T>> in) {
	public static <T> Collection<T> getSuccessors(String key, Collection<Tree<T>> in) {
		for (Tree<T> tree : in) {
//			if (tree.locate.containsKey(of)) {
//				return tree.getSuccessors(of);
//			}
			if (tree.locate.containsKey(key)) {
				return tree.getSuccessors(key);
			}
		}
		return new ArrayList<T>();
	}
	
	public ArrayList<Tree<T>> getLeafs() {
		if (this.leafs == null) {
			return new ArrayList<Tree<T>>();
		}
		return this.leafs;
	}
	
	public String toString() {
		return printTree(0);
	}
	
	private static final int indent = 2;

	private String printTree(int increment) {
		String s = "";
		String inc = "";
		for (int i=0; i<increment; ++i) {
			inc = inc + " ";
		}
		s = inc + head;
		for (Tree<T> child : leafs) {
			s += "\n" + child.printTree(increment + indent);
		}
		return s;
	}
	
	/**
	 * Fungsi recursive untuk menghitung total anggota yang dimiliki di bawah
	 * si root
	 *  
	 * @param root
	 * @return
	 */
//	public int getSuccessorsSize(T root) {
	public int getSuccessorsSize(String key) {
//		Collection<T> successors = getSuccessors(root);
		Collection<T> successors = getSuccessors(key);
		int count = successors.size();
		for (T node : successors) {
//			count += getSuccessorsSize(node);
//			count += getSuccessorsSize(node);
		}
		return count;
	}
	
	public boolean isDescendant(boolean found, T parent, T upline) {
		System.out.println("bandingkan "+ parent +" dengan "+ upline);
		if (parent.toString().equals(upline.toString())) {
			return found = true;
		}
//		Collection<T> successors = getSuccessors(parent);
//		for (T node : successors) {
//			found = isDescendant(false, node, upline);
//			if (found) {
//				break;
//			}
//		}
		return found;
		
	}
}
