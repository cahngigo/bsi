package id.co.birumerah.bsi.tree;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import id.co.birumerah.bsi.member.MemberTree;
import id.co.birumerah.bsi.member.MemberTreeDAO;
import id.co.birumerah.bsi.member.MemberTreeFcd;

public class MemberTreeTreeDAO {

	private MemberTreeDAO memberTreeDAO;
	
	public MemberTreeTreeDAO() {
		super();
	}
	
	public void setMemberTreeDAO(MemberTreeDAO memberTreeDAO) {
		this.memberTreeDAO = memberTreeDAO;
	}
	
	public void saveOrUpdate(MemberTreeTree memTree) {
		List<Node<MemberTree>> members = memTree.toList();
		// save the tree in reverse order, starting from the leaf nodes
        // and going up to the root of the tree.
		int numberOfNodes = members.size();
		try {
			for (int i = numberOfNodes - 1; i >= 0; i--) {
				Node<MemberTree> memberElement = members.get(i);
				MemberTree mTree = memberElement.getData();
				memberTreeDAO.update(mTree);
				String parentId = mTree.getMemberId();
				for (Iterator<Node<MemberTree>> it = memberElement.getChildren().iterator(); it.hasNext();) {
					Node<MemberTree> childElement = it.next();
					MemberTree childMemTree = childElement.getData();
					childMemTree.setUplineId(parentId);
					memberTreeDAO.update(childMemTree);
				}
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public MemberTreeTree get(String memberId) {
		MemberTreeTree memTree = new MemberTreeTree();
//		try {
//			Node<MemberTree> rootElement = new Node<MemberTree>(MemberTreeFcd.getMemberTreeById(memberId));
//			getRecursive(rootElement, memTree);
//			memTree.setRootElement(rootElement);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return memTree;
	}

	private void getRecursive(Node<MemberTree> rootElement,
			MemberTreeTree memTree) {
		try {
			List<MemberTree> children = MemberTreeFcd.getMemberTreeByUplineId(rootElement.getData().getMemberId());
			List<Node<MemberTree>> childElements = new ArrayList<Node<MemberTree>>();
			for (Iterator<MemberTree> it = children.iterator(); it.hasNext();) {
				MemberTree childMemTree = it.next();
//				Node<MemberTree> childElement = new Node<MemberTree>(childMemTree);
//				childElements.add(childElement);
//				getRecursive(childElement, memTree);
			}
			rootElement.setChildren(childElements);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
